﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using NUnit.Framework.Constraints;

namespace MathTutor
{
    public class SystemConfig
    {
        #region Study 2 Testing Parameters
        public const string UserId = "09";
        public static bool Study0Mode = true; // practice or test

        public int ProblemIndex = 400; //practice problem number
        public bool DualReprModality = true; // Experiment Practice (within)
        public bool DualInputModality = true; // Experiment Practice (between)
        public bool DualReprWithin = true; // Experiment Testing
        public bool DualInputBetween = true; // Experiment Testing

        #endregion

        /***********************************************/ 

        #region Parameters

        public const int TutorReasonOn = 0;          //0: No Tutor or Reasoning // 1: Has Tutor Mode

        public const bool InteractiveProblem = false; //under problem mode, interactive problem option.

        public const bool MetaTutor = true;

        public const int ShowTutorMode = 1;          //0: Demonstration mode //1:tutor mode

        public const int ShowFreeMode = 0;           //0: free mode //1:problem mode

        #endregion

        #region Study 1 Parameters

        public const bool GeometrySketchInput = false; //Study 1

        /*      
         * public static bool Study00Mode = false;
         * public static bool SketchFirst = false;
         */
        #endregion

        #region Singleton Model

        private static SystemConfig _instance;

        private SystemConfig()
        {
            InitStudy00();
            InitStudy0();
        }

        public static SystemConfig Instance
        {
            get { return _instance ?? (_instance = new SystemConfig()); }
        }

        #endregion

        #region Instance Model

        public bool IsDualInput()
        {
            if (Study0Mode) return DualInputBetween;
            return DualInputModality;
        }

        public bool IsDualRepr()
        {
            if (Study0Mode) return DualReprWithin;
            return DualReprModality;
        }

        public bool IsGeometrySketchInput()
        {
            return GeometrySketchInput;
        }

        #region study obsolete

        /*        public bool StartTestProblem()
        {
/*            if (Study00Mode)
            {
                return _problemCount == 13;
            }#1#

            if (Study0Mode)
            {
                return _problemCount == 1;
            }

            return false;
        }*/

    /*    public bool IsTestingProblem()
        {
/*            if (Study00Mode)
            {
                return 13 < _problemCount && _problemCount <= 17;                
            }#1#

            if (Study0Mode)
            {
                return 2 < _problemCount && _problemCount <= 6;
            }

            return true;
        }*/

        /*
                public void SetFinishHalfFlag()
                {
        /*            if (Study00Mode)
                    {
                        if (_problemCount == 17)
                        {
                            if (!_finishHalfProblems)
                            {
                                _problemCount = 0;
                                _finishHalfProblems = true;
                            }
                            else
                            {
                                _finishAll = true;
                            }
                        }                
                    }#1#

                    if (Study0Mode)
                    {
                        if (_problemCount == 5)
                        {
                            if (!_finishHalfProblems)
                            {
                                _problemCount = 0;
                                _finishHalfProblems = true;
                            }
                            else
                            {
                                _finishAll = true;
                            }
                        }
                    }
                }

                public bool JustHalf()
                {
        /*            if (Study00Mode)
                    {
                        return _finishHalfProblems && _problemCount == 1;                
                    }#1#

                    if (Study0Mode)
                    {
                        return _finishHalfProblems && _problemCount == 1;
                    }

                    return false;
                }
        */

        #endregion

        public int? GetNextProblem()
        {
            int value;
/*            if (Study00Mode)
            {
                if (_finishAll) return null;
                if (SketchFirst)
                {
                    value = !_finishHalfProblems ? _sketchedProblemIds[_problemCount] : _structureProblemIds[_problemCount];
                }
                else
                {
                    value = _finishHalfProblems ? _sketchedProblemIds[_problemCount] : _structureProblemIds[_problemCount];
                }
                _problemCount++;
                return value;
            }*/
            if (Study0Mode)
            {
                if (_problemCount == 2) return null;
                if (DualReprWithin)
                {
                    value = _dualRepr_with_problemIds[_problemCount];
                }
                else
                {
                    value = _dualRepr_without_problemIds[_problemCount];
                }
                _problemCount++;
                return value;
            }

            return ProblemIndex;
        }

        public static bool _finishHalfProblems = false;
        public static bool _finishAll = false;
        private int _problemCount = 0;

        #region Study 2 Setup

        private IList<int> _dualRepr_without_problemIds;
        private IList<int> _dualRepr_with_problemIds;

        private void InitStudy0()
        {
            _dualRepr_without_problemIds = new List<int>()
            {
                406,402
            };
            _dualRepr_with_problemIds = new List<int>()
            {
                405,403
            };
        }

        /*private int GetNextRandomNumberInStructureMode()
       {
           var random = new Random();
           while (true)
           {
               int nextNumber = random.Next(61, 65);
               if (!_structureProblemIds[nextNumber])
               {
                   _structureProblemIds[nextNumber] = true;
                   return nextNumber;
               }
           }
       }*/

        /*  private int GetNextRandomNumberInSketchMode()
          {
              var random = new Random();
              while (true)
              {
                  int nextNumber = random.Next(65, 69);
                  if (!_sketchedProblemIds[nextNumber])
                  {
                      _sketchedProblemIds[nextNumber] = true;
                      return nextNumber;
                  }
              }
          }*/

        /*        private bool CheckVisitAll(IEnumerable<KeyValuePair<int, bool>> dict)
                {
                    return dict.All(pair => pair.Value);
                }*/

        #endregion

        #region Study 00 Setup

        //200-240,61,62,63,64
        //300-340,65,66,67,68

        private IList<int> _structureProblemIds;

        private IList<int> _sketchedProblemIds;

        private void InitStudy00()
        {
            _structureProblemIds = new List<int>
           {
              200,201,202,203,204,205,206,207,208,209,210,211,212,
              61,62,63,64
           };
            _sketchedProblemIds = new List<int>
           {
              300,301,302,303,304,305,306,307,308,309,310,311,312,
              65,66,67,68
           };
        }

        #endregion

        #endregion
    }
}