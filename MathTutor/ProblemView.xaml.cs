﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MathCog.NLP;
using MathCogUI;

namespace MathTutorUserControl.Problem
{
    /// <summary>
    /// Interaction logic for Problem.xaml
    /// </summary>
    public partial class ProblemView : MetroContentControl
    {
        private Paragraph _problemParagraph;
        public Paragraph Problem
        {
            set { _problemParagraph = value; }
            get { return _problemParagraph; }
        }

        private bool _interactive;
        public bool Interactive
        {
            get { return _interactive; }
            set { _interactive = value; }
        }

        public ProblemView()
        {
            InitializeComponent();
            _problemParagraph = new Paragraph() { FontSize = 17 };
            ProblemDoc.Blocks.Add(Problem);
        }

        public ProblemView(bool interactive)
            : base()
        {
            _interactive = interactive;
            ProblemInstruction.Visibility = interactive ? Visibility.Visible : Visibility.Collapsed;
        }

        public void LoadProblem(int problemIndex)
        {
            _problemParagraph.Inlines.Clear();
            MathProblem mp = ProblemLoader.Instance.Load(problemIndex);
            if (Interactive)
            {
                var corpus = ProblemGenerator.Generate(mp, Background);
                foreach (var run in corpus)
                {
                    if (run == null) continue;

                    /*var inputText = run as InputText;
                    if (inputText != null)
                    {
                        _rootMenu.RegisterProblemHint(inputText);
                    }

                    var conceptText = run as ConceptText;
                    if (conceptText != null)
                    {
                        _rootMenu.RegisterConceptHint(conceptText);
                    }*/
                    _problemParagraph.Inlines.Add(run);
                }
            }
            else
            {
                var run = new Run(mp.OriginalProblem);
                _problemParagraph.Inlines.Add(run);
            }
        }
    }
}
