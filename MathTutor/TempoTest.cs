﻿using System;
using System.Diagnostics;
using DynamicGeometry;
using MathCog2.UserModeling;
using NUnit.Framework;

namespace MathTutor.Test
{
    [TestFixture]
    public class TestSystemConfig
    {
        [Test]
        public void TimeSpan()
        {
            var ts1 = new TimeSpan();
            var ts2 = new TimeSpan(1, 20, 22, 65);
            ts1 = ts1.Add(ts2);
            Console.WriteLine(ts1.ToString());
        }

        [Test]
        public void TestStopWatch()
        {
            var watch1 = new Stopwatch();
            var watch2 = new Stopwatch();

            watch1.Start();
            for (int i = 0; i < 100000; i++)
            {
                watch2.Start();
                for (int j = 0; j < 10000; j++)
                {
                    
                }
                watch2.Stop();
            }
        }

        [Test]
        public void TestLoadProblem()
        {
            MathTutorEngine.Instance.Select(200);
            Console.WriteLine("Pass to load a problem.");
        }

    }
}
