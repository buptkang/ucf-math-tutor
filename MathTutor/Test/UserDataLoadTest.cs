﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace MathTutor.Test
{
    [TestFixture]
    public class UserDataLoadTest
    {
        [Test]
        public void LoadBiModalSequenceData()
        {
            string _path = @"C:\\1-Production\\MathApollo\\Math-Visual2\\LogFiles\\Data";
            string fileName = "Log_05_401_201662014461.bin";
            string path = System.IO.Path.Combine("@", _path, fileName);

            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(path,
                                      FileMode.Open,
                                      FileAccess.Read,
                                      FileShare.Read);
            var lst = formatter.Deserialize(stream) as List<object>;
            stream.Close();
            Assert.NotNull(lst);
            Assert.True(lst.Count == 4);
        }
    }
}
