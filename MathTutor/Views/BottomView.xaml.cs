﻿namespace MathTutor
{
    using MathCog2.UserModeling;
    using MathCogUI;
    using System;
    using System.IO;
    using System.Text;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;
    using Xceed.Wpf.Toolkit;
    using MessageBox = System.Windows.MessageBox;

    /// <summary>
    /// Interaction logic for BottomView.xaml
    /// </summary>
    public partial class BottomView
    {
        public BottomView()
        {
            InitializeComponent();
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Do you know what error you made by your self?");
        }

        private void Calculator(object sender, RoutedEventArgs e)
        {
            TabletDevice device = Tablet.CurrentTabletDevice;
            if (device == null || device.Type == TabletDeviceType.Touch)
            {
                return;
            }
            var cal = new Calculator();
            var newWindow = new Window();
            newWindow.Width = 300;
            newWindow.Height = 400;
            newWindow.Content = cal;
            newWindow.Show();
            newWindow.Topmost = true;
        }

        public T FindParent<T>(DependencyObject child) where T : DependencyObject
        {
            //get parent item
            DependencyObject parentObject = VisualTreeHelper.GetParent(child);

            //we've reached the end of the tree
            if (parentObject == null) return null;

            //check if the parent matches the type we're looking for
            T parent = parentObject as T;
            if (parent != null)
                return parent;
            else
                return FindParent<T>(parentObject);
        }

        private void Feedback(object sender, RoutedEventArgs e)
        {
            TabletDevice device = Tablet.CurrentTabletDevice;
            if (device == null || device.Type == TabletDeviceType.Touch)
            {
                return;
            }

            int problemIndex = MathTutorEngine.Instance.CurrentTutorInterp.CurrentProblemTutor.PL.CurrentProblemIndex + 1;

            #region Log Generation

/*            bool isTestingOnes = SystemConfig.Instance.IsTestingProblem();
            SystemConfig.Instance.SetFinishHalfFlag();*/
            InteractiveLogger.Instance.CacheProblemReport(SystemConfig.Study0Mode);

            #endregion

            #region Obsolete Take a screen shot for the grading purpose

/*
            var problemView = FindParent<ProblemMainView>(this);

            int width = Convert.ToInt32(problemView.ActualWidth);
            int height = Convert.ToInt32(problemView.ActualHeight);

            var renderTargetBitmap =
                new RenderTargetBitmap(width, height, 96, 96, PixelFormats.Pbgra32);
            renderTargetBitmap.Render(problemView);

            var pngImage = new PngBitmapEncoder();
            pngImage.Frames.Add(BitmapFrame.Create(renderTargetBitmap));

            string _current = System.IO.Path.GetDirectoryName(
                System.IO.Path.GetDirectoryName(Directory.GetCurrentDirectory()));
            string _path = Directory.GetParent(_current).ToString();

            var timeBuilder = new StringBuilder();
            timeBuilder
                .Append(DateTime.Now.Year.ToString())
                .Append(DateTime.Now.Month.ToString())
                .Append(DateTime.Now.Day.ToString())
                .Append(DateTime.Now.TimeOfDay.Hours.ToString())
                .Append(DateTime.Now.TimeOfDay.Minutes.ToString())
                .Append(DateTime.Now.TimeOfDay.Seconds.ToString());

            var builder = new StringBuilder();
            builder.Append("Log_")
                   .Append(SystemConfig.UserId)
                   .Append("_")
                   .Append(problemIndex)
                   .Append("_")
                   .Append(timeBuilder)
                   .Append(".png");

            string path = System.IO.Path.Combine("@", _path, "LogFiles\\Static\\", builder.ToString());

            using (Stream fileStream = File.Open(path, FileMode.OpenOrCreate))
            {
                pngImage.Save(fileStream);
            }
*/
            #endregion

            #region Store Writing Step Data

            var timeBuilder = new StringBuilder();
            timeBuilder
                .Append(DateTime.Now.Year.ToString())
                .Append(DateTime.Now.Month.ToString())
                .Append(DateTime.Now.Day.ToString())
                .Append(DateTime.Now.TimeOfDay.Hours.ToString())
                .Append(DateTime.Now.TimeOfDay.Minutes.ToString())
                .Append(DateTime.Now.TimeOfDay.Seconds.ToString());

            var mainWindowView = FindParent<MainWindow>(this);
            var currItem = mainWindowView.tabDynamic.SelectedItem as TabItem;
            var pv = currItem.Content as ProblemMainView;

            var builder = new StringBuilder();
            if (SystemConfig.Study0Mode)
            {
                if (SystemConfig.Instance.DualInputBetween)
                {
                    builder.Append("Log_")
                        .Append("BiModal_")
                        .Append(SystemConfig.UserId)
                        .Append("_")
                        .Append(problemIndex)
                        .Append("_")
                        .Append(timeBuilder);
                }
                else
                {
                    builder.Append("Log_")
                           .Append("UniModal_")
                           .Append(SystemConfig.UserId)
                           .Append("_")
                           .Append(problemIndex)
                           .Append("_")
                           .Append(timeBuilder)
                           .Append(".bin");
                }
            }

            string _current = Path.GetDirectoryName(Path.GetDirectoryName(Directory.GetCurrentDirectory()));
            string _path = Directory.GetParent(_current).ToString();
            

            if (SystemConfig.Study0Mode)
            {
                if (SystemConfig.Instance.DualInputBetween)
                {
                    var algebraBuilder = new StringBuilder();
                    algebraBuilder.Append(builder)
                        .Append("_algebra.bin");
                    string algebra_path = Path.Combine("@", _path, "LogFiles\\Data\\", algebraBuilder.ToString());

                    var geometryBuilder = new StringBuilder();
                    geometryBuilder.Append(builder)
                        .Append("_geometry.xml");
                    string geometry_path = Path.Combine("@", _path, "LogFiles\\Data\\", geometryBuilder.ToString());

                    pv.globalHost.SaveUserActions_AlgebraGeometry(algebra_path, geometry_path);
                }
                else
                {
                    string path = Path.Combine("@", _path, "LogFiles\\Data\\", builder.ToString());
                    pv.globalHost.SaveUserActions_AlgebraOnly(path);
                }
            }
            else
            {
/*                if (SystemConfig.Instance.DualInputModality)
                {
                    pv.globalHost.SaveUserActions_AlgebraGeometry(path);
                }
                else
                {
                    pv.globalHost.SaveUserActions_AlgebraOnly(path);
                }*/
            }

            #endregion

            //if (SystemConfig.Study00Mode || SystemConfig.Study0Mode)
            if (SystemConfig.Study0Mode)
            {               
                mainWindowView.NavigateToNextTask();
            }
            else
            {
                Application.Current.Shutdown();
            }          
        }
    }
}
