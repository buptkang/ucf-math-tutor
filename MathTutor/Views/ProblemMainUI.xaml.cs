﻿using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using MathCog2.UserModeling;

namespace MathTutor
{
    using System.Windows.Controls;
    using DynamicGeometry;
    using MathCogUI;
    using Loader;

    /// <summary>
    /// Inner-Loop Problem Tutoring
    /// </summary>
    public partial class ProblemMainView
    {
        public GlobalHost globalHost;
       
        public ProblemMainView(ProblemLoader pl)
        {
            InitializeComponent();
            InitProblemView(pl.CurrentProblem);
            InitDrawingHost();            
            ConceptScaffoldView.TriggerScaffold += ConceptScaffoldViewOnTriggerScaffold;
        }

        private void InitProblemView(MathProblem mp)
        {
            var corpus = ProblemGenerator.Generate(mp, Background);
            foreach (var run in corpus)
            {
                if (run == null) continue;
                var conceptText = run as ConceptText;
                if (conceptText != null)
                {
                    conceptText.ShowConceptScaffold += conceptText_ShowConceptScaffold;
                }
                ProblemView.Problem.Inlines.Add(run);
                ProblemView.Problem.Inlines.Add(new LineBreak());
            }
        }

        private void InitDrawingHost()
        {
            int index = MathTutorEngine.Instance.CurrentTutorInterp.CurrentProblemTutor.PL.CurrentProblemIndex;

            globalHost = new GlobalHost(index,
            SystemConfig.Instance.IsGeometrySketchInput(),
            SystemConfig.Instance.IsDualInput(),
            SystemConfig.Instance.IsDualRepr());
            
            ProblemGrid.Children.Add(globalHost);
            Grid.SetRow(globalHost, 2);
        }

        #region Context-Free Scaffold Event Publisher

        private void ConceptScaffoldViewOnTriggerScaffold(object sender, ScaffoldEventArgs args)
        {
            if (args.ScaffoldType == ScaffoldType.Geometry)
            {
                globalHost.VisualizeConceptScaffoldGeometric(args.Concept);
            }
            else if (args.ScaffoldType == ScaffoldType.Algebra)
            {
                globalHost.VisualizedConceptScaffoldAlgebraic(args.Concept);
            }
        }

        private void conceptText_ShowConceptScaffold(object sender, ConceptEventArgs args)
        {
            ConceptScaffoldView.Show(args);
        }

        #endregion
    }
}
