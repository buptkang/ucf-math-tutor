﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MathCogUI;
using MathTutorUserControl;

namespace MathTutor.Problem
{
    /// <summary>
    /// Interaction logic for ConceptScaffold.xaml
    /// </summary>
    public partial class ConceptScaffold : MetroContentControl
    {
        public event ScaffoldDelegate TriggerScaffold;

        public string CurrentConcept { get; set; }

        public ConceptScaffold()
        {
            InitializeComponent();
            panel.Visibility = Visibility.Collapsed;
            explainbox.StylusDown += explainbox_StylusDown;
        }

        void explainbox_StylusDown(object sender, StylusDownEventArgs e)
        {
            panel.Visibility = Visibility.Collapsed;
        }

        public void Show(ConceptEventArgs args)
        {
            //ConceptLabel.Content = args.ConceptName;
            ConceptExplanLabel.Text = args.ConceptExplain;
            CurrentConcept = args.ConceptName;
            panel.Visibility = Visibility.Visible;
        }

        private void GeometryExplain(object sender, RoutedEventArgs e)
        {
            if (geometryExplain.IsChecked == null ||
                geometryExplain.IsChecked.Value == true)
            {
                if (TriggerScaffold != null)
                {
                    TriggerScaffold(this, new ScaffoldEventArgs(CurrentConcept, ScaffoldType.Geometry));
                }                
            }
            else
            {
                var item = FindParent<ProblemMainView>(this);
                item.globalHost.Reset();
            }
        }

        public static T FindParent<T>(DependencyObject child) where T : DependencyObject
        {
            //get parent item
            DependencyObject parentObject = VisualTreeHelper.GetParent(child);

            //we've reached the end of the tree
            if (parentObject == null) return null;

            //check if the parent matches the type we're looking for
            T parent = parentObject as T;
            if (parent != null)
                return parent;
            else
                return FindParent<T>(parentObject);
        }

        private void AlgebraExplain(object sender, RoutedEventArgs e)
        {
            if (TriggerScaffold != null)
            {
                TriggerScaffold(this, new ScaffoldEventArgs(CurrentConcept, ScaffoldType.Algebra));
            }
        }
    }
}
