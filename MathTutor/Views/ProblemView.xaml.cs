﻿using MahApps.Metro.Controls;
using System.Windows.Documents;

namespace MathTutorUserControl.Problem
{
    public partial class ProblemView : MetroContentControl
    {
        private Paragraph _problemParagraph;
        public Paragraph Problem
        {
            set { _problemParagraph = value; }
            get { return _problemParagraph; }
        }

        public ProblemView()
        {
            InitializeComponent();
            _problemParagraph = new Paragraph() { FontSize = 17 };
            ProblemDoc.Blocks.Add(Problem);
            //ProblemInstruction.Visibility = SystemConfig.InteractiveProblem ? Visibility.Visible : Visibility.Collapsed;
        }
    }
}
