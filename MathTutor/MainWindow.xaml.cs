﻿using System;
using System.Diagnostics;
using System.Windows.Controls.Primitives;
using System.Windows.Forms;
using MathCog2.UserModeling;
using MathCogUI;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Application = System.Windows.Application;
using Button = System.Windows.Controls.Button;
using MessageBox = System.Windows.MessageBox;

namespace MathTutor
{
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            InitGeneralReasoning();
            OuterLoopProblemSelection();
        }

        /*
         * Problem Independent Init for the back-end tutoring engine.
         */
        private void InitGeneralReasoning()
        {
            InteractiveLogger.Instance.UserId = SystemConfig.UserId;
            InteractiveLogger.Instance.StartProblemSolvingTimer();

            MathTutorSettings.ReasoningOn = (SystemConfig.TutorReasonOn == 1);
            MathTutorSettings.TutorMode = (SystemConfig.ShowTutorMode == 1);
        }

        #region Outer-Loop Problem Selection

        private void OuterLoopProblemSelection()
        {
            // initialize tabItem array
            _tabItems = new List<TabItem>();
            // add a tabItem with + in header 
            _tabAdd = new TabItem();
            _tabAdd.Header = "+";
            // tabAdd.MouseLeftButtonUp += new MouseButtonEventHandler(tabAdd_MouseLeftButtonUp);
            _tabItems.Add(_tabAdd);
            // add first tab
            AddTabItem();
            // bind tab control
            tabDynamic.DataContext = _tabItems;
            tabDynamic.SelectedIndex = 0;
        }

        private List<TabItem> _tabItems;
        private TabItem _tabAdd;

        private TabItem AddTabItem()
        {
            int? problemIndex = SystemConfig.Instance.GetNextProblem();

/*            if (SystemConfig.Instance.JustHalf())
            {
                var result = MessageBox.Show("You have completed an experiment for one UI. Please ask the researcher to fill in a survey now.",
                    "Important Message", MessageBoxButton.OK);

                if (result == MessageBoxResult.OK)
                {
                    Application.Current.Shutdown();
                }
                return null;
            }*/

            if (problemIndex == null)
            {
                var result = MessageBox.Show("Excellent. Now talk to the researcher for the following task.!!!",
                    "Important Message", MessageBoxButton.OK);
                if (result == MessageBoxResult.OK)
                {
                    Application.Current.Shutdown();
                }
                return null;
            }

            Debug.Assert(problemIndex != null);

            MathTutorEngine.Instance.Select(problemIndex.Value);
            InteractiveLogger.Instance.LoadCurrentProblem(problemIndex.Value, SystemConfig.Instance.IsGeometrySketchInput());

            var pl = MathTutorEngine.Instance.CurrentTutorInterp.CurrentProblemTutor.PL;
            int count = _tabItems.Count;

            // create new tab item
            var tab = new TabItem();
            //tab.Header = string.Format("Problem {0}", pl.CurrentProblemIndex + 1);
            tab.Header = string.Format("Problem", pl.CurrentProblemIndex + 1);
            tab.Name = string.Format("tab{0}", pl.CurrentProblemIndex + 1);
            tab.HeaderTemplate = tabDynamic.FindResource("TabHeader") as DataTemplate;
            tab.MouseDoubleClick += tab_MouseDoubleClick;

            // add controls to tab item, this case I added just a textbox
            //var txt = new TextBox();
            //txt.Name = "txt";

            var problemMainView = new ProblemMainView(pl);
            tab.Content = problemMainView;

            tab.Tag = pl; //problem loader instance as the tag.

            // insert tab item right before the last (+) tab item
            _tabItems.Insert(count - 1, tab);
            return tab;
        }

        private void tabDynamic_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            TabItem tab = tabDynamic.SelectedItem as TabItem;
            if (tab == null) return;

            if (tab.Equals(_tabAdd))
            {
                if (InteractiveLogger.Instance.CurrentProblemAnalytic != null)
                {
                    MessageBox.Show("Please click the done button for the current problem first!");
                    // select newly added tab item
                    // clear tab control binding

                    // bind tab control
                    tabDynamic.DataContext = _tabItems;
                    tabDynamic.SelectedItem = _tabItems[_tabItems.Count - 2];
                    return;
                }

                if (_tabItems.Count> 8)
                {
                    // clear tab control binding
                    _tabItems.RemoveRange(0, _tabItems.Count-2);
                }

                // clear tab control binding
                tabDynamic.DataContext = null;

                TabItem newTab = AddTabItem();
                if (newTab == null) return;
                // bind tab control
                tabDynamic.DataContext = _tabItems;
                // select newly added tab item
                tabDynamic.SelectedItem = newTab;

            }
            else
            {
                // your code here...
            }
        }

        public void NavigateToNextTask()
        {
            if (_tabItems.Count > 8)
            {
                // clear tab control binding
                _tabItems.RemoveRange(0, _tabItems.Count - 2);
            }

            // clear tab control binding
            tabDynamic.DataContext = null;

            TabItem newTab = AddTabItem();
            if (newTab == null) return;
            // bind tab control
            tabDynamic.DataContext = _tabItems;
            // select newly added tab item
            tabDynamic.SelectedItem = newTab;
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            string tabName = (sender as Button).CommandParameter.ToString();

            var item = tabDynamic.Items.Cast<TabItem>().Where(i => i.Name.Equals(tabName)).SingleOrDefault();

            TabItem tab = item as TabItem;

            if (tab != null)
            {
                if (_tabItems.Count < 3)
                {
                    MessageBox.Show("Cannot remove last tab.");
                }
                else if (MessageBox.Show(string.Format("Are you sure you want to remove the tab '{0}'?", tab.Header.ToString()),
                    "Remove Tab", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    // get selected tab
                    TabItem selectedTab = tabDynamic.SelectedItem as TabItem;

                    // clear tab control binding
                    tabDynamic.DataContext = null;

                    _tabItems.Remove(tab);

                    // bind tab control
                    tabDynamic.DataContext = _tabItems;

                    // select previously selected tab. if that is removed then select first tab
                    if (selectedTab == null || selectedTab.Equals(tab))
                    {
                        selectedTab = _tabItems[0];
                    }
                    tabDynamic.SelectedItem = selectedTab;
                }
            }
        }

        private void tabAdd_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            // clear tab control binding
            tabDynamic.DataContext = null;

            TabItem tab = this.AddTabItem();
            // bind tab control
            tabDynamic.DataContext = _tabItems;
            // select newly added tab item
            tabDynamic.SelectedItem = tab;
        }

        private void tab_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            TabItem tab = sender as TabItem;

            /* TabProperty dlg = new TabProperty();

             // get existing header text
             dlg.txtTitle.Text = tab.Header.ToString();

             if (dlg.ShowDialog() == true)
             {
                 // change header text
                 tab.Header = dlg.txtTitle.Text.Trim();
             }*/
        }

        #endregion

        #region Settings

        private void ShowKnowledgeMap(object sender, RoutedEventArgs e)
        {
          /*  var analytics = new VisualAnalytics();
            analytics.Show();*/
        }

        private void ShowSettingPage(object sender, RoutedEventArgs e)
        {
            var setting = new Setting();
            setting.Show();
        }

        private void Clear(object sender, RoutedEventArgs e)
        {
            var tab = tabDynamic.SelectedItem as TabItem;

            if (tab == null) return;
            var problemView = tab.Content as ProblemMainView;
            if (problemView == null) return;
            problemView.globalHost.Reset();
            InteractiveLogger.Instance.CurrentProblemAnalytic.Reset();
        }

        private void MainWindow_OnClosed(object sender, EventArgs e)
        {
            InteractiveLogger.Instance.EndProblemSolvingTimer();
            InteractiveLogger.Instance.GenerateUserReport();
        }

        #endregion

        private void Sketch_Alternative_Click(object sender, RoutedEventArgs e)
        {
            var tab = tabDynamic.SelectedItem as TabItem;

            if (tab == null) return;
            var problemView = tab.Content as ProblemMainView;
            if (problemView == null) return;
           
            var button = sender as ToggleButton;
            Debug.Assert(button != null);
            if (button.IsChecked.Value)
            {
                button.Content = "Hide Alternative Sketch Recognition";
            }
            else
            {
                button.Content = "Show Alternative Sketch Recognition";
            }

            problemView.globalHost.ControlAlgebraSketchRecognition(button.IsChecked.Value);
        }


        private void Sketch_Alternative_Init(object sender, EventArgs e)
        {
            var button = sender as ToggleButton;
            Debug.Assert(button != null);
            button.IsChecked = true;
            button.Content = "Hide Alternative Sketch Recognition";
        }
    }
}
