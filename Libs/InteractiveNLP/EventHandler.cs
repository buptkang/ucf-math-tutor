﻿namespace MathCogUI
{
    using System;

    public delegate void ConceptDelegate(object sender, ConceptEventArgs args);

    public class ConceptEventArgs : EventArgs
    {
        public string ConceptName { get; set; }
        public string ConceptExplain { get; set; }

        public ConceptEventArgs(string conceptName, string conceptExplain)
        {
            ConceptName    = conceptName;
            ConceptExplain = conceptExplain;
        }
    }

   public delegate void DragDelegate(object sender, DragEventArgs args);

    public class DragEventArgs : EventArgs
    {
        public string Comment { get; set; }
        public bool ShowHint { get; set; }
        public HintType Type;

        public DragEventArgs(bool showHint, string str)
        {
            ShowHint = showHint;
            Comment = str;
            Type = HintType.System;
        }

        public DragEventArgs(bool showHint, string str, HintType _type)
        {
            ShowHint = showHint;
            Comment = "System Hint:" + str;
            Type = _type;
        }
    }

    public enum HintType
    {
        Strategy, Knowledge, Warning, System
    }
}
