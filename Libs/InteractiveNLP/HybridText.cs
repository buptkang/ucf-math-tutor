﻿/*******************************************************************************
 * Math Interactive Tutoring System
 * <p>
 * Copyright (C) 2015 Bo Kang, Joseph J. LaViola Jr.
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

using Loader;

namespace MathCogUI
{
    using System;
    using System.Diagnostics;
    using System.Windows.Input;
    using System.Windows;

    public class HybridText : InputText
    {
        public string ConceptName    { get; set; }
        public string ConceptExplain { get; set; }

        public HybridText(ProblemWord pw, System.Windows.Media.Brush background)
            :base(pw, background, true)
        {
            Foreground = System.Windows.Media.Brushes.DarkBlue;

            var tuple2 = pw.DragObject as Tuple<object, object>;
            Debug.Assert(tuple2 != null);
            var conceptTuple = tuple2.Item1 as Tuple<string, string>;
            Debug.Assert(conceptTuple != null);

            ConceptName = conceptTuple.Item1;
            ConceptExplain = conceptTuple.Item2;
        }

        protected override void OnStylusMove(StylusEventArgs e)
        {
            base.OnStylusMove(e);
            var data = new DataObject();
            data.SetData("HybridObject", this);
            DragDrop.DoDragDrop(this, data, DragDropEffects.Copy);
        }
    }
}
