﻿/*******************************************************************************
 * Math Interactive Tutoring System
 * <p>
 * Copyright (C) 2015 Bo Kang, Joseph J. LaViola Jr.
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

using System;
using System.Diagnostics;
using Loader;

namespace MathCogUI
{
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows;

    public class InputText : DragText
    {
        public string DragContent { get; set; }
        public bool? OnAlgebraSide { get; set; } //target drop interactive information

        public InputText(ProblemWord pw, Brush background, bool isHybrid)
            : base(pw, background)
        {
            var tuple2 = pw.DragObject as Tuple<object, object>;
            Debug.Assert(tuple2 != null);
            var inputStr = tuple2.Item2 as string;
            Debug.Assert(inputStr != null);
            DragContent = inputStr;
        }

        public InputText(ProblemWord pw, Brush background)
            : base(pw, background)
        {
            Foreground = Brushes.DarkSlateBlue;
            DragContent = (string)pw.DragObject;
        }

        protected override void OnStylusMove(StylusEventArgs e)
        {
            base.OnStylusMove(e);
            Cursor = Cursors.Hand;
            var data = new DataObject();
            //data.SetData(DataFormats.StringFormat, Phrase);
            data.SetData("ProblemDragObject", this);
            DragDrop.DoDragDrop(this, data, DragDropEffects.Copy);
        }
    }
}
