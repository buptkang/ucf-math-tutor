﻿/*******************************************************************************
 * Math Interactive Tutoring System
 * <p>
 * Copyright (C) 2015 Bo Kang, Joseph J. LaViola Jr.
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

using Loader;

namespace MathCogUI
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;

    public class DragText : TextBox
    {
        public event DragDelegate ShowSystemHint;

        public bool IsDraggable { get; set; } //drag action done or not.

        public DragText(ProblemWord pw, Brush background)
        {
            Text = pw.Word;
            IsReadOnly = true;
            Background = background;
            BorderBrush = Brushes.Transparent;
            BorderThickness = new Thickness(0.0, 0.0, 0.0, 0.0);
            IsDraggable = true;
        }

        protected override void OnStylusDown(StylusDownEventArgs e)
        {
            Cursor = Cursors.Hand;
            SelectAll();
            e.Handled = true;
        }

        protected override void OnStylusEnter(StylusEventArgs e)
        {
            FontWeight = FontWeights.SemiBold;
            Cursor = Cursors.Hand;
/*            if (ShowSystemHint != null)
            {
                ShowSystemHint(this, new SystemEventArgs(true, APConfig.InputTextDrag));
            }*/
            e.Handled = true;
        }

        protected override void OnStylusLeave(StylusEventArgs e)
        {
            FontWeight = FontWeights.Normal;
/*          if (ShowSystemHint != null)
            {
                ShowSystemHint(this, new SystemEventArgs(false, null));
            }*/
            //SelectionLength = 0;
            //Cursor = Cursors.None;
            e.Handled = true;
        }

        protected override void OnStylusMove(StylusEventArgs e)
        {
            if (ShowSystemHint != null)
            {
                ShowSystemHint(this, new DragEventArgs(false, null));
            }
        }
    }
}
