﻿/*******************************************************************************
 * Math Interactive Tutoring System
 * <p>
 * Copyright (C) 2015 Bo Kang, Joseph J. LaViola Jr.
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

using Loader;

namespace MathCogUI
{
    using System;
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Input;

    public class ConceptText : DragText
    {
        public event ConceptDelegate ShowConceptScaffold;

        public string ConceptName    { get; set; }
        public string ConceptExplain { get; set; }
        
        public ConceptText(ProblemWord pw, System.Windows.Media.Brush background):
            base(pw,background)
        {
            Foreground = System.Windows.Media.Brushes.DarkOliveGreen;
            var tuple2 = pw.DragObject as Tuple<string, string>;
            Debug.Assert(tuple2!= null);
            ConceptName = pw.Word;
            ConceptExplain = tuple2.Item2;
            IsDraggable = false;
        }

        protected override void OnStylusEnter(StylusEventArgs e)
        {
            base.OnStylusEnter(e);
            Cursor = Cursors.Help;
        }

        protected override void OnStylusDown(StylusDownEventArgs e)
        {
            base.OnStylusDown(e);
            if (ShowConceptScaffold != null)
            {
                ShowConceptScaffold(this, new ConceptEventArgs(ConceptName, ConceptExplain));
            }
        }

       /* protected override void OnStylusMove(StylusEventArgs e)
        {
            base.OnStylusMove(e);

            var data = new DataObject();
            //data.SetData(DataFormats.StringFormat, Phrase);
/*            data.SetData("ConceptObject", this);
            DragDrop.DoDragDrop(this, data, DragDropEffects.Copy);#1#
        }*/
    }
}
