﻿/*******************************************************************************
 * Math Interactive Tutoring System
 * <p>
 * Copyright (C) 2015 Bo Kang, Joseph J. LaViola Jr.
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

using Loader;

namespace MathCogUI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;

    public static class ProblemGenerator
    {
        public static IEnumerable<UIElement> Generate(MathProblem problem, Brush background)
        {
            return problem.Words.Select(pw => pw.Generate(background)).ToList();
        }

        /// <summary>
        /// Main Interactioin Method
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        public static UIElement Generate(this ProblemWord word, Brush background)
        {
            if (word.InteractType == ProblemWord.InteractionType.None)
            {
                return new TextBox()
                {
                    Text = word.Word,
                    IsReadOnly = true,
                    Background = background,
                    BorderBrush = Brushes.Transparent,
                    IsHitTestVisible = false,
                    Cursor = Cursors.None
                };
            }

            if (word.InteractType == ProblemWord.InteractionType.Input)
            {
                //Problem Input -- Action Drag
                return new InputText(word, background);
            }

            if (word.InteractType == ProblemWord.InteractionType.Concept)
            {
                return new ConceptText(word, background);
            }

            if (word.InteractType == ProblemWord.InteractionType.Hybrid)
            {
                return new HybridText(word, background);
            }
            throw new Exception("Cannot reach here!");
        }

        [Obsolete]
        private static Run Generate(this ProblemWord word)
        {
            return new WordRun(word);
        }
    }
}
