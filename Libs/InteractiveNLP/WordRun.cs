﻿/*******************************************************************************
 * Math Interactive Tutoring System
 * <p>
 * Copyright (C) 2015 Bo Kang, Joseph J. LaViola Jr.
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

using Loader;

namespace MathCogUI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using System.Windows.Documents;
    using System.Windows.Input;

    [Obsolete]
    public class WordRun : Run
    {
        public bool IsPopuableConcept { get; set; }
        public string ConceptExplain { get; set; }
        public object Phrase { get; set; } // string or mathML
        public bool IsInput { get; set; }

        public WordRun(ProblemWord pw)
            : base(pw.Word+" ")
        {
            Phrase = pw.Word;
            if (pw.InteractType == ProblemWord.InteractionType.Concept)
            {
                IsPopuableConcept = true;
                //ConceptExplain = pw.ConceptExplanation;
                
                this.ToolTip = new TextBlock { Text = ConceptExplain, FontSize = 18, FontWeight = FontWeights.Bold, FontStyle = FontStyles.Italic };
                ToolTipService.SetIsEnabled(this, false);
            }

            IsInput = pw.InteractType == ProblemWord.InteractionType.Input;

            this.StylusLeave += DraggableText_StylusLeave;
            this.StylusEnter += DraggableText_StylusEnter;
        }

        void DraggableText_StylusLeave(object sender, StylusEventArgs e)
        {
            if (IsPopuableConcept)
            {
                ToolTipService.SetIsEnabled(this, false);
            }

            if (IsInput)
            {
                FontWeight = FontWeights.Normal;
                //SelectionLength = 0;
                Cursor = Cursors.None;
            }

            e.Handled = true;
        }

        void DraggableText_StylusEnter(object sender, StylusEventArgs e)
        {
            if (IsPopuableConcept)
            {
                ToolTipService.SetIsEnabled(this, true);
                ToolTipService.SetPlacement(this, PlacementMode.Top);
                ToolTipService.SetHorizontalOffset(this, 1.0);
                Cursor = Cursors.Help;
            }
            if (IsInput)
            {
                FontWeight = FontWeights.SemiBold;
                Cursor = Cursors.Hand;
            }
            e.Handled = true;
        }
    }
}
