﻿/*******************************************************************************
 * Math Interactive Tutoring System
 * <p>
 * Copyright (C) 2015 Bo Kang
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection.Emit;
using System.Text;
using MathLogger;

namespace MathCogUI
{
    public class InteractiveLogger
    {
        #region Singleton, Constructor and Reset
        
        private static InteractiveLogger _instance;
        public static InteractiveLogger Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new InteractiveLogger();
                }
                return _instance;
            }
        }

        private InteractiveLogger()
        {
            ProblemsAnalytics = new List<InteractiveAnalytics>();
            ProblemReports    = new List<ProblemReport>();
            TestingReports    = new List<ProblemReport>();
        }

        public List<InteractiveAnalytics> ProblemsAnalytics { get; set; }
        public InteractiveAnalytics CurrentProblemAnalytic { get; set; }
        public List<ProblemReport> ProblemReports { get; set; }
        public List<ProblemReport> TestingReports { get; set; }

        public string UserId { get; set; }

        #endregion

        #region Metrics

        #region Metric: Time to solve all problems

        private readonly Stopwatch _solvingSummaryTimer = new Stopwatch();

        /*
         * 2. How long does it take for users to proceed problem-solving?
         * Note: The time starts from any interaction till the user clicks close button on the user interface. Time limit (lower bound of time : 4 minutes)
         */

        public void StartProblemSolvingTimer()
        {
            _solvingSummaryTimer.Start();
        }

        public void EndProblemSolvingTimer()
        {
            _solvingSummaryTimer.Stop();
        }

        public string AllProblemSolvingTime
        {
            get
            {
                var builder = new StringBuilder();
                builder.Append(InteractiveMetrics.S_TotalTime)
                       .Append(string.Format("Time elapsed: {0:hh\\:mm\\:ss}", _solvingSummaryTimer.Elapsed))
                       .Append("\n");
                return builder.ToString();
            }
        }

        #endregion

        #endregion

        #region Report

        public string Report()
        {
            var logger = new StringBuilder();
            logger.Append("Math Tutor Log file User Summary:")
                  .Append("\n")
                  .Append(AllProblemSolvingTime);
            return logger.ToString();
        }

        #endregion

        #region Public API

        public void LoadCurrentProblem(int problemIndex, bool sketchInput)
        {
            CurrentProblemAnalytic = new InteractiveAnalytics(problemIndex);
            CurrentProblemAnalytic.SketchInput = sketchInput;
            CurrentProblemAnalytic.StartProblemSolvingTimer();
        }

        public void CacheProblemReport(bool isTest)
        {
            CurrentProblemAnalytic.EndProblemSolvingTimer();
            CurrentProblemAnalytic.EndGeometryInteractTimer();

            var report = new ProblemReport(UserId, CurrentProblemAnalytic);
            if (!isTest)
            {
                ProblemReports.Add(report);                
            }
            else
            {
                TestingReports.Add(report);
            }
        }

        public void GenerateUserReport()
        {
            if (ProblemReports.Count != 0 || TestingReports.Count != 0)
            {
                //TODO, like how many problems, problem sequence, total time.
                //TODO, next learning study
               /* var report = new UserReport(this);
                report.GenerateLogFile();*/
            }

            //Practice : Control Study
            if (ProblemReports.Count != 0)
            {
                var reports = new ProblemsReport(UserId, ProblemReports, true);
                reports.GenerateLogFile();                
            }

            //Testing : Free Study
            if (TestingReports.Count != 0)
            {
                var reports2 = new ProblemsReport(UserId, TestingReports, false);
                reports2.GenerateLogFile();                
            }
        }

        #endregion
    }
}
