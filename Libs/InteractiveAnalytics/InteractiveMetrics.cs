﻿namespace MathLogger
{
    public class InteractiveMetrics
    {
        #region Interaction

        /*****************   Interactive quantitative data ********************/

        public static string I_SolvingTime   = "Total Time to solve the current problem: ";
        public static string I_Algebra_Time  = "Interaction Time on the algebraic canvas: ";

        //This counts the scribble or delete steps
        public static string I_Algebra_Steps = "Algebraic Expression Counts: ";
        public static string I_Geometry_Steps = "Geometry Concept Counts: ";

        #region Geometry 

        //Fine-Grained data capture in the geometry canvas.

        //Geo Interaction Time : Training only
        public static string I_Geometry_Interaction_Time = "Geo Interaction Time: ";

        //Input Time
        public static string I_Geometry_Input_Time    = "Geo Input Time: ";

        //Manipulation Time
        public static string I_Geometry_Manipulation_Time = "Geo Manipulation Time: ";

        //Delete Time
        public static string I_Geometry_Delete_Time = "Geo Delete Time: ";

        //Input Count
        public static string I_Geometry_Input_Count = "Geo Input Count: ";

        //Manipulation Count
        public static string I_Geometry_Manipulation_Count = "Geo Manipulation Count: ";

        //Delete Count
        public static string I_Geometry_Delete_Count    = "Geo Delete Count: ";

        //Structural Condition: Mode Switch Count and Timing
        public static string I_Geometry_Mode_Switch_Count = "Mode Switch Count: ";
        public static string I_Geometry_Mode_Switch_Time = "Mode Switch Time: ";

        #endregion

        #endregion

        #region Reasoning

        #region TODO Reasoning

        /*        public string Row4 = "4.How many times the student asks for the hint through the pen-inquiry gesture?";
        public string Row4_1 = "4.1.How many steps the system evaluated?";

        public string Row5_1 = "5.1. How many time the system shows the meta-cognition scaffolding user control?";
        public string Row5_2 = "5.2. How many times the system shows the answer scaffolding user control?";

        public string Row6_1 = "6.How many times the user rectify his result after querying?";*/

        #endregion

        #endregion

        #region Summary

        public static string S_TotalTime = "Total time to solve all problems: ";

        #endregion
    }
}