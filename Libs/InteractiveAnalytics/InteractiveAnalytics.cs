﻿/*******************************************************************************
 * Math Interactive Tutoring System
 * <p>
 * Copyright (C) 2015 Bo Kang, Joseph J. LaViola Jr.
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

using System;
using System.Diagnostics;
using System.Text;
using MathLogger;

namespace MathCogUI
{
    /*
     * Per-problem query analytics
     */
    public class InteractiveAnalytics
    {
        #region Parameters, Constructor, Reset and Report

        public int ProblemId { get; set; }

        public bool SketchInput { get; set; }

        public InteractiveAnalytics(int id)
        {
            ProblemId = id;
        }

        public void Reset()
        {
            //Interaction Metric : I_SolvingTime
            _problemSolvingTimeSpan = new TimeSpan();

            //Interaction Metric: I_Algebra_Time
            _algebraTimeSpan = new TimeSpan();

            //Interaction Metric: I_Algebra_Steps
            AlgebraicInputSteps = 0;

            //Interaction Metric: I_Geometry_Steps
            GeometryInputSteps = 0;

            //////////////////////////////////////////////////////

            //Interaction Metric: I_Geometry_Time, Interaction Time.
            _geometryInteractionTimeSpan = new TimeSpan();

            //Interaction Metric: I_Geometry_Time, Input Time
            _geometryInputSpan = new TimeSpan();

            //Interaction Metric: I_Geometry_Time, Manipulation Time
            _geometryManipulationSpan = new TimeSpan();

            //Interaction Metric: I_Geometry_Time, Delete Time
            if (SketchInput)
            {
                _geometrySketchGestureSpan = new TimeSpan();
            }
            else
            {
                _geometryStructuralTimeSpan = new TimeSpan();
            }

            //Interaction Metric: I_Geometry_Input_Count
            GeometryInputCount = 0;

            //Interaction Metric: I_Geometry_Manipulation_Count
            GeometryManipulationCount = 0;

            //Interaction Metric: I_Geometry_Time, Delete Count
            GeometryDeleteCount = 0;

            if (!SketchInput)
            {
                //Structural Mode-Switch Count
                StructuralModeSwitchCount = -1;
                _geometryModeSwitchTimeSpan = new TimeSpan();
            }
        }

        #region Report

        public string Report_PerProblem_Study2()
        {
            var logger = new StringBuilder();
            logger.Append("Problem Id: ")
                .Append(ProblemId)
                .Append("\n")
                .Append(SolvingProblemTime)
          /*      .Append(IAlgebraTime)*/
                //.Append(IAlgebraSteps)
                //.Append(IGeometrySteps)
                .Append("\n")
                .Append("\n");
         /*   if (SketchInput)
            {
                logger.Append("Current Geometry Input Condition: Sketch")
                    .Append("\n");
            }
            else
            {
                logger.Append("Current Geometry Input Condition: Structure")
                    .Append("\n");
            }
            logger
                .Append(IGeometryInteractTime)
                .Append(IGeometryInputTime)
                .Append(IGeometryManipulationTime)
                .Append(IGeometryDeleteTime)
                .Append(IGeometryInputCount)
                .Append(IGeometryManipulationCount)
                .Append(IGeometryDeleteCount);

            if (!SketchInput)
            {
                logger.Append(IStructuralModeSwitchCount)
                    .Append(IStructuralModeSwitchTime);
            }*/

            return logger.ToString();
        }

        public string Report_PerProblem_Study1()
        {
            var logger = new StringBuilder();
            logger.Append("Problem Id: ")
                .Append(ProblemId)
                .Append("\n")
                .Append(SolvingProblemTime)
                .Append(IAlgebraTime)
                //.Append(IAlgebraSteps)
                //.Append(IGeometrySteps)
                .Append("\n")
                .Append("\n");
            if (SketchInput)
            {
                logger.Append("Current Geometry Input Condition: Sketch")
                    .Append("\n");
            }
            else
            {
                logger.Append("Current Geometry Input Condition: Structure")
                    .Append("\n");
            }
            logger
                .Append(IGeometryInteractTime)
                .Append(IGeometryInputTime)
                .Append(IGeometryManipulationTime)
                .Append(IGeometryDeleteTime)
                .Append(IGeometryInputCount)
                .Append(IGeometryManipulationCount)
                .Append(IGeometryDeleteCount);

            if (!SketchInput)
            {
                logger.Append(IStructuralModeSwitchCount)
                    .Append(IStructuralModeSwitchTime);
            }

            return logger.ToString();
        }

        #endregion

        #endregion

        #region Metrics

        #region Interaction Metric : I_SolvingTime

        private TimeSpan _problemSolvingTimeSpan;

        private readonly Stopwatch _problemSolvingTimer = new Stopwatch();

        /*
         * 2. How long does it take for users to proceed problem-solving?
         * Note: The time starts from any interaction till the user clicks close button on the user interface. Time limit (lower bound of time : 4 minutes)
         */

        public void StartProblemSolvingTimer()
        {
            _problemSolvingTimer.Reset();
            _problemSolvingTimer.Start();
        }

        public void EndProblemSolvingTimer()
        {
            _problemSolvingTimer.Stop();
            TimeSpan ts = _problemSolvingTimer.Elapsed;
            _problemSolvingTimeSpan = _problemSolvingTimeSpan.Add(ts);
            _problemSolvingTimer.Reset();
            _problemSolvingTimer.Start();
        }

        public string SolvingProblemTime
        {
            get
            {
                var builder = new StringBuilder();
                builder.Append(InteractiveMetrics.I_SolvingTime)
                    .Append(string.Format("{0:hh\\:mm\\:ss}", _problemSolvingTimeSpan))
                    .Append("\n");
                return builder.ToString();
            }
        }

        #endregion

        #region Interaction Metric: I_Algebra_Time

        /*
         * Calculation: number of strokes * (Stylus Up - Stylus down)
         */ 

        private TimeSpan _algebraTimeSpan;

        private readonly Stopwatch _algebraTimer = new Stopwatch();

        public void StartAlgebraTimer()
        {
            _algebraTimer.Start();
        }

        public void EndAlgebraTimer()
        {
            _algebraTimer.Stop();
            TimeSpan ts = _algebraTimer.Elapsed;
            _algebraTimeSpan = _algebraTimeSpan.Add(ts);
            _algebraTimer.Reset();
        }

        public string IAlgebraTime
        {
            get
            {
                var builder = new StringBuilder();
                builder.Append(InteractiveMetrics.I_Algebra_Time)
                    .Append(string.Format("{0:hh\\:mm\\:ss\\:fffff}", _algebraTimeSpan))
                    .Append("\n");
                return builder.ToString();
            }
        }

        #endregion

        #region Interaction Metric: I_Geometry_Time

        #region Interaction Time

        /*
         * Calculation: Click "I am done" - first interaction upon geometry canvas.
         * 
         * first interaction can be:
         * 
         * 1. canvas manipulation.
         * 2. figure input, manipulate or delete.
         * 3. model switch (structural only)
         */ 

        private TimeSpan _geometryInteractionTimeSpan;

        private readonly Stopwatch _geometryInteractionTimer = new Stopwatch();

        public void StartGeometryInteractTimer()
        {
            if (_geometryInteractionTimer.IsRunning) return;
            _geometryInteractionTimer.Reset();
            _geometryInteractionTimer.Start();
        }

        public void EndGeometryInteractTimer()
        {
            _geometryInteractionTimer.Stop();
            TimeSpan ts = _geometryInteractionTimer.Elapsed;
            _geometryInteractionTimeSpan = _geometryInteractionTimeSpan.Add(ts);
        }

        public string IGeometryInteractTime
        {
            get
            {
                var builder = new StringBuilder();
                builder.Append(InteractiveMetrics.I_Geometry_Interaction_Time)
                       .Append(string.Format("{0:hh\\:mm\\:ss\\:fffff}", _geometryInteractionTimeSpan))
                       .Append("\n");
                return builder.ToString();
            }
        }

        #endregion

        #region Input Time

        private TimeSpan _geometryInputSpan = new TimeSpan();

        private readonly Stopwatch _geometryInputTimer = new Stopwatch();

        public void StartGeometryInputTimer()
        {
            GeometryInputCount++;
            StartGeometryInteractTimer();
            _geometryInputTimer.Reset();
            _geometryInputTimer.Start();
        }

        public void EndGeometryInputTimer()
        {
            _geometryInputTimer.Stop();
            TimeSpan ts = _geometryInputTimer.Elapsed;
            _geometryInputSpan = _geometryInputSpan.Add(ts);            
        }

        public string IGeometryInputTime
        {
            get
            {
                var builder = new StringBuilder();
                builder.Append(InteractiveMetrics.I_Geometry_Input_Time)
                       .Append(string.Format("{0:hh\\:mm\\:ss\\:fffff}", _geometryInputSpan))
                       .Append("\n");
                return builder.ToString();
            }
        }

        #endregion

        #region Manipulation Time

        private TimeSpan _geometryManipulationSpan;

        private readonly Stopwatch _geometryManipulationTimer = new Stopwatch();

        public void StartGeometryManipulationTimer()
        {
            GeometryManipulationCount++;
            StartGeometryInteractTimer();
            _geometryManipulationTimer.Start();
        }

        public void EndGeometryManipulationTimer()
        {
            _geometryManipulationTimer.Stop();
            TimeSpan ts = _geometryManipulationTimer.Elapsed;
            _geometryManipulationSpan = _geometryManipulationSpan.Add(ts);
            _geometryManipulationTimer.Reset();
        }

        public string IGeometryManipulationTime
        {
            get
            {
                var builder = new StringBuilder();
                builder.Append(InteractiveMetrics.I_Geometry_Manipulation_Time)
                       .Append(string.Format("{0:hh\\:mm\\:ss\\:fffff}", _geometryManipulationSpan))
                       .Append("\n");
                return builder.ToString();
            }
        }

        #endregion

        #region Delete Time

        #region Sketch Time

        /*
         * Gesture Scribble Time
         * 
         * Distinguish between ink input and gesture.
         * 
         * Adjustment is required.
         */

        private TimeSpan _geometrySketchGestureSpan;

        public void AdjustSketchGestureTime()
        {
            GeometryDeleteCount++;
            GeometryInputCount--;
            TimeSpan ts = _geometryInputTimer.Elapsed;
            _geometryInputSpan = _geometryInputSpan.Subtract(ts);
            _geometrySketchGestureSpan = _geometrySketchGestureSpan.Add(ts);
        }

        #endregion

        #region Structural Time 

        /*
         * (between select and click delete button)
         */

        private TimeSpan _geometryStructuralTimeSpan;

        private readonly Stopwatch _geometryStructuralDeleteTimer = new Stopwatch();

        public void StartGeometryStructuralDeleteTimer()
        {
            _geometryStructuralDeleteTimer.Reset();
            _geometryStructuralDeleteTimer.Start();
        }

        public void EndGeometryStructuralDeleteTimer()
        {
            _geometryStructuralDeleteTimer.Stop();
            TimeSpan ts = _geometryStructuralDeleteTimer.Elapsed;
            _geometryStructuralTimeSpan = _geometryStructuralTimeSpan.Add(ts);
        }


        //manipulation time, should be reduced to include the structural time.

        #endregion

        public string IGeometryDeleteTime
        {
            get
            {
                var builder = new StringBuilder();

                builder.Append(InteractiveMetrics.I_Geometry_Delete_Time)
                    .Append(string.Format("{0:hh\\:mm\\:ss\\:fffff}", SketchInput ? _geometrySketchGestureSpan : _geometryStructuralTimeSpan))
                       .Append("\n");
                return builder.ToString();
            }
        }

        #endregion

        #region Input Count

        public int GeometryInputCount { get; set; }

        public string IGeometryInputCount
        {
            get
            {
                var builder = new StringBuilder();
                builder.Append(InteractiveMetrics.I_Geometry_Input_Count)
                .Append(GeometryInputCount)
                .Append("\n");
                return builder.ToString();
            }
        }

        #endregion

        #region Manipulation Count

        public int GeometryManipulationCount { get; set; }

        public string IGeometryManipulationCount
        {
            get
            {
                var builder = new StringBuilder();
                builder.Append(InteractiveMetrics.I_Geometry_Manipulation_Count)
                .Append(GeometryManipulationCount)
                .Append("\n");
                return builder.ToString();
            }
        }

        #endregion

        #region Deletion Count

        public int GeometryDeleteCount { get; set; }

        public string IGeometryDeleteCount
        {
            get
            {
                var builder = new StringBuilder();
                builder.Append(InteractiveMetrics.I_Geometry_Delete_Count)
                .Append(GeometryDeleteCount)
                .Append("\n");
                return builder.ToString();
            }
        }

        #endregion

        #region Structural Mode-Switch Count

        public int StructuralModeSwitchCount = -1;

        public string IStructuralModeSwitchCount
        {
            get
            {
                var builder = new StringBuilder();
                builder.Append(InteractiveMetrics.I_Geometry_Mode_Switch_Count)
                        .Append(StructuralModeSwitchCount)
                        .Append("\n");
                return builder.ToString();
            }
        }

        #endregion

        #region Structural Mode-Switch Time

        private TimeSpan _geometryModeSwitchTimeSpan = new TimeSpan();

        private readonly Stopwatch _geometryModeSwitchTimer = new Stopwatch();

        public void StartGeometryModeSwitchTimer()
        {
            StartGeometryInteractTimer();
            _geometryModeSwitchTimer.Start();
        }

        public void EndGeometryModeSwitchTimer()
        {
            _geometryModeSwitchTimer.Stop();
            TimeSpan ts = _geometryModeSwitchTimer.Elapsed;
            _geometryModeSwitchTimeSpan = _geometryModeSwitchTimeSpan.Add(ts);
            _geometryModeSwitchTimer.Reset();
        }

        public string IStructuralModeSwitchTime
        {
            get
            {
                var builder = new StringBuilder();
                builder.Append(InteractiveMetrics.I_Geometry_Mode_Switch_Time)
                         .Append(string.Format("{0:hh\\:mm\\:ss\\:fffff}", _geometryModeSwitchTimeSpan))
                        .Append("\n");
                return builder.ToString();
            }
        }

        #endregion

        #endregion

        #region Interaction Metric: I_Algebra_Steps

        /* 3.1. How many steps the student inputs using the sketch through algebraic side?
         * (It is calculated in the end. The system does not record users' in-solving step deletion or modification.)*/

        public int AlgebraicInputSteps { get; set; }

        public string IAlgebraSteps
        {
            get
            {
                var builder = new StringBuilder();
                builder.Append(InteractiveMetrics.I_Algebra_Steps)
                       .Append(AlgebraicInputSteps)
                       .Append("\n");
                return builder.ToString();
            }
        }

        #endregion

        #region Interaction Metric: I_Geometry_Steps

        /* 
         * 3.2.How many steps the student inputs using the sketch through geometric side?
         * (It is calculated in the end. 
         * The system does not record users' in-solving step deletion 
         * or manipulation.
         * 
         * This does not equal to number of figures.
         * )
         */

        public int GeometryInputSteps;

        public string IGeometrySteps
        {
            get
            {
                var builder = new StringBuilder();
                builder.Append(InteractiveMetrics.I_Geometry_Steps)
                       .Append(GeometryInputSteps)
                       .Append("\n");
                return builder.ToString();
            }
        }

        #endregion

        /****************************************************************/

        #region TODO: Problem Solving

        /****************************************************************/
        /*
         * 1. Does the user finally solves this problem? 
         */

        /*        private bool _problemSolved;

                public bool SolvedProblem
                {
                    get { return _problemSolved; }
                    set { _problemSolved = value; }
                }*/

        /****************************************************************/

        #endregion

        #region TODO: Reasoning

        /******************************************************************/

        /*
         * 4. How many times the student asks for the hint through the pen-inquiry gesture?
         */
        public int QueryTimes { get; set; }

        /******************************************************************/

        /*
         * 4.1 How many steps the system evaluated?
         * (Students might skip certain steps, so it does not equal to the number of question 3.)
         */
        public int EvaluatedSteps { get; set; }

        /*****************************************************************/

        /*
         * 5.1. How many time the system shows the meta-cognition scaffolding user control?
         * 5.2. How many times the system shows the answer scaffolding user control?
         */
        public int QueryHintTimes { get; set; }
        public int QueryAnswerTimes { get; set; }

        /*        public void AnalyzeState(QueryFeedbackState state)
                {
                    switch (state)
                    {
                        case QueryFeedbackState.TutorQueryProcessedHint:
                            QueryHintTimes++;
                            break;
                        case QueryFeedbackState.TutorQueryProcessedAnswer:
                            QueryAnswerTimes++;
                            break;
                    }
                }*/

        /*********************************************************************/

        /*
         * 6.How many times the user rectify his result after querying?
         * (It is counted between every one wrong query result and the following correct query result.)
         */
        public bool? PreviousQueryResult { get; set; }
        public int RectifyCount { get; set; }

        #endregion

        #endregion 
    }
}