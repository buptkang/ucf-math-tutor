﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using MathCogUI;

namespace MathLogger
{
    public class UserReport
    {
        private InteractiveLogger _logger;

        public UserReport(InteractiveLogger log)
        {
            _logger = log;
        }

        private string GetCurrentTime()
        {
            var timeBuilder = new StringBuilder();
            timeBuilder
                .Append(DateTime.Now.Year.ToString())
                .Append(DateTime.Now.Month.ToString())
                .Append(DateTime.Now.Day.ToString())
                .Append(DateTime.Now.TimeOfDay.Hours.ToString())
                .Append(DateTime.Now.TimeOfDay.Minutes.ToString())
                .Append(DateTime.Now.TimeOfDay.Seconds.ToString());
            return timeBuilder.ToString();
        }

        //Log_userId_problemid_time.txt
        private string GetLogFileName()
        {
            var builder = new StringBuilder();
            builder.Append("Log_summary_")
                   .Append(_logger.UserId)
                   .Append("_")
                   .Append(GetCurrentTime())
                   .Append(".txt");
            return builder.ToString();
        }

        public void GenerateLogFile()
        {
            //string _path = Path.GetDirectoryName(Path.GetDirectoryName(Directory.GetCurrentDirectory()));
            string _current = Path.GetDirectoryName(Path.GetDirectoryName(Directory.GetCurrentDirectory()));
            string _path = Directory.GetParent(_current).ToString();
            string path = Path.Combine("@", _path, "LogFiles\\", GetLogFileName());
            File.WriteAllText(path, _logger.Report());
        }
    }
}
