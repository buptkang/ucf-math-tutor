﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using MathCogUI;

namespace MathLogger
{
    public class ProblemsReport
    {
        private List<ProblemReport> _reports;
        private string _userId;
        private bool _practice;

        public ProblemsReport(string userId, List<ProblemReport> lst, bool isPractice)
        {
            _reports = lst;
            _userId = userId;
            _practice = isPractice;
        }

        #region Log File of user interaction

        private string GetCurrentTime()
        {
            var timeBuilder = new StringBuilder();
            timeBuilder
                .Append(DateTime.Now.Year.ToString())
                .Append(DateTime.Now.Month.ToString())
                .Append(DateTime.Now.Day.ToString())
                .Append(DateTime.Now.TimeOfDay.Hours.ToString())
                .Append(DateTime.Now.TimeOfDay.Minutes.ToString())
                .Append(DateTime.Now.TimeOfDay.Seconds.ToString());
            return timeBuilder.ToString();
        }

        //Log_userId_problemid_time.txt
        private string GetLogFileName()
        {
            if (_practice)
            {
                var builder = new StringBuilder();
                builder.Append("Log_")
                       .Append(_userId)
                       .Append("_")
                       .Append("Practice")
                       .Append("_")
                       .Append(GetCurrentTime())
                       .Append(".txt");
                return builder.ToString();                
            }
            else
            {
                var builder = new StringBuilder();
                builder.Append("Log_")
                       .Append(_userId)
                       .Append("_")
                       .Append("Test")
                       .Append("_")
                       .Append(GetCurrentTime())
                       .Append(".txt");
                return builder.ToString();  
            }
        }

        public void GenerateLogFile()
        {
            //string _path = Path.GetDirectoryName(Path.GetDirectoryName(Directory.GetCurrentDirectory()));
            string _current = Path.GetDirectoryName(Path.GetDirectoryName(Directory.GetCurrentDirectory()));
            string _path = Directory.GetParent(_current).ToString();
            string path = Path.Combine("@", _path, "LogFiles\\", GetLogFileName());

            var builder = new StringBuilder();

            foreach (ProblemReport pr in _reports)
            {
                builder.Append(pr.Analytics.Report_PerProblem_Study2())
                    .Append("**********************************************************************")
                    .Append("\n").Append("\n");
            }

            File.WriteAllText(path, builder.ToString());
        }

        #endregion
    }
}
