﻿using System;
using System.IO;
using System.Text;
using MathCogUI;

namespace MathLogger
{
    public class ProblemReport
    {
        private string _userId;
        private InteractiveAnalytics _analytics;

        public InteractiveAnalytics Analytics
        {
            get { return _analytics; }
        }

        public ProblemReport(string userId, InteractiveAnalytics analytics)
        {
            _userId = userId;
            _analytics = analytics;
        }

        #region Log File of user interaction

        private string GetCurrentTime()
        {
            var timeBuilder = new StringBuilder();
            timeBuilder
                .Append(DateTime.Now.Year.ToString())
                .Append(DateTime.Now.Month.ToString())
                .Append(DateTime.Now.Day.ToString())
                .Append(DateTime.Now.TimeOfDay.Hours.ToString())
                .Append(DateTime.Now.TimeOfDay.Minutes.ToString())
                .Append(DateTime.Now.TimeOfDay.Seconds.ToString());
            return timeBuilder.ToString();
        }

        //Log_userId_problemid_time.txt
        private string GetLogFileName()
        {
            var builder = new StringBuilder();
            builder.Append("Log_")
                   .Append(_userId)
                   .Append("_")
                   .Append(_analytics.ProblemId)
                   .Append("_")
                   .Append(GetCurrentTime())
                   .Append(".txt");
            return builder.ToString();
        }

        public void GenerateLogFilePerProblem()
        {
            //string _path = Path.GetDirectoryName(Path.GetDirectoryName(Directory.GetCurrentDirectory()));
            string _current = Path.GetDirectoryName(Path.GetDirectoryName(Directory.GetCurrentDirectory()));
            string _path = Directory.GetParent(_current).ToString();
            string path = Path.Combine("@", _path, "LogFiles\\", GetLogFileName());
            File.WriteAllText(path, _analytics.Report_PerProblem_Study2());
        }

        #endregion
    }
}
