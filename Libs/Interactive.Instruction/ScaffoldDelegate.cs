﻿using System;

namespace MathTutor
{
    public delegate void ScaffoldDelegate(object sender, ScaffoldEventArgs args);

    public enum ScaffoldType
    {
        Algebra = 0,
        Geometry = 1,
        Both = 2
    }

    public class ScaffoldEventArgs : EventArgs
    {
        public string Concept { get; set; }

        public ScaffoldType ScaffoldType { get; set; }

        public ScaffoldEventArgs(string conceptName, ScaffoldType type)
        {
            Concept = conceptName;
            ScaffoldType = type;
        }
    }
}