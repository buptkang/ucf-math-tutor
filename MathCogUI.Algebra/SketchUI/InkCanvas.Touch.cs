﻿/*******************************************************************************
 * Math Interactive Tutoring System
 * <p>
 * Copyright (C) 2015 Bo Kang, Joseph J. LaViola Jr.
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

namespace MathCogUI
{
    using System.Windows.Input;
    using System.Windows.Media;

    public partial class AlgebraicInkCanvas
    {
        #region Manipulate the canvas
        void AGAlgebraicInkCanvas_ManipulationDelta(object sender, ManipulationDeltaEventArgs e)
        {
            var xform = this.RenderTransform as MatrixTransform;
            Matrix matrix = xform.Matrix;
            ManipulationDelta delta = e.DeltaManipulation;
            matrix.Translate(delta.Translation.X, delta.Translation.Y);
            this.RenderTransform = new MatrixTransform(matrix);
            var editor = AGEditor as AlgebraicEditor;
            if (editor != null)
                editor.Underlay.RenderTransform = new MatrixTransform(matrix);
        }
        #endregion     

        #region Touch Node Selection 

        void AGAlgebraicInkCanvas_TouchDown(object sender, TouchEventArgs e)
        {
            //TODO solve constraint issue
           /* TouchPoint tp = e.GetTouchPoint(this);

            var editor = AGEditor as AlgebraicEditor;
            if (editor != null)
            {
                editor.SelectKnowledge(tp.Position);
            }*/
        }

        #endregion

        #region Manipulate the expression 

     /*   //Attach Label to IFigure
        private DrawingVisual hitTestVisual;

        void AGGeometryInkCanvas_ManipulationCompleted(object sender,
                                        ManipulationCompletedEventArgs e)
        {
            var editor = AGEditor as AGGeometryEditor;
            Debug.Assert(editor != null);
            if (hitTestVisual == null) return;
            GeometrySketchVisual geometryVisualRepr
                    = editor.SketchVisuals.ToList()
                    .Find(x => x.DC.Equals(hitTestVisual));
            if (geometryVisualRepr == null || geometryVisualRepr.Expr == null
                || geometryVisualRepr.DC.Drawing == null)
            {
                hitTestVisual = null;
                e.Handled = true;
                return;
            }
            var r = geometryVisualRepr.DC.Drawing.Bounds;
            var newC = new Point(r.TopLeft.X + r.Width / 2, r.TopLeft.Y + r.Height / 2);
            newC = DC.Drawing.CoordinateSystem.ToLogical(newC);
            var iFigure = DC.Drawing.Figures.HitTest(newC);
            object agLabel;
            bool isLabel = geometryVisualRepr.Expr.IsLabel(out agLabel);

            if (iFigure == null || !isLabel)
            {
                hitTestVisual = null;
                e.Handled = true;
                return;
            }

            //Label Assignment
            Underlay.Children.Remove(hitTestVisual); //delete drawingVisual
            editor.SketchVisuals.Remove(geometryVisualRepr); //remove from RenderVisual
            iFigure.UpdateLabel(agLabel, newC);
            hitTestVisual = null;
            e.Handled = true;
        }



        void AGGeometryInkCanvas_ManipulationDelta(object sender, ManipulationDeltaEventArgs e)
        {
            var editor = AGEditor as AGGeometryEditor;
            Debug.Assert(editor != null);
            if (hitTestVisual == null) return;
            int index = Underlay.Children.IndexOf(hitTestVisual);
            if (index == -1) return;
            Underlay.Children.RemoveAt(index);
            ManipulationDelta delta = e.DeltaManipulation;

            var algebraVisualRepr = editor.SketchVisuals.ToList()
                                    .Find(x => x.DC.Equals(hitTestVisual));
            if (algebraVisualRepr == null || algebraVisualRepr.Expr == null) return;

            Rct bb;
            var pt = new Point(e.ManipulationOrigin.X + delta.Translation.X,
                               e.ManipulationOrigin.Y + delta.Translation.Y);

            DrawingVisual dv = MathUtils.RetrieveDrawingVisual(
                               new Pt(pt.X, pt.Y), algebraVisualRepr.Expr,
                               Colors.Blue, out bb);
            Underlay.Children.Insert(index, dv);
            hitTestVisual = dv;
            algebraVisualRepr.DC = hitTestVisual;
            e.Handled = true;
        }

        void AGGeometryInkCanvas_ManipulationStarted(object sender, ManipulationStartedEventArgs e)
        {
            hitTestVisual = null;
            HitTestResult result = VisualTreeHelper.HitTest(Underlay, e.ManipulationOrigin);
            if (result != null && result.VisualHit is DrawingVisual)
            {
                hitTestVisual = result.VisualHit as DrawingVisual;
                e.Handled = true;
            }
        }*/


        #endregion
    }
}
