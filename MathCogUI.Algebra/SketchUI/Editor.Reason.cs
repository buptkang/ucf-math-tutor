﻿/*******************************************************************************
 * Math Interactive Tutoring System
 * <p>
 * Copyright (C) 2015 Bo Kang, Joseph J. LaViola Jr.
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

using System.Linq;

namespace MathCogUI
{
    using MathCog.UserModeling;
    using MathCog;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using CSharpLogic;
    using starPadSDK.Geom;
    using starPadSDK.MathRecognizer;

    public partial class AlgebraicEditor
    {
        #region Sketch-based reasoning

        /// <summary>
        /// without calling back-end reasoning engine
        /// </summary>
        /// <param name="range"></param>
        private void ReAdd(Parser.Range range)
        {
            var result = SearchKnowledgeRegion(range);
            if (result == null) return;
            int index = KnowledgeRegions.IndexOf(result);
            KnowledgeRegions.Remove(result);
            //AddRange(range);
            KnowledgeRegions.Insert(index, result);
            //KnowledgeRegions.Add(result);
        }

        protected void AddRangeInternal(Parser.Range range, ShapeType _selectedType,
                            out IKnowledge k, out Rct bb, bool userInput,
                            bool isUpdate = false)
        {
            k = null;
            bb = GetRangeBoundingBox(range);
            if (range.Parse == null) return;

            object obj = _selectedType == ShapeType.None ?
                BackReasoner.HCILoad(range.Parse.expr, null, userInput) :
                BackReasoner.HCILoad(range.Parse.expr, _selectedType, userInput);
            k = obj as IKnowledge;
            var agQueryExpr = k as AGQueryExpr;
            if (agQueryExpr != null)
            {
                var query = agQueryExpr.QueryTag;
                Debug.Assert(query != null);
                if (!query.Success)
                {
                    var shapeTypes = query.FeedBack as List<ShapeType>;
                    if (shapeTypes != null)
                    {
                        //ShowSidebarAlts(range, shapeTypes);
                    }
                }
            }
            if (!isUpdate)
            {
                SendFeedback(k, userInput);
            }
        }

        protected virtual void AddRange(Parser.Range range, ShapeType st, bool userInput = false, bool isUpdate = false)
        {
            //InteractiveAnalytics.Instance.StartTimer();
            IKnowledge k;
            Rct bb;
            AddRangeInternal(range, st, out k, out bb, userInput, isUpdate);
            KnowledgeRegions.Add(new SketchKnowledgeRegion(range, k, bb, userInput));
        }

        private bool DeleteRange(Parser.Range range, out ShapeType st, bool isUpdate = false)
        {
            bool userInput = false;
            st = ShapeType.None;
            var deleteKR = SearchKnowledgeRegion(range);
            if (deleteKR != null)
            {
                userInput = deleteKR.UserInput;
            }

            if (deleteKR != null && deleteKR.Knowledge != null)
            {
                var queryExpr = deleteKR.Knowledge as AGQueryExpr;
                if (queryExpr != null)
                {
                    var currentST = queryExpr.QueryTag.Constraint2;
                    if (currentST != null)
                    {
                        st = currentST.Value;
                    }
                }
                BackReasoner.HCIUnLoad(deleteKR.Knowledge.Expr);
                BackReasoner.UnQuery(deleteKR.Knowledge, isUpdate);
            }
            if (!isUpdate)
            {
                InkCanvas.NotifyClientInfo(AGWarning.DeleteInputKnowledge, true);

                if (deleteKR != null && deleteKR.ExprVisual != null)
                {
                    BackQuerier.UpdateVerifyRegion(deleteKR.ExprVisual.BoundingBox);

                    foreach (var region in deleteKR.RenderedRegions)
                    {
                        BackQuerier.UpdateSelectRegion(region.ExprVisual.BoundingBox);
                    }                   
                }
            }            

            bool result = KnowledgeRegions.Remove(deleteKR);
            if (!result)
            {
                for (int i = 0; i < KnowledgeRegions.Count; i++)
                {
                    var kr = KnowledgeRegions[i].Knowledge;
                    if (deleteKR.Knowledge.Expr.Equals(kr.Expr))
                    {
                        KnowledgeRegions.Remove(KnowledgeRegions[i]);
                    }
                }
            }

            return userInput;
        }

        public void RemoveRange(SketchKnowledgeRegion region)
        {
            InkCanvas.NotifyClientInfo(AGWarning.DeleteInputKnowledge, true);
            InkCanvas.DeleteStroqs(region.Range);
            Parse();
        }

        private void UpdateRange(Parser.Range oldRange, Parser.Range newRange, ShapeType userInputST)
        {
            if (!oldRange.Equals(newRange))
            {
                var deleteKR = SearchKnowledgeRegion(oldRange);
                if (deleteKR != null && deleteKR.ExprVisual != null)
                {
                    BackQuerier.UpdateVerifyRegion(deleteKR.ExprVisual.BoundingBox);
                }
            }

            ShapeType st;
            bool userInput = DeleteRange(oldRange, out st, true);
            if (userInputST != ShapeType.None)
            {
                AddRange(newRange, userInputST, userInput, true);
            }
            else
            {
                AddRange(newRange, st, userInput, true);
            }
        }

        #endregion

        #region Drag-and-Drop based reasoning

        protected void RegisterShapeRegion(IKnowledge knowledge)
        {
            var shapeExpr = knowledge as AGShapeExpr;
            if (shapeExpr == null) return;
            shapeExpr.ShapeSymbol.ReifyShapeUpdated += ShapeSymbol_ReifyShapeUpdated;            
        }

        private void ShapeSymbol_ReifyShapeUpdated(object sender, EventArgs args)
        {
            var reifyShapeSymbol = sender as ShapeSymbol;
            if (reifyShapeSymbol == null) return;

            Region region = SearchIKnowledgeRegion(reifyShapeSymbol);

            var dragKnowledgeRegion = region as DragKnowledgeRegion;
            if (dragKnowledgeRegion != null)
            {
                KnowledgeRegions.Remove(region);
                KnowledgeRegions.Add(region);
            }
        }

        private Region SearchIKnowledgeRegion(ShapeSymbol ss)
        {
            foreach (var region in KnowledgeRegions)
            {
                var shapeExpr = region.Knowledge as AGShapeExpr;
                if (shapeExpr == null) continue;

                var tempSs = shapeExpr.ShapeSymbol;
                if (tempSs.Equals(ss)) return region;

            }
            return null;
        }

        public void ReAdd(DragKnowledgeRegion region)
        {
            int index = KnowledgeRegions.IndexOf(region);

            KnowledgeRegions.Remove(region);
            KnowledgeRegions.Insert(index, region);


          /*  RemoveRange(region, true);
            AddRange(region.InputText, region.Position, true);*/
        }

        public virtual InputRegion AddRange(InputText it, System.Windows.Point? pt, bool isUpdate = false)
        {
            //InteractiveAnalytics.Instance.StartTimer();
            it.IsDraggable = false;
            object obj = BackReasoner.HCILoad(it.DragContent);
            if (obj == null) throw new Exception("TODO");

            var k = obj as IKnowledge;
            if (k == null) return null;
            if (!isUpdate)
            {
                SendFeedback(k);                
            }

            //Buf Fix for real-time drag reify shape update 
            RegisterShapeRegion(k);

            if (pt == null)
            {
                pt = FindPlaceToRender();
            }
          
            var region = new DragKnowledgeRegion(k, pt.Value, it);
            KnowledgeRegions.Add(region);

            return region;
        }

        public Pt FindPlaceToRender()
        {
            if (KnowledgeRegions.Count == 0)
            {
                //if (!HCIReasoner.Instance.TutorMode || (HCIReasoner.Instance.TutorMode && APConfig.AllowMappingInTutor))
                if (!HCIReasoner.TutorMode)
                {
                    return new Pt(200, 200);
                }
                else
                {
                    return new Pt(3000, 3000);
                }
            }
            else
            {
                Pt largest = new Pt();
                foreach (Region region in KnowledgeRegions)
                {
                    if (region.ExprVisual.DrawPos == null) continue;
                    Pt curr = region.ExprVisual.DrawPos.Value;
                    if (curr.X > largest.X)
                    {
                        largest.X = curr.X;
                    }
                    if (curr.Y > largest.Y)
                    {
                        largest.Y = curr.Y;
                    }
                }
                return new Pt(largest.X, largest.Y + 200);
            }
        }

        public void RemoveRange(DragKnowledgeRegion region, bool isUpdate = false)
        {
            if (!isUpdate)
            {
                InkCanvas.NotifyClientInfo(AGWarning.DeleteInputKnowledge, true);                
            }
            BackReasoner.HCIUnLoad(region.InputText.DragContent);
            bool result = KnowledgeRegions.Remove(region);
            if (!result)
            {
                for (int i = 0; i < KnowledgeRegions.Count; i++)
                {
                    var kr = KnowledgeRegions[i].Knowledge;
                    
                    if (region.Knowledge.Expr.Equals(kr.Expr))
                    {
                        KnowledgeRegions.Remove(KnowledgeRegions[i]);
                    }
                }
            }
        }

        #endregion

        /// <summary>
        /// State Machine to Select and Deselect Knowledge
        /// Heurisic Search on Regions
        /// difference between tutor mode and demonstration mode.
        /// 
        /// obj format: System.Windows.Point pt
        ///             Rect boundingBox
        /// 
        /// </summary>
        public bool Select(object obj, out string message,
                                       out object hitRegion)
        {
            hitRegion = null;
            message = null;
            if (HCIReasoner.TutorMode) return false;

            var rct = obj as Rct?;
            Debug.Assert(rct != null);

            foreach (var region in KnowledgeRegions)
            {
                bool result = region.HitTest(obj);
                if (!result) continue;
                var inputRegion = region as InputRegion;
                hitRegion = inputRegion;
                if (inputRegion == null) return true;
                if (inputRegion.RenderedRegions != null)
                {
                    for (int i = 0; i < inputRegion.RenderedRegions.Count; i++)
                    {
                        bool isSelect = inputRegion.RenderedRegions[i].IsSelected;
                        if (inputRegion.Knowledge != null
                            && inputRegion.Knowledge.RenderKnowledge != null)
                        {
                            inputRegion.Knowledge.RenderKnowledge[i].IsSelected = isSelect;
                        }
                    }
                }
                BackQuerier.Select(rct.Value, inputRegion, out message);
                return true;
            }
            return false;
        }

        public QueryFeedbackState QueryVerify(object pos, bool gestureTrigger,
            out string message,
            out object feedbackRegion,
            out object obj)
        {
            feedbackRegion = null;
            obj = null;
            if (!HCIReasoner.TutorMode) return Query(pos, out message, out feedbackRegion, out obj);
            Debug.Assert(HCIReasoner.TutorMode);
            if (!gestureTrigger) return Query(pos, out message, out feedbackRegion, out obj);
            Debug.Assert(gestureTrigger);
            Debug.Assert(KnowledgeRegions.Count != 0);
            return BackQuerier.VerifiedAll(KnowledgeRegions.ToList(), out message, out obj);
        }

        /// <summary>
        /// Question mark is the user handler to inquire internal 
        /// procedural knowledge.
        /// </summary>
        /// <param name="bb"></param>
        /// <param name="obj">Rct bb</param>
        /// <param name="message"></param>
        /// <returns>Query success or not</returns>
        public QueryFeedbackState Query(object pos,
                                out string message,
                                out object feedbackRegion,
                                out object obj)
        {
            feedbackRegion = null;
            var pt = pos as System.Windows.Point?;
            if (pt == null) throw new Exception("TODO");
            var renderPt = pt.Value;
            Debug.Assert(renderPt != null);

            IKnowledge knowledgeExpr;

            var state = BackQuerier.Query(out message, out obj);

            if (state == QueryFeedbackState.DemonQueryProcessed)
            {
                var tuple = obj as Tuple<object, object, object, object>;
                Debug.Assert(tuple != null);
                knowledgeExpr = tuple.Item2 as IKnowledge;
                obj = new Tuple<object, object, object>(tuple.Item1, tuple.Item3, tuple.Item4);
                feedbackRegion = new GeneratedRegion(knowledgeExpr, renderPt);
            }
            else if (state == QueryFeedbackState.TutorQueryProcessedAnswer)
            {
                var tuple = obj as Tuple<object, object, object, object>;
                Debug.Assert(tuple != null);
                knowledgeExpr = tuple.Item2 as IKnowledge;
                obj = new Tuple<object, object, object>(tuple.Item1, tuple.Item3, tuple.Item4);
                feedbackRegion = new GeneratedRegion(knowledgeExpr, renderPt);
            }

            if (state != QueryFeedbackState.QueryFailed)
            {
                BackQuerier.QuerySession = true;
            }

            return state;
        }

        #region Query Feedback

        protected virtual void SendFeedback(IKnowledge k, bool userInput = false)
        {
            if (HCIReasoner.TutorMode)
            {
                InkCanvas.NotifyClientInfo(AGTutorMessage.WriteAStep, true);
            }
            else
            {
                var agQueryExpr = k as AGQueryExpr;
                if (agQueryExpr != null)
                {
                    var query = agQueryExpr.QueryTag;
                    Debug.Assert(query != null);
                    if (!query.Success)
                    {
                        InkCanvas.NotifyClientInfo(AGWarning.QueryFailed, true);
                    }
                    else
                    {
                        InkCanvas.NotifyClientInfo(AGDemonstrationMessage.InputQuery, true);
                    }
                    return;
                }

                if (userInput)
                {
                    InkCanvas.NotifyClientInfo(AGTutorMessage.WriteAStep, true);
                }
                else
                {
                    InkCanvas.NotifyClientInfo(AGDemonstrationMessage.InputKnowledge, true);
                }
            }
        }

        #endregion
    
    
    
    }
}
