﻿/*******************************************************************************
 * Math Interactive Tutoring System
 * <p>
 * Copyright (C) 2015 Bo Kang, Joseph J. LaViola Jr.
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

namespace MathCogUI
{
    using System.ComponentModel;
    using Algebra.LogicSketch.Annotations;
    using GeometryShapeRecognizer;
    using System.Collections.Generic;
    using System.Linq;
    using CSharpLogic;
    using starPadSDK.CharRecognizer;
    using starPadSDK.Geom;
    using starPadSDK.Inq;
    using starPadSDK.MathRecognizer;
    using starPadSDK.MathUI;

    public class AGMathEditor : INotifyPropertyChanged
    {
        #region Properties

        public AlternatesMenuCreator _altsMenuCrea;

        protected MathRecognition _mrec;
        public MathRecognition MathRecognizer
        {
            get { return _mrec; }
        }

        protected Rct GetRangeBoundingBox(Parser.Range rrr)
        {
            Rct rangebbox = MathRecognizer.Sim[rrr.Strokes].Aggregate(Rct.Null,
                   (Rct r, Stroq s) => r.Union(s.GetBounds()));
            Rct box = rangebbox.Inflate(8, 8);
            return box;
        }

        #endregion

        #region Alternative Menu

        public void hideSidebarAlts()
        {
            _altsMenuCrea.Clear();            
        }

        public void showSidebarAlts(ICollection<Recognition> recogs, StroqCollection stroqs)
        {
            _altsMenuCrea.Populate(recogs, stroqs);
        }

        #endregion

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}