﻿/*******************************************************************************
 * Math Interactive Tutoring System
 * <p>
 * Copyright (C) 2015 Bo Kang, Joseph J. LaViola Jr.
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

namespace MathCogUI
{
    using DomainSpecificRecoginzer;
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Input;
    using Microsoft.Ink;
    using starPadSDK.CharRecognizer;
    using starPadSDK.Geom;
    using starPadSDK.Inq;
    using starPadSDK.Inq.MSInkCompat;
    using starPadSDK.MathRecognizer;
    using starPadSDK.UnicodeNs;
    using starPadSDK.Utils;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Windows.Media;

    public class GestureInkCanvas : InqCanvas
    {
        #region Properties and Constructors

        public AGMathEditor AGEditor { get; set; }

        //Ink Selection Timer
        protected Queue<Stroq> _slidingWindow;
        protected Timer myTimer;
        public static int TriggerGestureAnalyzerInterval = 3000;
        public static int TriggerGestureAnalyzerDueTime = 1000;

        public StroqCollection _cachedStroqs; // Keep a clone of stroqs to send to gesture recognizer before starPad.

        public GestureInkCanvas()
        {
            _cachedStroqs = new StroqCollection();
            _slidingWindow = new Queue<Stroq>();
            PreviewMouseDown += GestureInkCanvas_PreviewMouseDown;
            StylusSystemGesture += GestureInkCanvas_StylusSystemGesture;
            ActivateShapeRecognizer = false;
        }

        public virtual void Reset() { }

        public bool ActivateShapeRecognizer { get; set; }

        #endregion

        #region IO Queue Processing (Distinguish between Gesture and Ink)

        public void AnalyzeSlidingWindow(Object info)
        {
            try
            {
                int count = _slidingWindow.Count;
                if (count == 0) return;

                Stroq stroq1, stroq2, stroq3;
                switch (count)
                {
                    case 1:
                        stroq1 = _slidingWindow.Dequeue();
                        if (!OneStrokeAnalysis(stroq1))
                        {
                            _slidingWindow.Enqueue(stroq1);
                            ProcessStrokes();
                        }
                        break;
                    case 2:
                        stroq1 = _slidingWindow.Dequeue();
                        stroq2 = _slidingWindow.Dequeue();
                        if (!TwoStrokeAnalysis(stroq1, stroq2))
                        {
                            _slidingWindow.Enqueue(stroq1);
                            _slidingWindow.Enqueue(stroq2);
                            ProcessStrokes();
                        }
                        break;
                    case 3:
                        stroq1 = _slidingWindow.Dequeue();
                        stroq2 = _slidingWindow.Dequeue();
                        stroq3 = _slidingWindow.Dequeue();
                        _slidingWindow.Enqueue(stroq1);
                        _slidingWindow.Enqueue(stroq2);
                        _slidingWindow.Enqueue(stroq3);
                        if (!ThreeStrokesAnalysis(stroq1, stroq2, stroq3))
                        {
                            ProcessStrokes();
                        }
                        break;
                    default:
                        MoreStrokesAnalysis();
                        break;
                }

                Dispatcher.Invoke((Action)(UpdateParse));
            }
            catch (Exception)
            {
                return;
            }

        }

        protected virtual void UpdateParse()
        {
            var algebraEditor = AGEditor as AlgebraicEditor;
            if (algebraEditor != null)
            {
                algebraEditor.Parse();
                //Console.WriteLine("Profile");
            }

/*            Dispatcher.Invoke((Action)(() =>
            {
                var algebraEditor = AGEditor as AlgebraicEditor;
                if (algebraEditor != null)
                {
                    algebraEditor.Parse();
                }
            }));*/
        }

        bool OneStrokeAnalysis(Stroq stroq)
        {
            Rct bb;
            if (ActOnCircleInk(stroq, out bb))
            {
                Dispatcher.Invoke((() =>
                {
                    Stroqs.Remove(stroq);
                    Point pOnCircle;
                    Point center = FindCenterAndRadius(bb, out pOnCircle);
                    InputShapeCircle(center, pOnCircle);
                }));
                return true;
            }
            Point pt1, pt2;
            if (ActOnLineInk(stroq, out pt1, out pt2))
            {
                Dispatcher.Invoke((() =>
                {
                    var lst = stroq.StylusPoints.Select(stylusPt => stylusPt.ToPoint()).ToList();
                    InputShapeLine(lst);
                    Stroqs.Remove(stroq);
                }));
                return true;
            }

            if (ActOnLassoInk(stroq, out bb))
            {
               //TODO
            }

/*            if (ActOnArcInk(stroq, out pt1, out pt2))
            {
                Dispatcher.Invoke((() =>
                {
                    InputShapeArc(pt1, pt2);
                    Stroqs.Remove(stroq);
                }));
                return true;
            }*/
            return false;
        }

        private bool TwoStrokeAnalysis(Stroq stroq1, Stroq stroq2)
        {
            //if (ActOnDownArrowGesture(stroq1, stroq2)) return; //Knowledge Reasoning Func
            //if (ActOnLassoCheckMark(stroq1, stroq2)) return; //Knowledge Reasoning Func
            //if (ActOnDoubleTab(stroq1, stroq2)) return;
            Rct bb;
            if (ActOnQuestionMark(stroq1, stroq2, out bb))
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    this.Stroqs.Remove(stroq1);
                    this.Stroqs.Remove(stroq2);
                    Query(bb, true);
                }));
                return true;
            }

           /* if (ActOnArrowGesture(stroq1, stroq2, out bb))
            {
                Dispatcher.Invoke((Action)(() =>
                {
                    this.Stroqs.Remove(stroq1);
                    this.Stroqs.Remove(stroq2);
                    Query(bb, true);
                }));
                return true;
            }*/
            //if (ActOnEqualSign(stroq1, stroq2)) return;        
            return false;
        }

        private bool ThreeStrokesAnalysis(Stroq stroq1, Stroq stroq2, Stroq stroq3)
        {
            //Rct bb;
            /* if (ActOnLassoQuestionMark(stroq1, stroq2, stroq3, out bb))
             {
                 this.Dispatcher.Invoke((Action)(() =>
                 {
                     this.Stroqs.Remove(stroq1);
                     this.Stroqs.Remove(stroq2);
                     this.Stroqs.Remove(stroq3);
                 }));
                 Query(bb);
                 return true;
             }*/
            return false;
        }

        public virtual void MoreStrokesAnalysis()
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                Stroq stroq1 = null;
                Stroq stroq2 = null;

                int counter = 0;

                while (_slidingWindow.Count != 0)
                {
                    if (_slidingWindow.Count == 2)
                    {
                        stroq1 = _slidingWindow.Dequeue();
                    }
                    else if (_slidingWindow.Count == 1)
                    {
                        stroq2 = _slidingWindow.Dequeue();
                    }
                    else
                    {
                        Stroq temp = _slidingWindow.Dequeue();
                        AddStrokeToBackEnd(temp);
                    }
                    counter++;
                }

                AddStrokeToBackEnd(stroq1);
                AddStrokeToBackEnd(stroq2);

               /* Rct bb;
                bool result = ActOnQuestionMark(stroq1, stroq2, out bb);
                if (!result)
                {
                    AddStrokeToBackEnd(stroq1);
                    AddStrokeToBackEnd(stroq2);
                }
                else
                {
                    Query(bb, true);
                    this.Stroqs.Remove(stroq1);
                    this.Stroqs.Remove(stroq2);
                }*/
            }));
        }

        public virtual void ProcessStrokes()
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                var lst = new StroqCollection();
                while (_slidingWindow.Count != 0)
                {
                    lst.Add(_slidingWindow.Dequeue());
                }
                AddStrokeToBackEnd(lst);
            }));
        }

        #region Stroke: IO

        //Locker mechanism to add or delete strokes for the _algebraicStroqs

        private static readonly object _SyncLock = new object();

        public bool isDeleteSignal = false;

        public void AddStrokeToBackEnd(Stroq stroq)
        {
            if (stroq != null)
            {
                isDeleteSignal = false;
                _cachedStroqs.Add(stroq);
            }
        }

        public void AddStrokeToBackEnd(StroqCollection stroqs)
        {
            lock (_SyncLock)
            {
                isDeleteSignal = false;
                _cachedStroqs.Add(stroqs);
            }
        }

        public void RemoveStrokeFromBackEnd(Stroq stroq)
        {
            lock (_SyncLock)
            {
                isDeleteSignal = true;
                _cachedStroqs.Remove(stroq);
            }
        }

        public void RemoveStrokeFromBackEnd(StroqCollection stroqs)
        {
            lock (_SyncLock)
            {
                isDeleteSignal = true;
                _cachedStroqs.Remove(stroqs);
            }
        }

        #endregion

        #endregion

        #region Gesture: Pattern Match

        #region Gesture: Scribble

        protected bool DetectScribble(Stroq stroq)
        {
            bool canBeScribble = stroq.OldPolylineCusps().Length > 4;
            if (stroq.OldPolylineCusps().Length == 4)
            {
                int[] pcusps = stroq.OldPolylineCusps();
                Deg a1 = fpdangle(stroq[0], stroq[pcusps[1]], stroq[pcusps[2]] - stroq[pcusps[1]]);
                Deg a2 = fpdangle(stroq[pcusps[1]], stroq[pcusps[1]], stroq[pcusps[3]] - stroq[pcusps[1]]);
                if (a1 < 35 && a2 < 35)
                    canBeScribble = stroq.BackingStroke.HitTest(stroq.ConvexHull().First(), 1);
            }
            return canBeScribble;
        }

        public void AddRange(Parser.Range range)
        {
            var strokes = AGEditor.MathRecognizer.Sim[range.Strokes].ToList();
            Stroqs.Add(strokes);

            foreach (var stroq in strokes)
            {
                AddStrokeToBackEnd(stroq);
            }
        }

        public void DeleteStroqs(Parser.Range range)
        {
            var deletedStroqs = new StroqCollection();
            var strokes = AGEditor.MathRecognizer.Sim[range.Strokes];
            deletedStroqs.Add(strokes);
            Stroqs.Remove(deletedStroqs);
            RemoveStrokeFromBackEnd(deletedStroqs);
        }

        protected bool DeleteStroqs(Stroq stroq, out StroqCollection erasedStroqs)
        {
            erasedStroqs = null;
            IEnumerable<Pt> hull = stroq.ConvexHull();
            StroqCollection stqs = this.Stroqs.HitTest(hull, 1);
            if (stqs.Count > 1)
            {
                Stroqs.Remove(stqs);
                erasedStroqs = stqs;
                Stroqs.Remove(stroq);
                return true;
            }
            return false;
        }

        #endregion

        #region Gesture: Question Mark -> Reasoning Tool

        private bool ActOnQuestionMark(Stroq stroq1, Stroq stroq2, out Rct bb)
        {
            bb = new Rct();
            if (DomainGestures.IsQuestionMark(stroq1, stroq2))
            {
                if (stroq1 != null && stroq2 != null)
                {
                    bb = stroq1.GetBounds().Union(stroq2.GetBounds());
                }
                return true;
            }
            return false;
        }

        public virtual void Query(object obj, bool gesture = false)
        {
        }

        #endregion

        #region TODO

        private bool ActOnArrowGesture(Stroq stroq1, out Rct bb)
        {
            bb = new Rct();
            return false;
            /* bb = new Rct();
            //TODO any arrow type
            if (DomainGestures.IsDownArrow(stroq1))
            {
                return true;
            }
            return false;*/
        }

        private bool ActOnArrowGesture(Stroq stroq1, Stroq stroq2, out Rct bb)
        {
            bb = new Rct();
            return false;
            /*bb = new Rct();
            //TODO any arrow type
            if (DomainGestures.IsDownArrow(stroq1, stroq2))
            {
                return true;
            }
            return false;*/
        }

        private bool ActOnPointInk(Stroq stroq, out Point pt)
        {
            pt = stroq.StylusPoints[0].ToPoint();
            if (DomainGestures.IsShape(stroq, "point"))
            {
                Debug.Assert(stroq != null);
                return true;
            }
            return false;
        }

        #endregion

        #region Gesture: Tap and DoubleTap -> Point

        protected bool IsTapGestureStroke = false;
        void GestureInkCanvas_StylusSystemGesture(object sender, StylusSystemGestureEventArgs e)
        {
            IsTapGestureStroke = false;
            if (e.StylusDevice.TabletDevice.Type == TabletDeviceType.Stylus)
            {
                switch (e.SystemGesture)
                {
                    case System.Windows.Input.SystemGesture.Tap:

                        InputShapePoint(e.GetPosition(this));
                        IsTapGestureStroke = true;
                        break;
                }
            }
        }

        /*
        private bool _isFirstTab = false;
        private bool _isSecondTab = false;
        private Stopwatch stopWatch = new Stopwatch();
        private TimeSpan TwoTabsElapseTime;
        private Point _firstTabPt;
        private Point _secondTabPt;
 * 
 * 
*/
        /*
                    if (!_isFirstTab)
                    {
                        _isFirstTab = true;
                        stopWatch.Start();
                        _firstTabPt = e.GetPosition(this);
                    }
                    else
                    {
                        _isSecondTab = true;
                        stopWatch.Stop();
                        TwoTabsElapseTime = stopWatch.Elapsed;
                        _secondTabPt = e.GetPosition(this);
                    }

        private double DistP1P2(Point p1, Point p2)
        {
            double dx = p2.X - p1.X;
            double dy = p2.Y - p1.Y;
            return Math.Sqrt(dx * dx + dy * dy);
        }

         
         */

        /*   private bool ActOnDoubleTab(Stroq stroq1, Stroq stroq2)
           {
               if (_isFirstTab && _isSecondTab && TwoTabsElapseTime.Milliseconds < 1000
                   && DistP1P2(_firstTabPt, _secondTabPt) < 350.0)
               {
                   this.Dispatcher.Invoke((Action)(() =>
                   {
                       this.Stroqs.Remove(stroq1);
                       this.Stroqs.Remove(stroq2);

                   }));

                   _isFirstTab = false;
                   _isSecondTab = false;
                   return true;
               }
               else
               {
                   return false;
               }
           }

           public bool DetectDoubleTapOnDrawingVisual(System.Windows.Point pt, out DrawingVisual outputDV)
           {
               foreach (Visual visual in AlgebraicEditor.Underlay.Children)
               {
                   var dv = visual as DrawingVisual;

                   Rect rect = dv.Drawing.Bounds;
                   var tempRct = new Rct(rect.TopLeft, rect.BottomRight);

                   if (tempRct.Contains(pt))
                   {
                       outputDV = dv;
                       return true;
                   }
               }

               outputDV = null;
               return false;
           }*/

        #endregion

        #region Gesture: Lasso(No) Template Matching  -> Circle

        private bool ActOnCircleInk(Stroq stroq, out Rct bb)
        {
            bb = new Rct();
            if (ActivateShapeRecognizer)
            {
                bool isCircle = DomainGestures.IsShape(stroq, "circle");
                if (isCircle)
                {
                    bb = stroq.GetBounds();
                    return true;
                }
            }
            return false;
        }

        private bool ActOnLassoInk(Stroq stroq, out Rct bb)
        {
            /* check for lassos/circles around stuff */
            bb = new Rct();
            if (IsLassoSelect(stroq))
            {
                bb = stroq.GetBounds();
                return true;
            }
            return false;
        }

        /// <summary>
        /// This lasso select uses for the cognitive circle gesture checking.
        /// </summary>
        /// <param name="stroq"></param>
        /// <returns></returns>
        public bool IsLassoSelect(Stroq stroq)
        {
            if (stroq.OldPolylineCusps().Length <= 4 && stroq.Count > 4)
            {
                Stroq estroq = stroq;
                starPadSDK.Inq.BobsCusps.FeaturePointDetector.CuspSet cs = starPadSDK.Inq.BobsCusps.FeaturePointDetector.FeaturePoints(estroq);

                Pt[] first = new Pt[cs.pts.Count / 2];
                for (int i = 0; i < first.Length; i++)
                    if (cs.distances[i] > cs.dist / 2)
                        break;
                    else first[i] = cs.pts[i];
                Pt[] second = new Pt[cs.pts.Count - first.Length];
                for (int j = 0; j < second.Length; j++) second[j] = cs.pts[first.Length + j];
                Stroq s1 = new Stroq(first);
                Stroq s2 = new Stroq(second);
                float d1, d2;
                s1.OldNearestPoint(s2[-1], out d1);
                s2.OldNearestPoint(s1[0], out d2);
                if (Math.Min(d1, d2) / Math.Max(estroq.GetBounds().Width, estroq.GetBounds().Height) < 0.3f)
                {
                    bool value = false;
                    Dispatcher.Invoke((Action)(() =>
                    {
                        value = Select(stroq);
                    }));

                    if (value) return true;
                }
            }
            return false;
        }

        public virtual bool Select(object obj)
        {
            return false;
        }

        #endregion

        #region Gesture: Template Matching -> Line

        private bool ActOnLineInk(Stroq stroq, out Point pt1, out Point pt2)
        {
            pt1 = stroq.StylusPoints[0].ToPoint();
            int count = stroq.StylusPoints.Count();
            pt2 = stroq.StylusPoints[count - 1].ToPoint();

            if (ActivateShapeRecognizer)
            {
                if (DomainGestures.IsShape(stroq, "line"))
                {
                    return true;
                }                
            }
            return false;
        }

        #endregion

        #region Gesture: Template Matching -> Arc

        private bool ActOnArcInk(Stroq stroq, out Point pt1, out Point pt2)
        {
            pt1 = stroq.StylusPoints[0].ToPoint();
            int count = stroq.StylusPoints.Count();
            pt2 = stroq.StylusPoints[count - 1].ToPoint();

            if (ActivateShapeRecognizer)
            {
                if (DomainGestures.IsShape(stroq, "arc"))
                {
                    return true;
                }                
            }
            return false;
        }

        #endregion

        #region Geometrical Shape Recognition

        //Single Stroke Inference: Point, Line, LineSegment and Circle

        protected virtual void InputShapePoint(Point pt)
        {
        }

        protected virtual void InputShapeLine(List<Point> linePt)
        {
        }

        protected virtual void InputShapeCircle(Point center, Point pt)
        {

        }

        protected virtual void InputShapeArc(Point pt1, Point pt2)
        {
            
        }

        #endregion

        #endregion

        #region Utils and Events

        //ad hoc circle inference 
        private Point FindCenterAndRadius(Rct bb, out Point onCirclePt)
        {
            double length = Math.Abs(bb.Right - bb.Left);
            double height = Math.Abs(bb.Bottom - bb.Top);

            var center = new Point((bb.Right + bb.Left)/2.0, (bb.Bottom + bb.Top)/2.0);
            if (length > height)
            {
                onCirclePt = new Point(bb.Left, (bb.Bottom + bb.Top) / 2.0);
            }
            else
            {
                onCirclePt = new Point((bb.Left + bb.Bottom)/2.0, bb.Top);
            }
            return center;
        }

        public Deg fpdangle(Pt a, Pt b, Vec v)
        {
            return (a - b).Normalized().UnsignedAngle(v.Normalized());
        }

        public Rct bbox(Strokes stks)
        {
            return AGEditor.MathRecognizer.Sim[stks].Aggregate(Rct.Null, (Rct r, Stroq s) => r.Union(s.GetBounds()));
        }

        void GestureInkCanvas_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            //NotifyClientInfo(null, false);
        }

        //System Log onto the user interface
        public event SystemDelegate ShowSystemHint;

        public void NotifyClientInfo(string message, bool showMessage)
        {
            if (ShowSystemHint != null)
            {
                ShowSystemHint(this, new SystemEventArgs(showMessage, message));
            }
        }

        public event ShowConceptHintDelegate HintShowed;

        public void NotifyClientConcept(object ct)
        {
            if (HintShowed != null)
            {
                HintShowed(this, new ShowConceptHintArgs(ct));
            }
        }

        public event ShowStrategyHintDelegate StrategyShowed;

        public void NotifyClientStrategy(List<string> strategies, int strategyIndex)
        {
            if (StrategyShowed != null)
            {
                StrategyShowed(this, new ShowStrategyHintArgs(strategies, strategyIndex));
            }
        }

        #endregion

        #region (Obsolete) Ink Lasso or Selection

        public Selection Selected = new Selection();
        public void Deselect()
        {
            Selected.Contents = null;
            if (AGEditor != null)
                AGEditor.hideSidebarAlts();
        }

        public bool LassoSelectInk(Stroq stroq)
        {
            if (stroq.OldPolylineCusps().Length <= 4 && stroq.Count > 4)
            {
                Stroq estroq = stroq;
                starPadSDK.Inq.BobsCusps.FeaturePointDetector.CuspSet cs = starPadSDK.Inq.BobsCusps.FeaturePointDetector.FeaturePoints(estroq);

                Pt[] first = new Pt[cs.pts.Count / 2];
                for (int i = 0; i < first.Length; i++)
                    if (cs.distances[i] > cs.dist / 2)
                        break;
                    else first[i] = cs.pts[i];
                Pt[] second = new Pt[cs.pts.Count - first.Length];
                for (int j = 0; j < second.Length; j++) second[j] = cs.pts[first.Length + j];
                Stroq s1 = new Stroq(first);
                Stroq s2 = new Stroq(second);
                float d1, d2;
                s1.OldNearestPoint(s2[-1], out d1);
                s2.OldNearestPoint(s1[0], out d2);
                if (Math.Min(d1, d2) / Math.Max(estroq.GetBounds().Width, estroq.GetBounds().Height) < 0.3f)
                {
                    StroqCollection stqs = Stroqs.HitTest(estroq, 50);
                    StroqCollection stqs2 = Stroqs.HitTest(estroq.Reverse1(), 50);
                    if (stqs2.Count > stqs.Count)
                        stqs = stqs2;
                    stqs.Remove(estroq);
                    StroqCollection stqs3 = new StroqCollection(stqs.Where((Stroq s) => AGEditor.MathRecognizer.Charreco.Classification(AGEditor.MathRecognizer.Sim[s]) != null));
                    stqs = stqs3;
                    Recognition rtemp = AGEditor.MathRecognizer.ClassifyOneTemp(estroq);
                    if (stqs.Count > 0 && (rtemp == null || !rtemp.alts.Contains(new Recognition.Result(Unicode.S.SQUARE_ROOT))))
                    {
                        if (rtemp != null) Console.WriteLine("select recognized for " + rtemp.allograph);

                        this.Dispatcher.Invoke((Action)(() =>
                        {
                            Deselect();
                            stroq.BackingStroke.DrawingAttributes.Color = Colors.Purple;
                            Selected.Contents = new StroqSel(stqs, stroq, (Stroq s) => AGEditor.MathRecognizer.Charreco.Classification(AGEditor.MathRecognizer.Sim[s]),
                                (Recognition r) => AGEditor.MathRecognizer.Sim[r.strokes], this.Stroqs);
                            StroqSel Sel = (StroqSel)Selected.Contents;
                            HashSet<Recognition> recogs = new HashSet<Recognition>(Sel.AllStroqs.Select((Stroq s) => AGEditor.MathRecognizer.Charreco.Classification(AGEditor.MathRecognizer.Sim[s]))
                                .Where((Recognition r) => r != null));
                            if (recogs.Count != 0) AGEditor.showSidebarAlts(recogs, Sel.AllStroqs);
                        }));

                        return true;
                    }
                    else
                    {
                        // Generic additional selections would be called here.
                        return false;
                    }
                }
            }
            return false;
        }

        #endregion

        #region (Obsolete) Space Orientation Detection

        /*                    SketchKnowledgeRegion kr;
            bool intersected =
                IntersectWithKnowledgeRegion(bb, out kr);

            if (!intersected) return false;

            if (kr.Range == null || kr.Range.Parse == null) return false;
            string variable;
            Expr expr = kr.Range.Parse.expr;
            bool result = false;
            //bool result = expr.IsQueryFormWithoutQuestionMark(out variable);
            if (!result) return false;
            //Interpreter.Instance.Query(variable);
            return false;*/

        /*        //Left Intersection 
                //infer left side knowledge region
                var topLeft = new Point(bb.TopLeft.X - 2 * bb.Width, bb.TopLeft.Y);
                var bottomRight = new Point(bb.BottomRight.X, bb.BottomRight.Y);
                var leftRect = new Rect(topLeft, bottomRight);

                bool result = editor.Query(leftRect);

                //Up Intersection 

                //Down Intersection

                //Itself Intersection

                //Right Intersection           */



        /*                why lasso question-mark
                this.Dispatcher.Invoke((Action)(() =>
                {
                    this.Stroqs.Remove(stroq1);
                    this.Stroqs.Remove(stroq2);
                    this.Stroqs.Remove(stroq3);

                    DrawingVisual outputDV;
                    if (DetectInnerExpr(stroq1.GetBounds(), out outputDV))
                    {
                        int index = AlgebraicEditor.RenderVisuals.ToList().FindIndex(x => x.DC.Equals(outputDV));
                        AGAlgebraicVisualRepr repr = AlgebraicEditor.RenderVisuals[index];

                        DoWhy(repr);
                    }
                    else
                    {
                        ShowKnowledgeHintEvent(this,
                                new ShowKnowledgeHintEventArgs(AGWarning.NoContentOnWhy, true, ShowKnowledgeHintEventArgs.HintType.Warning));
                    }
                }));*/




        /* public bool DetecUpperPositionExpr(Rct bb, out DrawingVisual outputDV, out Rect strategyBB)
         {
             strategyBB = Rect.Empty;

             var upperRct = new Rct(bb.Left, bb.Top - bb.Height, bb.Right, bb.Bottom);

             foreach (Visual visual in AlgebraicEditor.Underlay.Children)
             {
                 var dv = visual as DrawingVisual;

                 Rect rect = dv.Drawing.Bounds;
                 var tempRct = new Rct(rect.TopLeft, rect.BottomRight);

                 if (upperRct.IntersectsWith(tempRct))
                 {
                     outputDV = dv;
                     return true;
                 }
             }

             foreach (FeedbackControl strategy in FeedbackControls)
             {
                 if (strategy.StrategyBoundingBox.IntersectsWith(upperRct))
                 {
                     outputDV = strategy.RootDrawingVisual;
                     strategyBB = strategy.StrategyBoundingBox;
                     return true;
                 }
             }

             outputDV = null;
             return false;
         }

         /// <summary>
         /// Used By ActOnQuestionMark
         /// </summary>
         /// <param name="rect"></param>
         /// <param name="outputDV"></param>
         /// <returns></returns>
         public bool DetectExprThroughQuestionMark(Stroq stroq1, Stroq stroq2, out DrawingVisual outputDV)
         {
             Rct rect = stroq1.GetBounds().Union(stroq2.GetBounds());

             var leftRect = new Rect(new System.Windows.Point(rect.TopLeft.X - 2 * rect.Width, rect.TopLeft.Y),
                                     new System.Windows.Point(rect.BottomRight.X, rect.BottomRight.Y));
             foreach (Visual visual in AlgebraicEditor.Underlay.Children)
             {
                 var dv = visual as DrawingVisual;

                 Rect localRct = dv.Drawing.Bounds;
                 if (localRct.IntersectsWith(leftRect))
                 {
                     outputDV = dv;
                     return true;
                 }
             }
             outputDV = null;
             return false;
         }

         private bool ActOnLassoQuestionMark(Stroq stroq1, Stroq stroq2, Stroq stroq3,
                                             out Rct bb)
         {
             bb = new Rct();
             if (IsLassoSelect(stroq1) 
                 && DomainGestures.IsQuestionMark(stroq2, stroq3))
             {
                 bb = stroq1.GetBounds();
                 return true;
             }
             return false;
         }

         /// <summary>
         /// Use By ActOnLassoQuestionMark
         /// </summary>
         /// <param name="bb"></param>
         /// <param name="outputDV"></param>
         /// <returns></returns>
         public bool DetectInnerExpr(Rct bb, out DrawingVisual outputDV)
         {
             var sketchRect = new Rect(bb.TopLeft, bb.BottomRight);
             foreach (Visual visual in AlgebraicEditor.Underlay.Children)
             {
                 var dv = visual as DrawingVisual;
                 Rect localRct = dv.Drawing.Bounds;
                 if (sketchRect.Contains(localRct))
                 {
                     outputDV = dv;
                     return true;
                 }
             }
             outputDV = null;
             return false;
         }

         /// <summary>
         /// Used By ActOnEqualQuestionMark
         /// </summary>
         /// <param name="rect"></param>
         /// <param name="upperDV"></param>
         /// <param name="lowerDV"></param>
         /// <returns></returns>
         public bool DetectUpperLowerDrawingVisuals(Rct rect, out DrawingVisual upperDV, out DrawingVisual lowerDV)
         {
             var upperRect = new Rect(new System.Windows.Point(rect.TopLeft.X, rect.TopLeft.Y - rect.Height), rect.BottomRight);
             var lowerRect = new Rect(rect.BottomLeft, new System.Windows.Point(rect.BottomRight.X, rect.BottomRight.Y + rect.Height));
             upperDV = null;
             lowerDV = null;
             foreach (Visual visual in AlgebraicEditor.Underlay.Children)
             {
                 var dv = visual as DrawingVisual;
                 Rect localRct = dv.Drawing.Bounds;
                 if (upperRect.IntersectsWith(localRct))
                 {
                     upperDV = dv;
                 }
                 if (lowerRect.IntersectsWith(localRct))
                 {
                     lowerDV = dv;
                 }

                 if (upperDV != null && lowerDV != null)
                 {
                     return true;
                 }
             }
             return false;
         }*/

        #endregion
    }
}