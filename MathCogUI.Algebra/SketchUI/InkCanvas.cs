﻿/*******************************************************************************
 * Math Interactive Tutoring System
 * <p>
 * Copyright (C) 2015 Bo Kang, Joseph J. LaViola Jr.
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

namespace MathCogUI
{
    using starPadSDK.Geom;
    using starPadSDK.Inq;
    using starPadSDK.WPFHelp;
    using System.Collections.ObjectModel;
    using System.Diagnostics;
    using System.Threading;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;
    using Tablet = System.Windows.Input.Tablet;
    using MathCog.UserModeling;

    public partial class AlgebraicInkCanvas : GestureInkCanvas
    {
        #region Constructor and Initialization

        public AlgebraicInkCanvas()
            : base()
        {
            ManipulationDelta += AGAlgebraicInkCanvas_ManipulationDelta;
            IsManipulationEnabled = true;
            AllowDrop = true;
            Background = Brushes.Transparent;
            Height = 50000;
            Width = 50000;
            myTimer = new Timer(AnalyzeSlidingWindow, null, TriggerGestureAnalyzerDueTime, TriggerGestureAnalyzerInterval);

            FeedbackControls = new ObservableCollection<FeedbackControl>();
            FeedbackControls.CollectionChanged += StrategyControls_CollectionChanged;
        }

        public virtual void InitInkCanvas(ToolBar creator, ContainerVisualHost underlay)
        {
            StroqCollected += CustomInkCanvas_StroqCollected;
            AGEditor = new AlgebraicEditor(this, creator, underlay);
        }

        public override void Reset()
        {
            Stroqs.Clear();
            _cachedStroqs.Clear();
            _slidingWindow.Clear();

            var editor = AGEditor as AlgebraicEditor;
            Debug.Assert(editor != null);
            editor.Reset();

            ResetQueryAndFeedback();
            NotifyClientStrategy(null,0);
            NotifyClientConcept(null);
            NotifyClientInfo(null, false);
        }

        #endregion

        #region Stroq Collect and scribble event handler

        protected virtual bool ScribbleDelete(Stroq stroq, out StroqCollection erasedStroqs)
        {
            erasedStroqs = null;
            bool canBeScribble = DetectScribble(stroq);
            if (!canBeScribble) return false;
            bool result1 = DeleteStroqs(stroq, out erasedStroqs);
            //Hit Test Generated Region
            Rct stkBB = stroq.GetBounds();

            var algebraEditor = AGEditor as AlgebraicEditor;
            Debug.Assert(algebraEditor != null);
            bool result2 = algebraEditor.Delete(stkBB);
            if (result2) Stroqs.Remove(stroq);
            return result1 || result2;
        }

        protected virtual void CustomInkCanvas_StroqCollected(object sender, InqCanvas.StroqCollectedEventArgs e)
        {
            TabletDevice device = Tablet.CurrentTabletDevice;
            if (device == null || device.Type == TabletDeviceType.Touch)
            {
                Stroqs.Remove(e.Stroq);
                return;
            }

            if (StrokeHitTestFeedbackControls(e.Stroq))
            {
                Stroqs.Remove(e.Stroq);
                return;
            }

            if (_isHitTestStroke)
            {
                Stroqs.Remove(e.Stroq);
                return;
            }

            /* If we get here, it's a real stroke (not movement), so deselect any selection */
            Deselect();

            #region Scribble Gesture Recognition

            StroqCollection stroqCollection;
            /* check for scribble delete */
            if (ScribbleDelete(e.Stroq, out stroqCollection))
            {
                while (_slidingWindow.Count != 0)
                {
                    AddStrokeToBackEnd(_slidingWindow.Dequeue());
                }

                if (stroqCollection != null)
                    RemoveStrokeFromBackEnd(stroqCollection);

                //trigger back-end analysis
                var algebraEditor = AGEditor as AlgebraicEditor;
                if (algebraEditor != null)
                {
                    algebraEditor.Parse();
                }

                return;
            }

            #endregion

            _slidingWindow.Enqueue(e.Stroq);
            myTimer.Change(TriggerGestureAnalyzerDueTime, TriggerGestureAnalyzerInterval);
        }

        #endregion

        #region Visual Control Hit Test

        private bool _isHitTestStroke = false;

        private FeedbackControl _hitControl;

        private bool StrokeHitTestFeedbackControls(Stroq stroq)
        {
            foreach (var pt in stroq.StylusPoints)
            {
                var point = pt.ToPoint();
                HitTestResult result = VisualTreeHelper.HitTest(this, point);
                if (result == null) continue;
                var obj = GetVisualParent<FeedbackControl>(result.VisualHit);
                if (obj != null) return true;
            }
            return false;
        }

        protected override void OnStylusDown(System.Windows.Input.StylusDownEventArgs e)
        {
            NotifyClientInfo(null, false);
            base.OnStylusDown(e);
            _hitControl = null;
            Point pt = e.GetPosition(this);
            HitTestResult result = VisualTreeHelper.HitTest(this, pt);
            if (result == null) return;

            var obj = GetVisualParent<Button>(result.VisualHit);
            if (obj == null)
            {
                _isHitTestStroke = false;
                return;
            }

            _isHitTestStroke = true;
            var feedback = GetVisualParent<FeedbackControl>(result.VisualHit);
            Debug.Assert(feedback != null);
            if (obj.Tag.Equals("Close"))
            {
                RemoveFeedbackControl(feedback);
                ResetQueryEngine(feedback);
                var algebraEditor = AGEditor as AlgebraicEditor;
                Debug.Assert(algebraEditor != null);
                algebraEditor.Parse();
            }
            else if (obj.Tag.Equals("Next"))
            {
                /*var bottomPt = feedback.RetrieveBottomPos();
                Rct rct;
                if (bottomPt == null)
                {
                    var topLeft = new Pt(pt.X - 0.9 * feedback.ActualWidth, feedback.Position.Y + 2.0 * feedback.ActualHeight);
                    var bottomRight = new Pt(pt.X, feedback.Position.Y + 4.0 * feedback.ActualHeight);
                    rct = new Rct(topLeft, bottomRight);
                }
                else
                {
                    //pt.X - 0.5* feedback.ActualWidth
                    //var topLeft = new Pt(bottomPt.Value.X, bottomPt.Value.Y + 1.0 * feedback.ActualHeight);
                    var topLeft = new Pt(pt.X - 0.9 * feedback.ActualWidth, bottomPt.Value.Y + feedback.ActualHeight);
                    var bottomRight = new Pt(bottomPt.Value.X, bottomPt.Value.Y + 2.0 * feedback.ActualHeight);
                    rct = new Rct(topLeft, bottomRight);
                }
                var tutorHintFeedback = feedback as TutorQueryHintFeedbackControl;
                if(tutorHintFeedback!= null)
                {
                    HCIReasoner.Instance.AdjustQueryParameter();
                }
                Query(rct);*/
            }
            else if (obj.Tag.Equals("Move"))
            {
                _hitControl = feedback;
            }
        }

        protected override void OnPreviewStylusMove(StylusEventArgs e)
        {
            base.OnPreviewStylusMove(e);
            Point pt = e.GetPosition(this);
            if (_hitControl == null) return;
            FeedbackControls.Remove(_hitControl);
            _hitControl.Position = new Point(pt.X - _hitControl.ActualWidth, pt.Y - 40.0);
            var demonFC = _hitControl as DemonQueryProcessedFeedbackControl;
            var tutorAnswerFC = _hitControl as TutorQueryAnswerFeedbackControl;
            if (demonFC != null)
            {
                var region = demonFC.FeedbackRegion;
                demonFC.FeedbackRegion = new GeneratedRegion(region.Knowledge, new Point(pt.X, pt.Y + _hitControl.ActualHeight));
            }
            if (tutorAnswerFC != null)
            {
                var region = tutorAnswerFC.FeedbackRegion;
                tutorAnswerFC.FeedbackRegion = new GeneratedRegion(region.Knowledge, new Point(pt.X, pt.Y + _hitControl.ActualHeight));
            }
            FeedbackControls.Add(_hitControl);
        }


        public T GetVisualParent<T>(DependencyObject element) where T : DependencyObject
        {
            while (element != null && !(element is T))
                element = VisualTreeHelper.GetParent(element);

            return (T)element;
        }

        #endregion
    }
}
