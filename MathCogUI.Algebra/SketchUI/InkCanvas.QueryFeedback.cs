﻿/*******************************************************************************
 * Math Interactive Tutoring System
 * <p>
 * Copyright (C) 2015 Bo Kang, Joseph J. LaViola Jr.
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

using System.Linq;
using MathCog;
using MathCog.UserModeling;

namespace MathCogUI
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.Diagnostics;
    using Point = System.Windows.Point;

    public partial class AlgebraicInkCanvas
    {
        public static int feedbackIndex = 0;

        /// <summary>
        /// Remove State Machine
        /// </summary>
        /// <param name="fc"></param>
        private void RemoveFeedbackControl(FeedbackControl fc)
        {
            #region Demonstration

            var fc1 = fc as DemonQueryStartFeedbackControl;
            var fc2 = fc as DemonQueryProcessedFeedbackControl;
         
            if (fc1 != null)
            {
                int index = fc1.RenderIndex;
                for (int i = index; i <= feedbackIndex; i++)
                {
                    var tempFc = FeedbackControls.FirstOrDefault(ifc => i.Equals(ifc.RenderIndex));
                    if (tempFc != null) FeedbackControls.Remove(tempFc);
                }
            }

            if (fc2 != null)
            {
                int index = fc2.RenderIndex;
                for (int i = index; i <= feedbackIndex; i++)
                {
                    var tempFc = FeedbackControls.FirstOrDefault(ifc => i.Equals(ifc.RenderIndex));
                    if (tempFc != null) FeedbackControls.Remove(tempFc);
                }
            }

            #endregion
                       
            #region Tutor

            var fc3 = fc as TutorQueryStartFeedbackControl;
            var fc4 = fc as TutorQueryHintFeedbackControl;
            var fc5 = fc as TutorQueryAnswerFeedbackControl;

            if (fc3 != null)
            {
                int index = fc3.RenderIndex;
                for (int i = index; i <= feedbackIndex; i++)
                {
                    var tempFc = FeedbackControls.FirstOrDefault(ifc => i.Equals(ifc.RenderIndex));
                    if (tempFc != null) FeedbackControls.Remove(tempFc);
                }
            }

            if (fc4 != null)
            {
                int index = fc4.RenderIndex;
                for (int i = index; i <= feedbackIndex; i++)
                {
                    var tempFc = FeedbackControls.FirstOrDefault(ifc => i.Equals(ifc.RenderIndex));
                    if (tempFc != null) FeedbackControls.Remove(tempFc);
                }
            }

            if (fc5 != null)
            {
                int index = fc5.RenderIndex;
                for (int i = index; i <= feedbackIndex; i++)
                {
                    var tempFc = FeedbackControls.FirstOrDefault(ifc => i.Equals(ifc.RenderIndex));
                    if (tempFc != null) FeedbackControls.Remove(tempFc);
                }
            }
            #endregion
        }

        private void ResetQueryEngine(FeedbackControl fc)
        {
           /* var fc1 = fc as DemonQueryStartFeedbackControl;
            var fc2 = fc as DemonQueryProcessedFeedbackControl;
            var fc3 = fc as TutorQueryStartFeedbackControl;
            var fc4 = fc as TutorQueryHintFeedbackControl;
            var fc5 = fc as TutorQueryAnswerFeedbackControl;

            if (fc1 != null)
            {
                QueryUIEngine.Instance.Reset(null);
                HCIReasoner.Instance.QueriedKnowledge = null;
                QueryUIEngine.Instance.QuerySession = false;
                QueryUIEngine.Instance.SelectedRct = null;
                NotifyClientInfo(AGDemonstrationMessage.CloseReasoning, true);
                NotifyClientStrategy(null,0);
                
            }
            if (fc2 != null)
            {
                var knowledge = fc2.Tag as IKnowledge;
                Debug.Assert(knowledge != null);
                QueryUIEngine.Instance.Reset(knowledge);
                NotifyClientInfo(AGDemonstrationMessage.CloseAStep, true);
            }
            if (fc3 != null)
            {
                QueryUIEngine.Instance.Reset(null);
            }
            if (fc4 != null)
            {
                HCIReasoner.Instance.CurrentStepHintRequired = true;
                NotifyClientStrategy(null, 0);
            }
            if (fc5 != null)
            {
                var knowledge = fc5.Tag as IKnowledge;
                Debug.Assert(knowledge != null);
                QueryUIEngine.Instance.Reset(knowledge);
                NotifyClientStrategy(null, 0);
            } */
        }

        private void ResetQueryAndFeedback()
        {
            for (var i = FeedbackControls.Count -1; i >= 0; i--)
            {
                FeedbackControls.RemoveAt(i);
            }

            feedbackIndex = 0;
        }

        public ObservableCollection<FeedbackControl> FeedbackControls { get; set; }

        private List<FeedbackControl> SearchFeedbackControlByIndex(Tuple<int, int> tuple)
        {
            var lst = new List<FeedbackControl>();

            foreach (FeedbackControl fc in FeedbackControls)
            {
                var indexTag = fc.Tag as Tuple<int,int>;
                if (indexTag != null 
                    && indexTag.Item1 == tuple.Item1 
                    && indexTag.Item2 == tuple.Item2)
                {
                    lst.Add(fc);
                }
            }
            return lst;
        }

        private List<FeedbackControl> SearchFeedbackControlByOuterIndex(int outerIndex)
        {
            var lst = new List<FeedbackControl>();
            foreach (FeedbackControl fc in FeedbackControls)
            {
                var indexTag = fc.Tag as Tuple<int, int>;
                if (indexTag != null
                    && indexTag.Item1 == outerIndex)
                {
                    lst.Add(fc);
                }
            }
            return lst;
        }

        private List<FeedbackControl> SearchFeedbackControlByInnerIndexGreater(int outIndex, int innerIndex)
        {
            var lst = new List<FeedbackControl>();

            foreach (FeedbackControl fc in FeedbackControls)
            {
                var indexTag = fc.Tag as Tuple<int, int>;
                if (indexTag != null
                    && indexTag.Item1 == outIndex
                    && indexTag.Item2 >= innerIndex)
                {
                    lst.Add(fc);
                }
            }
            return lst;
        }

        void StrategyControls_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (var item in e.NewItems)
                {
                    var strategyControl = item as FeedbackControl;
                    if (strategyControl == null) continue;

                    if (Children.Contains(strategyControl)) return;

                    Children.Add(strategyControl);
                    Point position = strategyControl.Position;
                    SetLeft(strategyControl, position.X);
                    SetTop(strategyControl, position.Y);

                    var tutorAnswer = strategyControl as TutorQueryAnswerFeedbackControl;
                    if (tutorAnswer != null)
                    {
                        var editor = AGEditor as AlgebraicEditor;
                        Debug.Assert(editor != null);
                        editor.KnowledgeRegions.Add(tutorAnswer.FeedbackRegion); 
                    }

                    var demonAnswer = strategyControl as DemonQueryProcessedFeedbackControl;
                    if (demonAnswer != null)
                    {
                        var editor = AGEditor as AlgebraicEditor;
                        Debug.Assert(editor != null);
                        editor.KnowledgeRegions.Add(demonAnswer.FeedbackRegion);
                    }
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (var item in e.OldItems)
                {
                    var removeStrategyControl = item as FeedbackControl;
                    if (removeStrategyControl != null)
                    {
                        if (Children.Contains(removeStrategyControl))
                        {
                            Children.Remove(removeStrategyControl);
                        }

                        var processFc = item as DemonQueryProcessedFeedbackControl;
                        if (processFc != null)
                        {
                            var editor = AGEditor as AlgebraicEditor;
                            Debug.Assert(editor != null);
                            editor.KnowledgeRegions.Remove(processFc.FeedbackRegion);
                        }

                        var processTutor = item as TutorQueryAnswerFeedbackControl;
                        if (processTutor != null)
                        {
                            var editor = AGEditor as AlgebraicEditor;
                            Debug.Assert(editor != null);
                            editor.KnowledgeRegions.Remove(processTutor.FeedbackRegion);                            
                        }
                    }
                }
            }
        }

        //public event ShowKnowledgeHintBarDelegate ShowKnowledgeHint;

        private void FeedbackOnDemonQueryStart(string hintCount, Point position)
        {
            //generate DemonQueryStartFeedbackControl
            var control = new DemonQueryStartFeedbackControl(hintCount, position);
            control.RenderIndex = feedbackIndex++;
            FeedbackControls.Add(control);
        }

        private void FeedbackOnDemonQueryProcessed(string msg, 
            Point position, int leftCount, object feedbackRegion)
        {
            //what: show applied rule
            //how:  generate
            var gRegion = feedbackRegion as GeneratedRegion;
            Debug.Assert(gRegion != null);

            int leftHintCount = leftCount;

            var control = new DemonQueryProcessedFeedbackControl(msg, leftHintCount.ToString(),position);
            control.RenderIndex = feedbackIndex++;
            control.FeedbackRegion = gRegion;
            control.Tag = gRegion.Knowledge;
            FeedbackControls.Add(control);
        }

        /// <summary>
        /// Provide strategy to solve the current question.
        /// </summary>
        /// <param name="position"></param>
        private void FeedbackOnTutorQueryStart(Point position)
        { 
            var control = new TutorQueryStartFeedbackControl(position);
            control.RenderIndex = feedbackIndex++;
            FeedbackControls.Add(control);
        }

        private void FeedbackOnTutorQueryProcessedHint(string msg, Point position)
        {
            var control = new TutorQueryHintFeedbackControl(msg, position);
            control.RenderIndex = feedbackIndex++;
            FeedbackControls.Add(control);
        }

        private void FeedbackOnTutorQueryProcessedAnswer(string msg,
                    Point position, object feedbackRegion)
        {
            //what: show applied rule
            //how:  generate
            var gRegion = feedbackRegion as GeneratedRegion;
            Debug.Assert(gRegion != null);

            TutorQueryHintFeedbackControl hc = null;
            foreach(var fb in FeedbackControls)
            {
                var hintControl = fb as TutorQueryHintFeedbackControl;
                if (hintControl != null)
                {
                    if (hintControl.RenderIndex == feedbackIndex - 1)
                    {
                        hc = hintControl;
                    }
                }
            }
            Debug.Assert(hc != null);
            FeedbackControls.Remove(hc);
            var control = new TutorQueryAnswerFeedbackControl(msg, hc.Position);
            control.RenderIndex = feedbackIndex++;
            control.Tag = gRegion.Knowledge;
            control.FeedbackRegion = gRegion;
            FeedbackControls.Add(control);
        }

    }
}
