﻿/*******************************************************************************
 * Math Interactive Tutoring System
 * <p>
 * Copyright (C) 2015 Bo Kang, Joseph J. LaViola Jr.
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

using System;

namespace MathCogUI
{
    using CSharpLogic;
    using MathCog;
    using MathCog.UserModeling;
    using starPadSDK.CharRecognizer;
    using starPadSDK.Geom;
    using starPadSDK.Inq;
    using starPadSDK.MathRecognizer;
    using starPadSDK.MathUI;
    using starPadSDK.WPFHelp;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Documents;
    using System.Windows.Media;

    public partial class AlgebraicEditor : AGMathEditor
    {
        #region Properties and Constructor

        public ContainerVisualHost Underlay { get; set; }
        private AlgebraicInkCanvas _canvas;
        public AlgebraicInkCanvas InkCanvas
        {
            get { return _canvas; }
        }

        //Sync with back-end reasoning cache
        public ObservableCollection<Region> KnowledgeRegions { get; set; }

        private HCIReasoner _reasoner;
        public HCIReasoner BackReasoner
        {
            get { return _reasoner; }
            set
            {
                _reasoner = value;
                BackQuerier = new QueryUIEngine(_reasoner);
            }
        }

        public QueryUIEngine BackQuerier { get; set; }  
        
        public AlgebraicEditor(AlgebraicInkCanvas canvas, 
                               ToolBar alternatesMenu, 
                               ContainerVisualHost underlay)
        {
            Underlay = underlay;
            KnowledgeRegions = new ObservableCollection<Region>();
            KnowledgeRegions.CollectionChanged += KnowledgeRegions_CollectionChanged;
            _canvas = canvas;
            _mrec = new MathRecognition(_canvas._cachedStroqs);
            _mrec.EnsureLoaded();
            // this is optional, and should only be called once per program run
            _mrec.ParseUpdated += _mrec_ParseUpdated;
            _altsMenuCrea = new AlternatesMenuCreator(alternatesMenu, _mrec);

        }

        public void Reset()
        {
            KnowledgeRegions.Clear();
            Reasoner.Instance.Reset();
        }

        #endregion

        #region UI Visualization Output Generation

        protected virtual void KnowledgeRegions_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
/*            try
            {*/
                if (e.Action == NotifyCollectionChangedAction.Add)
                {
                    foreach (Region region in e.NewItems)
                    {
                        region.PropertyChanged += region_PropertyChanged;
                        var ir = region as InputRegion;
                        if (ir != null)
                        {
                            ir.GenerateVisualRegions();
                            AddVisualInputRegion(ir);
                        }
                        var gr = region as GeneratedRegion;
                        if (gr != null)
                        {
                            AddVisualGenerateRegion(gr);
                        }
                    }
                }
                else if (e.Action == NotifyCollectionChangedAction.Remove)
                {
                    foreach (Region region in e.OldItems)
                    {
                        region.PropertyChanged -= region_PropertyChanged;
                        var ir = region as InputRegion;
                        if (ir != null)
                        {
                            RemoveVisualInputRegion(ir);
                        }
                        var gr = region as GeneratedRegion;
                        if (gr != null)
                        {
                            RemoveVisualGenerateRegion(gr);
                        }
                    }
                }
/*            }
            catch (Exception eee)
            {
                Console.WriteLine("Exception Message: " + eee.Message);
            }
            finally
            {*/
                //Interactive Analytics
                InteractiveAnalyzeKnowledgeRegion();
            //}
        }

        protected virtual void RemoveVisualInputRegion(InputRegion kr)
        {
            foreach (DrawingVisual dv in kr.RenderVisuals)
            {
                if (Underlay.Children.Contains(dv))
                {
                    Underlay.Children.Remove(dv);
                }
            }

            var dr = kr as DragKnowledgeRegion;
            if (dr != null)
            {
                dr.InputText.IsDraggable = true;
            }
        }

        protected virtual void AddVisualInputRegion(InputRegion kr)
        {
            Debug.Assert(kr != null);
            kr.RenderVisuals = new List<DrawingVisual>();

            var sketchInputRegion = kr as SketchKnowledgeRegion;
            if (sketchInputRegion != null)
            {
                #region Region Background Obosolete
                /*  if (StudySettings.ShowAlgebraRecognition)
                {
                    #region Region background

                    var bb = GetRangeBoundingBox(sketchInputRegion.Range);
                    Brush fill3 = new SolidColorBrush(Color.FromArgb(50, 255, 255, 180));
                    Brush fill2 = new SolidColorBrush(Color.FromArgb(75, 255, 255, 180));
                    Brush fill1 = new SolidColorBrush(Color.FromArgb(100, 255, 255, 180));

                    var blackPen = new Pen(Brushes.Black, 1);
                    blackPen.DashStyle = DashStyles.Dash;

                    var dd = new DrawingVisual();
                    DrawingContext dc = dd.RenderOpen();
                    dc.DrawRoundedRectangle(fill3, null, bb, 4, 4);
                    dc.DrawRoundedRectangle(fill2, null, bb.Inflate(-4, -4), 4, 4);
                    dc.DrawRoundedRectangle(fill1, null, bb.Inflate(-8, -8), 4, 4);

                    dc.Close();
                    kr.RenderVisuals.Add(dd);

                    #endregion
                }*/
                #endregion
            }

            var dragInputRegion = kr as DragKnowledgeRegion;
            if (dragInputRegion != null)
            {
                //Tutor Mode does not visualize the query knowledge
                var queryExpr = dragInputRegion.Knowledge as AGQueryExpr;
                if (queryExpr != null && HCIReasoner.TutorMode) return;
            }

            foreach (GeneratedRegion gr in kr.RenderedRegions)
            {
/*                if (StudySettings.ShowAlgebraRecognition)
                {*/
                    kr.RenderVisuals.Add(gr.CurrentDrawingVisual);
                    if (gr.VerifySuccess != null)
                    {
                        #region Region background

                        var bb = gr.ExprVisual.BoundingBox;
                        Brush fill3 = new SolidColorBrush(Color.FromArgb(50, 255, 255, 180));
                        Brush fill2 = new SolidColorBrush(Color.FromArgb(75, 255, 255, 180));
                        Brush fill1 = new SolidColorBrush(Color.FromArgb(100, 255, 255, 180));

                        var blackPen = new Pen(Brushes.Black, 3);
                        blackPen.DashStyle = DashStyles.Dash;

                        var dd = new DrawingVisual();
                        DrawingContext dc = dd.RenderOpen();
                        /*
                                            dc.DrawRoundedRectangle(fill3, null, bb, 4, 4);
                                            dc.DrawRoundedRectangle(fill2, null, bb.Inflate(-4, -4), 4, 4);
                                            dc.DrawRoundedRectangle(fill1, null, bb.Inflate(-8, -8), 4, 4);
                        */
                        string testString;
                        if (gr.VerifySuccess.Value)
                        {
                            testString = "Correct";
                        }
                        else
                        {
                            testString = "Wrong";
                        }
                        var text = new FormattedText(testString,
                            CultureInfo.GetCultureInfo("en-us"),
                            FlowDirection.LeftToRight,
                            new Typeface("Verdana"),
                            10,
                            Brushes.Black);

                        dc.DrawLine(blackPen, bb.BottomLeft, bb.BottomRight);
                        dc.DrawText(text, bb.BottomRight);
                        dc.Close();
                        kr.RenderVisuals.Add(dd);

                        #endregion
//                    }                    
                }
            }

            foreach (DrawingVisual dv in kr.RenderVisuals)
            {
                if (!Underlay.Children.Contains(dv))
                {
                    Underlay.Children.Add(dv);
                }
            }
        }

        protected virtual void AddVisualGenerateRegion(GeneratedRegion gr)
        {
            if (!Underlay.Children.Contains(gr.CurrentDrawingVisual))
            {
                Underlay.Children.Add(gr.CurrentDrawingVisual);
            }
        }

        protected virtual void RemoveVisualGenerateRegion(GeneratedRegion gr)
        {
            if (Underlay.Children.Contains(gr.CurrentDrawingVisual))
            {
                Underlay.Children.Remove(gr.CurrentDrawingVisual);
            }
        }

        protected void region_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var dr = sender as InputRegion;
            if (e.PropertyName.Equals("IsRenderRegionSelected"))
            {
                RemoveVisualInputRegion(dr);
                AddVisualInputRegion(dr);
            }
        }

        #endregion

        #region Input Range Evaluation

        /// <summary>
        /// Real Time Evaluation
        /// </summary>
        /// <param name="inputCount"></param>
        /// <param name="backCount"></param>
        /// <param name="oldLst"></param>
        /// <param name="newLst"></param>
        private void Evaluate(int inputCount, int backCount,
            List<Parser.Range> oldLst, List<Parser.Range> newLst)
        {
            if (inputCount == backCount)
            {
                //update only allow user to edit one range at a time.\
                if (oldLst.Count != 1) return;
                Parser.Range oldRange = oldLst[0];

                if (newLst.Count != 1) return;
                Parser.Range newRange = newLst[0];
                UpdateRange(oldRange, newRange, _userSelectedShapeType);
            }
            else if (inputCount > backCount)
            {
                if (!_canvas.isDeleteSignal)
                {
                    //Assert KnowledgeRegions should be all in Ranges 
                    foreach (Parser.Range r in newLst)
                    {
                        AddRange(r, _userSelectedShapeType, BackQuerier.QuerySession);
                    }
                }
            }
            else // inputCount < backCount
            {
                if (_canvas.isDeleteSignal)
                {
                    foreach (Parser.Range r in oldLst)
                    {
                        ShapeType st;
                        DeleteRange(r, out st);
                    }
                }
            }
        }

        public virtual void Parse()
        {
            using (var batchLock = _mrec.BatchEdit())
            {
                /* reset geometry displayed: range displays, etc */

                Underlay.Children.Clear();

                #region Sketch Knowledge region

                int inputCount = MathRecognizer.Ranges.Count;
                int backCount = KnowledgeRegions.OfType<SketchKnowledgeRegion>().Count();

                var unchangedLst = MathRecognizer.Ranges.Where(x => !x.Updated).ToList();

                var newLst = MathRecognizer.Ranges.Where(x => x.Updated).ToList();
                var oldLst = new List<Parser.Range>();
                foreach (Region r in KnowledgeRegions)
                {
                    var kr = r as SketchKnowledgeRegion;
                    if (kr != null)
                    {
                        if (!unchangedLst.Contains(kr.Range))
                        {
                            oldLst.Add(kr.Range);
                        }
                    }
                }

                if (oldLst.Count != 0 || newLst.Count != 0)
                {
                    Evaluate(inputCount, backCount, oldLst, newLst);
                }

                foreach (Parser.Range pr in unchangedLst)
                {
                    ReAdd(pr);
                }

                #endregion

                #region generate region re-parsing

                IEnumerable<GeneratedRegion> gRegions =
                    KnowledgeRegions.OfType<GeneratedRegion>();
                foreach (GeneratedRegion gR in gRegions)
                {
                    Underlay.Children.Add(gR.CurrentDrawingVisual);
                }

                #endregion

                #region drag knowledge region re-parsing

                List<DragKnowledgeRegion> dRegions =
                    KnowledgeRegions.OfType<DragKnowledgeRegion>().ToList();

                for (int i = 0; i < dRegions.Count(); i++)
                {
                    ReAdd(dRegions[i]);
                }

                #endregion
            }
        }

        void _mrec_ParseUpdated(MathRecognition source, Recognition chchanged, bool updateMath)
        {
            /* Evaluate math if necessary */
            if (updateMath)
            {
                try
                {
                    Evaluator.UpdateMath(MathRecognizer.Ranges.Select((Parser.Range r) => r.Parse));
                }
                catch
                { }
            }

            Parse();

            /* Update alternates menu if user wrote a char */
            if (chchanged != null)
            {
                showSidebarAlts(new[] { chchanged }, new StroqCollection(MathRecognizer.Sim[chchanged.strokes]));
            }
        }

        protected ShapeType _userSelectedShapeType = ShapeType.None;
        protected virtual void _altsMenuCrea_AlternativeEvent(object sender)
        {
            Parse();
        }

        #endregion

        #region Utils

        public virtual void InteractiveAnalyzeKnowledgeRegion()
        {
           /* List<InputRegion> inputRegions = RetrieveAllInputRegion();
            InteractiveAnalytics.Instance.TotalInputSteps = inputRegions.Count;
            InteractiveAnalytics.Instance.AlgebraicInputSteps = inputRegions.Count;
            InteractiveAnalytics.Instance.GeometryInputSteps = 0;*/
        }

        public List<SketchKnowledgeRegion> RetrieveSketchRegion()
        {
            return KnowledgeRegions.OfType<SketchKnowledgeRegion>().ToList();
        }

        public List<InputRegion> RetrieveAllInputRegion()
        {
            var lst = new List<InputRegion>();
            foreach (Region region in KnowledgeRegions)
            {
                var inputRegion = region as InputRegion;
                if (inputRegion == null) continue;
                lst.Add(inputRegion);
            }
            return lst;
        }

        public virtual bool Delete(Rct bb)
        {
            var rct = new Rect(bb.TopLeft, bb.BottomRight);
            var regions = IntersectRegion(rct);
            if (regions != null && regions.Count != 0)
            {
                foreach (var region in regions)
                {
                    var dragInputRegion = region as DragKnowledgeRegion;
                    var generateRegion = region as GeneratedRegion;
                    var sketchInputRegion = region as SketchKnowledgeRegion;
                    if (dragInputRegion != null) RemoveRange(dragInputRegion);
                    if (generateRegion != null) KnowledgeRegions.Remove(region);
                }
                return true;
            }
            return false;
        }

        private SketchKnowledgeRegion SearchKnowledgeRegion(Parser.Range range)
        {
            SketchKnowledgeRegion result = null;
            foreach (var kr in KnowledgeRegions.OfType<SketchKnowledgeRegion>()
                .Where(kr => kr.Range.Equals(range)))
            {
                result = kr;
            }
            return result;
        }

        public List<Region> IntersectRegion(Rect bb)
        {
            var gRegions = IntersectGeneratedRegion(bb);
            var dRegions = IntersectDragKnowledgeRegion(bb);
            var lst = gRegions.ToList();
            lst.AddRange(dRegions);
            return lst;
        }

        private List<Region> IntersectGeneratedRegion(Rect bb)
        {
            var lst = new List<Region>();
            foreach (var r in KnowledgeRegions)
            {
                var gR = r as GeneratedRegion;
                if (gR == null || gR.CurrentDrawingVisual == null
                    || gR.CurrentDrawingVisual.Drawing == null) continue;

                if (gR.CurrentDrawingVisual.Drawing.Bounds.IntersectsWith(bb))
                {
                    lst.Add(gR);
                }
            }
            return lst;
        }

        private List<Region> IntersectDragKnowledgeRegion(Rect bb)
        {
            var lst = new List<Region>();
            foreach (Region r in KnowledgeRegions)
            {
                var gR = r as DragKnowledgeRegion;
                if (gR == null || gR.CurrentDrawingVisual == null
                    || gR.CurrentDrawingVisual.Drawing == null) continue;

                if (gR.CurrentDrawingVisual.Drawing.Bounds.IntersectsWith(bb))
                {
                    lst.Add(gR);
                }
            }
            return lst;
        }

        #endregion
    }
}
