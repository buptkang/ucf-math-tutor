﻿/*******************************************************************************
 * Math Interactive Tutoring System
 * <p>
 * Copyright (C) 2015 Bo Kang, Joseph J. LaViola Jr.
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/


using MathCog.UserModeling;

namespace MathCogUI
{
    using starPadSDK.Geom;
    using starPadSDK.Inq;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Windows;

    public partial class AlgebraicInkCanvas
    {
        #region Two Gesture-based Actions (Selection & Query)

        /// <summary>
        /// //Rct bb
        /// </summary>
        /// <param name="obj"></param>
        public override void Query(object obj, bool gesture = false)
        {
            Point stepRenderPt;
            Point controlRenderPt;
            //TODO: obj region analysis
            var rct = obj as Rct?;
            if (rct == null) throw new Exception("TODO");

            //point-and-click
            //stepRenderPt = new Point(rct.Value.BottomRight.X, rct.Value.BottomRight.Y);
            if (HCIReasoner.TutorMode)
            {
                stepRenderPt = new Point(rct.Value.BottomRight.X, rct.Value.TopRight.Y);
            }
            else
            {
                stepRenderPt = new Point(rct.Value.BottomRight.X, rct.Value.BottomRight.Y);
            }

            controlRenderPt = new Point(rct.Value.TopLeft.X, rct.Value.TopLeft.Y);

            var editor = AGEditor as AlgebraicEditor;
            Debug.Assert(editor != null);

            string message;
            object feedbackRegion;
            object tag;
            QueryFeedbackState feedbackState =
                editor.QueryVerify(stepRenderPt, gesture, out message, out feedbackRegion, out tag);

            QueryFeedback(feedbackState, message, feedbackRegion, tag, controlRenderPt);
        }

        public virtual void QueryFeedback(QueryFeedbackState feedbackState,
                                   string message,
                                   object feedbackRegion,
                                   object tag,
                                   Point controlRenderPt)
        {
            switch (feedbackState)
            {
                case QueryFeedbackState.QueryFailed:
                    break;
                case QueryFeedbackState.DemonQueryStarted:
                    var tuple0 = tag as Tuple<object, object>;
                    Debug.Assert(tuple0 != null);
                    var strategies1 = tuple0.Item1 as List<string>;
                    Debug.Assert(strategies1!=null);
                    var leftCount = (int)tuple0.Item2;
                    FeedbackOnDemonQueryStart(leftCount.ToString(), controlRenderPt);
                    NotifyClientStrategy(strategies1, -1);
                    break;
                case QueryFeedbackState.DemonQueryProcessed:
                    var tuple = tag as Tuple<object, object, object>;
                    Debug.Assert(tuple != null);
                    var appliedRule = tuple.Item1 as string;
                    Debug.Assert(appliedRule != null);
                    var leftHintCount = (int) tuple.Item2;
                    FeedbackOnDemonQueryProcessed(appliedRule,
                        controlRenderPt, leftHintCount, feedbackRegion);
                    var tuple2 = tuple.Item3 as Tuple<object, object>;
                    Debug.Assert(tuple2 != null);
                    var lst = tuple2.Item1 as List<string>;
                    Debug.Assert(lst != null);
                    int index = (int)tuple2.Item2;
                    NotifyClientStrategy(lst, index);
                    break;
                case QueryFeedbackState.TutorQueryStarted:
                    var strategies = tag as List<string>;
                    Debug.Assert(strategies != null);
                    FeedbackOnTutorQueryStart(controlRenderPt);
                    NotifyClientStrategy(strategies,-1);
                    break;
                case QueryFeedbackState.TutorQueryProcessedHint:
                    var tuple5 = tag as Tuple<object, object, object>;
                    Debug.Assert(tuple5 != null);
                    var rule1 = tuple5.Item1 as string;
                    Debug.Assert(rule1 != null);
                    var strategies5 = tuple5.Item2 as List<string>;
                    Debug.Assert(strategies5 != null);
                    int strategyIndex = (int) tuple5.Item3;
                    FeedbackOnTutorQueryProcessedHint(rule1, controlRenderPt);
                    NotifyClientStrategy(strategies5, strategyIndex);
                    break;
                case QueryFeedbackState.TutorQueryProcessedAnswer:
                    var tuple6 = tag as Tuple<object, object, object>;
                    if (tuple6 == null) return;
                    Debug.Assert(tuple6 != null);
                    var rule2 = tuple6.Item1 as string;
                    Debug.Assert(rule2 != null);
                    var strategies6 = tuple6.Item2 as List<string>;
                    Debug.Assert(strategies6 != null);
                    int strategyIndex1 = (int) tuple6.Item3;
                    FeedbackOnTutorQueryProcessedAnswer(rule2,
                        controlRenderPt, feedbackRegion);
                    NotifyClientStrategy(strategies6, strategyIndex1);
                    break;
            }
            NotifyClientInfo(message, true);
        }

        public override bool Select(object obj)
        {
            var stroq = obj as Stroq;
            if (stroq == null) return false;
            var bound = stroq.GetBounds();

            var editor = AGEditor as AlgebraicEditor;
            if (editor == null) return false;

            string message;
            object hitRegion;
            bool result = editor.Select(bound, out message, out hitRegion);
            if (result)
            {
                var hitInputRegion = hitRegion as InputRegion;
                if (hitInputRegion == null) return false;
                NotifyClientInfo(message, true);
                return true;
            }
            NotifyClientInfo(AGWarning.NoSelection, true);
            return false;
        }

        private bool SelectFeedback(string message, InputRegion hitRegion)
        {
            var hitInputRegion = hitRegion as InputRegion;
            Debug.Assert(hitInputRegion != null);
            if (!hitInputRegion.IsRenderRegionSelected)
            {
                ResetQueryAndFeedback();
            }
            NotifyClientInfo(message, true);
            return true;
        }

        #endregion
    }
}
