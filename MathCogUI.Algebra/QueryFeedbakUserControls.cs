﻿/*******************************************************************************
 * Math Interactive Tutoring System
 * <p>
 * Copyright (C) 2015 Bo Kang, Joseph J. LaViola Jr.
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

namespace MathCogUI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using starPadSDK.Geom;

    public class ConceptControl : WrapPanel
    {
        public Border Border { get; set; }
        public GroupBox GroupBox { get; set; }

        public object Concept { get; set; }
        public string ConceptExplain { get; set; }

        public ConceptControl()
        {
            HorizontalAlignment = HorizontalAlignment.Left;
            VerticalAlignment = VerticalAlignment.Bottom;
            Margin = new Thickness(8);

            Border = new Border();
            Border.Padding = new Thickness(4);
            Border.Background = new SolidColorBrush(Color.FromArgb(255, 255, 255, 233));
            Border.BorderBrush = new SolidColorBrush(Colors.Black);
            Border.BorderThickness = new Thickness(1);

            Border.MouseLeftButtonDown += Border_MouseLeftButtonDown;
            Border.StylusDown += Border_StylusDown;

            GroupBox = new GroupBox();
            Border.Child = GroupBox;
            this.Children.Add(Border);
            this.Visibility = Visibility.Collapsed;
        }

        private void Border_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.Visibility = Visibility.Collapsed;
        }

        private void Border_StylusDown(object sender, System.Windows.Input.StylusDownEventArgs e)
        {
            this.Visibility = Visibility.Collapsed;
        }
    }

    public class FeedbackControl : Grid
    {
        public new object Tag { get; set; } // owner of strategy
        public int RenderIndex { get; set; }
        public Point Position { get; set; }
        public Button CloseButton { get; set; }
        public Button NextButton { get; set; }
        public Button MoveButton { get; set; }
        public Button HelpButton { get; set; }

        public FeedbackControl(Point pt)
        {
            Position = pt;
        }

        public virtual Pt? RetrieveBottomPos()
        {
            return null;
        }    
    }

    public class DemonQueryStartFeedbackControl : FeedbackControl
    {
        public DemonQueryStartFeedbackControl(string hintCount, Point pt) : base(pt)
        {
            Height = 60.0;
            RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1.0, GridUnitType.Star) });
            RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1.0, GridUnitType.Star) });

            ColumnDefinitions.Add(new ColumnDefinition());
            ColumnDefinitions.Add(new ColumnDefinition());
            ColumnDefinitions.Add(new ColumnDefinition());
            
            string s = "Starting from problem statement:";
            var label = new Label();
            label.Content = s;
            label.Foreground = Brushes.Red;
            label.FontWeight = FontWeights.Bold;
            label.FontStyle = FontStyles.Italic;
            label.Opacity = 0.7;
         
            var border = new Border();
            border.IsHitTestVisible = false;
            border.Background = new SolidColorBrush(Colors.LightSkyBlue);
            border.BorderBrush = new SolidColorBrush(Colors.Black);
            border.BorderThickness = new Thickness(1);
            border.Child = label;

            string s0 = "Total " + hintCount + " hints";
            var label0 = new Label();
            label0.Content = s0;
            label0.Foreground = Brushes.Black;
            label0.FontWeight = FontWeights.Bold;
            label0.Opacity = 0.7;
            label0.HorizontalAlignment = HorizontalAlignment.Right;

            var border0 = new Border();
            border0.IsHitTestVisible = false;
            border0.Background = new SolidColorBrush(Colors.LightSkyBlue);
            border0.BorderBrush = new SolidColorBrush(Colors.Black);
            border0.BorderThickness = new Thickness(1);
            border0.Child = label0;
            border0.HorizontalAlignment = HorizontalAlignment.Stretch;

            string s1 = "Show Next Hint:";
            var label1 = new Label();
            label1.Content = s1;
            label1.Foreground = Brushes.Black;
            label1.FontWeight = FontWeights.Bold;
            label1.Opacity = 0.7;
            label1.HorizontalAlignment = HorizontalAlignment.Right;

            var border1 = new Border();
            border1.IsHitTestVisible = false;
            border1.Background = new SolidColorBrush(Colors.LightSkyBlue);
            border1.BorderBrush = new SolidColorBrush(Colors.Black);
            border1.BorderThickness = new Thickness(1);
            border1.Child = label1;
            border1.HorizontalAlignment = HorizontalAlignment.Stretch;
           
            var st = new StackPanel();
            st.Orientation = Orientation.Horizontal;
            st.Children.Add(border0);
            st.Children.Add(border1);
            st.HorizontalAlignment = HorizontalAlignment.Right;
             
            var brush = new ImageBrush();
            brush.ImageSource = new BitmapImage(new Uri(@"Image/cancel.png", UriKind.Relative));
            CloseButton = new Button();
            CloseButton.Background = brush;
            CloseButton.Width  = 20.0;
            CloseButton.Height = 30.0;
            CloseButton.Tag = "Close";

            var brush1 = new ImageBrush();
            brush1.ImageSource = new BitmapImage(new Uri(@"Image/help.png", UriKind.Relative));
            HelpButton = new Button();
            HelpButton.Background = brush1;
            HelpButton.Width = 20.0;
            HelpButton.Height = 30.0;
            HelpButton.Tag = "Help";

            var brush2 = new ImageBrush();
            brush2.ImageSource = new BitmapImage(new Uri(@"Image/next.png", UriKind.Relative));
            NextButton = new Button();
            NextButton.Background = brush2;
            NextButton.Width = 20.0;
            NextButton.Height = 30.0;
            NextButton.Tag = "Next";

            var brush3 = new ImageBrush();
            brush3.ImageSource = new BitmapImage(new Uri(@"Image/hand.png", UriKind.Relative));
            MoveButton = new Button();
            MoveButton.Background = brush3;
            MoveButton.Width = 20.0;
            MoveButton.Height = 30.0;
            MoveButton.Tag = "Move";

            Children.Add(border);
            Children.Add(st);
            Children.Add(CloseButton);
            Children.Add(NextButton);
            Children.Add(MoveButton);
            Children.Add(HelpButton);

            Grid.SetColumn(border, 0);
            Grid.SetRow(border, 0);
           
            Grid.SetColumn(st, 0);
            Grid.SetRow(st, 1);

            Grid.SetColumn(HelpButton, 1);
            Grid.SetRow(HelpButton, 0);

            Grid.SetColumn(CloseButton, 2);
            Grid.SetRow(CloseButton, 0);

            Grid.SetColumn(NextButton, 1);
            Grid.SetRow(NextButton, 1);           

            Grid.SetColumn(MoveButton, 2);
            Grid.SetRow(MoveButton, 1);
        }
    }

    public class DemonQueryProcessedFeedbackControl : FeedbackControl
    {
        public DemonQueryProcessedFeedbackControl(string msg, string hintLeftCount, Point pt):base(pt)
        {
            Height = 60.0;
            RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1.0, GridUnitType.Star) });
            RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1.0, GridUnitType.Star) });

            ColumnDefinitions.Add(new ColumnDefinition());
            ColumnDefinitions.Add(new ColumnDefinition());
            ColumnDefinitions.Add(new ColumnDefinition());

            string s = msg;
            var label = new Label();
            label.Content = s;
            label.Foreground = Brushes.Red;
            label.FontWeight = FontWeights.Bold;
            label.FontStyle = FontStyles.Italic;
            label.Opacity = 0.7;

            var border = new Border();
            border.IsHitTestVisible = false;
            border.Background = new SolidColorBrush(Colors.LightSkyBlue);
            border.BorderBrush = new SolidColorBrush(Colors.Black);
            border.BorderThickness = new Thickness(1);
            border.Child = label;

            string s0 = hintLeftCount + " hints left";
            var label0 = new Label();
            label0.Content = s0;
            label0.Foreground = Brushes.Black;
            label0.FontWeight = FontWeights.Bold;
            label0.Opacity = 0.7;
            label0.HorizontalAlignment = HorizontalAlignment.Right;

            var border0 = new Border();
            border0.IsHitTestVisible = false;
            border0.Background = new SolidColorBrush(Colors.LightSkyBlue);
            border0.BorderBrush = new SolidColorBrush(Colors.Black);
            border0.BorderThickness = new Thickness(1);
            border0.Child = label0;
            border0.HorizontalAlignment = HorizontalAlignment.Stretch;

            string s1 = "Show Next Hint:";
            var label1 = new Label();
            label1.Content = s1;
            label1.Foreground = Brushes.Black;
            label1.FontWeight = FontWeights.Bold;
            label1.Opacity = 0.7;
            label1.HorizontalAlignment = HorizontalAlignment.Right;

            var border1 = new Border();
            border1.IsHitTestVisible = false;
            border1.Background = new SolidColorBrush(Colors.LightSkyBlue);
            border1.BorderBrush = new SolidColorBrush(Colors.Black);
            border1.BorderThickness = new Thickness(1);
            border1.Child = label1;
            border1.HorizontalAlignment = HorizontalAlignment.Stretch;

            var st = new StackPanel();
            st.Orientation = Orientation.Horizontal;
            st.Children.Add(border0);
            st.Children.Add(border1);
            st.HorizontalAlignment = HorizontalAlignment.Right;

            var brush = new ImageBrush();
            brush.ImageSource = new BitmapImage(new Uri(@"Image/cancel.png", UriKind.Relative));
            CloseButton = new Button();
            CloseButton.Background = brush;
            CloseButton.Width = 20.0;
            CloseButton.Height = 30.0;
            CloseButton.Tag = "Close";

            var brush1 = new ImageBrush();
            brush1.ImageSource = new BitmapImage(new Uri(@"Image/help.png", UriKind.Relative));
            HelpButton = new Button();
            HelpButton.Background = brush1;
            HelpButton.Width = 20.0;
            HelpButton.Height = 30.0;
            HelpButton.Tag = "Help";

            var brush2 = new ImageBrush();
            brush2.ImageSource = new BitmapImage(new Uri(@"Image/next.png", UriKind.Relative));
            NextButton = new Button();
            NextButton.Background = brush2;
            NextButton.Width = 20.0;
            NextButton.Height = 30.0;
            NextButton.Tag = "Next";

            var brush3 = new ImageBrush();
            brush3.ImageSource = new BitmapImage(new Uri(@"Image/hand.png", UriKind.Relative));
            MoveButton = new Button();
            MoveButton.Background = brush3;
            MoveButton.Width = 20.0;
            MoveButton.Height = 30.0;
            MoveButton.Tag = "Move";

            Children.Add(border);
            Children.Add(st);
            Children.Add(CloseButton);
            Children.Add(NextButton);
            Children.Add(MoveButton);
            Children.Add(HelpButton);

            Grid.SetColumn(border, 0);
            Grid.SetRow(border, 0);

            Grid.SetColumn(st, 0);
            Grid.SetRow(st, 1);

            Grid.SetColumn(HelpButton, 1);
            Grid.SetRow(HelpButton, 0);

            Grid.SetColumn(CloseButton, 2);
            Grid.SetRow(CloseButton, 0);

            Grid.SetColumn(NextButton, 1);
            Grid.SetRow(NextButton, 1);

            Grid.SetColumn(MoveButton, 2);
            Grid.SetRow(MoveButton, 1);
        }

        public GeneratedRegion FeedbackRegion { get; set; }

        public override Pt? RetrieveBottomPos()
        {
           return FeedbackRegion.ExprVisual.DrawPos;
        }
    }

    public class TutorQueryStartFeedbackControl : FeedbackControl
    {
        public TutorQueryStartFeedbackControl(Point pt)
            : base(pt)
        {
            Height = 60.0;
            RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1.0, GridUnitType.Star) });
            RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1.0, GridUnitType.Star) });

            ColumnDefinitions.Add(new ColumnDefinition());
            ColumnDefinitions.Add(new ColumnDefinition());
            ColumnDefinitions.Add(new ColumnDefinition());

            string s = "Refer to the solving strategy and start to solve this question on the sketch paper:";
            var label = new Label();
            label.Content = s;
            label.Foreground = Brushes.Red;
            label.FontWeight = FontWeights.Bold;
            label.FontStyle = FontStyles.Italic;
            label.Opacity = 0.7;

            var border = new Border();
            border.IsHitTestVisible = false;
            border.Background = new SolidColorBrush(Colors.LightSkyBlue);
            border.BorderBrush = new SolidColorBrush(Colors.Black);
            border.BorderThickness = new Thickness(1);
            border.Child = label;

            string s0 = "Ask for further hints in details";
            var label0 = new Label();
            label0.Content = s0;
            label0.Foreground = Brushes.Black;
            label0.FontWeight = FontWeights.Bold;
            label0.Opacity = 0.7;
            label0.HorizontalAlignment = HorizontalAlignment.Right;

            var border0 = new Border();
            border0.IsHitTestVisible = false;
            border0.Background = new SolidColorBrush(Colors.LightSkyBlue);
            border0.BorderBrush = new SolidColorBrush(Colors.Black);
            border0.BorderThickness = new Thickness(1);
            border0.Child = label0;
            border0.HorizontalAlignment = HorizontalAlignment.Stretch;

            string s1 = "Show Next Hint:";
            var label1 = new Label();
            label1.Content = s1;
            label1.Foreground = Brushes.Black;
            label1.FontWeight = FontWeights.Bold;
            label1.Opacity = 0.7;
            label1.HorizontalAlignment = HorizontalAlignment.Right;

            var border1 = new Border();
            border1.IsHitTestVisible = false;
            border1.Background = new SolidColorBrush(Colors.LightSkyBlue);
            border1.BorderBrush = new SolidColorBrush(Colors.Black);
            border1.BorderThickness = new Thickness(1);
            border1.Child = label1;
            border1.HorizontalAlignment = HorizontalAlignment.Stretch;

            var st = new StackPanel();
            st.Orientation = Orientation.Horizontal;
            st.Children.Add(border0);
            st.Children.Add(border1);
            st.HorizontalAlignment = HorizontalAlignment.Right;

            var brush = new ImageBrush();
            brush.ImageSource = new BitmapImage(new Uri(@"Image/cancel.png", UriKind.Relative));
            CloseButton = new Button();
            CloseButton.Background = brush;
            CloseButton.Width = 20.0;
            CloseButton.Height = 30.0;
            CloseButton.Tag = "Close";

            var brush1 = new ImageBrush();
            brush1.ImageSource = new BitmapImage(new Uri(@"Image/help.png", UriKind.Relative));
            HelpButton = new Button();
            HelpButton.Background = brush1;
            HelpButton.Width = 20.0;
            HelpButton.Height = 30.0;
            HelpButton.Tag = "Help";

            var brush2 = new ImageBrush();
            brush2.ImageSource = new BitmapImage(new Uri(@"Image/next.png", UriKind.Relative));
            NextButton = new Button();
            NextButton.Background = brush2;
            NextButton.Width = 20.0;
            NextButton.Height = 30.0;
            NextButton.Tag = "Next";

            var brush3 = new ImageBrush();
            brush3.ImageSource = new BitmapImage(new Uri(@"Image/hand.png", UriKind.Relative));
            MoveButton = new Button();
            MoveButton.Background = brush3;
            MoveButton.Width = 20.0;
            MoveButton.Height = 30.0;
            MoveButton.Tag = "Move";

            Children.Add(border);
            Children.Add(st);
            Children.Add(CloseButton);
            Children.Add(NextButton);
            Children.Add(MoveButton);
            Children.Add(HelpButton);

            Grid.SetColumn(border, 0);
            Grid.SetRow(border, 0);

            Grid.SetColumn(st, 0);
            Grid.SetRow(st, 1);

            Grid.SetColumn(HelpButton, 1);
            Grid.SetRow(HelpButton, 0);

            Grid.SetColumn(CloseButton, 2);
            Grid.SetRow(CloseButton, 0);

            Grid.SetColumn(NextButton, 1);
            Grid.SetRow(NextButton, 1);

            Grid.SetColumn(MoveButton, 2);
            Grid.SetRow(MoveButton, 1);
        }
    }

    public class TutorQueryHintFeedbackControl : FeedbackControl
    {
        public TutorQueryHintFeedbackControl(string msg, Point pt)
            : base(pt)
        {
            Height = 60.0;
            RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1.0, GridUnitType.Star) });
            RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1.0, GridUnitType.Star) });

            ColumnDefinitions.Add(new ColumnDefinition());
            ColumnDefinitions.Add(new ColumnDefinition());
            ColumnDefinitions.Add(new ColumnDefinition());

            string s = msg;
            var label = new Label();
            label.Content = s;
            label.Foreground = Brushes.Red;
            label.FontWeight = FontWeights.Bold;
            label.FontStyle = FontStyles.Italic;
            label.Opacity = 0.7;

            var border = new Border();
            border.IsHitTestVisible = false;
            border.Background = new SolidColorBrush(Colors.LightSkyBlue);
            border.BorderBrush = new SolidColorBrush(Colors.Black);
            border.BorderThickness = new Thickness(1);
            border.Child = label;

            string s0 = "Ask for further hints in details";
            var label0 = new Label();
            label0.Content = s0;
            label0.Foreground = Brushes.Black;
            label0.FontWeight = FontWeights.Bold;
            label0.Opacity = 0.7;
            label0.HorizontalAlignment = HorizontalAlignment.Right;

            var border0 = new Border();
            border0.IsHitTestVisible = false;
            border0.Background = new SolidColorBrush(Colors.LightSkyBlue);
            border0.BorderBrush = new SolidColorBrush(Colors.Black);
            border0.BorderThickness = new Thickness(1);
            border0.Child = label0;
            border0.HorizontalAlignment = HorizontalAlignment.Stretch;

            string s1 = "Show Next Hint:";
            var label1 = new Label();
            label1.Content = s1;
            label1.Foreground = Brushes.Black;
            label1.FontWeight = FontWeights.Bold;
            label1.Opacity = 0.7;
            label1.HorizontalAlignment = HorizontalAlignment.Right;

            var border1 = new Border();
            border1.IsHitTestVisible = false;
            border1.Background = new SolidColorBrush(Colors.LightSkyBlue);
            border1.BorderBrush = new SolidColorBrush(Colors.Black);
            border1.BorderThickness = new Thickness(1);
            border1.Child = label1;
            border1.HorizontalAlignment = HorizontalAlignment.Stretch;

            var st = new StackPanel();
            st.Orientation = Orientation.Horizontal;
            st.Children.Add(border0);
            st.Children.Add(border1);
            st.HorizontalAlignment = HorizontalAlignment.Right;

            var brush = new ImageBrush();
            brush.ImageSource = new BitmapImage(new Uri(@"Image/cancel.png", UriKind.Relative));
            CloseButton = new Button();
            CloseButton.Background = brush;
            CloseButton.Width = 20.0;
            CloseButton.Height = 30.0;
            CloseButton.Tag = "Close";

            var brush1 = new ImageBrush();
            brush1.ImageSource = new BitmapImage(new Uri(@"Image/help.png", UriKind.Relative));
            HelpButton = new Button();
            HelpButton.Background = brush1;
            HelpButton.Width = 20.0;
            HelpButton.Height = 30.0;
            HelpButton.Tag = "Help";

            var brush2 = new ImageBrush();
            brush2.ImageSource = new BitmapImage(new Uri(@"Image/next.png", UriKind.Relative));
            NextButton = new Button();
            NextButton.Background = brush2;
            NextButton.Width = 20.0;
            NextButton.Height = 30.0;
            NextButton.Tag = "Next";

            var brush3 = new ImageBrush();
            brush3.ImageSource = new BitmapImage(new Uri(@"Image/hand.png", UriKind.Relative));
            MoveButton = new Button();
            MoveButton.Background = brush3;
            MoveButton.Width = 20.0;
            MoveButton.Height = 30.0;
            MoveButton.Tag = "Move";

            Children.Add(border);
            Children.Add(st);
            Children.Add(CloseButton);
            Children.Add(NextButton);
            Children.Add(MoveButton);
            Children.Add(HelpButton);

            Grid.SetColumn(border, 0);
            Grid.SetRow(border, 0);

            Grid.SetColumn(st, 0);
            Grid.SetRow(st, 1);

            Grid.SetColumn(HelpButton, 1);
            Grid.SetRow(HelpButton, 0);

            Grid.SetColumn(CloseButton, 2);
            Grid.SetRow(CloseButton, 0);

            Grid.SetColumn(NextButton, 1);
            Grid.SetRow(NextButton, 1);

            Grid.SetColumn(MoveButton, 2);
            Grid.SetRow(MoveButton, 1);
        }
    }

    public class TutorQueryAnswerFeedbackControl : FeedbackControl
    {
        public TutorQueryAnswerFeedbackControl(string msg, Point pt)
            : base(pt)
        {
            Height = 60.0;
            RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1.0, GridUnitType.Star) });
            RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1.0, GridUnitType.Star) });

            ColumnDefinitions.Add(new ColumnDefinition());
            ColumnDefinitions.Add(new ColumnDefinition());
            ColumnDefinitions.Add(new ColumnDefinition());

            string s = msg;
            var label = new Label();
            label.Content = s;
            label.Foreground = Brushes.Red;
            label.FontWeight = FontWeights.Bold;
            label.FontStyle = FontStyles.Italic;
            label.Opacity = 0.7;

            var border = new Border();
            border.IsHitTestVisible = false;
            border.Background = new SolidColorBrush(Colors.LightSkyBlue);
            border.BorderBrush = new SolidColorBrush(Colors.Black);
            border.BorderThickness = new Thickness(1);
            border.Child = label;

            string s0 = "Ask for further hints in details";
            var label0 = new Label();
            label0.Content = s0;
            label0.Foreground = Brushes.Black;
            label0.FontWeight = FontWeights.Bold;
            label0.Opacity = 0.7;
            label0.HorizontalAlignment = HorizontalAlignment.Right;

            var border0 = new Border();
            border0.IsHitTestVisible = false;
            border0.Background = new SolidColorBrush(Colors.LightSkyBlue);
            border0.BorderBrush = new SolidColorBrush(Colors.Black);
            border0.BorderThickness = new Thickness(1);
            border0.Child = label0;
            border0.HorizontalAlignment = HorizontalAlignment.Stretch;

            string s1 = "Show Next Hint:";
            var label1 = new Label();
            label1.Content = s1;
            label1.Foreground = Brushes.Black;
            label1.FontWeight = FontWeights.Bold;
            label1.Opacity = 0.7;
            label1.HorizontalAlignment = HorizontalAlignment.Right;

            var border1 = new Border();
            border1.IsHitTestVisible = false;
            border1.Background = new SolidColorBrush(Colors.LightSkyBlue);
            border1.BorderBrush = new SolidColorBrush(Colors.Black);
            border1.BorderThickness = new Thickness(1);
            border1.Child = label1;
            border1.HorizontalAlignment = HorizontalAlignment.Stretch;

            var st = new StackPanel();
            st.Orientation = Orientation.Horizontal;
            st.Children.Add(border0);
            st.Children.Add(border1);
            st.HorizontalAlignment = HorizontalAlignment.Right;

            var brush = new ImageBrush();
            brush.ImageSource = new BitmapImage(new Uri(@"Image/cancel.png", UriKind.Relative));
            CloseButton = new Button();
            CloseButton.Background = brush;
            CloseButton.Width = 20.0;
            CloseButton.Height = 30.0;
            CloseButton.Tag = "Close";

            var brush1 = new ImageBrush();
            brush1.ImageSource = new BitmapImage(new Uri(@"Image/help.png", UriKind.Relative));
            HelpButton = new Button();
            HelpButton.Background = brush1;
            HelpButton.Width = 20.0;
            HelpButton.Height = 30.0;
            HelpButton.Tag = "Help";

            var brush2 = new ImageBrush();
            brush2.ImageSource = new BitmapImage(new Uri(@"Image/next.png", UriKind.Relative));
            NextButton = new Button();
            NextButton.Background = brush2;
            NextButton.Width = 20.0;
            NextButton.Height = 30.0;
            NextButton.Tag = "Next";

            var brush3 = new ImageBrush();
            brush3.ImageSource = new BitmapImage(new Uri(@"Image/hand.png", UriKind.Relative));
            MoveButton = new Button();
            MoveButton.Background = brush3;
            MoveButton.Width = 20.0;
            MoveButton.Height = 30.0;
            MoveButton.Tag = "Move";

            Children.Add(border);
            Children.Add(st);
            Children.Add(CloseButton);
            Children.Add(NextButton);
            Children.Add(MoveButton);
            Children.Add(HelpButton);

            Grid.SetColumn(border, 0);
            Grid.SetRow(border, 0);

            Grid.SetColumn(st, 0);
            Grid.SetRow(st, 1);

            Grid.SetColumn(HelpButton, 1);
            Grid.SetRow(HelpButton, 0);

            Grid.SetColumn(CloseButton, 2);
            Grid.SetRow(CloseButton, 0);

            Grid.SetColumn(NextButton, 1);
            Grid.SetRow(NextButton, 1);

            Grid.SetColumn(MoveButton, 2);
            Grid.SetRow(MoveButton, 1);
        }

        public GeneratedRegion FeedbackRegion { get; set; }

        public override Pt? RetrieveBottomPos()
        {
            return FeedbackRegion.ExprVisual.DrawPos;
        }
    }
}
