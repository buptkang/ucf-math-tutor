﻿/*******************************************************************************
 * Math Interactive Tutoring System
 * <p>
 * Copyright (C) 2015 Bo Kang.
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

namespace MathCogUI
{
    using Algebra.LogicSketch.Annotations;
    using MathCog;
    using MathCog.UserModeling;
    using starPadSDK.Geom;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;

    /// <summary>
    /// Query State Machine
    /// </summary>
    public class QueryUIEngine : INotifyPropertyChanged
    {
        #region Parameters and Constructor

        private bool _querySession;
        public bool QuerySession
        {
            get { return _querySession; }
            set { _querySession = value; }
        }

        private IKnowledge _userInputKnowledge;
        public IKnowledge UserInputKnowledge
        {
            get { return _userInputKnowledge; }
            set
            {
                _userInputKnowledge = value;
            }
        }

        private object _selectedRct;  // delete operation update.//Rct?
        public object SelectedRct
        {
            get
            {
                return _selectedRct;
            }
            set
            {
                _selectedRct = value;
                OnPropertyChanged("SelectedRct");
            }
        }

        private Dictionary<Rct, bool?> VerifiedRcts { get; set; }

        public HCIReasoner Reasoner { get; set; }

        public QueryUIEngine(HCIReasoner reasoner)
        {
            Reasoner = reasoner;
            VerifiedRcts = new Dictionary<Rct, bool?>();
        }

        public void ResetQueryEngine()
        {
            VerifiedRcts = new Dictionary<Rct, bool?>();
        }

        public void Reset(object obj)
        {
            Reasoner.Reset(obj);
        }

        #endregion

        #region API

        public QueryFeedbackState VerifiedAll(List<Region> regions, out string msg, out object obj)
        {
            msg = null;
            obj = null;
            Debug.Assert(HCIReasoner.TutorMode);
            var state = QueryFeedbackState.QueryFailed;

            if (regions.Count == 0)
            {
                state = Reasoner.Query(null, out msg, out obj);
            }

            bool verifiedSuccess = false;
            bool containWrongStep = false;
            for (int i = regions.Count - 1; i >= 0; i--)
            {
                var region = regions[i];
                if (!verifiedSuccess) // skip steps to check
                {
                    state = Reasoner.Query(region.Knowledge, out msg, out obj);
                    //checked steps update
                    //InteractiveAnalytics.Instance.EvaluatedSteps++;
                }
                bool? result = VerifyFeedback(msg, region);
                if (result == null) continue;
                if (result.Value) verifiedSuccess = true;
                if (!result.Value) containWrongStep = true;
            }

            if (containWrongStep) // user's action is wrong
            {
                string message;
                state = Reasoner.Query(null, out message, out obj);
                msg = AGTutorMessage.VerifyWrong;

                //6.How many times the user rectify his result after querying?

                //InteractiveAnalytics.Instance.PreviousQueryResult = false;
            }
            else
            {
                //TODO
/*                if (InteractiveAnalytics.Instance.PreviousQueryResult != null
                    && !InteractiveAnalytics.Instance.PreviousQueryResult.Value)
                {
                    //Previous query result is wrong
                    //Current query result is correct
                    InteractiveAnalytics.Instance.RectifyCount++;
                }
                InteractiveAnalytics.Instance.PreviousQueryResult = true;*/
            }

            //Query Analytics
            //2. How long does it take for users to proceed problem-solving?
            //InteractiveAnalytics.Instance.CheckEndTimer();

            //4. How many times the student asks for the hint through the pen-inquiry gesture?
            //InteractiveAnalytics.Instance.QueryTimes++;

            /*
              * 5.1. How many time the system shows the meta-cognition scaffolding user control?
              * 5.2. How many times the system shows the answer scaffolding user control?
              */
            //InteractiveAnalytics.Instance.AnalyzeState(state);

            return state;
        }

        private bool? VerifyFeedback(string msg, Region region)
        {
            if (msg == AGTutorMessage.VerifyCorrect
                || msg == AGTutorMessage.SolvedProblem
                || msg == AGTutorMessage.SolvingPartialProblem)
            {
                if (!VerifiedRcts.ContainsKey(region.ExprVisual.BoundingBox))
                {
                    VerifiedRcts.Add(region.ExprVisual.BoundingBox, true);
                }
                return true;
            }

            if (msg == AGTutorMessage.VerifyWrong)
            {
                if (!VerifiedRcts.ContainsKey(region.ExprVisual.BoundingBox))
                {
                    VerifiedRcts.Add(region.ExprVisual.BoundingBox, false);
                }
                return false;
            }

            if (!VerifiedRcts.ContainsKey(region.ExprVisual.BoundingBox))
            {
                VerifiedRcts.Add(region.ExprVisual.BoundingBox, null);
            }
            return null;
        }

        #region Verify Hit Test

        public void UpdateVerifyRegion(Rct bb)
        {
            Rct? deleteRct = null;

            foreach (var pair in VerifiedRcts)
            {
                if (pair.Key.IntersectsWith(bb)) deleteRct = pair.Key;
            }

            if (deleteRct != null) VerifiedRcts.Remove(deleteRct.Value);
        }

        public bool? VerifyRegion(Rct inputRct)
        {
            foreach (Rct rct in VerifiedRcts.Keys)
            {
                if (rct.Contains(inputRct) || rct.IntersectsWith(inputRct)) return VerifiedRcts[rct];
            }
            return null;
        }

        #endregion

      
        public QueryFeedbackState Query(out string msg,
                                        out object obj)
        {
            var state = Reasoner.Query(UserInputKnowledge, out msg, out obj);

            /*
             * 5.1. How many time the system shows the meta-cognition scaffolding user control?
             * 5.2. How many times the system shows the answer scaffolding user control?
             */
            //InteractiveAnalytics.Instance.AnalyzeState(state);

            return state;
        }

        #region Selection API

        public void Select(Rct rct, InputRegion ir, out string message)
        {
            //bool isSelect = false;
            Rct? intersectRct;
            //bool? tutorVerified = false;
            bool intersected = FindIntersectRct(rct, out intersectRct);
            if (!intersected)
            {
                SelectedRct = rct;
                #region Obsolete
                /*                if (HCIReasoner.Instance.TutorMode)
                {
                    tutorVerified = VerifyRegion(ir.ExprVisual.BoundingBox);
                    //if (tutorVerified == null || !tutorVerified.Value)
                    if (tutorVerified != null && !tutorVerified.Value)
                    {
                        SelectedRct = rct;
                    }
                }
                else
                {
                    SelectedRct = rct;
                }*/
                #endregion
                //isSelect = true;
            }
            else
            {
                Debug.Assert(intersectRct != null);
                SelectedRct = null;
            }
            SelectInDemonstration(ir, out message);

            #region Obsolete
            // Do not need to select in tutor mode
            /*   if (HCIReasoner.Instance.TutorMode)
            {
                SelectInTutorMode(ir, out message, isSelect, tutorVerified);
            }
            else
            {
                
            }*/
            #endregion
        }

        public void SelectInDemonstration(InputRegion ir, out string message)
        {
            message = null;
            //input -> not null: select, null: unselect
            if (Reasoner.QueriedKnowledge == null)
            {
                //select
                Debug.Assert(ir.Knowledge != null);
                Reasoner.QueriedKnowledge = ir.Knowledge;
                message = AGDemonstrationMessage.SelectAnswer;
                return;
            }
            if (Reasoner.QueriedKnowledge.Equals(ir.Knowledge))
            {
                //deselect
                Reasoner.QueriedKnowledge = null;
                message = AGDemonstrationMessage.DeSelectAnswer;
            }
            else
            {
                //select
                Debug.Assert(ir.Knowledge != null);
                Reasoner.QueriedKnowledge = ir.Knowledge;
                message = AGDemonstrationMessage.SelectAnswer;
            }
        }

        #region Hit Test Selection API

        public void UpdateSelectRegion(Rct bb)
        {
            var rct = SelectedRct as Rct?;
            if (rct != null)
            {
                if (rct.Value.IntersectsWith(bb))
                {
                    _selectedRct = null;
                    Reasoner.QueriedKnowledge = null;
                }
            }
        }

        private bool FindIntersectRct(Rct rct, out Rct? outputRct)
        {
            outputRct = null;
            if (SelectedRct == null) return false;

            var currentRct = SelectedRct as Rct?;
            if (currentRct == null) return false;

            if (rct.IntersectsWith(currentRct.Value))
            {
                outputRct = currentRct;
                return true;
            }
            return false;
        }

        public bool HitTestRegion(Pt pt)
        {
            if (SelectedRct == null) return false;
            var currentRct = SelectedRct as Rct?;
            if (currentRct == null) return false;
            return currentRct.Value.Contains(pt);
        }

        #endregion

        #endregion

        #endregion

        #region User select event

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}