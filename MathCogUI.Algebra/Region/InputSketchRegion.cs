﻿/*******************************************************************************
 * Math Interactive Tutoring System
 * <p>
 * Copyright (C) 2015 Bo Kang, Joseph J. LaViola Jr.
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

namespace MathCogUI
{
    using starPadSDK.MathExpr;
    using MathCog;
    using starPadSDK.Geom;
    using starPadSDK.MathRecognizer;

    /// <summary>
    /// Input Region: can be either knowledge or query.
    /// </summary>
    public class SketchKnowledgeRegion : InputRegion
    {
        public Expr OriginExpr { get; set; }

        public Parser.Range Range { get; set; }

        private Rct sketchBB;

        public SketchKnowledgeRegion(Parser.Range r, 
                                     IKnowledge k, 
                                     Rct bb,
                                     bool userInput)
            : base(k, userInput)
        {
            Range = r;
            sketchBB = bb;
            if (Range.Parse != null && Range.Parse.expr != null)
            {
                GenerateCurrentVisual(Range.Parse.expr, bb.TopLeft, DefaultColor);
            }
            RenderPosition = sketchBB.BottomLeft;//TODO 
        }
    }
}