﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using AlgebraGeometry;
using AlgebraGeometry.Expr;
using ExprSemantic;
using Microsoft.Ink;
using starPadSDK.Geom;
using starPadSDK.MathExpr;
using starPadSDK.MathRecognizer;

namespace Algebra.LogicSketch
{
    public class KnowledgeRegion : Region
    {
        public Parser.Range Range { get; set; }
        //public IKnowledge Knowledge { get; set; } //IKnowledgeExpr
        public IEnumerable<DrawingVisual> AttachedVisuals { get; set; } 
       
        /// <summary>
        /// r.Parse must be not null
        /// Knowledge could be null
        /// </summary>
        /// <param name="r"></param>
        /// <param name="k"></param>
        public KnowledgeRegion(Parser.Range r, IKnowledge k)
        {
            Range = r;
            Knowledge = k;
            InitBase();
        }

        public KnowledgeRegion(Parser.Range r, IKnowledge k, Pt drawPos)
        {
            Range = r;
            Knowledge = k;
            InitBase();
            DrawPos = drawPos;
        }

        private void InitBase()
        {
            if (Range.Parse != null && Range.Parse.expr != null)
            {
                DrawExpr = Range.Parse.expr;
            }
            //Rectangle rc = Range.Strokes.GetBoundingBox();
            //DrawPos = new Pt(rc.X,rc.Y);
        }

        private IEnumerable<DrawingVisual> Generate(Rct box)
        {
            #region null case

            if (Knowledge == null) // not knowledge
            {
                var dv = MathUtils.RetrieveDrawingVisual(new Pt(box.Left, box.Bottom + 24), Range.Parse.expr, Colors.Blue);
                //var dv = MathUtils.RetrieveDrawingVisual(new Pt(box.Left, box.Bottom + 24), Range, Colors.Blue);
                return new List<DrawingVisual>() { dv };
            }

            #endregion

            #region AGQueryExpr case

            var query = Knowledge as AGQueryExpr;
            if (query != null)
            {
                if (query.QuerySuccess)
                {
                    var dv = MathUtils.RetrieveDrawingVisual(new Pt(box.Left, box.Bottom + 24), query.Expr, Colors.Blue);
                    //var dv =  MathUtils.RetrieveDrawingVisual(new Pt(box.Left, box.Bottom + 24), keyExpr, Colors.Blue);
                    return new List<DrawingVisual>() { dv };
                }
                else
                {
                    var dv = MathUtils.RetrieveDrawingVisual(new Pt(box.Left, box.Bottom + 24), Range.Parse.expr, Colors.Blue);
                    //var dv =  MathUtils.RetrieveDrawingVisual(new Pt(box.Left, box.Bottom + 24), keyExpr, Colors.Blue);
                    return new List<DrawingVisual>() { dv };
                }
            }

            #endregion 

            #region AGPropertyExpr case

            var prop = Knowledge as AGPropertyExpr;
            if (prop != null)
            {
                var dv = MathUtils.RetrieveDrawingVisual(new Pt(box.Left, box.Bottom + 24), Range.Parse.expr, Colors.Blue);
                //var dv =  MathUtils.RetrieveDrawingVisual(new Pt(box.Left, box.Bottom + 24), keyExpr, Colors.Blue);
                return new List<DrawingVisual>() {dv};
            }

            #endregion 

            #region AGShapeExpr case 

            var shape = Knowledge as AGShapeExpr;
            if (shape != null)
            {
                if (shape.ShapeSymbol.Shape.CachedSymbols.Count == 0)
                {
                    //var dv = MathUtils.RetrieveDrawingVisual(new Pt(box.Left, box.Bottom + 24), keyExpr, Colors.Blue);
                    var dv = MathUtils.RetrieveDrawingVisual(new Pt(box.Left, box.Bottom + 24), Range.Parse.expr, Colors.Blue);
                    return new List<DrawingVisual>() { dv };                    
                }
                else
                {
                    var lst = new List<DrawingVisual>();
                    double gap = box.Bottom + 24;
                    //generation
                    IEnumerable<IKnowledge> shapes = shape.RetrieveGeneratedShapes();
                    foreach (IKnowledge k in shapes)
                    {
                        DrawingVisual dv = null;
                        try
                        {
                            dv = MathUtils.RetrieveDrawingVisual(new Pt(box.Left, gap), k.Expr, Colors.Blue);
                           
                        }
                        catch (Exception e)
                        {
                            
                        }
                        if(dv != null) lst.Add(dv);
                        gap += 24;
                    }
                    return lst;
                }
            }

            #endregion 
            
            return null;
        }

        public void GenerateVisuals(Rct box)
        {
            AttachedVisuals = Generate(box);
        }
    }
}