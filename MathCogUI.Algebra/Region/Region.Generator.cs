﻿/*******************************************************************************
 * Math Interactive Tutoring System
 * <p>
 * Copyright (C) 2015 Bo Kang, Joseph J. LaViola Jr.
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

namespace MathCogUI
{
    using MathCog;
    using MathCog.UserModeling;
    using starPadSDK.Geom;
    using System.Collections.Generic;
    using System.Diagnostics;
    using Color = System.Windows.Media.Color;

    public partial class InputRegion
    {
        private int yGap = 30;

        /// <summary>
        /// Select state re-assignment based on GenerateRegion
        /// </summary>
        public virtual void GenerateVisualRegions()
        {
            RenderedRegions.Clear();

            #region Non-Knowledge

            if (Knowledge == null) // not knowledge
            {
                var sketchRegion = this as SketchKnowledgeRegion;
                Debug.Assert(sketchRegion != null);

                sketchRegion.RenderedRegions = new List<GeneratedRegion>();
                var gr = new GeneratedRegion(sketchRegion.Knowledge);
                Color color;
                if (HCIReasoner.TutorMode)
                {
                    color = UserColor;
                }
                else
                {
                    color = UserInput ? UserColor : DefaultColor;
                }

                var pt = new Pt(sketchRegion.RenderPosition.X, sketchRegion.RenderPosition.Y + 24);
/*                var selectedRegion = QueryUIEngine.Instance.HitTestRegion(pt);
                if (selectedRegion)
                {
                    gr.IsSelected = true;
                }*/
                gr.GenerateCurrentVisual(sketchRegion.Range.Parse.expr, pt, color);
                sketchRegion.RenderedRegions.Add(gr);
                return;
            }
            #endregion

            #region Tutor Mode

            if (HCIReasoner.TutorMode)
            {
                var gr = new GeneratedRegion(Knowledge);
                var pt = new Pt(RenderPosition.X, RenderPosition.Y);                
                gr.GenerateCurrentVisual(Knowledge.Expr, pt, UserColor);
                /*var selectedRegion = QueryUIEngine.Instance.HitTestRegion(pt);
                if (selectedRegion)
                {
                    Knowledge.IsSelected = true;
                    gr.IsSelected = true;
                }
                gr.VerifySuccess = QueryUIEngine.Instance.VerifyRegion(ExprVisual.BoundingBox);
                */
                RenderedRegions.Add(gr);
                return;
            }

            #endregion

            #region Demonstration Mode

            RenderedRegions = new List<GeneratedRegion>();

            if (UserInput)
            {
                var gr = new GeneratedRegion(Knowledge);
                var pt = new Pt(RenderPosition.X, RenderPosition.Y + yGap);
               /* var selectedRegion = QueryUIEngine.Instance.HitTestRegion(pt);
                if (selectedRegion)
                {
                    Knowledge.IsSelected = true;
                    gr.IsSelected = true;
                }*/
                gr.GenerateCurrentVisual(Knowledge.Expr, pt, UserColor);
                RenderedRegions.Add(gr);
                return;
            }

            //Non-User Input
            var prop = Knowledge as AGPropertyExpr;
            var shape = Knowledge as AGShapeExpr;
            var queryExpr = Knowledge as AGQueryExpr;
            var equationExpr = Knowledge as AGEquationExpr;
            var traceExpr = Knowledge as IKnowledge;

            Debug.Assert(Knowledge != null);

            if (prop != null)
            {
                #region AGPropertyExpr case
                var gr = new GeneratedRegion(Knowledge);
                var pt = new Pt(RenderPosition.X, RenderPosition.Y + 24);
               /* var selectedRegion = QueryUIEngine.Instance.HitTestRegion(pt);
                if (selectedRegion)
                {
                    Knowledge.IsSelected = true;
                    gr.IsSelected = true;
                }*/
                gr.GenerateCurrentVisual(Knowledge.Expr, pt, DefaultColor);
                RenderedRegions.Add(gr);
                #endregion
            }
            else if (shape != null)
            {
                #region AGShapeExpr case
                double gap = RenderPosition.Y + yGap;
                //generation
                shape.RetrieveRenderKnowledge();
                if (shape.RenderKnowledge == null) return;
                foreach (IKnowledge k in shape.RenderKnowledge)
                {
                    var gr = new GeneratedRegion(Knowledge);
                    var pt = new Pt(RenderPosition.X, gap + yGap);
                   /* var selectedRegion = QueryUIEngine.Instance.HitTestRegion(pt);
                    if (selectedRegion)
                    {
                        k.IsSelected = true;
                        gr.IsSelected = true;
                    }*/
                    gr.GenerateCurrentVisual(k.Expr, pt, DefaultColor);
                    RenderedRegions.Add(gr);
                    gap += yGap;
                }
                #endregion
            }
            else if (queryExpr != null)
            {
                #region AGQueryExpr case
                var query = queryExpr.QueryTag;
                if (query.CachedEntities.Count != 0)
                {
                    double gap = RenderPosition.Y + 24;
                    queryExpr.RetrieveRenderKnowledge();
                    foreach (IKnowledge k in queryExpr.RenderKnowledge)
                    {
                        var gr = new GeneratedRegion(k);
                        var pt = new Pt(RenderPosition.X, gap + yGap);
                       /* var selectedRegion = QueryUIEngine.Instance.HitTestRegion(pt);
                        if (selectedRegion)
                        {
                            k.IsSelected = true;
                            gr.IsSelected = true;
                        }*/
                        gr.GenerateCurrentVisual(k.Expr, pt, DefaultColor);
                        RenderedRegions.Add(gr);
                        gap += yGap;
                    }
                }
                else
                {
                    double gap = RenderPosition.Y + yGap;
                    var gr = new GeneratedRegion(queryExpr);
                    var pt = new Pt(RenderPosition.X, gap + yGap);
                   /* var selectedRegion = QueryUIEngine.Instance.HitTestRegion(RenderPosition);
                    if (selectedRegion)
                    {
                        Knowledge.IsSelected = true;
                        gr.IsSelected = true;
                    }*/
                    gr.GenerateCurrentVisual(queryExpr.Expr, pt, DefaultColor);
                    RenderedRegions.Add(gr);
                }
                #endregion
            }
            else if (equationExpr != null)
            {
                #region AGEquationExpr case
                var equation = equationExpr.Equation;
                if (equation.CachedEntities.Count != 0)
                {
                    double gap = RenderPosition.Y + yGap;
                    equationExpr.RetrieveRenderKnowledge();
                    foreach (IKnowledge k in equationExpr.RenderKnowledge)
                    {
                        var gr = new GeneratedRegion(Knowledge);
                        var pt = new Pt(RenderPosition.X, gap + yGap);
                       /* var selectedRegion = QueryUIEngine.Instance.HitTestRegion(pt);
                        if (selectedRegion)
                        {
                            k.IsSelected = true;
                            gr.IsSelected = true;
                        }*/
                        gr.GenerateCurrentVisual(k.Expr, pt, DefaultColor);
                        RenderedRegions.Add(gr);
                        gap += yGap;
                    }
                }
                else
                {
                    double gap = RenderPosition.Y + yGap;
                    var gr = new GeneratedRegion(equationExpr);
                    var pt = new Pt(RenderPosition.X, gap + yGap);
                    /*var selectedRegion = QueryUIEngine.Instance.HitTestRegion(RenderPosition);
                    if (selectedRegion)
                    {
                        Knowledge.IsSelected = true;
                        gr.IsSelected = true;
                    }*/
                    gr.GenerateCurrentVisual(equationExpr.Expr, pt, DefaultColor);
                    RenderedRegions.Add(gr);
                }
                #endregion
            }
            else if (traceExpr != null)
            {
                var gr = new GeneratedRegion(Knowledge);
                var pt = new Pt(RenderPosition.X, RenderPosition.Y + yGap);
                /*var selectedRegion = QueryUIEngine.Instance.HitTestRegion(pt);
                if (selectedRegion)
                {
                    Knowledge.IsSelected = true;
                    gr.IsSelected = true;
                }*/
                gr.GenerateCurrentVisual(Knowledge.Expr, pt, DefaultColor);
                RenderedRegions.Add(gr);
            }

            #endregion 
        }
    }
}
