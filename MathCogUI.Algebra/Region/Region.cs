﻿/*******************************************************************************
 * Math Interactive Tutoring System
 * <p>
 * Copyright (C) 2015 Bo Kang, Joseph J. LaViola Jr.
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

namespace MathCogUI
{
    using MathCog;
    using starPadSDK.Geom;
    using starPadSDK.MathExpr;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Linq;
    using System.Windows.Media;
    using Color = System.Windows.Media.Color;

    /// <summary>
    /// The front-end knowledge representation.
    /// </summary>
    public abstract class Region : INotifyPropertyChanged
    {
        #region Properties and Constructors

        public ExprVisual ExprVisual { get; set; }

        public IKnowledge Knowledge { get; set; }

        public DrawingVisual CurrentDrawingVisual
        {
            get
            {
                if (IsSelected) return ExprVisual._selectedDV;
                return ExprVisual._unselectedDV;
            }
        }

        private bool _isSelected;
        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                _isSelected = value;
                if(Knowledge != null) Knowledge.IsSelected = value;
                RaisePropertyChangeEvent("IsSelected");

            }
        }

        protected Region(IKnowledge knowledge)
        {
            Knowledge = knowledge;
        }

        #endregion

        #region Utils

        public void GenerateCurrentVisual(Expr _expr, Pt _position, Color _color)
        {
            ExprVisual = new ExprVisual(_expr, _position, _color);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void RaisePropertyChangeEvent(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        #endregion

        #region Intersection Test

        public bool Intersect(object obj)
        {
            var point = obj as System.Windows.Point?;
            if (point != null) return Intersect(point.Value);

            var rect = obj as System.Windows.Rect?;
            if (rect != null) return Intersect(rect.Value);

            var pt = obj as Pt?;
            if (pt != null) return Intersect(pt.Value);

            var rct = obj as Rct?;
            if (rct != null) return Intersect(rct.Value);

            throw new Exception("Cannot reach here");
        }

        private bool Intersect(System.Windows.Point point)
        {
            var pt = new Pt(point.X, point.Y);
            return Intersect(pt);
        }

        private bool Intersect(System.Windows.Rect rect)
        {
            var rct = new Rct(rect.TopLeft, rect.BottomRight);
            return Intersect(rct);
        }

        private bool Intersect(Pt pt)
        {
            Debug.Assert(ExprVisual.BoundingBox != null);
            bool result = ExprVisual.BoundingBox.Contains(new Pt(pt.X, pt.Y));
            if (result)
            {
                IsSelected = !IsSelected;
            }
            return result;
        }

        private bool Intersect(Rct rect)
        {
            Debug.Assert(ExprVisual.BoundingBox != null);
            bool result = ExprVisual.BoundingBox.IntersectsWith(rect);
            if (result)
            {
                IsSelected = !IsSelected;
            }
                
            return result;
        }

        public virtual bool HitTest(object obj)
        {
            return false;
        }

        #endregion
    }

    public partial class InputRegion : Region
    {
        #region Properties and Constructors

        public bool UserInput;

        public static Color DefaultColor = Colors.Blue;
        public static Color UserColor = Colors.Black;

        public List<DrawingVisual> RenderVisuals { get; set; }
        public List<GeneratedRegion> RenderedRegions { get; set; }

        public Pt RenderPosition { get; set; }
        private bool _isRenderRegionSelected;
        public bool IsRenderRegionSelected
        {
            get { return _isRenderRegionSelected; }
            set
            {
                _isRenderRegionSelected = value;
                RaisePropertyChangeEvent("IsRenderRegionSelected");
            }
        }

        protected InputRegion(IKnowledge knowledge, bool userInput)
            : base(knowledge)
        {
            UserInput = userInput;
            RenderedRegions = new List<GeneratedRegion>();
            RenderVisuals = new List<DrawingVisual>();
            _isRenderRegionSelected = false;
        }

        #endregion

        #region Utils

        public override bool HitTest(object obj)
        {
            foreach (var gRegion in RenderedRegions)
            {
                bool result = gRegion.Intersect(obj);
                if (result)
                {
                    IsRenderRegionSelected = !_isRenderRegionSelected;
                    return true;
                }
            }
            return false;
        }

        #endregion
    }
}
