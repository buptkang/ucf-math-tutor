﻿/*******************************************************************************
 * Math Interactive Tutoring System
 * <p>
 * Copyright (C) 2015 Bo Kang, Joseph J. LaViola Jr.
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

namespace MathCogUI
{
    using starPadSDK.Geom;
    using starPadSDK.MathExpr;
    using System.ComponentModel;
    using System.Windows.Media;
    using Color = System.Windows.Media.Color;

    public class ExprVisual : INotifyPropertyChanged
    {
        #region Three properties to render expression

        private Expr _expr;
        public Expr DrawExpr
        {
            get { return _expr; }
            set
            {
                _expr = value;
                RaisePropertyChangeEvent("Expr");
                UpdateDrawingVisual();
            }
        }

        private Pt? _position;
        public Pt? DrawPos
        {
            get { return _position; }
            set
            {
                _position = value;
                RaisePropertyChangeEvent("Position");
                UpdateDrawingVisual();
            }
        }

        private Color? _color;
        public Color? Color
        {
            get { return _color; }
            set
            {
                _color = value;
                RaisePropertyChangeEvent("Color");
                UpdateDrawingVisual();
            }
        }

        private Rct _boundingBox;
        public Rct BoundingBox
        {
            get { return _boundingBox; }
            set { _boundingBox = value; }
        }

        public DrawingVisual _selectedDV;
        public DrawingVisual _unselectedDV;

        public ExprVisual(Expr expr, Pt position, Color color)
        {
            _expr = expr;
            _position = position;
            _color = color;
            UpdateDrawingVisual();
        }

        private Color _selectedColor = Colors.Red;

        public void UpdateDrawingVisual()
        {
            if (_expr != null && _position != null && _color != null)
            {
                Rct bb;
                _selectedDV = MathUtils.RetrieveDrawingVisual(
                        _position.Value, _expr, _selectedColor, out bb);
                _unselectedDV = MathUtils.RetrieveDrawingVisual(
                       _position.Value, _expr, _color.Value, out bb);
                BoundingBox = bb;
            }
        }

        #endregion

        #region Event Handler

        public event PropertyChangedEventHandler PropertyChanged;

        public void RaisePropertyChangeEvent(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        #endregion
    }
}
