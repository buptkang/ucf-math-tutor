﻿/*******************************************************************************
 * Math Interactive Tutoring System
 * <p>
 * Copyright (C) 2015 Bo Kang, Joseph J. LaViola Jr.
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

namespace MathCogUI
{
    using System.Windows.Media;
    using MathCog;
    using starPadSDK.Geom;

    /// <summary>
    /// The purpose of generated region is to render
    /// trace-step region 
    /// </summary>
    public partial class GeneratedRegion : Region
    {
        private bool? _verifySuccess;
        public bool? VerifySuccess
        {
            get { return _verifySuccess; }
            set
            {
                _verifySuccess = value;
            }
        }

        public static Color DefaultColor = Colors.BlueViolet;

        public object GeometryTag { get; set; }

        public GeneratedRegion(IKnowledge k) :base(k)
        {
          
        }

        public GeneratedRegion(IKnowledge k, System.Windows.Point pt)
            :base(k)
        {
            GenerateCurrentVisual(k.Expr, new Pt(pt.X, pt.Y), DefaultColor);
        }
    }
}
