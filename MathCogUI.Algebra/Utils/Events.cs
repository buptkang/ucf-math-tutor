﻿/*******************************************************************************
 * Math Interactive Tutoring System
 * <p>
 * Copyright (C) 2015 Bo Kang, Joseph J. LaViola Jr.
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

namespace MathCogUI
{
    using System.Collections.Generic;
    using System;

    public delegate void SystemDelegate(object sender, SystemEventArgs args);

    public class SystemEventArgs : EventArgs
    {
        public string Comment { get; set; }
        public bool ShowHint { get; set; }
        public MathCogUI.HintType Type;

        public SystemEventArgs(bool showHint, string str)
        {
            ShowHint = showHint;
            Comment = str;
            Type = HintType.System;
        }

        public SystemEventArgs(bool showHint, string str, MathCogUI.HintType _type)
        {
            ShowHint = showHint;
            Comment = "System Hint:" + str;
            Type = _type;
        }
    }

    public class ShowConceptHintArgs : EventArgs
    {
        public object Concept; //ConceptText or HybridText
        public ShowConceptHintArgs(object con)
        {
            Concept = con; 
        }
    }

    public class ShowStrategyHintArgs : EventArgs
    {
        public int Index;
        public List<string> Strategies;
        public ShowStrategyHintArgs(List<string> strats, int index)
        {
            Strategies = strats;
            Index = index;
        }
    }

    public delegate void ShowConceptHintDelegate(object sender, ShowConceptHintArgs args);

    public delegate void ShowStrategyHintDelegate(object sender, ShowStrategyHintArgs args);

}
