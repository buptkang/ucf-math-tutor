﻿/*******************************************************************************
 * Math Interactive Tutoring System
 * <p>
 * Copyright (C) 2015 Bo Kang, Joseph J. LaViola Jr.
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

namespace MathCogUI
{
    using starPadSDK.Geom;
    using starPadSDK.MathExpr;
    using starPadSDK.WPFHelp;
    using System.Windows;
    using System.Windows.Media;

    public class MathUtils
    {
        

        public static DrawingVisual RetrieveDrawingVisual(Pt pt, Expr expr, Color color
            , out Rct currBoundingBox)
        {
            var dv = new DrawingVisual();
            DrawingContext dc = dv.RenderOpen();
            // this is an example of normal drawing of an expr
            currBoundingBox = starPadSDK.MathExpr.ExprWPF.EWPF.DrawTop(expr, 25, dc, color, pt, true).rect;
            dc.Close();
            return dv;
        }

        public static Pt RandomDrawPosition(ContainerVisualHost Underlay)
        {
            if (Underlay.Children.Count == 0)
            {
                return new Pt(100.0, 100.0);
            }
            else
            {
                var lowestRect = new Rect(new System.Windows.Point(0.0, 0.0), new Size(0.1, 0.1));
                double contentHeight = double.MinValue;
                foreach (Visual temp in Underlay.Children)
                {
                    var dv = temp as DrawingVisual;
                    Rect rct = dv.ContentBounds;
                    if (rct.Bottom > lowestRect.Bottom)
                    {
                        lowestRect = rct;
                    }
                    if (rct.Height > contentHeight)
                    {
                        contentHeight = rct.Height;
                    }
                }
                return new Pt(lowestRect.BottomLeft.X, lowestRect.BottomLeft.Y + contentHeight);
            }
        }

    }
}
