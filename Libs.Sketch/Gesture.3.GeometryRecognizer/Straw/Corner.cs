﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starPadSDK.DollarRecognizer;

namespace Segmentation.ShortStraw
{
    public class Corner
    {
        private bool isCorner;
        private PointR point;
        private double straw;

        public Corner(PointR point)
            : this(point, 0.0, false)
        {
        }

        public Corner(PointR point, double straw)
            : this(point, straw, false)
        {
        }

        public Corner(PointR point, double straw, bool isCorner)
        {
            this.point = point;
            this.straw = straw;
            this.isCorner = isCorner;
        }

        public PointR Point
        {
            get { return point; }
            set { point = value; }
        }

        public double Straw
        {
            get { return straw; }
            set { straw = value; }
        }

        public bool IsCorner
        {
            get { return isCorner; }
            set { isCorner = value; }
        }

        public override bool Equals(object obj)
        {
            if (obj is Corner)
            {
                Corner c = (Corner)obj;
                return (Point.Equals(c.Point) && Straw.Equals(c.Straw)
                    && IsCorner.Equals(c.IsCorner));
            }
            return false;
        }

        public override int GetHashCode()
        {
            return this.Point.GetHashCode();
        }
    }
}
