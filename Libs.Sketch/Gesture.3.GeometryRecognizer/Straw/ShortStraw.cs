﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starPadSDK.DollarRecognizer;


namespace Segmentation.ShortStraw
{
    public class ShortStraw
    {
        /**
         The main ShortStraw Corner Finding Algorithm    
        */
        public static ArrayList ShortStrawCornerFinding(List<PointR> points)
        {
            List<PointR> resampledPoints = starPadSDK.DollarRecognizer.Utils.Resample(points, 64);
            
            ArrayList corners = Utils.GetCornerShortStraw(new ArrayList(resampledPoints));
            /*
            var returnPoints = new ArrayList();

            foreach (Corner corner in corners)
            {
                if(corner.IsCorner)
                {
                    returnPoints.Add(corner.Point);                   
                }
            }
            return returnPoints;
             * */
            return corners;
        }

        public static ArrayList IStrawCornerFinding(List<PointR> points)
        {
            List<PointR> resampledPoints = starPadSDK.DollarRecognizer.Utils.Resample(points, 64);

            ArrayList corners = Utils.GetCornersIStraw(new ArrayList(resampledPoints));

            /*
            var returnPoints = new ArrayList();

            foreach (Corner corner in corners)
            {
                if (corner.IsCorner)
                {
                    returnPoints.Add(corner.Point);
                }
            }
            return returnPoints;
             * */
            return corners;
        }
    }
}
