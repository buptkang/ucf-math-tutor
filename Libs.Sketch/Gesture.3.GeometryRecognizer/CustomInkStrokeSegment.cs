﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using Segmentation.ShortStraw;
using starPadSDK.DollarRecognizer;
using Utils = Segmentation.ShortStraw.Utils;

namespace _020.AGGeometryRecognizer.SingleStrokeRecognizer
{
    /// <summary>
    /// A stroke segment.
    /// The segment must exactly have two corners.
    /// </summary>
    public class CustomInkStrokeSegment
    {
        #region public Properties
        public ArrayList CornerPoints { get; set; } //List of corners(Corner) for the current Stroke Segment
        public ArrayList ShortStrawCornerPoints { get; set; } // List of ShortStraw(Corner) for the current Stroke segment 
        public ArrayList Points { get; set; } // List of (PointR) for the current Stroke Segment        
        public int PreNumCorners { get; set; } // IStrawCorners
        public int NextNumCorners { get; set; } // ShortStrawCorners, in general ShortStrawCorners > IStrawCorners

        public Rect BoundingBox { get; set; }
        public bool IsCurve { get; set; } //Is Bezier Curve Or Not 
        public bool IsNoise { get; set; }
        public double Distance { get; set; }
        public PointR CentroidPt { get; set; }
        public double Slope { get; set; } // slope of the line
        public double AverageSlope { get; set; }

        public double XDiff { get; set; }
        public double YDiff { get; set; }
        public SegmentTypeEnum SegmentType { get; set; }

        public double InkLength
        {
            get
            {
                double length = 0.0f;
                for (int i = 0; i < Points.Count - 1; i++)
                {
                    var pt1 = (PointR)Points[i];
                    var pt2 = (PointR)Points[i + 1];
                    length += Utils.ComputeDistanceBetweenTwoPoints(pt1, pt2);
                }
                return length;
            }
        }

        #endregion

        #region public Constructors

        /// <summary>
        /// Line Segment
        /// </summary>
        /// <param name="cornerPoints"></param>
        /// <param name="recursive"></param>
        public CustomInkStrokeSegment(ArrayList cornerPoints, bool recursive)
        {
            var startCorner = cornerPoints[0] as Corner;
            var endCorner = cornerPoints[cornerPoints.Count - 1] as Corner;
            Points = new ArrayList(); // Original PointR sample points for current segment
            CornerPoints = cornerPoints;

            foreach (Corner corner in CornerPoints)
            {
                Points.Add(corner.Point);
            }

            BoundingBox = Utils.ComputeBoundingBox(Points);

            XDiff = Math.Pow(startCorner.Point.X - endCorner.Point.X, 2.0) / (BoundingBox.Width * BoundingBox.Height);
            YDiff = Math.Pow(startCorner.Point.Y - endCorner.Point.Y, 2.0) / (BoundingBox.Width * BoundingBox.Height);

            IsCurve = false;
            Slope = ComputeLineSlope(startCorner.Point, endCorner.Point);

            if (XDiff < 0.1)
                SegmentType = SegmentTypeEnum.VerticalLine;
            else if (YDiff < 0.1)
                SegmentType = SegmentTypeEnum.HorizontalLine;
            else if (Slope > 0.0)
                SegmentType = SegmentTypeEnum.NegativeLine;
            else if (Slope < 0.0)
                SegmentType = SegmentTypeEnum.PositiveLine;

            Distance = Utils.EuclidenaDistancePointR(startCorner.Point, endCorner.Point);

            int centralIndex = Points.Count / 2 - 1;
            CentroidPt = (PointR)Points[centralIndex];

            PointR startPt = (PointR)Points[0];
            PointR endPt = (PointR)Points[Points.Count - 1];


            AverageSlope = ComputeAverageSlope(Points);

            //LineConstraint = new LineConstraint(startPt.X, startPt.Y, endPt.X, endPt.Y, AverageSlope);
        }

        public CustomInkStrokeSegment(ArrayList cornerPoints)
            : base()
        {
            var startCorner = cornerPoints[0] as Corner;
            var endCorner = cornerPoints[cornerPoints.Count - 1] as Corner;
            //var startPoint = (PointR) Points[0];
            //var endPoint = (PointR)Points[Points.Count - 1];
            Points = new ArrayList(); // Original PointR sample points for current segment
            PreNumCorners = 0;
            NextNumCorners = 0;
            CornerPoints = cornerPoints;

            //double maxAngle = 0.0;
            foreach (Corner corner in CornerPoints)
            {
                Points.Add(corner.Point);
                if (corner.IsCorner)
                {
                    PreNumCorners++;
                }
            }

            PointR startPt = (PointR)Points[0];
            PointR endPt = (PointR)Points[Points.Count - 1];

            AverageSlope = ComputeAverageSlope(Points);
            //LineConstraint = new LineConstraint(startPt.X, startPt.Y, endPt.X, endPt.Y, AverageSlope);

            BoundingBox = Utils.ComputeBoundingBox(Points);

            XDiff = Math.Pow(startCorner.Point.X - endCorner.Point.X, 2.0) / (BoundingBox.Width * BoundingBox.Height);
            YDiff = Math.Pow(startCorner.Point.Y - endCorner.Point.Y, 2.0) / (BoundingBox.Width * BoundingBox.Height);

            ShortStrawCornerPoints = Segmentation.ShortStraw.Utils.GetCornerShortStraw(Points);

            foreach (Corner corner in ShortStrawCornerPoints)
            {
                if (corner.IsCorner)
                {
                    NextNumCorners++;
                    //CurveMiddleCornerPoints.Add(corner.Point);
                }
            }
            if (NextNumCorners != PreNumCorners)
            {
                this.IsCurve = true;
            }

            //Label the segment as one of the primitive type
            if (!this.IsCurve) // more than two corner points in the current segment
            {
                //line or close circle
                Slope = ComputeLineSlope(startCorner.Point, endCorner.Point);
                Distance = Utils.EuclidenaDistancePointR(startCorner.Point, endCorner.Point);
                int centralIndex = Points.Count / 2 - 1;
                CentroidPt = (PointR)Points[centralIndex];

                if (XDiff < 0.1)
                    SegmentType = SegmentTypeEnum.VerticalLine;
                else if (YDiff < 0.1)
                    SegmentType = SegmentTypeEnum.HorizontalLine;
                else if (Slope > 0.0)
                    SegmentType = SegmentTypeEnum.NegativeLine;
                else if (Slope < 0.0)
                    SegmentType = SegmentTypeEnum.PositiveLine;
            }
            else
            {
                double tempDist = 0.0f;
                for (int i = 1; i < Points.Count; i++)
                {
                    tempDist = Utils.EuclidenaDistancePointR((PointR)Points[i - 1], (PointR)Points[i]);
                    Distance += tempDist;
                }
                CentroidPt = starPadSDK.DollarRecognizer.Utils.Centroid(Utils.AGConvertArrayList(Points));
            }
        }


        public double ComputeAverageSlope(ArrayList pointRs)
        {
            double slopeSum = 0.0;

            PointR first, next;
            for (int i = 0; i < pointRs.Count - 1; i++)
            {
                first = (PointR)pointRs[i];
                next = (PointR)pointRs[i + 1];
                slopeSum += (next.Y - first.Y) / (next.X - first.X);
            }

            return slopeSum / (pointRs.Count - 1);
        }

        private double ComputeLineSlope(PointR p1, PointR p2)
        {
            return (p2.Y - p1.Y) / (p2.X - p1.X);
        }

        #endregion

        #region public Methods

        public ArrayList ExtractSubSegments()
        {
            var subSegments = new ArrayList();

            //Split this arc segment into line segments to store
            int prevCornerIndex = -1;
            for (int j = 0; j < ShortStrawCornerPoints.Count; j++)
            {
                var tempCorner = (Corner)ShortStrawCornerPoints[j];
                if (tempCorner.IsCorner)
                {
                    if (prevCornerIndex == -1)
                    {
                        prevCornerIndex = j;
                    }
                    else
                    {
                        subSegments.Add(new CustomInkStrokeSegment(ShortStrawCornerPoints.GetRange(prevCornerIndex, j - prevCornerIndex + 1), true));
                        prevCornerIndex = j;
                    }
                }
            }

            return subSegments;
        }

        public StylusPointCollection GetStylusPointCollection()
        {
            var stylusPoints = new StylusPointCollection();

            for (int i = 0; i < Points.Count; i++)
            {
                PointR tempPointR = (PointR)Points[i];
                StylusPoint temp = new StylusPoint(tempPointR.X, tempPointR.Y);
                stylusPoints.Add(temp);
            }
            return stylusPoints;
        }

        public bool OverlapSegment(CustomInkStrokeSegment seg)
        {
            ArrayList subPoints = seg.Points;

            //First check Euclidean distance between start corner point and end corner point 


            //First check 

            for (int i = 0; i < subPoints.Count; i++)
            {
            }
            return false;
        }

        #endregion

    }

    public enum SegmentTypeEnum
    {
        Curve, PositiveLine, NegativeLine, VerticalLine, HorizontalLine
    }

    /**
   * Uni Stroke Segmentation and corner detection. 
   *
   */
    public class PaleoSketch
    {
        #region Properties
        private static PaleoSketch _paleoSketch;

        private PaleoSketch()
        {
        }

        public static PaleoSketch Instance
        {
            get
            {
                if (_paleoSketch == null)
                {
                    _paleoSketch = new PaleoSketch();
                }
                return _paleoSketch;
            }
        }

        public ArrayList Points { get; set; }

        #endregion

        #region Public Methods

        public void FetchDataPoints(ArrayList points)
        {
            Points = points;
        }

        #region Obsolete
        /*
        public ArrayList SegmentStrawStroke()
        {
            //var iStrawCorners = Segmentation.ShortStraw.ShortStraw.IStrawCornerFinding(Points);

            var StrawCorners = Segmentation.ShortStraw.ShortStraw.ShortStrawCornerFinding(Points);

            var segments = new ArrayList();
            CustomInkStrokeSegment segment;
            int index = 0;
            int prevIndex = 0;
            Corner corner = null;
            Corner preCorner = null;
            for (int i = 0; i < StrawCorners.Count; i++)
            {
                corner = (Corner)StrawCorners[i];

                if (corner.IsCorner && index == 0)
                {
                    index++;
                    prevIndex = i;
                    continue;
                }
                if (corner.IsCorner && index == 1)
                {
                    index = 0;
                    segment = new CustomInkStrokeSegment(StrawCorners.GetRange(prevIndex, i - prevIndex + 1));
                    segments.Add(segment);
                    i--;
                }
            }
            //segment time analysis
            //Remove segment with short time noise flick motion in the last
            if (segments.Count > 1)
            {
                CustomInkStrokeSegment seg = segments[segments.Count - 1] as CustomInkStrokeSegment;
                #region noise segment checking

                //1: time interval
                double timeIntervalForLast = Utils.ComputeTimeInterval(seg.Points);
                double maxTimeInterval = double.NegativeInfinity;
                double timeIntervalThreshold = 0.35;

                double longDistForLast = Utils.FindLongestDistance(seg.Points);
                double maxLongDist = double.NegativeInfinity;
                double distThreshold = 0.20;

                for (int k = 0; k < segments.Count - 1; k++)
                {
                    CustomInkStrokeSegment segPrev = segments[k] as CustomInkStrokeSegment;
                    double timeInterval = Utils.ComputeTimeInterval(segPrev.Points);
                    double longDist = Utils.FindLongestDistance(segPrev.Points);

                    if (timeInterval > maxTimeInterval)
                    {
                        maxTimeInterval = timeInterval;
                    }

                    if (longDist > maxLongDist)
                    {
                        maxLongDist = longDist;
                    }
                }

                //timer = timeIntervalForLast / maxTimeInterval + " " + longDistForLast / maxLongDist;
                if (timeIntervalForLast / maxTimeInterval < timeIntervalThreshold &&
                    longDistForLast / maxLongDist < distThreshold)
                {
                    seg.IsNoise = true;
                }

                #endregion
            }
            return segments;
        }*/
        #endregion

        //public static string timer = null;
        public ArrayList SegmentIStrawStroke()
        {
            var iStrawCorners = Segmentation.ShortStraw.ShortStraw.IStrawCornerFinding(Utils.AGConvertArrayList(Points));
            var segments = new ArrayList();
            CustomInkStrokeSegment segment;
            int index = 0;
            int prevIndex = 0;
            Corner corner = null;
            //Corner preCorner = null;
            for (int i = 0; i < iStrawCorners.Count; i++)
            {
                corner = (Corner)iStrawCorners[i];

                if (corner.IsCorner && index == 0)
                {
                    index++;
                    prevIndex = i;
                    continue;
                }
                if (corner.IsCorner && index == 1)
                {
                    index = 0;
                    ArrayList tmpCornerPoints = iStrawCorners.GetRange(prevIndex, i - prevIndex + 1);
                    if (tmpCornerPoints.Count > 5)
                    {
                        segment = new CustomInkStrokeSegment(tmpCornerPoints);
                        //ConstraintInference.Instance.Infer(segments, segment);
                        segments.Add(segment);
                        i--;
                    }
                }
            }

            //segment time analysis
            //Remove segment with short time noise flick motion in the last
            if (segments.Count > 1)
            {
                /*
                Console.WriteLine("Stroke Information:");
                Console.WriteLine("Points Count: " + Points.Count);
                Console.WriteLine("Stroke Points Time: " + Utils.ComputeTimeInterval(Points));
                Console.WriteLine("Stroke Points Length: " + Utils.ComputeInkLength(Points));
                Console.WriteLine();

                int counter = 1;
                foreach(CustomInkStrokeSegment segtmp in segments)
                {
                    Console.WriteLine("Segment Nmbr: "         + counter);
                    Console.WriteLine("Segment Points Count: " + segtmp.Points.Count);
                    Console.WriteLine("Segment Points Time: "  + Utils.ComputeTimeInterval(segtmp.Points));
                    Console.WriteLine("Segment Points Length: "  + Utils.ComputeInkLength(segtmp.Points));
                    Console.WriteLine();
                    counter++;
                }
                */
                #region noise segment checking

                double totalTime = Utils.ComputeTimeInterval(Points);
                double totalLength = Utils.ComputeInkLength(Points);

                const double timeIntervalThreshold = 0.2;
                const double lengthIntervalThreshold = 0.2;
                foreach (CustomInkStrokeSegment segtmp in segments)
                {
                    var localTime = Utils.ComputeTimeInterval(segtmp.Points);
                    var localLength = Utils.ComputeInkLength(segtmp.Points);

                    if ((localTime / totalTime < timeIntervalThreshold) || (localLength / totalLength) < lengthIntervalThreshold)
                    {
                        segtmp.IsNoise = true;
                    }
                }

                #endregion
            }
            return segments;
        }

        /// <summary>
        ///     Test only: Get IShortStrawCorners
        /// </summary>
        /// <returns>ArrayList of Segmentation.ShortStraw.Corner </returns>
        public ArrayList GetIShortStrawCorners()
        {
            var shortStrawCorners = Segmentation.ShortStraw.ShortStraw.IStrawCornerFinding(Utils.AGConvertArrayList(Points));
            var corners = new ArrayList();
            foreach (Corner corner in shortStrawCorners)
            {
                if (corner.IsCorner)
                {
                    corners.Add(corner.Point);
                }
            }
            return corners;
        }

        public ArrayList GetShortStrawCorners()
        {
            var shortStrawCorners = Segmentation.ShortStraw.ShortStraw.ShortStrawCornerFinding(Utils.AGConvertArrayList(Points));
            var corners = new ArrayList();
            foreach (Corner corner in shortStrawCorners)
            {
                if (corner.IsCorner)
                {
                    corners.Add(corner.Point);
                }
            }
            return corners;
        }

        #endregion
    }
}
