﻿namespace ShapeRecognizer
{
    public class GeometryRecognizer
    {
        #region Singleton

        private static GeometryRecognizer instance;

        private GeometryRecognizer() { }

        public static GeometryRecognizer Instance
        {
            get { return instance ?? (instance = new GeometryRecognizer {RecognizedSymbol = GeometryTypes.None}); }
        }

        #endregion

        public GeometryTypes RecognizedSymbol { get; set; }
       
        public void Reset()
        {
            RecognizedSymbol = GeometryTypes.None;
        }
    }
}
