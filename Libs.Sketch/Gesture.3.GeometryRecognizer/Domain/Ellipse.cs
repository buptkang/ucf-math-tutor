﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.RightsManagement;
using System.Text;

namespace GeometryShapeRecognizer
{
    public class AGEllipse
    {
        public AGEllipse(AGPoint center, double majarSemi, double minorSemi)
        {
            Center = center;
            MinorSemi = minorSemi;
            MajarSemi = majarSemi;
        }

        public AGPoint Center { get; set; }
        public double MajarSemi { get; set; }
        public double MinorSemi { get; set; }
    }
}
