﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GeometryShapeRecognizer
{
    public class AGPoint
    {
        public AGPoint(double x, double y)
        {
            X = x;
            Y = y;
        }

        public double X { get; set; }
        public double Y { get; set; }
    }
}
