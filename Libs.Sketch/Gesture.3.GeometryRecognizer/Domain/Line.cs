﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GeometryShapeRecognizer
{
    [Serializable]
    public class AGLine
    {
        public double X0 { set; get; }
        public double Y0 { set; get; }
        public double X1 { set; get; }
        public double Y1 { set; get; }
       
        public AGLine(double slope, double intercept)
        {
            Slope = slope;
            Intercept = intercept;
        }

        public double Slope { get; set; }
        public double Intercept { get; set; }
    }
}
