﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShapeRecognizer
{
    public enum GeometryTypes
    {
        Point = 101,
        Line = 102,
        Circle = 103,
        Ellipse = 104,
        None,
    }
}
