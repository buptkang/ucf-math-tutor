﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GeometryShapeRecognizer
{
    public class AGCircle
    {
        public AGCircle(AGPoint center, double radius)
        {
            Center = center;
            Radius = radius;
        }

        public AGPoint Center;
        public double Radius;
    }
}
