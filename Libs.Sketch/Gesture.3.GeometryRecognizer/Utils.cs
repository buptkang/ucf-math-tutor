﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Windows;
using System.Windows.Shapes;
using System.Windows.Controls;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml;

using Segmentation.ShortStraw;
using starPadSDK.DollarRecognizer;
using Brushes = System.Windows.Media.Brushes;
using Path = System.IO.Path;
using Pen = System.Windows.Media.Pen;
using Point = System.Windows.Point;
using Rectangle = System.Drawing.Rectangle;

namespace _020.AGGeometryRecognizer.SingleStrokeRecognizer
{
    public class Utils
    {
        public static List<PointR> AGConvertArrayList(ArrayList lst)
        {
            return lst.Cast<PointR>().ToList();
        }

        #region 2D Conversion

        public static PointR ConvertStylusPointToPointR(StylusPoint point)
        {
            return new PointR(point.X, point.Y);
        }


        public static StylusPoint ConvertPointRToStylusPoint(PointR point)
        {
            return new StylusPoint(point.X, point.Y);
        }

        public static ArrayList ConvertMsStrokeToStroke(Stroke stroke)
        {
            var list = new ArrayList(stroke.StylusPoints.Count);
            foreach (StylusPoint point in stroke.StylusPoints)
            {
                list.Add(ConvertStylusPointToPointR(point));
            }
            return list;
        }

        public static Stroke ConvertStrokeToMsStroke(ArrayList list)
        {
            var collection = new StylusPointCollection();
            foreach (PointR point in list)
            {
                collection.Add(ConvertPointRToStylusPoint(point));
            }
            return new Stroke(collection);
        }

        public static ArrayList ConvertMsStrokeCollectionToStrokeCollection(StrokeCollection strokes)
        {
            var myStrokes = new ArrayList(strokes.Count);
            foreach (Stroke stroke in strokes)
            {
                myStrokes.Add(ConvertMsStrokeToStroke(stroke));
            }

            return myStrokes;
        }

        public static StrokeCollection ConvertStrokeCollectionToMsStrokeCollection(ArrayList strokes)
        {
            var collection = new StrokeCollection();
            foreach (ArrayList stroke in strokes)
            {
                collection.Add(ConvertStrokeToMsStroke(stroke));
            }
            return collection;
        }

        public static List<Recognizer.NDollar.PointR> ConvertMsStrokeToList(Stroke stk)
        {
            var lst = stk.StylusPoints.Select(ConvertStylusPointToNPointR).ToList();
            return lst;
        }

        public static List<starPadSDK.DollarRecognizer.PointR> ConvertMsStrokeToList2(Stroke stk)
        {
            var lst = stk.StylusPoints.Select(ConvertStylusPointToNPointR2).ToList();
            return lst;
        }

        public static List<List<Recognizer.NDollar.PointR>> ConvertMsStrokeCollectionToStrokeList(StrokeCollection stks)
        {
            return stks.Select(ConvertMsStrokeToList).ToList();
        }


        public static List<List<starPadSDK.DollarRecognizer.PointR>> ConvertMsStrokeCollectionToStrokeList2(StrokeCollection stks)
        {
            return stks.Select(ConvertMsStrokeToList2).ToList();
        }

        public static starPadSDK.DollarRecognizer.PointR ConvertStylusPointToNPointR2(StylusPoint stylusPoint)
        {
            return new starPadSDK.DollarRecognizer.PointR(stylusPoint.X, stylusPoint.Y);
        }


        public static Recognizer.NDollar.PointR ConvertStylusPointToNPointR(StylusPoint stylusPoint)
        {
            return new Recognizer.NDollar.PointR(stylusPoint.X, stylusPoint.Y);
        }

        public static StylusPoint CovnertNPointRToStylusPoint(Recognizer.NDollar.PointR point)
        {
            return new StylusPoint(point.X, point.Y);
        }

        public static Stroke ConvertStrokeLstToMsStroke(List<Recognizer.NDollar.PointR> list)
        {
            var collection = new StylusPointCollection();
            foreach (Recognizer.NDollar.PointR point in list)
            {
                collection.Add(CovnertNPointRToStylusPoint(point));
            }
            return new Stroke(collection);
        }

        public static StrokeCollection ConvertStrokeListToMsStrokeCollection(List<List<Recognizer.NDollar.PointR>> strokes)
        {
            var collection = new StrokeCollection();
            foreach (List<Recognizer.NDollar.PointR> stroke in strokes)
            {
                collection.Add(ConvertStrokeLstToMsStroke(stroke));
            }

            return collection;
        }

        //Drawing Purpose
        public static ArrayList ConvertPointRsToWindowsPoints(ArrayList pointRs)
        {
            var windows2DPoints = new ArrayList();
            Point temp;
            foreach (PointR pointR in pointRs)
            {
                temp = new Point(pointR.X, pointR.Y);
                windows2DPoints.Add(temp);
            }
            return windows2DPoints;
        }

        #endregion

        #region 3D Conversion

        public static PointR ConvertStylusPointToPointR(StylusPoint point, int T)
        {
            return new PointR(point.X, point.Y, T);
        }

        public static ArrayList ConvertStylusPointsToPoint3Rs(StylusPointCollection points, ArrayList pointsTimers)
        {
            ArrayList point3Rs = new ArrayList(points.Count);
            PointR pointR;
            for (int i = 0; i < points.Count; i++)
            {
                pointR = ConvertStylusPointToPointR(points[i], (int)pointsTimers[i]);
                point3Rs.Add(pointR);
            }
            return point3Rs;
        }

        /*
        public static Point3R ConvertStylusPointToPoint3R(StylusPoint point, long T)
        {
            return new Point3R(ConvertStylusPointToPointR(point), T);
        }

        public static long[] RetrieveTimeArrayFromStroke(Stroke stroke)
        {
            var times = stroke.GetPropertyData(Recognizer.DollarT.Utils.TimingGuid) as long[];
            return times;
        }

         * */
        #endregion

        #region BoundingBox Boolean Operation

        public static PointR ComputeCentralInBB(Rect bb)
        {
            return new PointR(bb.TopLeft.X + bb.Width / 2, bb.TopLeft.Y + bb.Height / 2);
        }

        public static Rect ComputeBoundingBox(PointR p1, PointR p2, PointR p3, PointR p4)
        {
            double xMin = Math.Min(Math.Min(Math.Min(p1.X, p2.X), p3.X), p4.X);
            double yMin = Math.Min(Math.Min(Math.Min(p1.Y, p2.Y), p3.Y), p4.Y);
            double xMax = Math.Max(Math.Max(Math.Max(p1.X, p2.X), p3.X), p4.X);
            double yMax = Math.Max(Math.Max(Math.Max(p1.Y, p2.Y), p3.Y), p4.Y);
            return new Rect(new Point(xMin, yMin), new Point(xMax, yMax));
        }


        public static Rect ComputeBoundingBox(StylusPointCollection MyStylusPoints)
        {
            double minX = Double.PositiveInfinity, minY = Double.PositiveInfinity, maxX = Double.NegativeInfinity, maxY = Double.NegativeInfinity;
            foreach (StylusPoint pt in MyStylusPoints)
            {
                if (pt.X < minX)
                    minX = pt.X;
                if (pt.X > maxX)
                    maxX = pt.X;
                if (pt.Y < minY)
                    minY = pt.Y;
                if (pt.Y > maxY)
                    maxY = pt.Y;
            }
            return new Rect(minX, minY, maxX - minX, maxY - minY);
        }

        public static Rect ComputeBoundingBox(ArrayList Points)
        {
            double minX = Double.PositiveInfinity, minY = Double.PositiveInfinity, maxX = Double.NegativeInfinity, maxY = Double.NegativeInfinity;
            foreach (PointR pt in Points)
            {
                if (pt.X < minX)
                    minX = pt.X;
                if (pt.X > maxX)
                    maxX = pt.X;
                if (pt.Y < minY)
                    minY = pt.Y;
                if (pt.Y > maxY)
                    maxY = pt.Y;
            }
            return new Rect(minX, minY, maxX - minX, maxY - minY);
        }

        public static Rect ComputeBoundingBox(List<Recognizer.NDollar.PointR> points)
        {
            double minX = Double.PositiveInfinity, minY = Double.PositiveInfinity, maxX = Double.NegativeInfinity, maxY = Double.NegativeInfinity;
            foreach (Recognizer.NDollar.PointR pt in points)
            {
                if (pt.X < minX)
                    minX = pt.X;
                if (pt.X > maxX)
                    maxX = pt.X;
                if (pt.Y < minY)
                    minY = pt.Y;
                if (pt.Y > maxY)
                    maxY = pt.Y;
            }
            return new Rect(minX, minY, maxX - minX, maxY - minY);
        }

        #endregion

        #region stroke preprocessing

        public static void RemoveNoisePoints(ref ArrayList DollarPoints)
        {
            //Noise deduction: Reduce those points with same X and Y
            PointR point0, point1;
            for (int i = 1; i < DollarPoints.Count; i++)
            {
                point0 = (PointR)DollarPoints[i - 1];
                point1 = (PointR)DollarPoints[i];
                if (Math.Abs(point0.X - point1.X) < Double.Epsilon
                    && Math.Abs(point0.Y - point1.Y) < Double.Epsilon)
                {
                    DollarPoints.Remove(point0);
                }
            }
        }

        #endregion

        #region Drawing Method

        public static System.Windows.Shapes.Path ToRectangle(Rect value)
        {
            System.Windows.Shapes.Path myPath1 = new System.Windows.Shapes.Path();
            myPath1.Stroke = Brushes.Red;
            myPath1.StrokeThickness = 1.0;
            RectangleGeometry myRectangleGeometry1 = new RectangleGeometry();
            myRectangleGeometry1.Rect = value;
            GeometryGroup geoGroup = new GeometryGroup();
            geoGroup.Children.Add(myRectangleGeometry1);
            myPath1.Data = geoGroup;
            return myPath1;
        }

        public static Polygon DrawTriangle(PointR pt1, PointR pt2, PointR pt3)
        {
            Polygon ply = new Polygon();

            ply.Points.Add(new Point(pt1.X, pt1.Y));
            ply.Points.Add(new Point(pt2.X, pt2.Y));
            ply.Points.Add(new Point(pt3.X, pt3.Y));
            ply.Points.Add(new Point(pt1.X, pt1.Y));

            ply.Stroke = Brushes.Gray;
            ply.StrokeThickness = 2.0;
            Brush sh = new SolidColorBrush(Colors.Gray);
            sh.Opacity = 0.9;
            ply.Fill = sh;

            return ply;
        }


        public static void DrawLineSegment(DrawingContext drawingContext, CustomInkStrokeSegment segment)
        {
            //PointR startPointR = (PointR)segment.Points[0];
            //PointR endPointR = (PointR)segment.Points[segment.Points.Count - 1]; 
            //Point startPoint = new Point(startPointR.X, startPointR.Y);
            //Point endPoint = new Point(endPointR.X, endPointR.Y);

            //Point startPoint = new Point(segment.LineConstraint.X0, segment.LineConstraint.Y0);
            //Point endPoint = new Point(segment.LineConstraint.X1, segment.LineConstraint.Y1);

            //drawingContext.DrawLine(new Pen(Brushes.Black, 4.0), startPoint, endPoint);
        }

        public static void DrawBezierCurveSegment(DrawingContext drawingContext, CustomInkStrokeSegment segment)
        {
            Point[] cp1, cp2;
            Point[] windowPoints = new Point[segment.Points.Count];
            Point temp;
            PointR tempR;
            for (int i = 0; i < segment.Points.Count; i++)
            {
                tempR = (PointR)segment.Points[i];
                temp = new Point(tempR.X, tempR.Y);
                windowPoints[i] = temp;
            }
            BezierSpline.GetCurveControlPoints(windowPoints, out cp1, out cp2);
            // Draw curve by Bezier.
            PathSegmentCollection lines = new PathSegmentCollection();
            for (int i = 0; i < cp1.Length; ++i)
            {
                lines.Add(new BezierSegment(cp1[i], cp2[i], windowPoints[i + 1], true));
            }
            PathFigure f = new PathFigure(windowPoints[0], lines, false);
            PathGeometry g = new PathGeometry(new PathFigure[] { f });
            //Path path = new Path() { Stroke = Brushes.Red, StrokeThickness = 1, Data = g };
            //drawingContext.DrawGeometry(new SolidColorBrush(drawingAttributes.Color), null, g);
            drawingContext.DrawGeometry(null, new Pen(Brushes.Blue, 2), g);
        }

        public static void DrawBoundingBox(DrawingContext drawingContext, Rect boundingBox)
        {
            Pen pen = new Pen(Brushes.Black, 1);
            pen.DashStyle = DashStyles.Dash;
            drawingContext.DrawRectangle(null, pen, boundingBox);
        }

        /*
         * Drawing a List of PointR
         * 
         */
        public static void DrawPointRs(DrawingContext drawingContext, ArrayList points)
        {
            //var myWindowPoints = Utils.ConvertPointRsToWindowsPoints(points);
            foreach (object t in points)
            {
                DrawingPointR(drawingContext, (PointR)t);
            }
        }

        /*
         *  Drawing a pointR 
         * 
         */
        public static void DrawingPointR(DrawingContext drawingContext, PointR point)
        {
            drawingContext.DrawEllipse(new SolidColorBrush(Colors.Red), new Pen(Brushes.Black, 1), new Point(point.X, point.Y), 4d, 4d);
        }

        #endregion

        #region Segments Pruning

        public static void PruneOverlappingSegments(ref ArrayList segments)
        {
            List<int> deleteSegmentIndexList = new List<int>();
            CustomInkStrokeSegment seg1, seg2;
            for (int i = 0; i < segments.Count; i++)
            {
                seg1 = (CustomInkStrokeSegment)segments[i];

                for (int j = i + 1; j < segments.Count; j++)
                {
                    seg2 = (CustomInkStrokeSegment)segments[j];
                    //TODO Overlapping issue between Segment1 and Segment2
                    /*
                    if(seg1.OverlapSegment(seg2))
                    {

                    }
                     * */
                }
            }
        }

        public static bool MatchTwoSegments(ArrayList seg1Points, ArrayList seg2Points)
        {

            return true;
        }






        #endregion

        #region Type Conversion

        public static Rectangle RectToRectangle(Rect rect)
        {
            var rectangle = new Rectangle
            {
                X = (int)rect.X,
                Y = (int)rect.Y,
                Height = (int)rect.Height,
                Width = (int)rect.Width
            };

            return rectangle;
        }

        public static Rect RectangleToRect(Rectangle rectangle)
        {
            var rect = new Rect
            {
                X = (int)rectangle.X,
                Y = (int)rectangle.Y,
                Height = (int)rectangle.Height,
                Width = (int)rectangle.Width
            };
            return rect;
        }

        public static bool MatchArrayListAndList(ArrayList arrLst, List<int> lst)
        {
            int arrayCount = arrLst.Count;
            int lstCount = lst.Count;

            if (arrayCount != lstCount) return false;

            for (int i = 0; i < arrayCount; i++)
            {
                int arrValue = (int)arrLst[i];
                int lstValue = lst[i];
                if (arrValue != lstValue)
                {
                    return false;
                }
            }

            return true;
        }

        #endregion

        #region numeric methods

        /// <summary>
        /// Pass value to NDollar Gesture Recognizer
        /// </summary>
        /// <returns></returns>
        public static List<Recognizer.NDollar.PointR> TransformPointRArrayListToLists(ArrayList dollarPoints)
        {
            var pointRList = new List<Recognizer.NDollar.PointR>();

            if (dollarPoints.Count != 0)
            {
                for (int i = 0; i < dollarPoints.Count; i++)
                {
                    var dollar = (PointR)dollarPoints[i];
                    var pt = new Recognizer.NDollar.PointR(dollar.X, dollar.Y, dollar.T);
                    pointRList.Add(pt);
                }
            }
            return pointRList;
        }

        public static double ComputeDistanceBetweenTwoPoints(PointR p1, PointR p2)
        {
            return Math.Sqrt(Math.Pow(p2.X - p1.X, 2.0) + Math.Pow(p2.Y - p1.Y, 2.0));
        }

        public static double ComputeInkLength(ArrayList points)
        {
            double result = 0.0;

            for (int i = 0; i < points.Count - 1; i++)
            {
                var pt1 = (PointR)points[i];
                var pt2 = (PointR)points[i + 1];

                result += Utils.ComputeDistanceBetweenTwoPoints(pt1, pt2);
            }

            return result;
        }

        public static double FindLongestDistance(ArrayList points)
        {
            double longDist = double.NegativeInfinity;

            for (int i = 0; i < points.Count; i++)
            {
                PointR point = (PointR)points[i];

                for (int j = 0; j < points.Count; j++)
                {
                    if (i != j)
                    {
                        PointR point1 = (PointR)points[j];
                        double dist = starPadSDK.DollarRecognizer.Utils.Distance(point, point1);
                        if (dist > longDist)
                        {
                            longDist = dist;
                        }
                    }
                }
            }
            return longDist;
        }

        public static double EuclideanDistance(Point p1, Point p2)
        {
            return Math.Sqrt(Math.Pow(p1.X - p2.X, 2) + Math.Pow(p1.Y - p2.Y, 2));
        }

        public static double EuclidenaDistancePointR(PointR p1, PointR p2)
        {
            double dx = p2.X - p1.X;
            double dy = p2.Y - p1.Y;
            return Math.Sqrt(dx * dx + dy * dy);
        }


        public static double Mean(List<int> values)
        {
            if (values.Count == 0)
            {
                return 0.0;
            }

            double returnValue = values.Aggregate(0.0, (current, value) => current + value);

            return returnValue / (double)values.Count;
        }

        public static double Mean(List<double> values)
        {
            if (values.Count == 0)
                return 0.0;
            double returnValue = values.Sum();
            return returnValue / (double)values.Count;
        }

        public static T Median<T>(List<T> values)
        {
            if (values.Count == 0)
            {
                return default(T);
            }
            values.Sort();
            return values[(values.Count / 2)];
        }


        public static double Variance(List<double> values)
        {
            if (values == null || values.Count == 0)
            {
                return 0;
            }
            double meanValue = Mean(values);

            double Sum = 0;

            for (int x = 0; x < values.Count; x++)
            {
                Sum += Math.Pow(values[x] - meanValue, 2);
            }
            return Sum / (Double)values.Count;
        }

        public static double StandardDeviation(List<double> values)
        {
            return Math.Sqrt(Variance(values));
        }

        #endregion

        #region XML FileIO

        public static List<int> LoadRawEncodingSequence(XmlReader xmlReader, out string labelName)
        {
            var lst = new List<int>();

            labelName = null;

            while (xmlReader.Read())
            {
                switch (xmlReader.NodeType)
                {
                    case XmlNodeType.Element:
                        switch (xmlReader.Name)
                        {
                            case "ChemStructure":
                                labelName = xmlReader.GetAttribute("Name");
                                break;
                            case "Encoding":
                                lst.Add(Convert.ToInt32(xmlReader.GetAttribute("Value")));
                                break;
                            default:
                                break;
                        }
                        break;
                    default:
                        break;
                }
            }

            return lst;
        }

        public static StrokeCollection LoadRawXMLAsStrokeCollection(XmlTextReader xmlReader, out string symbolName)
        {
            var customStks = new StrokeCollection();
            var stks = new StrokeCollection();
            symbolName = null;
            ArrayList timers = null;
            StylusPoint pt;
            StylusPointCollection pts = null;
            //int sktCount;
            while (xmlReader.Read())
            {
                switch (xmlReader.NodeType)
                {
                    case XmlNodeType.Element: // The node is an element.
                        switch (xmlReader.Name)
                        {
                            case "Sketch":
                                symbolName = xmlReader.GetAttribute("Name");
                                //sktCount = Convert.ToInt32(xmlReader.GetAttribute("Count"));
                                break;
                            case "Stroke":
                                pts = new StylusPointCollection();
                                timers = new ArrayList();
                                break;
                            case "Point":
                                pt = new StylusPoint(Convert.ToDouble(xmlReader.GetAttribute("X")),
                                                     Convert.ToDouble(xmlReader.GetAttribute("Y")));
                                Debug.Assert(timers != null, "timers != null");
                                timers.Add(Convert.ToInt32(xmlReader.GetAttribute("T")));
                                Debug.Assert(pts != null, "pts != null");
                                pts.Add(pt);
                                break;
                            default:
                                break;
                        }
                        break;
                    case XmlNodeType.EndElement:
                        switch (xmlReader.Name)
                        {
                            case "Sketch":
                                break;
                            case "Stroke":
                                if (pts != null)
                                {
                                    stks.Add(new Stroke(pts));
                                    customStks.Add(new CustomInkStroke(pts, timers));
                                }
                                break;
                            default:
                                break;
                        }
                        break;
                    default:
                        break;
                }
            }
            return customStks;
        }


        public static ArrayList LoadXMLIntoCodeBook(string filePath, out string symbolName, out int[] encodings)
        {
            var xmlReader = new XmlTextReader(filePath);

            var segs = new ArrayList();
            string encodingsStr = null;
            string tempSymbolName = null;
            PointR pt;
            Corner corner;
            ArrayList pts = null;
            while (xmlReader.Read())
            {
                switch (xmlReader.NodeType)
                {
                    case XmlNodeType.Element: // The node is an element.
                        switch (xmlReader.Name)
                        {
                            case "Sketch":
                                tempSymbolName = xmlReader.GetAttribute("Name");
                                //stkCount = Convert.ToInt32(xmlReader.GetAttribute("Count"));
                                break;
                            case "Stroke":
                                break;
                            case "Segment":
                                //index = Convert.ToInt32(xmlReader.GetAttribute("index")) - 1;
                                pts = new ArrayList();
                                break;
                            case "Point":
                                pt = new PointR(Convert.ToDouble(xmlReader.GetAttribute("X")), Convert.ToDouble(xmlReader.GetAttribute("Y")));
                                corner = new Corner(pt);
                                if (Convert.ToBoolean(xmlReader.GetAttribute("Corner")))
                                {
                                    corner.IsCorner = true;
                                }
                                Debug.Assert(pts != null, "pts != null");
                                pts.Add(corner);
                                break;
                            case "Encoding":
                                encodingsStr = xmlReader.GetAttribute("Sequence");
                                break;
                            default:
                                break;
                        }

                        break;
                    case XmlNodeType.EndElement:
                        switch (xmlReader.Name)
                        {
                            case "Segment":
                                if (pts != null) segs.Add(new CustomInkStrokeSegment(pts));
                                break;
                            case "Encoding":
                                break;
                            case "Sketch":
                                //this.MyInkCanvas.Strokes = stks;
                                break;
                            case "Stroke":
                                break;
                            default:
                                break;
                        }
                        break;
                    default:
                        break;
                }
            }
            // string symbolName = file.GetAttribute("Name");
            // int stksCount = Convert.ToInt32(file.GetAttribute("NumberOfStrokes"));

            // Console.WriteLine(symbolName);  
            symbolName = tempSymbolName;

            Debug.Assert(encodingsStr != null, "encodingsStr != null");
            char[] arrays = encodingsStr.ToArray();
            //int[] arrays = encodingsStr.Select(n => Convert.ToInt32(n)).ToArray();
            //char[] arrays = encodingsStr.ToCharArray();
            encodings = new int[arrays.Length];
            for (int i = 0; i < arrays.Length; i++)
            {
                encodings[i] = Convert.ToInt32(arrays[i].ToString());
            }

            return segs;

        }

        public static ArrayList LoadXMLAsDollarPtStrokeCollection(XmlTextReader xmlReader, out string symbolName)
        {
            var stks = new ArrayList();
            //Stroke stk;
            string tempSymbolName = null;
            //int index;
            StylusPoint pt;
            StylusPointCollection pts = null;
            while (xmlReader.Read())
            {
                switch (xmlReader.NodeType)
                {
                    case XmlNodeType.Element: // The node is an element.
                        switch (xmlReader.Name)
                        {
                            case "Sketch":
                                tempSymbolName = xmlReader.GetAttribute("Name");
                                //stkCount = Convert.ToInt32(xmlReader.GetAttribute("Count"));
                                break;
                            case "Stroke":
                                //index = Convert.ToInt32(xmlReader.GetAttribute("index")) - 1;
                                pts = new StylusPointCollection();
                                break;
                            case "Point":
                                pt = new StylusPoint(Convert.ToDouble(xmlReader.GetAttribute("X")), Convert.ToDouble(xmlReader.GetAttribute("Y")), 0.0f);
                                Debug.Assert(pts != null, "pts != null");
                                pts.Add(pt);
                                break;
                            default:
                                break;
                        }

                        break;
                    case XmlNodeType.EndElement:
                        switch (xmlReader.Name)
                        {
                            case "Stroke":
                                if (pts != null) stks.Add(new Stroke(pts));
                                break;
                            case "Sketch":
                                //this.MyInkCanvas.Strokes = stks;
                                break;
                            default:
                                break;
                        }
                        break;
                    default:
                        break;
                }
            }
            // string symbolName = file.GetAttribute("Name");
            // int stksCount = Convert.ToInt32(file.GetAttribute("NumberOfStrokes"));

            // Console.WriteLine(symbolName);  
            symbolName = tempSymbolName;
            return stks;
        }

        #endregion

        #region Time Information

        public static double ComputeTimeInterval(ArrayList pointRs)
        {
            PointR start = (PointR)pointRs[0];
            PointR end = (PointR)pointRs[pointRs.Count - 1];
            return Math.Abs(start.T - end.T);
        }

        #endregion
      
        public static double ComputeHorizontalDistPercentage(Rect prevBB, Rect nextBB)
        {
            double nearDist = 0.0;
            double farDist = 0.0;
            double percentage;

            if (prevBB.IntersectsWith(nextBB))
            {
                return -1.0;
            }

            if (prevBB.Right < nextBB.Left)
            {
                nearDist = Math.Abs(nextBB.Left - prevBB.Right);
                farDist = Math.Abs(nextBB.Right - prevBB.Left);
            }
            else if (nextBB.Right < prevBB.Left)
            {
                nearDist = Math.Abs(prevBB.Left - nextBB.Right);
                farDist = Math.Abs(prevBB.Right - nextBB.Left);
            }
            percentage = Math.Abs(nearDist / farDist);
            Console.WriteLine("Horizontal Dist Percentage: " + percentage);
            return percentage;
        }

        public static double ComputeBottomDiff(Rect prevBB, Rect nextBB)
        {
            return Math.Abs(prevBB.Bottom - nextBB.Bottom) /
                                      Math.Max(prevBB.Height, nextBB.Height);
        }

    }

    public class Permutations<T>
    {
        public static System.Collections.Generic.IEnumerable<T[]> AllFor(T[] array)
        {
            if (array == null || array.Length == 0)
            {
                yield return new T[0];
            }
            else
            {
                for (int pick = 0; pick < array.Length; ++pick)
                {
                    T item = array[pick];
                    int i = -1;
                    T[] rest = System.Array.FindAll<T>(
                        array, delegate(T p) { return ++i != pick; }
                    );
                    foreach (T[] restPermuted in AllFor(rest))
                    {
                        i = -1;
                        yield return System.Array.ConvertAll<T, T>(
                            array,
                            delegate(T p)
                            {
                                return ++i == 0 ? item : restPermuted[i - 1];
                            }
                        );
                    }
                }
            }
        }
    }

    public static class BezierSpline
    {
        public static void GetCurveControlPoints(Point[] knots, out Point[] firstControlPoints, out Point[] secondControlPoints)
        {
            if (knots == null)
                throw new ArgumentNullException("knots");
            int n = knots.Length - 1;
            if (n < 1)
                throw new ArgumentException("At least two knot points required", "knots");
            if (n == 1)
            { // Special case: Bezier curve should be a straight line.
                firstControlPoints = new Point[1];
                // 3P1 = 2P0 + P3
                firstControlPoints[0].X = (2 * knots[0].X + knots[1].X) / 3;
                firstControlPoints[0].Y = (2 * knots[0].Y + knots[1].Y) / 3;

                secondControlPoints = new Point[1];
                // P2 = 2P1 – P0
                secondControlPoints[0].X = 2 * firstControlPoints[0].X - knots[0].X;
                secondControlPoints[0].Y = 2 * firstControlPoints[0].Y - knots[0].Y;
                return;
            }

            // Calculate first Bezier control points
            // Right hand side vector
            double[] rhs = new double[n];

            // Set right hand side X values
            for (int i = 1; i < n - 1; ++i)
                rhs[i] = 4 * knots[i].X + 2 * knots[i + 1].X;
            rhs[0] = knots[0].X + 2 * knots[1].X;
            rhs[n - 1] = (8 * knots[n - 1].X + knots[n].X) / 2.0;
            // Get first control points X-values
            double[] x = GetFirstControlPoints(rhs);

            // Set right hand side Y values
            for (int i = 1; i < n - 1; ++i)
                rhs[i] = 4 * knots[i].Y + 2 * knots[i + 1].Y;
            rhs[0] = knots[0].Y + 2 * knots[1].Y;
            rhs[n - 1] = (8 * knots[n - 1].Y + knots[n].Y) / 2.0;
            // Get first control points Y-values
            double[] y = GetFirstControlPoints(rhs);

            // Fill output arrays.
            firstControlPoints = new Point[n];
            secondControlPoints = new Point[n];
            for (int i = 0; i < n; ++i)
            {
                // First control point
                firstControlPoints[i] = new Point(x[i], y[i]);
                // Second control point
                if (i < n - 1)
                    secondControlPoints[i] = new Point(2 * knots[i + 1].X - x[i + 1], 2 * knots[i + 1].Y - y[i + 1]);
                else
                    secondControlPoints[i] = new Point((knots[n].X + x[n - 1]) / 2, (knots[n].Y + y[n - 1]) / 2);
            }
        }

        private static double[] GetFirstControlPoints(double[] rhs)
        {
            int n = rhs.Length;
            double[] x = new double[n]; // Solution vector.
            double[] tmp = new double[n]; // Temp workspace.

            double b = 2.0;
            x[0] = rhs[0] / b;
            for (int i = 1; i < n; i++) // Decomposition and forward substitution.
            {
                tmp[i] = 1 / b;
                b = (i < n - 1 ? 4.0 : 3.5) - tmp[i];
                x[i] = (rhs[i] - x[i - 1]) / b;
            }
            for (int i = 1; i < n; i++)
                x[n - i - 1] -= tmp[n - i] * x[n - i]; // Backsubstitution.

            return x;
        }
    }
}
