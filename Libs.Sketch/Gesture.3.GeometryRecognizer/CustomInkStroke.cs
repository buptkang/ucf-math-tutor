﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Ink;
using System.Windows.Input;
using starPadSDK.DollarRecognizer;

namespace _020.AGGeometryRecognizer.SingleStrokeRecognizer
{
    public class CustomInkStroke : Stroke
    {
        #region public Properties

        public new StylusPointCollection StylusPoints { get; set; }
        public ArrayList StylusPointsTimers { get; set; }
        public System.Windows.Rect BoundingBox { get; set; }

        public ArrayList DollarPoints { set; get; } //List of PointR

        public ArrayList StrokeCornerPoints { set; get; } // List of IShortStraw PointR
        public ArrayList StrokeSegments { set; get; } // List of CustomInkStrokeSegment, (Sequential Segments)

        public ArrayList StrokeShortCornerPoints { set; get; } // List of ShortStraw PointR
        //public ArrayList StrokeShortSegments { set; get; } // List of Short CustomInkStrokeSegment

        public int NmbrSegments { set; get; } // Number Of Segments
        public bool ContainArcSeg { set; get; }

        public bool IsCloseForm
        {
            get
            {
                PointR startPt = (PointR)DollarPoints[0];
                PointR endPt = (PointR)DollarPoints[DollarPoints.Count - 1];
                double dist = Utils.ComputeDistanceBetweenTwoPoints(startPt, endPt);

                Rect rect = this.BoundingBox;
                double diagonalLength = Utils.ComputeDistanceBetweenTwoPoints(new PointR(rect.TopLeft.X, rect.TopLeft.Y),
                                                                          new PointR(rect.BottomRight.X, rect.BottomRight.Y));

                double threshold = 0.2;
                if ((dist / diagonalLength) > threshold)
                    return false;
                else
                    return true;
            }
        }

        public double InkLength
        {
            get
            {
                double length = 0.0f;
                for (int i = 0; i < DollarPoints.Count - 1; i++)
                {
                    var pt1 = (PointR)DollarPoints[i];
                    var pt2 = (PointR)DollarPoints[i + 1];
                    length += Utils.ComputeDistanceBetweenTwoPoints(pt1, pt2);
                }
                return length;
            }
        }

        public enum DrawingMode
        {
            Raw, Beautify
        }

        private DrawingMode strokeMode = DrawingMode.Raw;

        public DrawingMode Mode
        {
            get
            {
                return strokeMode;
            }
            set
            {
                if (strokeMode != value)
                {
                    strokeMode = value;
                    this.OnInvalidated(new EventArgs());
                }
            }
        }

        //public CustomClassificationRegion BelongedRegion { set; get; }
        #endregion

        #region public Constructor

        /// <summary>
        /// Prediction Custom InkStroke Constructor
        /// </summary>
        /// <param name="points"></param>
        public CustomInkStroke(StylusPointCollection points)
            : base(points)
        {
            StylusPoints = points;
            BoundingBox = Utils.ComputeBoundingBox(StylusPoints);
        }

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="points"></param>
        /// <param name="pointsTimers"></param>
        /// <param name="_mode"></param>        
        public CustomInkStroke(StylusPointCollection points, ArrayList pointsTimers)
            : base(points)
        {
            StylusPoints = points;
            StylusPointsTimers = pointsTimers;
            BoundingBox = Utils.ComputeBoundingBox(StylusPoints);
            ArrayList dollarPoints = Utils.ConvertStylusPointsToPoint3Rs(StylusPoints, StylusPointsTimers);
            Utils.RemoveNoisePoints(ref dollarPoints);
            DollarPoints = dollarPoints;
            AnalyzeStroke();
        }

        #endregion

        #region Methods

        public void UpdateDollarPoint()
        {
            BoundingBox = Utils.ComputeBoundingBox(StylusPoints);
            ArrayList dollarPoints = Utils.ConvertStylusPointsToPoint3Rs(StylusPoints, StylusPointsTimers);
            Utils.RemoveNoisePoints(ref dollarPoints);
            DollarPoints = dollarPoints;
            AnalyzeStroke();
        }

        /// <summary>
        /// Pass value to NDollar Gesture Recognizer
        /// </summary>
        /// <returns></returns>
        public List<Recognizer.NDollar.PointR> TransformPointRArrayListToLists()
        {
            var pointRList = new List<Recognizer.NDollar.PointR>();

            if (DollarPoints.Count != 0)
            {
                for (int i = 0; i < DollarPoints.Count; i++)
                {
                    var dollar = (PointR)DollarPoints[i];
                    var pt = new Recognizer.NDollar.PointR(dollar.X, dollar.Y, dollar.T);
                    pointRList.Add(pt);
                }
            }
            return pointRList;
        }

        private void AnalyzeStroke()
        {
            //Step 2.1: Stroke Segmentation: Finding Corners and Generates Line and bezier segments
            //Step 2.2: Label each stroke segment as the primitive type
            PaleoSketch.Instance.FetchDataPoints(DollarPoints);
            //IStraw Corners            
            StrokeCornerPoints = PaleoSketch.Instance.GetIShortStrawCorners();
            StrokeSegments = PaleoSketch.Instance.SegmentIStrawStroke();

            StrokeShortCornerPoints = PaleoSketch.Instance.GetShortStrawCorners();
            //StrokeShortSegments = PaleoSketch.Instance.SegmentStrawStroke();

            foreach (CustomInkStrokeSegment seg in StrokeSegments)
            {
                if (seg.IsCurve)
                {
                    ContainArcSeg = true;
                    break;
                }
            }
            NmbrSegments = StrokeSegments.Cast<CustomInkStrokeSegment>().Count(seg => !seg.IsNoise);
        }

        public void SwapIStrawSegsToShortStrawSegs()
        {
            var newSegs = new ArrayList();
            foreach (CustomInkStrokeSegment seg in StrokeSegments)
            {
                if (seg.IsCurve)
                {
                    ArrayList innerSegs = seg.ExtractSubSegments();
                    foreach (CustomInkStrokeSegment segTemp in innerSegs)
                    {
                        newSegs.Add(segTemp);
                    }
                }
                else
                {
                    newSegs.Add(seg);
                }
            }
            StrokeSegments = newSegs;
        }

        #endregion

    }

}
