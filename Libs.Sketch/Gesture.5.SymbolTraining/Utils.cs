﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Ink;
using System.Windows.Input;
using System.Xml;
using Recognizer.NDollar;

namespace AGSymbolTraining
{
    public class Utils
    {
        public static string GetSymbolStorePath()
        {
            var info = new DirectoryInfo(Directory.GetCurrentDirectory());
            string dir = info.Parent.Parent.FullName;
            return Path.Combine(dir, "AGData");
        }

        public static void SaveSymbolStrokes(string path, String symbolName, List<List<PointR>> strokes)
        {
            string fileName = symbolName + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".xml";
            string filePath = Path.Combine(path, fileName);

            if (strokes.Count > 0)
            {
                #region Data Storage
                using (var writer = new XmlTextWriter(filePath, Encoding.UTF8))
                {
                    writer.Formatting = Formatting.Indented;
                    writer.WriteStartDocument(true);
                    writer.WriteStartElement("Gesture");
                    writer.WriteAttributeString("Name", symbolName);
                    writer.WriteAttributeString("Count", strokes.Count.ToString());

                    //Here, XML save the prepocessed stroke with sampled points for each stroke

                    //int totalIndex = 1;
                    for (int i = 0; i < strokes.Count; i++)
                    {
                        var points = strokes[i] as List<PointR>;
                        writer.WriteStartElement("Stroke");
                        writer.WriteAttributeString("index", XmlConvert.ToString(i + 1));
                        foreach (PointR p in points)
                        {
                            writer.WriteStartElement("Point");
                            writer.WriteAttributeString("X", XmlConvert.ToString(p.X));
                            writer.WriteAttributeString("Y", XmlConvert.ToString(p.Y));
                            writer.WriteAttributeString("T", XmlConvert.ToString(p.T));
                            writer.WriteEndElement(); // <Point/>
                        }
                        writer.WriteEndElement(); // </Gesture/>
                    }
                }
                #endregion
            }
        }


        public static List<List<Recognizer.NDollar.PointR>> ConvertMsStrokeCollectionToStrokeList(StrokeCollection stks)
        {
            return stks.Select(ConvertMsStrokeToList).ToList();
        }

        public static List<Recognizer.NDollar.PointR> ConvertMsStrokeToList(Stroke stk)
        {
            var lst = stk.StylusPoints.Select(ConvertStylusPointToNPointR).ToList();
            return lst;
        }

        public static Recognizer.NDollar.PointR ConvertStylusPointToNPointR(StylusPoint stylusPoint)
        {
            return new Recognizer.NDollar.PointR(stylusPoint.X, stylusPoint.Y);
        }
    
    }
}
