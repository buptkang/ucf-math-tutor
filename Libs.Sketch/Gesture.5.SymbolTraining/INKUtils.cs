﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Windows;
using System.Windows.Shapes;
using System.Windows.Controls;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml;
using Recognizer.NDollar;

using Brushes = System.Windows.Media.Brushes;
using Path = System.IO.Path;
using Pen = System.Windows.Media.Pen;
using Point = System.Windows.Point;

namespace ChemSketchRecognizer
{
    public class Utils
    {
        #region 2D Conversion

        public static PointR ConvertStylusPointToPointR(StylusPoint point)
        {
            return new PointR(point.X, point.Y);
        }

        
        public static StylusPoint ConvertPointRToStylusPoint(PointR point)
        {
            return new StylusPoint(point.X, point.Y);
        }

        public static ArrayList ConvertMsStrokeToStroke(Stroke stroke)
        {
            var list = new ArrayList(stroke.StylusPoints.Count);
            foreach(StylusPoint point in stroke.StylusPoints)
            {
                list.Add(ConvertStylusPointToPointR(point));
            }
            return list;
        }

        public static Stroke ConvertStrokeToMsStroke(ArrayList list)
        {
            var collection = new StylusPointCollection();
            foreach(PointR point in list)
            {
                collection.Add(ConvertPointRToStylusPoint(point));
            }
            return new Stroke(collection);
        }

        public static ArrayList ConvertMsStrokeCollectionToStrokeCollection(StrokeCollection strokes)
        {
            var myStrokes = new ArrayList(strokes.Count);
            foreach(Stroke stroke in strokes)
            {
                myStrokes.Add(ConvertMsStrokeToStroke(stroke));
            }

            return myStrokes;
        }

        public static StrokeCollection ConvertStrokeCollectionToMsStrokeCollection(ArrayList strokes)
        {
            var collection = new StrokeCollection();
            foreach(ArrayList stroke in strokes)
            {
                collection.Add(ConvertStrokeToMsStroke(stroke));
            }
            return collection;
        }

        public static List<Recognizer.NDollar.PointR> ConvertMsStrokeToList(Stroke stk)
        {
            var lst = stk.StylusPoints.Select(ConvertStylusPointToNPointR).ToList();
            return lst;
        }

        public static List<List<Recognizer.NDollar.PointR>> ConvertMsStrokeCollectionToStrokeList(StrokeCollection stks)
        {
            return stks.Select(ConvertMsStrokeToList).ToList();
        }

        public static Recognizer.NDollar.PointR ConvertStylusPointToNPointR(StylusPoint stylusPoint)
        {
            return new Recognizer.NDollar.PointR(stylusPoint.X, stylusPoint.Y);
        }

        public static StylusPoint CovnertNPointRToStylusPoint(Recognizer.NDollar.PointR point)
        {
            return new StylusPoint(point.X, point.Y);
        }

        public static Stroke ConvertStrokeLstToMsStroke(List<Recognizer.NDollar.PointR> list)
        {
            var collection = new StylusPointCollection();
            foreach(Recognizer.NDollar.PointR point in list)
            {
                collection.Add(CovnertNPointRToStylusPoint(point));
            }
            return new Stroke(collection);
        }

        public static StrokeCollection ConvertStrokeListToMsStrokeCollection(List<List<Recognizer.NDollar.PointR>> strokes)
        {
            var collection = new StrokeCollection();
            foreach(List<Recognizer.NDollar.PointR> stroke in strokes)
            {
                collection.Add(ConvertStrokeLstToMsStroke(stroke));
            }

            return collection;
        }

        //Drawing Purpose
        public static ArrayList ConvertPointRsToWindowsPoints(ArrayList pointRs)
        {
            var windows2DPoints = new ArrayList();
            Point temp;
            foreach (PointR pointR in pointRs)
            {
                temp = new Point(pointR.X, pointR.Y);
                windows2DPoints.Add(temp);
            }
            return windows2DPoints;
        }

        #endregion

        #region 3D Conversion

        public static PointR ConvertStylusPointToPointR(StylusPoint point, int T)
        {
            return new PointR(point.X, point.Y, T);
        }

        public static ArrayList ConvertStylusPointsToPoint3Rs(StylusPointCollection points, ArrayList pointsTimers)
        {
            ArrayList point3Rs = new ArrayList(points.Count);
            PointR pointR;
            for (int i = 0; i < points.Count; i++)
            {
                pointR = ConvertStylusPointToPointR(points[i], (int)pointsTimers[i]);
                point3Rs.Add(pointR);
            }
            return point3Rs;
        }

        /*
        public static Point3R ConvertStylusPointToPoint3R(StylusPoint point, long T)
        {
            return new Point3R(ConvertStylusPointToPointR(point), T);
        }

        public static long[] RetrieveTimeArrayFromStroke(Stroke stroke)
        {
            var times = stroke.GetPropertyData(Recognizer.DollarT.Utils.TimingGuid) as long[];
            return times;
        }

         * */
        #endregion

        #region BoundingBox Boolean Operation

        public static PointR ComputeCentralInBB(Rect bb)
        {
            return new PointR(bb.TopLeft.X + bb.Width / 2, bb.TopLeft.Y + bb.Height / 2);
        }

        public static Rect ComputeBoundingBox(PointR p1, PointR p2, PointR p3, PointR p4)
        {
            double xMin = Math.Min(Math.Min(Math.Min(p1.X, p2.X), p3.X), p4.X);
            double yMin = Math.Min(Math.Min(Math.Min(p1.Y, p2.Y), p3.Y), p4.Y);
            double xMax = Math.Max(Math.Max(Math.Max(p1.X, p2.X), p3.X), p4.X);
            double yMax = Math.Max(Math.Max(Math.Max(p1.Y, p2.Y), p3.Y), p4.Y);
            return new Rect(new Point(xMin, yMin), new Point(xMax, yMax));
        }


        public static Rect ComputeBoundingBox(StylusPointCollection MyStylusPoints)
        {
            double minX = Double.PositiveInfinity, minY = Double.PositiveInfinity, maxX = Double.NegativeInfinity, maxY = Double.NegativeInfinity;
            foreach (StylusPoint pt in MyStylusPoints)
            {
                if (pt.X < minX)
                    minX = pt.X;
                if (pt.X > maxX)
                    maxX = pt.X;
                if (pt.Y < minY)
                    minY = pt.Y;
                if (pt.Y > maxY)
                    maxY = pt.Y;
            }
            return new Rect(minX, minY, maxX - minX, maxY - minY);
        }

        public static Rect ComputeBoundingBox(ArrayList Points)
        {
            double minX = Double.PositiveInfinity, minY = Double.PositiveInfinity, maxX = Double.NegativeInfinity, maxY = Double.NegativeInfinity;
            foreach (PointR pt in Points)
            {
                if (pt.X < minX)
                    minX = pt.X;
                if (pt.X > maxX)
                    maxX = pt.X;
                if (pt.Y < minY)
                    minY = pt.Y;
                if (pt.Y > maxY)
                    maxY = pt.Y;
            }
            return new Rect(minX, minY, maxX - minX, maxY - minY);
        }

        public static Rect ComputeBoundingBox(List<Recognizer.NDollar.PointR> points)
        {
            double minX = Double.PositiveInfinity, minY = Double.PositiveInfinity, maxX = Double.NegativeInfinity, maxY = Double.NegativeInfinity;
            foreach (Recognizer.NDollar.PointR pt in points)
            {
                if (pt.X < minX)
                    minX = pt.X;
                if (pt.X > maxX)
                    maxX = pt.X;
                if (pt.Y < minY)
                    minY = pt.Y;
                if (pt.Y > maxY)
                    maxY = pt.Y;
            }
            return new Rect(minX, minY, maxX - minX, maxY - minY);
        }

        #endregion
        
        #region stroke preprocessing

        public static void RemoveNoisePoints(ref ArrayList DollarPoints)
        {
           //Noise deduction: Reduce those points with same X and Y
            PointR point0, point1;
            for (int i = 1; i < DollarPoints.Count; i++)
            {
                point0 = (PointR)DollarPoints[i - 1];
                point1 = (PointR)DollarPoints[i];
                if (Math.Abs(point0.X - point1.X) < Double.Epsilon
                    && Math.Abs(point0.Y - point1.Y) < Double.Epsilon)
                {
                    DollarPoints.Remove(point0);
                }
            }
        }

        #endregion

        #region numeric methods

        /// <summary>
        /// Pass value to NDollar Gesture Recognizer
        /// </summary>
        /// <returns></returns>
        public static List<Recognizer.NDollar.PointR> TransformPointRArrayListToLists(ArrayList dollarPoints)
        {
            var pointRList = new List<Recognizer.NDollar.PointR>();

            if (dollarPoints.Count != 0)
            {
                for (int i = 0; i < dollarPoints.Count; i++)
                {
                    var dollar = (PointR)dollarPoints[i];
                    var pt = new Recognizer.NDollar.PointR(dollar.X, dollar.Y, dollar.T);
                    pointRList.Add(pt);
                }
            }
            return pointRList;
        }

        public static double ComputeDistanceBetweenTwoPoints(PointR p1, PointR p2)
        {
            return Math.Sqrt(Math.Pow(p2.X- p1.X, 2.0) + Math.Pow(p2.Y - p1.Y, 2.0));
        }

        public static double ComputeInkLength(ArrayList points)
        {
            double result = 0.0;

            for (int i = 0; i < points.Count - 1; i++)
            {
                var pt1 = (PointR) points[i];
                var pt2 = (PointR) points[i + 1];

                result += Utils.ComputeDistanceBetweenTwoPoints(pt1, pt2);
            }

            return result;
        }

        public static double FindLongestDistance(ArrayList points)
        {
            double longDist = double.NegativeInfinity;

            for(int i = 0; i < points.Count; i++)
            {
                PointR point = (PointR) points[i];

                for(int j = 0; j < points.Count; j++)
                {
                    if(i != j)
                    {
                        PointR point1 = (PointR) points[j];
                        double dist = Dollar.Utils.Distance(point, point1);
                        if(dist > longDist)
                        {
                            longDist = dist;
                        }
                    }
                }
            }
            return longDist;
        }

        public static double EuclideanDistance(Point p1, Point p2)
        {
            return Math.Sqrt(Math.Pow(p1.X - p2.X, 2) + Math.Pow(p1.Y - p2.Y, 2));
        }

        public static double EuclidenaDistancePointR(PointR p1, PointR p2)
        {
            double dx = p2.X - p1.X;
            double dy = p2.Y - p1.Y;
            return Math.Sqrt(dx * dx + dy * dy);
        }


        public static double Mean(List<int> values)
        {
            if (values.Count == 0)
            {
                return 0.0;
            }

            double returnValue = values.Aggregate(0.0, (current, value) => current + value);

            return returnValue / (double)values.Count;
        }

        public static double Mean(List<double> values)
        {
            if (values.Count == 0)
                return 0.0;
            double returnValue = values.Sum();
            return returnValue / (double)values.Count;
        }

        public static T Median<T>(List<T> values)
        {
            if (values.Count == 0)
            {
                return default(T);
            }
            values.Sort();
            return values[(values.Count / 2)];
        }


        public static double Variance(List<double> values)
        {
            if (values == null || values.Count == 0)
            {
                return 0;
            }
            double meanValue = Mean(values);

            double Sum = 0;

            for (int x = 0; x < values.Count; x++)
            {
                Sum += Math.Pow(values[x] - meanValue, 2);
            }
            return Sum / (Double)values.Count;
        }

        public static double StandardDeviation(List<double> values)
        {
            return Math.Sqrt(Variance(values));
        }

        #endregion

        #region XML FileIO

        public static List<int> LoadRawEncodingSequence(XmlReader xmlReader, out string labelName)
        {
            var lst = new List<int>();

            labelName = null;

            while (xmlReader.Read())
            {
                switch (xmlReader.NodeType)
                {
                    case XmlNodeType.Element:
                        switch(xmlReader.Name)
                        {
                            case "ChemStructure":
                                labelName = xmlReader.GetAttribute("Name");
                                break;
                            case "Encoding":
                                lst.Add(Convert.ToInt32(xmlReader.GetAttribute("Value")));
                                break;
                            default:
                                break;
                        }
                        break;
                    default:
                        break;
                }
            }

            return lst;
        }

        public static StrokeCollection LoadRawXMLAsStrokeCollection(XmlTextReader xmlReader, out string symbolName)
        {
            var customStks = new StrokeCollection();
            var stks = new StrokeCollection();
            symbolName = null;
            ArrayList timers = null;            
            StylusPoint pt;
            StylusPointCollection pts = null;
            //int sktCount;
            while (xmlReader.Read())
            {
                switch (xmlReader.NodeType)
                {
                    case XmlNodeType.Element: // The node is an element.
                        switch (xmlReader.Name)
                        {
                            case "Sketch":
                                symbolName = xmlReader.GetAttribute("Name");
                                //sktCount = Convert.ToInt32(xmlReader.GetAttribute("Count"));
                                break;
                            case "Stroke":
                                pts = new StylusPointCollection();
                                timers = new ArrayList();
                                break;
                            case "Point":
                                pt = new StylusPoint(Convert.ToDouble(xmlReader.GetAttribute("X")), 
                                                     Convert.ToDouble(xmlReader.GetAttribute("Y")));
                                Debug.Assert(timers != null, "timers != null");
                                timers.Add(Convert.ToInt32(xmlReader.GetAttribute("T")));
                                Debug.Assert(pts != null, "pts != null");
                                pts.Add(pt);
                                break; 
                            default:
                                break;
                        }
                        break;
                    case XmlNodeType.EndElement:
                        switch (xmlReader.Name)
                        {
                            case "Sketch":
                                break;
                            case "Stroke":
                                if (pts != null)
                                {
                                    stks.Add(new Stroke(pts));
                                    customStks.Add(new CustomInkStroke(pts, timers));
                                } 
                                break;
                            default:
                                break;
                        }
                        break;
                    default:
                        break;
                }
            }            
            return customStks;
        }


        public static ArrayList LoadXMLIntoCodeBook(string filePath, out string symbolName, out int[] encodings)
        {
                var xmlReader = new XmlTextReader(filePath);
            
                var segs = new ArrayList();
                string encodingsStr = null;
                string tempSymbolName = null;
                PointR pt;
                Corner corner;
                ArrayList pts = null;
                while (xmlReader.Read())
                {
                    switch (xmlReader.NodeType)
                    {
                        case XmlNodeType.Element: // The node is an element.
                            switch (xmlReader.Name)
                            {
                                case "Sketch":
                                    tempSymbolName = xmlReader.GetAttribute("Name");
                                    //stkCount = Convert.ToInt32(xmlReader.GetAttribute("Count"));
                                    break;
                                case "Stroke":
                                    break;
                                case "Segment":
                                    //index = Convert.ToInt32(xmlReader.GetAttribute("index")) - 1;
                                    pts = new ArrayList();
                                    break;
                                case "Point":
                                    pt = new PointR(Convert.ToDouble(xmlReader.GetAttribute("X")), Convert.ToDouble(xmlReader.GetAttribute("Y")));
                                    corner = new Corner(pt);
                                    if (Convert.ToBoolean(xmlReader.GetAttribute("Corner")))
                                    {
                                        corner.IsCorner = true;
                                    }
                                    Debug.Assert(pts != null, "pts != null");
                                    pts.Add(corner);
                                    break;
                                case "Encoding":
                                    encodingsStr = xmlReader.GetAttribute("Sequence");
                                    break;
                                default:
                                    break;
                            }

                            break;
                        case XmlNodeType.EndElement:
                            switch (xmlReader.Name)
                            {
                                case "Segment":
                                    if (pts != null) segs.Add(new CustomInkStrokeSegment(pts));
                                    break;
                                case "Encoding":
                                    break;
                                case "Sketch":
                                    //this.MyInkCanvas.Strokes = stks;
                                    break;
                                case "Stroke":
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                }
                // string symbolName = file.GetAttribute("Name");
                // int stksCount = Convert.ToInt32(file.GetAttribute("NumberOfStrokes"));

                // Console.WriteLine(symbolName);  
                symbolName = tempSymbolName;

                Debug.Assert(encodingsStr != null, "encodingsStr != null");
                char[] arrays = encodingsStr.ToArray();
                //int[] arrays = encodingsStr.Select(n => Convert.ToInt32(n)).ToArray();
                //char[] arrays = encodingsStr.ToCharArray();
                encodings = new int[arrays.Length];
                for(int i = 0; i < arrays.Length; i++)
                {
                    encodings[i] = Convert.ToInt32(arrays[i].ToString());
                }
           
                return segs;
             
        }

        public static ArrayList LoadXMLAsDollarPtStrokeCollection(XmlTextReader xmlReader, out string symbolName)
        {
            var stks = new ArrayList();
            Stroke stk;
            string tempSymbolName = null;
            //int index;
            StylusPoint pt;
            StylusPointCollection pts = null;
            while (xmlReader.Read())
            {
                switch (xmlReader.NodeType)
                {
                    case XmlNodeType.Element: // The node is an element.
                        switch (xmlReader.Name)
                        {
                            case "Sketch":
                                tempSymbolName = xmlReader.GetAttribute("Name");
                                //stkCount = Convert.ToInt32(xmlReader.GetAttribute("Count"));
                                break;
                            case "Stroke":
                                //index = Convert.ToInt32(xmlReader.GetAttribute("index")) - 1;
                                pts = new StylusPointCollection();
                                break;
                            case "Point":
                                pt = new StylusPoint(Convert.ToDouble(xmlReader.GetAttribute("X")), Convert.ToDouble(xmlReader.GetAttribute("Y")), 0.0f);
                                Debug.Assert(pts != null, "pts != null");
                                pts.Add(pt);
                                break;
                            default:
                                break;
                        }

                        break;
                    case XmlNodeType.EndElement:
                        switch (xmlReader.Name)
                        {
                            case "Stroke":
                                if (pts != null) stks.Add(new Stroke(pts));
                                break;
                            case "Sketch":
                                //this.MyInkCanvas.Strokes = stks;
                                break;
                            default:
                                break;
                        }
                        break;
                    default:
                        break;
                }
            }
            // string symbolName = file.GetAttribute("Name");
            // int stksCount = Convert.ToInt32(file.GetAttribute("NumberOfStrokes"));

            // Console.WriteLine(symbolName);  
            symbolName = tempSymbolName;
            return stks;
        }

        #endregion

        #region Time Information

        public static double ComputeTimeInterval(ArrayList pointRs)
        {
                PointR start = (PointR)pointRs[0];
                PointR end = (PointR)pointRs[pointRs.Count - 1];
                return Math.Abs(start.T - end.T);           
        }

        #endregion

        #region File IO

        public static Dictionary<string, List<List<int>>> LoadPartialEncodingData()
        {
            var dict = new Dictionary<string, List<List<int>>>();
            string[] fileEntries1 = Directory.GetFiles(CustomEnvironment.MoleculeTestingEncodingFolder);

            foreach (string filePath in fileEntries1)
            {
                string fileName = Path.GetFileName(filePath);
                if (fileName == null)
                    continue;
                List<int> encodingSequence;
                string label;
                using (var xmlReader = new XmlTextReader(filePath))
                {
                    encodingSequence = Utils.LoadRawEncodingSequence(xmlReader, out label);
                }
                if(dict.ContainsKey(label))
                {
                    dict[label].Add(encodingSequence);
                }
                else
                {
                    dict.Add(label, new List<List<int>>() { encodingSequence });
                }
            }
            return dict;
        }

        public static ArrayList LoadEncodingFile(string symbolName, string fileName)
        {
            string folderPath = CustomEnvironment.MoleculeEncodingFolder + symbolName;

            ArrayList element = null;
            using (var tr = new StreamReader(folderPath))
            {
                string line = null;
                while ((line = tr.ReadLine()) != null)
                {
                    if (line == null) break;
                    if (line.Equals(fileName))
                    {
                        line = tr.ReadLine();
                        line = line.Trim();
                        element = new ArrayList();
                        string[] arr = line.Split(' ');
                        for (int i = 0; i < arr.Length; i++)
                        {
                            element.Add(int.Parse(arr[i]));
                        }
                        break;
                    }
                    else
                    {
                        line = tr.ReadLine();
                    }
                }
            }
            return element;
        }

        public static void SaveCurrentEncodingToFile(string molecule, string fileName, ArrayList encodingSequence)
        {
            string filePath = CustomEnvironment.MoleculeEncodingFolder + molecule;
            using (TextWriter tw = new StreamWriter(filePath, true))
            {
                tw.Write(fileName);
                tw.Write("\n");
                foreach (int temp in encodingSequence)
                {
                    tw.Write(temp);
                    tw.Write(" ");
                }
                tw.Write("\n");
            }
        }

        private static List<int> StringToList(string stringToSplit, char splitDelimiter)
        {
            List<int> list = new List<int>();

            if (string.IsNullOrEmpty(stringToSplit))
                return list;

            string[] values = stringToSplit.Split(splitDelimiter);

            if (values.Length <= 1)
                return list;

            foreach (string s in values)
            {
                int i;
                if (Int32.TryParse(s, out i))
                    list.Add(i);
            }

            return list;
        }

        public static List<int> ArrayListToList(ArrayList arrayList)
        {
            var lst = new List<int>();

            for(int i = 0; i < arrayList.Count; i++)
            {
                lst.Add((int)arrayList[i]);
            }

            return lst;
        }

        public static ArrayList ListToArrayList(List<int> lst)
        {
            var arrayList = new ArrayList();
            foreach(int element in lst)
            {
                arrayList.Add(element);
            }
            return arrayList;
        }

        private static Dictionary<string, List<int>> ReadEncodingFiles(string filePath)
        {
            var rr = new Dictionary<string, List<int>>();
            //charArrayList = new Dictionary<string, List<string>>();

            using(var tr = new StreamReader(filePath))
            {
                string line;
                int index = 0;
                List<int> intSeq = null;
                //List<string> charSeq = null;
                string fileName = null;
              
                while((line = tr.ReadLine()) != null)
                {
                    if (index % 2 == 0)//FileName
                    {
                        if(index != 0)
                        {
                            rr.Add(fileName, intSeq);
                            //charArrayList.Add(fileName, charSeq);
                        }
                        fileName = line;
                    }
                    else if(index % 2 == 1) //Int Number Sequence
                    {
                        intSeq = Utils.StringToList(line, ' ');
                    }
                    index++;
                }
                //if(rr[fileName] == null)
                //{
                if (!rr.ContainsKey(fileName))
                    rr.Add(fileName, intSeq);
                /*}
                else
                {
                    List<int> tmp = rr[fileName];

                    if(tmp.Count != intSeq.Count)
                    {
                        rr.Add(fileName, intSeq);
                    }
                    else
                    {
                        bool diff = false;
                        for(int i = 0; i < tmp.Count; i++)
                        {
                            if(tmp[i] != intSeq[i])
                            {
                                diff = true;
                                break;
                            }
                        }
                        if(diff)
                        {
                            rr.Add(fileName, intSeq);
                        } 
                    }

                }*/
                
                tr.Close();
            }
            return rr;
        }
        


        #endregion

        public static double ComputeHorizontalDistPercentage(Rect prevBB, Rect nextBB)
        {
            double nearDist = 0.0;
            double farDist = 0.0;
            double percentage;

            if (prevBB.IntersectsWith(nextBB))
            {
                return -1.0;
            }

            if (prevBB.Right < nextBB.Left)
            {
                nearDist = Math.Abs(nextBB.Left - prevBB.Right);
                farDist = Math.Abs(nextBB.Right - prevBB.Left);
            }
            else if (nextBB.Right < prevBB.Left)
            {
                nearDist = Math.Abs(prevBB.Left - nextBB.Right);
                farDist = Math.Abs(prevBB.Right - nextBB.Left);
            }
            percentage = Math.Abs(nearDist / farDist);
            Console.WriteLine("Horizontal Dist Percentage: " + percentage);
            return percentage;
        }

        public static double ComputeBottomDiff(Rect prevBB, Rect nextBB)
        {
            return Math.Abs(prevBB.Bottom - nextBB.Bottom) /
                                      Math.Max(prevBB.Height, nextBB.Height);
        }

    }
}
