﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Ink;
using System.Windows.Media;

namespace AGSymbolTraining
{
    public class TrainingInkCanvas : InkCanvas
    {
        //public static string GestureName = "arc";
        //public static string GestureName = "line";
        //public static string GestureName = "QuestionMark";
        public static string GestureName = "circle";
        //public static string GestureName = "line";
        //public static string GestureName = "DownArrow";
        //public static string GestureName = "EqualSign";

        #region Properties

        private Rect _rect1;
        private Rect _rect2;
        private Rect _rect3;
        private Rect _rect4;
        private Rect _rect5;
        private Rect _rect6;
        private Rect _rect7;
        private Rect _rect8;
        private Rect _rect9;

        public StrokeCollection _rect1Strokes = new StrokeCollection();
        public StrokeCollection _rect2Strokes = new StrokeCollection();
        public StrokeCollection _rect3Strokes = new StrokeCollection();
        public StrokeCollection _rect4Strokes = new StrokeCollection();
        public StrokeCollection _rect5Strokes = new StrokeCollection();
        public StrokeCollection _rect6Strokes = new StrokeCollection();
        public StrokeCollection _rect7Strokes = new StrokeCollection();
        public StrokeCollection _rect8Strokes = new StrokeCollection();
        public StrokeCollection _rect9Strokes = new StrokeCollection();

        #endregion

        public TrainingInkCanvas()
        {
            this.StrokeCollected += TrainingInkCanvas_StrokeCollected;
        }

        void TrainingInkCanvas_StrokeCollected(object sender, InkCanvasStrokeCollectedEventArgs e)
        {
            if (_rect1.Contains(e.Stroke.GetBounds()))
            {
                _rect1Strokes.Add(e.Stroke);                
            }
            else if (_rect2.Contains(e.Stroke.GetBounds()))
            {
                _rect2Strokes.Add(e.Stroke);
            }
            else if (_rect3.Contains(e.Stroke.GetBounds()))
            {
                _rect3Strokes.Add(e.Stroke);
            }
            else if (_rect4.Contains(e.Stroke.GetBounds()))
            {
                _rect4Strokes.Add(e.Stroke);
            }
            else if (_rect5.Contains(e.Stroke.GetBounds()))
            {
                _rect5Strokes.Add(e.Stroke);
            }
            else if (_rect6.Contains(e.Stroke.GetBounds()))
            {
                _rect6Strokes.Add(e.Stroke);
            }
            else if (_rect7.Contains(e.Stroke.GetBounds()))
            {
                _rect7Strokes.Add(e.Stroke);
            }
            else if (_rect8.Contains(e.Stroke.GetBounds()))
            {
                _rect8Strokes.Add(e.Stroke);
            }
            else if (_rect9.Contains(e.Stroke.GetBounds()))
            {
                _rect9Strokes.Add(e.Stroke);
            }

        }

        protected override void OnRender(System.Windows.Media.DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);

            _rect1 = new Rect(10.0, this.ActualHeight * 0.2, this.ActualWidth / 8, this.ActualHeight / 6);
            _rect2 = new Rect(this.ActualWidth * 0.2, this.ActualHeight * 0.2, this.ActualWidth / 8, this.ActualHeight / 6);
            _rect3 = new Rect(this.ActualWidth * 0.4, this.ActualHeight * 0.2, this.ActualWidth / 8, this.ActualHeight / 6);
            _rect4 = new Rect(this.ActualWidth * 0.6, this.ActualHeight * 0.2, this.ActualWidth / 8, this.ActualHeight / 6);
            _rect5 = new Rect(this.ActualWidth * 0.8, this.ActualHeight * 0.2, this.ActualWidth / 8, this.ActualHeight / 6);

            _rect6 = new Rect(10.0, this.ActualHeight * 0.65, this.ActualWidth / 8, this.ActualHeight / 6);
            _rect7 = new Rect(this.ActualWidth * 0.2, this.ActualHeight * 0.65, this.ActualWidth / 8, this.ActualHeight / 6);
            _rect8 = new Rect(this.ActualWidth * 0.4, this.ActualHeight * 0.65, this.ActualWidth / 8, this.ActualHeight / 6);
            _rect9 = new Rect(this.ActualWidth * 0.6, this.ActualHeight * 0.65, this.ActualWidth / 8, this.ActualHeight / 6);

            drawingContext.DrawRoundedRectangle(null, new Pen(Brushes.Black, 2.0d), _rect1, 4d, 2d);
            drawingContext.DrawRoundedRectangle(null, new Pen(Brushes.Black, 2.0d), _rect2, 4d, 2d);
            drawingContext.DrawRoundedRectangle(null, new Pen(Brushes.Black, 2.0d), _rect3, 4d, 2d);
            drawingContext.DrawRoundedRectangle(null, new Pen(Brushes.Black, 2.0d), _rect4, 4d, 2d);
            drawingContext.DrawRoundedRectangle(null, new Pen(Brushes.Black, 2.0d), _rect5, 4d, 2d);
            drawingContext.DrawRoundedRectangle(null, new Pen(Brushes.Black, 2.0d), _rect6, 4d, 2d);
            drawingContext.DrawRoundedRectangle(null, new Pen(Brushes.Black, 2.0d), _rect7, 4d, 2d);
            drawingContext.DrawRoundedRectangle(null, new Pen(Brushes.Black, 2.0d), _rect8, 4d, 2d);
            drawingContext.DrawRoundedRectangle(null, new Pen(Brushes.Black, 2.0d), _rect9, 4d, 2d);
        }
    }
}
