﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AGSymbolTraining
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            gestureName.Content = TrainingInkCanvas.GestureName;
        }

        private void StatusButton_OnClick(object sender, RoutedEventArgs e)
        {
            string storedPath = Utils.GetSymbolStorePath();
            Utils.SaveSymbolStrokes(storedPath, TrainingInkCanvas.GestureName + "_1",
                    Utils.ConvertMsStrokeCollectionToStrokeList(inkCanvas._rect1Strokes));
            Utils.SaveSymbolStrokes(storedPath, TrainingInkCanvas.GestureName + "_2",
                    Utils.ConvertMsStrokeCollectionToStrokeList(inkCanvas._rect2Strokes));
            Utils.SaveSymbolStrokes(storedPath, TrainingInkCanvas.GestureName + "_3",
                    Utils.ConvertMsStrokeCollectionToStrokeList(inkCanvas._rect3Strokes));
            Utils.SaveSymbolStrokes(storedPath, TrainingInkCanvas.GestureName + "_4",
                    Utils.ConvertMsStrokeCollectionToStrokeList(inkCanvas._rect4Strokes));
            Utils.SaveSymbolStrokes(storedPath, TrainingInkCanvas.GestureName +"_5",
                    Utils.ConvertMsStrokeCollectionToStrokeList(inkCanvas._rect5Strokes));
            Utils.SaveSymbolStrokes(storedPath, TrainingInkCanvas.GestureName + "_6",
                    Utils.ConvertMsStrokeCollectionToStrokeList(inkCanvas._rect6Strokes));
            Utils.SaveSymbolStrokes(storedPath, TrainingInkCanvas.GestureName + "_7",
                    Utils.ConvertMsStrokeCollectionToStrokeList(inkCanvas._rect7Strokes));
            Utils.SaveSymbolStrokes(storedPath, TrainingInkCanvas.GestureName + "_8",
                    Utils.ConvertMsStrokeCollectionToStrokeList(inkCanvas._rect8Strokes));
            Utils.SaveSymbolStrokes(storedPath, TrainingInkCanvas.GestureName + "_9",
                    Utils.ConvertMsStrokeCollectionToStrokeList(inkCanvas._rect9Strokes));

            Application.Current.Shutdown();
        }
    }
}
