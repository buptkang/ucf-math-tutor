﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Ink;
using System.Windows.Media.Imaging;
using starPadSDK.Geom;
using starPadSDK.Utils;
using starPadSDK.Inq;
using System.Windows.Input;
using System.Windows.Input.StylusPlugIns;
using starPadSDK.Inq.MSInkCompat;
using System.Windows.Media;
using System.Windows.Shapes;
using starPadSDK.Inq.BobsCusps;
using starPadSDK.WPFHelp;
using System.IO;
using starPadSDK.MathExpr.ExprWPF;
using starPadSDK.MathExpr.ExprWPF.Boxes;
using starPadSDK.MathExpr;
using starPadSDK.MathRecognizer;
using starPadSDK.CharRecognizer;


namespace starPadSDK.AppLib {
    public class MathEditor : CommandSet.CommandEditor {
        MathRecognition      _mrec;
        ContainerVisualHost  _underlay   = new ContainerVisualHost();
        StroqCollection      _mathStroqs = new StroqCollection();
        Canvas               _inqCanvas  = new Canvas();
        starPadSDK.Utils.BatchLock _batchEdit = null;

        Rct  bbox(Microsoft.Ink.Strokes stks) {
            return _mrec.Sim[stks].Aggregate(Rct.Null, (Rct r, Stroq s) => r.Union(s.GetBounds()));
        }
        void mrec_ParseUpdated(MathRecognition source,  Recognition charChanged, bool updateMath) {
            /* Evaluate math if necessary */
            if (updateMath)
                try {
                    Evaluator.UpdateMath(_mrec.Ranges.Select((Parser.Range r) => r.Parse));
                }
                catch { }

            /* reset geometry displayed: range displays, etc */
            _underlay.Children.Clear();
            _inqCanvas.Children.Clear();

            /* set up to draw background yellow thing for range displays */
            Brush fill3 = new SolidColorBrush(Color.FromArgb(50, 255, 255, 180));
            Brush fill2 = new SolidColorBrush(Color.FromArgb(75, 255, 255, 180));
            Brush fill1 = new SolidColorBrush(Color.FromArgb(100, 255, 255, 180));
            Brush sqr3 = new SolidColorBrush(Color.FromArgb(50, 0, 255, 0));
            Brush sqr2 = new SolidColorBrush(Color.FromArgb(75, 0, 255, 0));
            Brush sqr1 = new SolidColorBrush(Color.FromArgb(100, 0, 255, 0));
            foreach (Parser.Range rrr in _mrec.Ranges) {
                Rct rangebbox = bbox(rrr.Strokes);
                Rct box = rangebbox.Inflate(8, 8);

                DrawingVisual dv = new DrawingVisual();
                DrawingContext dc = dv.RenderOpen();
                dc.DrawRoundedRectangle(fill3, null, box, 4, 4);
                dc.DrawRoundedRectangle(fill2, null, box.Inflate(-4, -4), 4, 4);
                dc.DrawRoundedRectangle(fill1, null, box.Inflate(-8, -8), 4, 4);
                dc.Close();
                _underlay.Children.Add(dv);

                if (rrr.Parse != null) {
                    if (rrr.Parse.finalSimp != null) {
                        Expr result = rrr.Parse.matrixOperationResult == null ? rrr.Parse.finalSimp : rrr.Parse.matrixOperationResult;

                        ContainerVisualHost cvh = new ContainerVisualHost();
                        dv = new DrawingVisual();
                        dc = dv.RenderOpen();
                        Rct nombbox = MathExpr.ExprWPF.EWPF.Draw(result, 18, dc, Colors.Black, new Pt(), true).rect;
                        dc.Close();
                        cvh.Children.Add(dv);
                        cvh.Width = nombbox.Width;
                        cvh.Height = nombbox.Height;
                        cvh.RenderTransform = new TranslateTransform(box.Right + 10, box.Center.Y);
                        _inqCanvas.Children.Add(cvh);
                    }
                }
                System.Drawing.Rectangle rbounds = rrr.Bounds;

                if (box.Contains(Mouse.GetPosition(_can)) && rrr.Parse != null && rrr.Parse.expr != null) {
                    ContainerVisualHost cvh = new ContainerVisualHost();
                    dv = new DrawingVisual();
                    dc = dv.RenderOpen();
                    Rct nombbox = MathExpr.ExprWPF.EWPF.DrawTop(rrr.Parse.expr, 18, dc, Colors.Blue, new Pt(), true).rect;
                    dc.Close();
                    cvh.Children.Add(dv);
                    cvh.Width = nombbox.Width;
                    cvh.Height = nombbox.Height;
                    cvh.RenderTransform = new TranslateTransform(box.Left, box.Bottom + 24);
                    _inqCanvas.Children.Add(cvh);
                }
            }
            
            if (false)
                debugLog();
        }

        private void debugLog() {
            /* print out log of current 1st-level parse, for debugging */
            List<string> resstrs = new List<string>();
            foreach (Parser.Range r in _mrec.Ranges) {
                Parser.ParseResult p = r.Parse;
                if (p != null && p.root != null)
                    resstrs.Add(p.root.Print());
            }
            if (resstrs.Count > 0) Console.WriteLine(resstrs.Aggregate((string a, string b) => a + " // " + b));
            foreach (Parser.Range r in _mrec.Ranges) {
                Parser.ParseResult pr = r.Parse;
                if (pr != null && pr.expr != null) Console.WriteLine(Text.Convert(pr.expr));
            }
        }
        void Feedback_SelectionPostTransformEvent(SelectionObj selection) { _batchEdit.Dispose(); }
        void Feedback_SelectionPreTransformEvent(SelectionObj selection) { _batchEdit = _mrec.BatchEdit(); }

        override protected void stroqRemoved(Stroq s) { _mathStroqs.Remove(s); }
        override protected bool stroqAdded(Stroq s)   { _mathStroqs.Add(s); return true; }

        public MathEditor(InqScene can):base(can) { 
            _mrec = new MathRecognition(_mathStroqs);
            _mrec.EnsureLoaded();
            _mrec.ParseUpdated += mrec_ParseUpdated;
            _can.Children.Insert(0, _underlay);
            _can.Children.Insert(0, _inqCanvas);
            _underlay.HorizontalAlignment = HorizontalAlignment.Stretch;
            _inqCanvas.HorizontalAlignment = HorizontalAlignment.Stretch;
            _can.Feedback.SelectionPreTransformEvent += new SelectionFeedback.SelectionTransformHandler(Feedback_SelectionPreTransformEvent);
            _can.Feedback.SelectionPostTransformEvent += new SelectionFeedback.SelectionTransformHandler(Feedback_SelectionPostTransformEvent);
            //Button freeze = new Button();
            //freeze.Content = "FREEZE";
            //freeze.Click += new RoutedEventHandler(freeze_Click);
            //_can.Children.Add(freeze);
        }

        Expr freeze = null;
        Box box = null;
        void freeze_Click(object sender, RoutedEventArgs e) {
            freeze = _mrec.Ranges[0].Parse.expr.Clone();

            ContainerVisualHost cvh = Expr2Visual(freeze);
            _inqCanvas.Children.Add(cvh);
            cvh.RenderTransform = new TranslateTransform(300, 300);

            DrawingContext dc = new DrawingVisual().RenderOpen();
            dc.Close();
            box = EWPF.Measure(freeze, 24, dc);
        }

        private ContainerVisualHost Expr2Visual(Expr expr) {
            ContainerVisualHost cvh = new ContainerVisualHost();
            cvh.MouseMove += new MouseEventHandler(cvh_MouseMove);
            cvh.ClipToBounds = true;
            DrawingVisual dv = new DrawingVisual();
            DrawingContext dc  = dv.RenderOpen();
            dc.DrawRectangle(Brushes.White, null, new Rect(0, 0, 1000, 1000));
            Rct nombbox = EWPF.DrawTop(expr, 24, dc, Colors.Black, new Pt(), true).rect;
            dc.Close();
            cvh.Children.Add(dv);
            cvh.Width = nombbox.Width;
            cvh.Height = nombbox.Height;
            return cvh;
        }

        public class HitBox
        {
            public Box Box;
            public List<Expr> Path;
            public HitBox(Box b, List<Expr> p) { Box = b; Path = new List<Expr>(p.ToArray());  }
        }
        HitBox findBox(Rct blob, Box b, List<Expr> selPath) {
            List<HitBox> subHits = new List<HitBox>();
            foreach (Box bb in b.SubBoxes) {
                selPath.Add(b.Expr);
                HitBox f = findBox(blob, bb, selPath);
                if (f != null)
                    if (f.Box.Expr == null)
                        return  new HitBox(b, selPath) ;
                    else 
                        subHits.Add(f);
                else
                    selPath.RemoveAt(selPath.Count - 1);
            }
            if (subHits.Count == 1)
                return subHits[0];
            if (subHits.Count > 1) {
                return new HitBox(b, selPath);
            }
            if (b.BBoxRefOrigin.IntersectsWith(blob)) {
                return new HitBox(b, selPath);
            }
            return null;
        }

        bool collectExprPath(Expr e, List<Expr> path, Expr target) {
            if (object.ReferenceEquals(e, target)) {
                path.Add(e);
                return true;
            }
            if (e is CompositeExpr) {
                foreach (Expr e1 in (e as CompositeExpr).Args)
                    if (collectExprPath(e1, path, target)) {
                        path.Add(e);
                        return true;
                    }
                if (collectExprPath((e as CompositeExpr).Head, path, target)) {
                    path.Add(e);
                    return true;
                }
            }
            return false;
        }

        ContainerVisualHost cvh = null;
        Rectangle rr = null;
        void cvh_MouseMove(object sender, MouseEventArgs e) {
            List<Expr> selPath = new List<Expr>();
            selPath.Add(box.Expr);
            HitBox found = findBox(new Rct(e.GetPosition(_inqCanvas) - new Vec(300, 300 - box.bbox.Top), new Vec(10, 10)), box, selPath);
            if (found != null) {
                Expr topLevel = selPath[0];
                selPath.Clear();
                collectExprPath(topLevel, selPath, found.Box.Expr);
                selPath.Reverse();
                if (rr != null)
                    _inqCanvas.Children.Remove(rr);
                if (cvh != null)
                    _inqCanvas.Children.Remove(cvh);

                rr = new Rectangle();
                rr.Width = found.Box.bbox.Width;
                rr.Height = found.Box.bbox.Height;
                rr.Stroke = Brushes.Gray;

                Expr unfactoredTerm = null;
                Expr factor = found.Box.Expr;
                Expr factoredTerm = null;
                unfactoredTerm = factoredTerm = selPath[selPath.Count-1];
                for (int i = selPath.Count - 1; i >= 0; i--) 
                    if (selPath[i] != null) {
                        Expr factored = Engine.Replace(selPath[i], unfactoredTerm, factoredTerm);
                        factoredTerm = FactorTerm(factored, factor);
                        unfactoredTerm = selPath[i]; // save original version of term for next iteration
                    }
                
                cvh = Expr2Visual(factoredTerm);
                //cvh = Expr2Visual(ExprTransform.MoveAcross(box.Expr,  found.Box.Expr));//new LetterSym('x')));
                _inqCanvas.Children.Add(cvh);
                cvh.RenderTransform = new TranslateTransform(200, 200);

                //ContainerVisualHost cvhF = Box2Visual(found);
                //cvhF.RenderTransform = new TranslateTransform(200,200);
                //_inqCanvas.Children.Add(cvhF);

                Canvas.SetLeft(rr, 300 + found.Box.BBoxRefOrigin.Left);
                Canvas.SetTop(rr, 300 + found.Box.BBoxRefOrigin.Top - box.bbox.Top);
                _inqCanvas.Children.Add(rr);
            }
        }

        static Expr FactorTerm(Expr term, Expr factor) {
            factor.Annotations["Factor"] = true;
            Expr tryfactored = ExprTransform.FactorOut(term, factor);
            Expr cloneFactor = factor.Clone();
            factor.Annotations.Remove("Factor");
            cloneFactor.Annotations["Factor"] = true;
            Expr afterFactor = new CompositeExpr(WellKnownSym.times, cloneFactor, tryfactored);
            if (tryfactored != term && afterFactor != term)
                return afterFactor;
            return term;
        }

        ContainerVisualHost Box2Visual(HitBox found) {
            cvh = new ContainerVisualHost();
            cvh.ClipToBounds = true;
            DrawingVisual dv = new DrawingVisual();
            DrawingContext dc = dv.RenderOpen();
            dc.DrawRectangle(Brushes.Yellow, null, new Rect(0, 0, 1000, 1000));
            EDrawingContext edc = new EDrawingContext(24, dc, Colors.Black);
            found.Box.Draw(edc, new Pt(0, -found.Box.nombbox.Top));
            dc.Close();
            cvh.Children.Add(dv);
            cvh.Width = found.Box.NomBBoxRefOrigin.Width;
            cvh.Height = found.Box.NomBBoxRefOrigin.Height;
            return cvh;
        }
    }
}
