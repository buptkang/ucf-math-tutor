﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Ink;
using System.Windows.Media.Imaging;
using starPadSDK.Geom;
using starPadSDK.Utils;
using starPadSDK.Inq;
using System.Windows.Input;
using System.Windows.Input.StylusPlugIns;
using System.Windows.Media;
using System.Windows.Shapes;
using starPadSDK.Inq.BobsCusps;
using starPadSDK.WPFHelp;

/// this class displays the UI manipulation and feedback widgets for an InqScene's current Selection
namespace starPadSDK.AppLib {
    /// <summary>
    /// Watches the Selection on the InqObjCanvas in order to display a selection boundary when objects
    /// are selected and to provide an interface for moving those objects
    /// </summary>
    public partial class SelectionFeedback {
        InqScene          _ican = null;
        Polygon           _feedbackWidget = new Polygon();
        Polygon           _moveWidget  = new Polygon();
        Floatie           _floatie     = new Floatie();
        Line              _rotLine     = new Line();    
        Stroq             _collecting  = null;
        MouseEventHandler _pendingMove = null;
        DateTime          _downTime = DateTime.MaxValue;
        bool              _moved = false;
        Mat               _xform = Mat.Identity;
        Pt                _selectionRelativePt;
        bool              _useHandles = false;

        bool inDragRgn(Pt where) { return _feedbackWidget.InputHitTest(WPFUtil.TransformFromAtoB(where, _ican, _feedbackWidget)) != null; }
        bool inFloatieRgn(Pt where) { return _floatie.Shadow.InputHitTest(WPFUtil.TransformFromAtoB(where, _ican, _floatie.Shadow)) != null; }

        /// <summary>
        /// Updates the boundary of the Selection widget based on the current SelectionObj.
        /// </summary>
        void updateBounds() {
            double zoom = (((Mat)_ican.RenderTransform.Value) * Vec.Xaxis).Length;
            // transform all selection border points into canvas coordinates (avoids scale artifacts w/ line properties)
            _feedbackWidget.Points = _ican.Selection.Outline.Points;
            Mat borderMat    = _ican.Selection.Outline.RenderTransform.Value;
            Rct borderBounds = Rct.Null;
            for (int i = 0; i < _feedbackWidget.Points.Count; i++) {
                borderBounds = borderBounds.Union(_feedbackWidget.Points[i]);
                _feedbackWidget.Points[i] = borderMat * _feedbackWidget.Points[i];
            }

            Pt[] movePts = new Pt[] { new Pt(borderBounds.Left+borderBounds.Width/6, (borderBounds.Top+borderBounds.Center.Y)/2), 
                                     new Pt((borderBounds.Left+borderBounds.Center.X)/2, borderBounds.Top+borderBounds.Height/6),
                                     new Pt((borderBounds.Right+borderBounds.Center.X)/2,borderBounds.Top+borderBounds.Height/6), 
                                     new Pt(borderBounds.Right-borderBounds.Width/6, (borderBounds.Top+borderBounds.Center.Y)/2),
                                     new Pt(borderBounds.Right-borderBounds.Width/6, (borderBounds.Bottom + borderBounds.Center.Y) / 2), 
                                     new Pt((borderBounds.Right + borderBounds.Center.X)/2,borderBounds.Bottom-borderBounds.Height/6),
                                     new Pt((borderBounds.Left+borderBounds.Center.X)/2, borderBounds.Bottom-borderBounds.Height/6), 
                                     new Pt(borderBounds.Left+borderBounds.Width/6, (borderBounds.Bottom+borderBounds.Center.Y)/2),
                                     new Pt(borderBounds.Left+borderBounds.Width/6, (borderBounds.Top+borderBounds.Center.Y)/2) };
            Pt[] octPts = new Pt[] { new Pt(borderBounds.Left, (borderBounds.Top+borderBounds.Center.Y)/2), 
                                     new Pt((borderBounds.Left+borderBounds.Center.X)/2, borderBounds.Top),
                                     new Pt((borderBounds.Right + borderBounds.Center.X)/2,borderBounds.Top), 
                                     new Pt(borderBounds.Right, (borderBounds.Top+borderBounds.Center.Y)/2),
                                     new Pt(borderBounds.Right, (borderBounds.Bottom + borderBounds.Center.Y) / 2), 
                                     new Pt((borderBounds.Right + borderBounds.Center.X)/2,borderBounds.Bottom),
                                     new Pt((borderBounds.Left+borderBounds.Center.X)/2, borderBounds.Bottom), 
                                     new Pt(borderBounds.Left, (borderBounds.Bottom+borderBounds.Center.Y)/2),
                                     new Pt(borderBounds.Left, (borderBounds.Top+borderBounds.Center.Y)/2) };
            _moveWidget.Points     = new PointCollection(movePts.Select((Pt p) => (Point)(borderMat*p)));
            _feedbackWidget.Points = new PointCollection(octPts.Select((Pt p) => (Point)(borderMat*p)));
            _feedbackWidget.StrokeThickness = _moveWidget.StrokeThickness = _rotLine.StrokeThickness = 0.5 / zoom;

            updateHandles(zoom, borderMat);
        }

        private void updateHandles(double zoom, Mat borderMat) {
            // move&scale the unit-spaced handles to bounding box of the selection outline
            Handles.RenderTransform = new MatrixTransform(Mat.Rect(GeomUtils.Bounds(_ican.Selection.Outline.Points.Select((p)=>(Pt)p))) * borderMat);

            // adjust the size and location of the handles to have constant screen space size and offset from selection bounds
            Vec size = new Vec(((Mat)Handles.RenderTransform.Value * new Vec(1, 0)).Length,
                               ((Mat)Handles.RenderTransform.Value * new Vec(0, 1)).Length);
            double handleSize = TopLeft.Width / 2 / zoom;
            Mat fixHandleSize = Mat.Scale(1 / size.X / zoom, 1 / size.Y / zoom) * Mat.Translate(-handleSize / size.X, -handleSize / size.Y);
            BotLeft.RenderTransform = new MatrixTransform(fixHandleSize);
            TopLeft.RenderTransform = new MatrixTransform(fixHandleSize);
            BotRight.RenderTransform = new MatrixTransform(fixHandleSize);
            TopRight.RenderTransform = new MatrixTransform(fixHandleSize);
            RotHdl.RenderTransform = new MatrixTransform(fixHandleSize * Mat.Translate(0, -0 / size.Y));
            RotHdl.Visibility = TopLeft.Visibility = TopRight.Visibility = BotLeft.Visibility = BotRight.Visibility = _useHandles ? Visibility.Visible : Visibility.Hidden;
            //_feedbackWidget.Opacity = 0.1;

            // update floatie and rotation line
            Rct bounds = GeomUtils.Bounds(_feedbackWidget.Points.Select((p)=>(Pt)p), _feedbackWidget.RenderTransform.Value);
            _floatie.RenderTransform = new MatrixTransform(1 / zoom, 0, 0, 1 / zoom,
                                                           bounds.TopLeft.X - 25 / zoom, bounds.TopLeft.Y - 25 / zoom - _floatie.ActualHeight / zoom);

            // bcz: want  WPFUtil.GetBounds(RotHdl, _ican).Center;   but it needs to call UpdateLayout to be correct, so we do it by hand instead
            Vec rotBounds = (Pt)RotHdl.RenderedGeometry.Bounds.TopLeft - (Pt)TopLeft.RenderedGeometry.Bounds.TopLeft;
            double left = (double)RotHdl.GetValue(Canvas.LeftProperty);
            double top = (double)RotHdl.GetValue(Canvas.TopProperty);
             Mat rotTransform = (((Mat)RotHdl.RenderTransform.Value) *Mat.Translate(left,top) * ((Mat)Handles.RenderTransform.Value) *((Mat)LayoutRoot.RenderTransform.Value)*((Mat)this.RenderTransform.Value)*((Mat)_ican.RenderTransform.Value));
            Pt rcent =  rotTransform * new Pt(RotHdl.Width/2, RotHdl.Height/2);
            Pt bcent = bounds.Center;
            _rotLine.X1 = rcent.X;
            _rotLine.Y1 = rcent.Y;
            _rotLine.X2 = (rcent + (rcent - bcent)).X;
            _rotLine.Y2 = (rcent + (rcent - bcent)).Y;
        }
        /// <summary>
        /// as the mouse moves with no buttons pressed, this highlights which part of the Selection widget is active
        /// and configures whether inking should be enabled.
        /// </summary>
        /// <param name="where"></param>
        /// <param name="barrelBtnPressed"></param>
        void prepareToGrab(Pt where, bool barrelBtnPressed) {
            _moveWidget.Opacity = _rotLine.Opacity = _floatie.Opacity = (barrelBtnPressed ? 0.1 : 1);
            if (IsHitTestVisible) {
                if (inDragRgn(where)) {
                    _ican.InkEnabled = false;
                    _feedbackWidget.Stroke = new SolidColorBrush(Color.FromArgb(255, 0, 0, 255));
                }
                else {
                    _ican.InkEnabled = barrelBtnPressed || inFloatieRgn(where);
                    _feedbackWidget.Stroke = new SolidColorBrush(Color.FromArgb(128, 0, 0, 255));
                }
            }
        }
        /// <summary>
        /// grabs the input focus after a MouseDown.  If Ink is enabled, then drawing begins, otherwise we start watching
        /// for crossing gestures to determine a direct manipulation mode.
        /// </summary>
        /// <param name="where"></param>
        /// <param name="stylus"></param>
        void grabInput(Pt where, StylusDevice stylus) {
            _moved = false;
            _xform = Mat.Identity;
            _downTime = DateTime.Now;
            _collecting = new Stroq(where);

            if (_ican.InkEnabled)
                startDrawing(stylus);
            else {
                Opacity = 0.1;
                Mouse.Capture(this);
                MouseMove += new MouseEventHandler(crossing_MouseMove);
                MouseUp += new MouseButtonEventHandler(mouseUp);
            }

            // compute location of click point in coordinate system of selection
            Matrix renderTransform = _ican.Selection.Outline.RenderTransform.Value;
            renderTransform.Invert();
            _selectionRelativePt = ((Mat)renderTransform) * where;
        }
        /// <summary>
        ///  switches to ink drawing after the mouse/stylus has been pressed.
        /// </summary>
        /// <param name="stylus"></param>
        void startDrawing(StylusDevice stylus) {
            this.MouseMove -= new MouseEventHandler(crossing_MouseMove);
            Mouse.Capture(null);
            _ican.InkEnabled = true;
            _ican.StartDrawing(stylus);
            _moved = true;
            Opacity = 1;
        }
        /// <summary>
        /// removes all input handlers installed by the Selection
        /// </summary>
        void releaseSelection() {
            // make sure all the move callbacks are gone...
            this.MouseUp -= new MouseButtonEventHandler(mouseUp);
            this.MouseMove -= new MouseEventHandler(rotHdlMove);
            this.MouseMove -= new MouseEventHandler(scaleHdlMove);
            this.MouseMove -= new MouseEventHandler(transHdlMove);
            this.MouseMove -= new MouseEventHandler(crossing_MouseMove);
            this.MouseMove -= new MouseEventHandler(flickMouseWait);
        }

        // mouse movement callbacks - check for flicks across widget edges or otherwise crossing events
        void mouseUp(object sender, MouseButtonEventArgs e) {
            if (!_ican.InkEnabled) {
                if (!_moved)
                    _ican.Selection = new SelectionObj();
                else
                    _ican.UndoRedo.Add(new XformAction(_ican.Selection, _xform, _ican));
                _ican.InkEnabled = true;
            }
            releaseSelection();
            Opacity = 1.0;
            Mouse.Capture(null);
        }
        void flickMouseWait(object sender, MouseEventArgs e) { flickWait(); }
        void flickWait() {
            if (DateTime.Now.Subtract(_downTime).TotalMilliseconds > 150 && _pendingMove != null) {
                // compute location of click point in coordinate system of selection
                Matrix renderTransform = _ican.Selection.Outline.RenderTransform.Value;
                renderTransform.Invert();
                _selectionRelativePt = ((Mat)renderTransform) * _selectionRelativePt;
                this.MouseMove -= new MouseEventHandler(flickMouseWait);
                this.MouseMove += _pendingMove;
            }
        }
        void crossing_MouseMove(object sender, MouseEventArgs e) { crossingMove(e.GetPosition(_ican), e.StylusDevice); }
        void crossingMove(Pt where, StylusDevice stylus) {
            _collecting.Add(where);
            if ((_collecting.Cusps().Length > 2 || _collecting.Cusps().Straightness(0, 1) > 0.16) && _collecting.Cusps()[_collecting.Cusps().Length - 1].dist > 25) {
                startDrawing(stylus);
                return;
            }
            Pt start = _ican.Selection.Outline.RenderTransform.Transform(_selectionRelativePt);
            Stroq outline = WPFUtil.PolygonOutline(_ican.Selection.Outline);
            LnSeg path = new LnSeg(start, where);
            Pt? hpt = null;
            double closest = 1, t;
            _pendingMove = null;

            // see if any widgets were crossed
            if ((hpt = path.Intersection(_moveWidget.Points.Select((Point p) => (Pt)p), out t)) != null && t < closest) { // crossed the translate boundary
                _selectionRelativePt = (Pt)hpt;
                _pendingMove = transHdlMove;
                closest = t;
            }
            if ((hpt = path.Intersection(WPFUtil.LineSeg(_rotLine), out t)) != null && t < closest) { // crossed the rotate boundary
                _selectionRelativePt = (Pt)hpt;
                _pendingMove = rotHdlMove;
                closest = t;
            }
            if ((hpt = path.Intersection(_feedbackWidget.Points.Select((Point p) => (Pt)p), out t)) != null && t < closest) { // crossed scale boundary
                _selectionRelativePt = (Pt)hpt;
                _pendingMove = scaleHdlMove;
                closest = t;
            }

            // perform 'flick' action on the first crossed widget
            if (_pendingMove == transHdlMove) {
                Vec dir = (path.B - path.A).Normal();
                if (SelectionPreTransformEvent != null)
                    SelectionPreTransformEvent(_ican.Selection);
                if (Math.Abs(dir.X) > Math.Abs(dir.Y))
                    _ican.Selection.MoveBy(new Vec((dir.X < 0) ? -1 : 1, 0));
                else _ican.Selection.MoveBy(new Vec(0, (dir.Y < 0) ? -1 : 1));
                if (SelectionPostTransformEvent != null)
                    SelectionPostTransformEvent(_ican.Selection);
            }

            if (_pendingMove != null) { // transition to flick/drag transition state
                _moved = true;
                this.MouseMove -= new MouseEventHandler(crossing_MouseMove);
                this.MouseMove += new MouseEventHandler(flickMouseWait);
            }
            else if (closest != 1)  // start an inking gesture
                startDrawing(stylus);
            else
                _moved = false; // keep waiting...
        }

        public delegate void SelectionTransformHandler(SelectionObj selection);
        public event SelectionTransformHandler SelectionPreTransformEvent;
        public event SelectionTransformHandler SelectionPostTransformEvent;
        // transformation movement callbacks - scale/rotate/translate
        void rotHdlMove(object sender, MouseEventArgs e) {
            if (e.LeftButton == MouseButtonState.Pressed && !_ican.Selection.Empty) {
                Pt rotCenter = _ican.Selection.ActualBounds.Center;// GeomUtils.Bounds(_feedbackWidget.Points).Center;
                Vec v1 = (Pt)e.GetPosition(_ican) - rotCenter; v1.Normalize();
                Vec v2 = WPFUtil.GetBounds(RotHdl, _ican).Center - rotCenter; v2.Normalize();
                _ican.Selection.XformBy(Mat.Rotate(v2.SignedAngle(v1), rotCenter));
                _xform = _xform * Mat.Rotate(v2.SignedAngle(v1), rotCenter);
                _moved = true;
            }
        }
        void transHdlMove(object sender, MouseEventArgs e) {
            if (!_ican.Selection.Empty) {
                if (SelectionPreTransformEvent != null)
                    SelectionPreTransformEvent(_ican.Selection);
                // transform selection point to canvas coordinates to compute translational delta
                Pt where = _ican.Selection.Outline.RenderTransform.Transform(_selectionRelativePt);
                _ican.Selection.MoveBy((Pt)e.GetPosition(_ican) - where);
                _xform = _xform * Mat.Translate((Pt)e.GetPosition(_ican) - where);
                if (SelectionPostTransformEvent != null)
                    SelectionPostTransformEvent(_ican.Selection);
            }
        }
        void scaleHdlMove(object sender, MouseEventArgs e) {
            if (e.LeftButton == MouseButtonState.Pressed && !_ican.Selection.Empty) {
                _moved = true;
                Rct actualBounds = _ican.Selection.ActualBounds;
                // transform selection point to canvas coordinates to compute translational delta
                Pt movingVert     = _ican.Selection.Outline.RenderTransform.Transform(_selectionRelativePt);
                Pt stationaryVert = actualBounds.Center;
                Pt newVertPos     = new Ln(stationaryVert, movingVert).ProjectPoint(e.GetPosition(_ican));
                Vec Xaxis = ((Mat)_ican.Selection.Outline.RenderTransform.Value * new Vec(1, 0)).Normal();
                Vec Yaxis = ((Mat)_ican.Selection.Outline.RenderTransform.Value * new Vec(0, -1)).Normal();
                Vec scaleVec = new Vec(Xaxis.Dot(newVertPos - stationaryVert) / Xaxis.Dot(movingVert - stationaryVert),
                                       Yaxis.Dot(newVertPos - stationaryVert) / Yaxis.Dot(movingVert - stationaryVert));
                // scale selection about opposite corner
                _ican.Selection.XformBy(Mat.Scale(scaleVec, stationaryVert));
                _xform = _xform * Mat.Scale(scaleVec, stationaryVert);
            }
        }

        // callbacks on the manipulation handles/regions
        void HandleMouseUp(object sender, MouseButtonEventArgs e) {
            Mouse.Capture(null);
            Opacity = 1;
        }
        void HandleMouseDown(object sender, MouseButtonEventArgs e) {
            Mouse.Capture(sender as Ellipse);
            e.Handled = true;
            Opacity = 0.1;
        }
        void HandleMouseMove(object sender, MouseEventArgs e) {
            if (e.LeftButton != MouseButtonState.Pressed)
                return;
            Pt tl = WPFUtil.GetBounds(TopLeft, _ican).Center;
            Pt tr = WPFUtil.GetBounds(TopRight, _ican).Center;
            Pt bl = WPFUtil.GetBounds(BotLeft, _ican).Center;
            Pt br = WPFUtil.GetBounds(BotRight, _ican).Center;
            Dictionary<Ellipse, List<Pt>> pairs = new Dictionary<Ellipse, List<Pt>>(){
              {TopLeft,  new List<Pt>(new Pt[] {tl, br})},
              {TopRight, new List<Pt>(new Pt[] {tr, bl})},
              {BotLeft,  new List<Pt>(new Pt[] {bl, tr})},
              {BotRight, new List<Pt>(new Pt[] {br, tl})}};
            Pt stationaryVert = pairs[sender as Ellipse][1];
            Pt movingVert = pairs[sender as Ellipse][0];
            Pt newVertPos = e.GetPosition(_ican);                   // get new coordinate
            double distFromDiagonal = new Ln(stationaryVert, movingVert).Distance(newVertPos);
            if (distFromDiagonal < 30)
                newVertPos = new Ln(stationaryVert, movingVert).ProjectPoint(newVertPos);
            Vec Xaxis = ((Mat)_ican.Selection.Outline.RenderTransform.Value * new Vec(1, 0)).Normal();
            Vec Yaxis = ((Mat)_ican.Selection.Outline.RenderTransform.Value * new Vec(0, -1)).Normal();
            Vec scaleVec = new Vec(Xaxis.Dot(newVertPos - stationaryVert) / Xaxis.Dot(movingVert - stationaryVert),
                                   Yaxis.Dot(newVertPos - stationaryVert) / Yaxis.Dot(movingVert - stationaryVert));
            // scale selection about opposite corner
            _ican.Selection.XformBy(Mat.Scale(scaleVec, (tr - tl).Normal(), stationaryVert));
            _xform = _xform * Mat.Scale(scaleVec, (tr - tl).Normal(), stationaryVert);
        }

        // callbacks from the Canvas 
        void ican_MouseDown(object sender, MouseButtonEventArgs e) {
            if (Visibility == Visibility.Visible) {
                e.Handled = true;
                grabInput(e.GetPosition(_ican), e.StylusDevice);
            }
        }
        void ican_MouseMove(object sender, MouseEventArgs e) {
            if (Visibility == Visibility.Visible &&
                e.LeftButton == MouseButtonState.Released &&
                e.RightButton == MouseButtonState.Released &&
                e.MiddleButton == MouseButtonState.Released)
                prepareToGrab(e.GetPosition(_ican), e.StylusDevice != null && e.StylusDevice.SwitchState(InqUtils.BarrelSwitch) == StylusButtonState.Down);
        }
        void ican_SelectionMoved(SelectionObj sel) { updateBounds(); }
        void ican_SelectedChanged(InqCanvas canvas) {
            // register for move events on the new selection
            _ican.Selection.SelectionMovedEvent += new SelectionObj.SelectionMovedHandler(ican_SelectionMoved);
            if (_ican.Selection.Empty)
                Visibility = Visibility.Hidden;
            else {
                Opacity = 1;
                updateBounds();
                Visibility = Visibility.Visible;
            }
        }

        public SelectionFeedback(InqScene ican) {
            this.InitializeComponent();

            _ican = ican;
            Visibility = Visibility.Hidden;
            _floatie.CommandPanel = new SelectionToolbar(_ican);
            _feedbackWidget.Fill = new SolidColorBrush(Color.FromArgb(18, 255, 200, 200));
            _feedbackWidget.Stroke = Brushes.Blue;
            _feedbackWidget.StrokeThickness = 0.5;
            _feedbackWidget.StrokeDashArray = new DoubleCollection(new double[] { 3, 3 });
            _moveWidget.Fill = new SolidColorBrush(Color.FromArgb(48, 255, 200, 200));
            _moveWidget.Stroke = Brushes.Red;
            _moveWidget.StrokeThickness = 0.5;
            _moveWidget.StrokeDashArray = new DoubleCollection(new double[] { 3, 3 });
            _rotLine.Stroke = Brushes.Blue;
            _rotLine.StrokeThickness = 0.5;
            _rotLine.StrokeDashArray = new DoubleCollection(new double[] { 3, 3 });
            _floatie.SetInkCanvas(_ican);
            LayoutRoot.Children.Insert(0, _floatie);
            LayoutRoot.Children.Insert(0, _moveWidget);
            LayoutRoot.Children.Insert(0, _feedbackWidget);
            LayoutRoot.Children.Insert(0, _rotLine);
            ican.SelectedChangedEvent += new InqScene.SelectedChangedHandler(ican_SelectedChanged);
            ican.MouseMove += new MouseEventHandler(ican_MouseMove);
            ican.MouseDown += new MouseButtonEventHandler(ican_MouseDown);
            RotHdl.PreviewMouseDown += new MouseButtonEventHandler(HandleMouseDown);
            BotRight.PreviewMouseDown += new MouseButtonEventHandler(HandleMouseDown);
            BotLeft.PreviewMouseDown += new MouseButtonEventHandler(HandleMouseDown);
            TopRight.PreviewMouseDown += new MouseButtonEventHandler(HandleMouseDown);
            TopLeft.PreviewMouseDown += new MouseButtonEventHandler(HandleMouseDown);
            RotHdl.MouseMove += new MouseEventHandler(rotHdlMove);
            BotRight.MouseMove += new MouseEventHandler(HandleMouseMove);
            BotLeft.MouseMove += new MouseEventHandler(HandleMouseMove);
            TopRight.MouseMove += new MouseEventHandler(HandleMouseMove);
            TopLeft.MouseMove += new MouseEventHandler(HandleMouseMove);
            RotHdl.MouseUp += new MouseButtonEventHandler(HandleMouseUp);
            BotRight.MouseUp += new MouseButtonEventHandler(HandleMouseUp);
            BotLeft.MouseUp += new MouseButtonEventHandler(HandleMouseUp);
            TopRight.MouseUp += new MouseButtonEventHandler(HandleMouseUp);
            TopLeft.MouseUp += new MouseButtonEventHandler(HandleMouseUp);
            RotHdl.Width = RotHdl.Height = 24;
            TopLeft.Width = TopLeft.Height = 24;
            TopRight.Width = TopRight.Height = 24;
            BotLeft.Width = BotLeft.Height = 24;
            BotRight.Width = BotRight.Height = 24;
        }
        /// <summary>
        /// whether resize and rotate handles are used on the Selection
        /// </summary>
        public bool UseHandles { get { return _useHandles; } set { _useHandles = value; } }
        public Floatie Floatie { get { return _floatie; } }
    }
}