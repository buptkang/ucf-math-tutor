﻿using System;
using System.IO;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Navigation;


namespace starPadSDK.AppLib {
    public partial class SelectionToolbar {
        InqScene _ican;

        void copy_Click(object sender, RoutedEventArgs e) { Clipboard.SetDataObject(_ican.Selection); }
        void cut_Click (object sender, RoutedEventArgs e) {
            Clipboard.SetDataObject(_ican.Selection);

            _ican.UndoRedo.Add(new DeleteAction(_ican.Selection, _ican));
            _ican.Selection = new SelectionObj();  // update the selection
        }


        public SelectionToolbar(InqScene ican) {
            _ican = ican;

            this.InitializeComponent();

            // An alternative to creating buttons w/ Expression Blend is to create them programmatically, as:
            Button cut = new Button();
            cut.Content = "Cut";
            cut.Click += new RoutedEventHandler(cut_Click);
            toolbar.Items.Add(cut);
        }
        public ToolBar ToolBar { get { return toolbar; } }
    }
}