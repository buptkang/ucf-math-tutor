﻿using starPadSDK.Inq;
using starPadSDK.WPFHelp;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace starPadSDK.AppLib {
    [Serializable()]
    public class InqScene : InqCanvas, ISerializable {
        [NonSerialized()] SelectionObj           _selection  = new SelectionObj();
        [NonSerialized()] SelectionFeedback      _feedback   = null;
        [NonSerialized()] GroupMgr               _groups     = new GroupMgr();
        [NonSerialized()] List<Wiggle>           _wiggles = new List<Wiggle>();
        [NonSerialized()] List<FrameworkElement> _elements = new List<FrameworkElement>();
        [NonSerialized()] Canvas                 _sceneLayer = new Canvas();
        [NonSerialized()] UndoRedo               _undoRedo   = new UndoRedo();
        [NonSerialized()] CommandSet             _cmds = null;  // gesture commands active on this page

        #region ISerializable Members
        [NonSerialized()]
        List<Stroq> _pending = new List<Stroq>(); // a list of Stroq's that need to be added to the Page after deserialization is completed.

        virtual protected void getObjectData(SerializationInfo info, StreamingContext context) {
            List<AnImage.ImageProxy> _images = new List<AnImage.ImageProxy>();
            List<ATextBox.ATextBoxProxy> _texts = new List<ATextBox.ATextBoxProxy>();
            foreach (FrameworkElement e in Elements)
                if (e is Image) _images.Add((e as Image).Proxy());
                else if (e is Canvas && (e as Canvas).Children[0] is Image) _images.Add(((e as Canvas).Children[0] as Image).Proxy(e as Canvas));
                else if (e is TextBox) _texts.Add((e as TextBox).Proxy());
            info.AddValue("Stroqs", new List<Stroq>(Stroqs));
            info.AddValue("Images", _images);
            info.AddValue("Texts", _texts);
        }
        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context) {
            getObjectData(info, context);
        }

        [OnDeserialized()]
        void onDeserializedFix(StreamingContext sc) {
            foreach (Stroq s in _pending) {
                s.onDeserializedFix(sc);
                AddWithUndo(s);
            }
            _pending.Clear();
        }
        #endregion

        /// <summary>
        /// Adds a handler for generating Undo entries when the TextBox is edited
        /// </summary>
        /// <param name="tb"></param>
        protected void set(TextBox tb) {
           tb.PreviewMouseLeftButtonDown += (((object s, MouseButtonEventArgs e) => UndoRedo.Add(new TextEnteredAction(s as TextBox, (s as TextBox).Text, this))));
        }
        /// <summary> 
        /// Prevents Ink from being started over TextBox's
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseMove(MouseEventArgs e) {
            if (e.LeftButton == MouseButtonState.Released &&
                e.MiddleButton == MouseButtonState.Released &&
                e.RightButton == MouseButtonState.Released &&
                _feedback.Visibility == Visibility.Hidden) {
                InkEnabled = true;
                foreach (FrameworkElement fe in Elements)
                    if (fe is TextBox && fe.InputHitTest(WPFUtil.TransformFromAtoB(e.GetPosition(this), this, fe)) != null)
                        InkEnabled = false;
            }
            base.OnMouseMove(e);
        }
        protected virtual void init() {
            _feedback = new SelectionFeedback(this);
            Children.Add(_feedback);
        }

        public delegate void ElementHandler(FrameworkElement e);
        public delegate void StroqHandler(Stroq s);
        public delegate bool StroqFilterHandler(Stroq s);
        public delegate bool StroqReplaceFilterHandler(Stroq[] orig, Stroq[] replace);
        public delegate void SelectedChangedHandler(InqScene canvas);
        public event StroqFilterHandler     StroqPreAddEvent;
        public event StroqReplaceFilterHandler StroqPreReplaceEvent;
        public event StroqHandler           StroqAddedEvent;
        public event StroqHandler           StroqRemovedEvent;
        public event ElementHandler        ElementAddedEvent;
        public event ElementHandler        ElementRemovedEvent;
        public event SelectedChangedHandler SelectedChangedEvent;
        
        public InqScene(SerializationInfo info, StreamingContext context):this() {
            List<Stroq>                   stroqs = (List<Stroq>)info.GetValue("Stroqs", typeof(List<Stroq>));
            List<ATextBox.ATextBoxProxy> _texts = (List<ATextBox.ATextBoxProxy>)info.GetValue("Texts", typeof(List<ATextBox.ATextBoxProxy>));
            List<AnImage.ImageProxy>     _images = (List<AnImage.ImageProxy>)info.GetValue("Images", typeof(List<AnImage.ImageProxy>));
            foreach (ATextBox.ATextBoxProxy t in _texts) AddNoUndo(t.Create());
            foreach (AnImage.ImageProxy ip in _images) AddNoUndo(ip.Create());
            _pending = stroqs;
        }

        public InqScene() : base(false) {
            _drawStroqs = false; // don't want the base InqCanvas to draw anything.  We'll do that ourself
            Children.Add(_sceneLayer);
            init();
        }
        public CommandSet         Commands           { get { return _cmds; } set { _cmds = value; } }
        public SelectionObj       Selection          {
            get { return _selection; }
            set {
                _selection = value;
                if (SelectedChangedEvent != null)
                    SelectedChangedEvent(this);
            }
        }
        public SelectionFeedback  Feedback           { get { return _feedback; } }
        public UndoRedo           UndoRedo           { get { return _undoRedo; } }
        public Canvas             SceneLayer         { get { return _sceneLayer; } set { _sceneLayer = value; } }
        public GroupMgr           Groups             { get { return _groups; } }
        public Wiggle[]           Wiggles            { get { return _wiggles.ToArray(); } }
        public FrameworkElement[] Elements           { get { return _elements.ToArray(); } }

        public void ReplaceNoUndo(Stroq[] orig, Stroq[] replace) {
            if (StroqPreReplaceEvent != null) 
                if (StroqPreReplaceEvent(orig, replace)) 
                    return;
            foreach (Stroq s in orig)
                if (Stroqs.Contains(s))
                    Rem(s);
            foreach (Stroq s in replace)
                if (!Stroqs.Contains(s)) {
                    AddNoUndo(s);
                }
        }
        public void AddWithUndo(TextBox t) { 
            AddNoUndo(t as FrameworkElement); 
            UndoRedo.Add(new TextEnteredAction(t, t.Text, this));
        }
        public void AddWithUndo(FrameworkElement c)  { 
            if (!_elements.Contains(c)) { 
                _elements.Add(c); 
                _sceneLayer.Children.Add(c);
                UndoRedo.Add(new ElementAddedAction(c, this));
                if (ElementAddedEvent != null)
                    ElementAddedEvent(c); 
                                                                 
                if (c is TextBox) set(c as TextBox);   
            } 
        }
        public void AddWithUndo(Stroq s)  {
            if (StroqPreAddEvent != null) 
                if (StroqPreAddEvent(s)) 
                    return; 
            if (!Stroqs.Contains(s)) {
                Stroqs.Add(s);
                _sceneLayer.Children.Add(s);
                UndoRedo.Add(new InkAddedAction(s, this));
                if (StroqAddedEvent != null) 
                    StroqAddedEvent(s); 
            }
        }
        public void AddWithUndo(Wiggle w) {
            if (!_wiggles.Contains(w)) {
                _wiggles.Add(w); 
                AddNoUndo(w.Line[0]); 
                AddNoUndo(w.A); 
                AddNoUndo(w.B);
                UndoRedo.Add(new WiggleAddedAction(w, this));
            }
        }
        public void Rem(Wiggle w)  { _wiggles.Remove(w); Rem(w.Line[0]); Rem(w.A); Rem(w.B); }
        public void Rem(FrameworkElement c)  { 
            _elements.Remove(c);
            _sceneLayer.Children.Remove(c);
            if (ElementRemovedEvent != null)
                ElementRemovedEvent(c);
        }
        public void Rem(Stroq s)  {
            Stroqs.Remove(s);
            foreach (Wiggle w in _wiggles)
                if (w.Line[0] == s) {
                    Rem(w.A);
                    Rem(w.B);
                    _wiggles.Remove(w);
                    break;
                }
            foreach (var e in _sceneLayer.Children)
                if (e is StroqElement && (e as StroqElement).Stroq == s) {
                    _sceneLayer.Children.Remove(e as StroqElement);
                    break;
                }
            if (StroqRemovedEvent != null)
                StroqRemovedEvent(s);
        }
        public void RemWithUndo(Wiggle w)                  { UndoRedo.Add(new DeleteAction(new SelectionObj(w.Line), this)); }
        public void RemWithUndo(Stroq s)                     { UndoRedo.Add(new DeleteAction(new SelectionObj(s), this)); }
        public void RemWithUndo(FrameworkElement f) { UndoRedo.Add(new DeleteAction(new SelectionObj(f), this)); }
        public void AddNoUndo(FrameworkElement c) {
            if (!_elements.Contains(c)) {
                _elements.Add(c); 
                _sceneLayer.Children.Add(c);
                if (c is TextBox) 
                    set(c as TextBox);
            }
        }
        public void AddNoUndo(Stroq s) { 
            if (!Stroqs.Contains(s)) { 
                Stroqs.Add(s);
                _sceneLayer.Children.Add(s); 
                if (StroqAddedEvent != null) StroqAddedEvent(s); 
            } 
        }
        public void AddNoUndo(Wiggle w) { 
            if (!_wiggles.Contains(w)) { 
                _wiggles.Add(w); 
                AddNoUndo(w.Line[0]); 
                AddNoUndo(w.A); 
                AddNoUndo(w.B); 
            } 
        }
        public void Clear() { Stroqs.Clear(); this.Selection = new SelectionObj();  _elements.Clear(); _sceneLayer.Children.Clear(); }
        public Wiggle       Wiggle(Stroq s)          {
            foreach (Wiggle w in _wiggles)
                if (w.Line[0] == s)
                    return w;
            return null;
        }
        public SelectionObj PasteSelection()         {
            SelectionObj sel = null;
            IDataObject obj = Clipboard.GetDataObject();
            if (obj != null && obj.GetDataPresent(typeof(BitmapSource))) {
                BitmapSource o = (BitmapSource)obj.GetData(typeof(BitmapSource));
                Image img = new Image();
                img.Width = o.Width;
                img.Height = o.Height;
                if (img.Width > ActualWidth / 2) {
                    img.Height = ActualWidth / 2 / img.Width * img.Height;
                    img.Width = ActualWidth / 2;
                }
                if (img.Height > ActualHeight / 2) {
                    img.Width = ActualHeight / 2 / img.Height * img.Width;
                    img.Height = ActualHeight / 2;
                }
                img.Source = o;
                img.VerticalAlignment = VerticalAlignment.Top;
                AddWithUndo(img);
                sel = new SelectionObj(null, new FrameworkElement[] { img }, null);
            }
            if (obj != null && obj.GetDataPresent(typeof(SelectionObj))) {
                sel = obj.GetData(typeof(SelectionObj)) as SelectionObj;
                if (sel != null) {
                    foreach (Stroq s in sel.Strokes)
                        AddNoUndo(s);
                    foreach (FrameworkElement i in sel.Elements)
                        AddNoUndo(i);
                    UndoRedo.Add(new SelectionAddedAction(sel, this));
                }
            }
            return sel;
        }
    }
}