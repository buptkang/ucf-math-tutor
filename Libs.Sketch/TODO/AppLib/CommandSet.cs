﻿using starPadSDK.Inq;
using System.Collections.Generic;
using System.Windows;

namespace starPadSDK.AppLib {

    public class CommandSet {

        public class CommandEditor {
            protected InqScene _can;
            bool can_StroqAddedEvent(Stroq s) {
                if (!Enabled)
                    return false;
                return stroqAdded(s);
            }
            void can_StroqRemovedEvent(Stroq s) {
                stroqRemoved(s);
            }
            virtual protected bool stroqAdded(Stroq s) { return false;  }
            virtual protected void stroqRemoved(Stroq a) { }

            public bool Enabled { get; set; }
            public CommandEditor(InqScene can) {
                _can = can;
                Enabled = false;
                _can.StroqPreAddEvent += new InqScene.StroqFilterHandler(can_StroqAddedEvent);
                _can.StroqRemovedEvent += new InqScene.StroqHandler(can_StroqRemovedEvent);
            }
        }

        protected InqScene            _can = null;
        protected Gesturizer          _gest = null;   // the gesture collection object
        protected Gesturizer          _right = null;
        protected List<CommandEditor> _editors = new List<CommandEditor>();
       
        virtual protected void keyDown(object sender, System.Windows.Input.KeyEventArgs e) {
            // See if this keyDown event follows a partial Text insertion gesture.  If so, then we wanto recognize the gesture 
            // and create a TextBox
            if (_gest.Pending.Count > 0 && _gest.Pending[0].IsInsertion()) {
                // force 'text' gesture to be recognized
                new TextCommand(_can).Fire(_gest.Pending.ToArray());
                // cleanup the recognizer since we didn't process the gesture the "normal" way
                _gest.Pending.Clear();
                _gest.Reset();
                // re-enable all widgets that were disabled by the first part of the text box gesture in StroqCollected
                // this would normally have been done in StroqCollected by the second stroke of the gesture in StroqCollected.
                foreach (FrameworkElement fe in _can.Elements)
                    fe.IsHitTestVisible = true;
                _can.Feedback.IsHitTestVisible = true;
                _can.InkEnabled = true;
            }
        }
       
        virtual protected void stroqCollected(object sender, InqCanvas.StroqCollectedEventArgs e) {
            if (e.RightButton)
                _right.Process(e.Stroq);
            else
                _gest.Process(e.Stroq);

            bool enableWidgets = _gest.Pending.Count == 0;
            // disable all widgets (ie, TextBox's) if we have a partial gesture so that the next gesture stroke can be drawn
            // over the widget if necessary.
            // The next stroke will re-enable these widgets as will a keyDown that creates a text gesture
            foreach (FrameworkElement fe in _can.Elements)
                fe.IsHitTestVisible = enableWidgets;
            _can.Feedback.IsHitTestVisible = enableWidgets;

            if (!enableWidgets)
                _can.InkEnabled = true;
        }

        virtual protected bool gestureUnrecognizedEvent(object sender, Stroq s) {
            if (_can.Selection != null && !_can.Selection.Empty)  // Policy: ignore unrecognized gesture strokes when there is a selection
                return true;

            s.BackingStroke.DrawingAttributes.Color = _can.DefaultDrawingAttributes.Color;  // restore Color of Stroq

            bool handled = false;
            if (NonGestureEvent != null)
                handled |= NonGestureEvent(this, s);
            if (!handled)
                _can.AddWithUndo(s);
            return true;
        }

        /// <summary>
        /// Initialize Gesture sets for application modes
        /// </summary>
        /// <param name="curveGestures"></param>
        virtual protected void InitGestures()      { _gest.Clear(); }
        virtual protected void InitRightGestures() { _right.Clear(); }
        virtual protected List<CommandEditor> InitEditors() { return new List<CommandEditor>(); }

        public event Gesturizer.StrokeUnrecognizedHandler NonGestureEvent;
        /// <summary>
        /// A default command set to associate with a canvas that includes generically useful and representative gestures.
        /// </summary>
        /// <param name="can"></param>
        public CommandSet(InqScene can) { Scene = can;  }

        public Gesturizer           GestureRecognizer { get { return _gest;  } }
        public List<CommandEditor> Editors { get { return _editors; } }
        public InqScene            Scene {
            get { return _can; }
            set {
                _can = value;
                _right = new Gesturizer(_can);
                _gest = new Gesturizer(_can);
                _can.Children.Insert(1, _right);
                _can.Children.Insert(1, _gest); // add _gest to Children list so that its feedback can be displayed

                _editors = InitEditors();
                InitGestures();
                InitRightGestures();

                _can.KeyDown += new System.Windows.Input.KeyEventHandler(keyDown);
                _can.StroqCollected += new InqScene.StroqCollectedEventHandler(stroqCollected);
                _gest.StrokeUnrecognizedEvent += new Gesturizer.StrokeUnrecognizedHandler(gestureUnrecognizedEvent);
            }
        }
    }
}
