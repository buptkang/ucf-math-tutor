﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Windows;
using System.Windows.Ink;
using System.Windows.Input;
using starPadSDK.Inq;

namespace starPadSDK.DollarRecognizer {
    public class DollarUtil {
        GeometricRecognizer _rec = new GeometricRecognizer();

        public GeometricRecognizer Rec {
            get { return _rec; }
            set { _rec = value; }
        }

        public DollarUtil(string dir, List<string> filterIn, List<string> filterOut) {
            try {
                foreach (string name in System.IO.Directory.GetFiles(dir + "\\DollarGestures", "*.xml")) {
                    string[] splits = System.IO.Path.GetFileName(name).Split(new char[] { '_' });
                    if (filterIn.Contains(splits[0]) && !filterOut.Contains(splits[0]))
                        _rec.LoadGesture(name);
                }
            } catch {
            }
        }

        public string Recognize(Stroke s, double thresh) {
            int t = 0;
            List<PointR> points = new List<PointR>();
            foreach (StylusPoint pp in s.StylusPoints) {
                points.Add(new PointR(pp.X, pp.Y, t));
                t+=10;
            }
            if (points.Count >= 5) // require 5 points for a valid gesture
                if (_rec.NumGestures > 0) // not recording, so testing
					{
                    NBestList result = _rec.Recognize(points); // where all the action is!!
                    if (result.Score > thresh && Math.Abs(result.Angle - Math.PI*(((int)result.Angle/Math.PI))) < Math.PI/4)
                        return result.Name.Split('_')[0];
                }
            return "";
        }
    }
    /// <summary>
    /// Adds two methods to a Stroq:
    ///  Dollar() - the name of the Dollar recognizer matched
    /// </summary>
    static public class DollarTester {
        static Guid DOLLAR = new Guid("0900773F-9EDC-43fd-862B-F33EA01D3403");
        static public string       Directory = System.IO.Directory.GetCurrentDirectory();
        static public List<string> FilterOut = new List<string>();
        class DollarRec {
            public string[] _gestures;
            public string   _rec;
            public DollarRec(string[] gestures, string recog) { _gestures = gestures; _rec = recog; }
        }
        static public string Dollar(this Stroq stroke, string[] filterIn) {
            string rec = "";
            if (!stroke.Property.Exists(DOLLAR) || ((DollarRec)stroke.Property[DOLLAR])._gestures != filterIn) {
                DollarUtil du = new DollarUtil(Directory, new List<string>(filterIn), FilterOut);
                rec = du.Recognize(stroke.BackingStroke, 0.8);
                stroke.Property[DOLLAR] = new DollarRec(filterIn, rec);
            } else
                rec = ((DollarRec)stroke.Property[DOLLAR])._rec;
            return rec;
        }
    }
}
