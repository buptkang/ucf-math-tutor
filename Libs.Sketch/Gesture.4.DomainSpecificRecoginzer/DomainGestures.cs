﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Recognizer.NDollar;
using System.IO;
using starPadSDK.CharRecognizer;
using starPadSDK.Geom;
using starPadSDK.Inq;


namespace DomainSpecificRecoginzer
{
    public class DomainGestureRecognizer
    {
        #region Singleton

        private static DomainGestureRecognizer _instance;

        public static DomainGestureRecognizer Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new DomainGestureRecognizer();        
                }
                return _instance;
            }
        }
        #endregion

        #region Property and Initialization

        public GeometricRecognizer NRecognizer { get; set; }

        private DomainGestureRecognizer()
        {
            NRecognizer = new GeometricRecognizer();
            InitNDollarRecognizer();
        }

        private void InitNDollarRecognizer()
        {
            string[] fileEntries = Directory.GetFiles(GetGestureTrainingData());
            foreach (string filePath in fileEntries)
            {
                NRecognizer.LoadGesture(filePath);
            }
        }

        private string GetGestureTrainingData()
        {
            var info = new DirectoryInfo(Directory.GetCurrentDirectory());
            string dir = info.Parent.Parent.Parent.FullName;
            return Path.Combine(dir, "Libs", "AGSymbolTraining", "AGData");
        }

        #endregion
    }

    public static class DomainGestures
    {
        public static bool IsCheckMark(Stroq stroq)
        {
            List<StylusPoint> stroqPts = stroq.StylusPoints.ToList();

            var points = new List<PointR>();
            points.AddRange(ConvertPointR(stroqPts));
            Recognizer.NDollar.NBestList result = DomainGestureRecognizer.Instance.NRecognizer.Recognize(points, 1); // where all the action is!!

            if (result.Score > 0.8 && result.Name.Substring(0, result.Name.IndexOf('_')) == "CheckMark")
            {
                return true;
            }

            return false;
        }

        public static bool IsQuestionMark(Stroq stroq1, Stroq stroq2)
        {
            if (stroq1 == null || stroq2 == null) return false;

            // combine the strokes into one unistroke, Lisa 8/8/2009
            var points = new List<PointR>();

            List<StylusPoint> stroq1Pts = stroq1.StylusPoints.ToList();
            List<StylusPoint> stroq2Pts = stroq2.StylusPoints.ToList();

            points.AddRange(ConvertPointR(stroq1Pts));
            points.AddRange(ConvertPointR(stroq2Pts));

            Recognizer.NDollar.NBestList result = DomainGestureRecognizer.Instance.NRecognizer.Recognize(points, 2); // where all the action is!!

            if (result.Score > 0.8 && result.Name.Substring(0, result.Name.IndexOf('_')) == "QuestionMark")
            {
                return true;
            }

            return false;
        }

        public static bool IsEqualSign(Stroq stroq1, Stroq stroq2)
        {
            // combine the strokes into one unistroke, Lisa 8/8/2009
            var points = new List<PointR>();

            List<StylusPoint> stroq1Pts = stroq1.StylusPoints.ToList();
            List<StylusPoint> stroq2Pts = stroq2.StylusPoints.ToList();

            points.AddRange(ConvertPointR(stroq1Pts));
            points.AddRange(ConvertPointR(stroq2Pts));

            Recognizer.NDollar.NBestList result = DomainGestureRecognizer.Instance.NRecognizer.Recognize(points, 2); // where all the action is!!

            if (result.Score > 0.8 && result.Name.Substring(0, result.Name.IndexOf('_')) == "EqualSign")
            {
                return true;
            }

            return false;
        }

        public static bool IsDownArrow(Stroq stroq)
        {
            List<StylusPoint> stroqPts = stroq.StylusPoints.ToList();

            var points = new List<PointR>();
            points.AddRange(ConvertPointR(stroqPts));
            Recognizer.NDollar.NBestList result = DomainGestureRecognizer.Instance.NRecognizer.Recognize(points, 1); // where all the action is!!

            if (result.Score > 0.8 && result.Name.Substring(0, result.Name.IndexOf('_')) == "DownArrow")
            {
                return true;
            }

            return false;
        }

        public static bool IsDownArrow(Stroq stroq, Stroq stroq2)
        {
            List<StylusPoint> stroqPts = stroq.StylusPoints.ToList();
            List<StylusPoint> stroqPts1 = stroq2.StylusPoints.ToList();
            var points = new List<PointR>();

            points.AddRange(ConvertPointR(stroqPts));
            points.AddRange(ConvertPointR(stroqPts1));

            Recognizer.NDollar.NBestList result = DomainGestureRecognizer.Instance.NRecognizer.Recognize(points, 1); // where all the action is!!

            if (result.Score > 0.8 && result.Name.Substring(0, result.Name.IndexOf('_')) == "DownArrow")
            {
                return true;
            }

            return false;
        }

        private static List<PointR> ConvertPointR(List<StylusPoint> sps)
        {
            return sps.Select(sp => new PointR(sp.X, sp.Y)).ToList();
        }

        #region Add Hoc Geometric Shapes

        public static bool IsShape(Stroq stroq, String name)
        {
            // combine the strokes into one unistroke, Lisa 8/8/2009
            var points = new List<PointR>();

            List<StylusPoint> stroq1Pts = stroq.StylusPoints.ToList();

            points.AddRange(ConvertPointR(stroq1Pts));

            try
            {
                NBestList result = DomainGestureRecognizer.Instance.NRecognizer.Recognize(points, 1);
                    // where all the action is!!
                if (result.Score > 0.8 && result.Name.Substring(0, result.Name.IndexOf('_')) == name)
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(@"get error exception: {0}", e);
                return false;
            }
            return false;
        }

        #endregion
    }
}
