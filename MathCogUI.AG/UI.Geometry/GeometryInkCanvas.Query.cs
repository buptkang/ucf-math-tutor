﻿/*******************************************************************************
 * Math Interactive Tutoring System
 * <p>
 * Copyright (C) 2015 Bo Kang, Joseph J. LaViola Jr.
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using DynamicGeometry;
using MathCog.UserModeling;
using MathCogUI;
using starPadSDK.Geom;

namespace MathCogUIAG
{
    using starPadSDK.Inq;
    using System;

    public partial class AGGeometryInkCanvas
    {
        public override void Query(object obj, bool gesture = false)
        {
            var algebraCanvas = _gh.AlgebraGrid.AlgebraicInkCanvas;
            algebraCanvas.Query(obj, gesture);
        }

        public override bool Select(object obj)
        {
            return true;
            #region Obsolete object label interaction

            /*  var stroq = obj as Stroq;
            if (stroq == null) return false;

            foreach (StylusPoint sp in stroq.StylusPoints)
            {
                var pt1 = DC.Drawing.CoordinateSystem.ToLogical(sp.ToPoint());
                foreach (var figure in DC.Drawing.Figures)
                {
                    var hitFigure = figure.HitTest(pt1);
                    var grid = hitFigure as CartesianGrid;
                    if (grid != null) continue;
                    if (hitFigure != null)
                    {
                        string message;
                        bool result = GeoQueryUIEngine.Instance.Select(pt1, hitFigure, out message);
                        NotifyClientInfo(message, true);
                        return true;
                    }
                }
            }

            Rct rct = stroq.GetBounds();
            var pt11 = DC.Drawing.CoordinateSystem.ToLogical(rct.TopLeft);
            var pt22 = DC.Drawing.CoordinateSystem.ToLogical(rct.BottomRight);
            var gRect = new Rect(pt11, pt22);

            foreach (var figure in DC.Drawing.Figures)
            {
                var cPoint = figure.Center;
                if (gRect.Contains(cPoint))
                {
                    var grid = figure as CartesianGrid;
                    if (grid != null) continue;

                    string message;
                    GeoQueryUIEngine.Instance.Select(gRect, figure, out message);
                    NotifyClientInfo(message, true);
                    return true;
                }
            }

            return false;*/

            #endregion
        }

        public override void MoreStrokesAnalysis()
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                var lst = new StroqCollection();
                while (_slidingWindow.Count != 0)
                {
                    Stroqs.Remove(_slidingWindow.Dequeue());
                }
            }));
        }

        public override void ProcessStrokes()
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                var lst = new StroqCollection();
                while (_slidingWindow.Count != 0)
                {
                    Stroqs.Remove(_slidingWindow.Dequeue());
                }
/*                var lst = new StroqCollection();
                while (_slidingWindow.Count != 0)
                {
                    lst.Add(_slidingWindow.Dequeue());
                }
                AddStrokeToBackEnd(lst);

                if (lst.Count > 1)
                {
                    var geoEditor = AGEditor as AGGeometryEditor;
                    if (geoEditor != null)
                    {
                        geoEditor.ShowSidebarAlts();
                    }
                }*/
            }));
        }

        protected override void UpdateParse()
        {
            var agEditor = _gh.AlgebraGrid.AlgebraicInkCanvas.AGEditor as AlgebraicEditor;

            if (agEditor != null)
            {
                agEditor.Parse();
            }

         /*   Dispatcher.Invoke((Action)(() =>
            {
               
            }));*/
        }
    }
}
