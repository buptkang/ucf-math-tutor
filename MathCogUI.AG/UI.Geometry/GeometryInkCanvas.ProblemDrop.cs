﻿/*******************************************************************************
 * Math Interactive Tutoring System
 * <p>
 * Copyright (C) 2015 Bo Kang, Joseph J. LaViola Jr.
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

using MathCog;
using MathCog.UserModeling;
using starPadSDK.Geom;

namespace MathCogUIAG
{
    using System.Diagnostics;
    using MathCogUI;

    public partial class AGGeometryInkCanvas
    {
        protected override void OnDrop(System.Windows.DragEventArgs e)
        {
            //InteractiveAnalytics.Instance.StartTimer();

            System.Windows.Point pt = e.GetPosition(this);
            var algebraEditor = _gh.AlgebraGrid.AlgebraicInkCanvas.AGEditor as AGAlgebraEditor;
            Debug.Assert(algebraEditor != null);

            #region Concept Input

            var obj1 = e.Data.GetData("ConceptObject") as ConceptText;
            if (obj1 != null)
            {
                //MessageBox.Show("Drag this concept onto the algebra side instead!");
                _gh.AlgebraGrid.AlgebraicInkCanvas.NotifyClientConcept(obj1);

            }
            #endregion

            #region Hybrid Input

            var obj2 = e.Data.GetData("HybridObject") as HybridText;
            if (obj2 != null)
            {
                _gh.AlgebraGrid.AlgebraicInkCanvas.NotifyClientConcept(obj2);

                obj2.OnAlgebraSide = false;

                if (!obj2.IsDraggable)
                {
                    NotifyClientInfo(AGWarning.DragKnowledge, true);
                    return;
                }

                //QueryUIEngine.Instance.QueriedKnowledge = null;
                if (!HCIReasoner.TutorMode)
                {
                    algebraEditor.AddRange(obj2, pt);
                    algebraEditor.Parse();
                }
                else
                {
                    bool isQuery = Reasoner.Instance.RelationGraph.FindQuery(obj2.DragContent);
                    if (isQuery)
                    {
                        var bottomRight = new Pt(pt.X + 20.0, pt.Y + 20.0);
                        var rct = new Rct(pt, bottomRight);
                        //Query();
                        Query(rct);
                    }
                    else
                    {
                        bool? result = Reasoner.Instance.VerifyConcreteSymbol(obj2.DragContent);

                        if (result == null)
                        {
                            NotifyClientInfo(AGWarning.DragFailedOnGeo1, true);
                            return;
                        }

                        if (!result.Value)
                        {
                            NotifyClientInfo(AGWarning.DragFailedOnGeo2, true);
                            return;
                        }

                        algebraEditor.AddRange(obj2, pt);
                        algebraEditor.Parse();
                    }
                }
            }

            #endregion

            #region Problem Input
            var dragSource = e.Data.GetData("ProblemDragObject") as InputText;

            if (dragSource != null)
            {
                dragSource.OnAlgebraSide = false;
                if (!dragSource.IsDraggable)
                {
                    NotifyClientInfo(AGWarning.DragKnowledge, true);
                    return;
                }

                //QueryUIEngine.Instance.QueriedKnowledge = null;
                if (!HCIReasoner.TutorMode)
                {
                    algebraEditor.AddRange(dragSource, pt);
                    algebraEditor.Parse();
                }
                else
                {
                    bool isQuery = Reasoner.Instance.RelationGraph.FindQuery(dragSource.DragContent);
                    if (isQuery)
                    {
                        var bottomRight = new Pt(pt.X + 20.0, pt.Y + 20.0);
                        var rct = new Rct(pt, bottomRight);
                        //Query();
                        Query(rct);
                    }
                    else
                    {
                        bool? result = Reasoner.Instance.VerifyConcreteSymbol(dragSource.DragContent);

                        if (result == null)
                        {
                            NotifyClientInfo(AGWarning.DragFailedOnGeo1, true);
                            return;
                        }

                        if (!result.Value)
                        {
                            NotifyClientInfo(AGWarning.DragFailedOnGeo2, true);
                            return;
                        }

                        algebraEditor.AddRange(dragSource, pt);
                        algebraEditor.Parse();
                    }
                }
            }

            #endregion           
        }
    }
}