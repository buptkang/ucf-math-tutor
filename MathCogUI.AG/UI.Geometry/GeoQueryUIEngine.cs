﻿/*******************************************************************************
 * Math Interactive Tutoring System
 * <p>
 * Copyright (C) 2015 Bo Kang, Joseph J. LaViola Jr.
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

using System.Collections.Generic;

namespace MathCogUI
{
    using DynamicGeometry;
    using MathCog.UserModeling;
    using starPadSDK.Geom;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Windows;

    /// <summary>
    /// Query State Machine
    /// </summary>
    public class GeoQueryUIEngine : INotifyPropertyChanged
    {
        #region Event Handler

        public event PropertyChangedEventHandler PropertyChanged;

        [DynamicGeometry.Annotations.NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));

        }

        #endregion

        #region Singleton, Constructor and Reset

        private object _selectedFigureRect;  // delete operation update.//Rct?
        public object SelectedFigureRect
        {
            get
            {
                return _selectedFigureRect;
            }
            set
            {
                _selectedFigureRect = value;
                OnPropertyChanged("SelectedFigureRect");
            }
        }

        //private Dictionary<Point, bool?> VerifiedFigures { get; set; }

        public static GeoQueryUIEngine _instance;
        public static GeoQueryUIEngine Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new GeoQueryUIEngine();
                }
                return _instance;
            }
        }

        private GeoQueryUIEngine()
        {
            //VerifiedFigures = new Dictionary<Point, bool?>();
        }

        public void Reset()
        {
            SelectedFigureRect = null;
        }

        #endregion

        #region Label usage Select and Hit-Test

        public bool Select(object obj, IFigure figure, out string message)
        {
            if (IntersectCheck(obj))
            {
                SelectedFigureRect = null;
                message = AGGeometry.DeSelectFigure;
                return false;
            }
            else
            {
                SelectedFigureRect = figure.Center;
                message = AGGeometry.SelectFigure;
                return true;
            }
        }

        public bool IntersectCheck(object obj)
        {
            var rct = obj as Rect?;
            var pos = obj as Point?;

            if (SelectedFigureRect == null) return false;

            //var selfRct = SelectedFigureRect as Rct?;
            var selfPos = SelectedFigureRect as Point?;

            Debug.Assert(selfPos != null);


            if (rct != null)
            {
                return rct.Value.Contains(selfPos.Value);
/*                //if (selfRct != null) return rct.Value.IntersectsWith(selfRct.Value);
                if (selfPos != null) 
                return false;*/
            }
            if (pos != null)
            {
                return selfPos.Value.Equals(pos.Value);
/*                if (selfRct != null) return selfRct.Value.Contains(pos.Value);
                if (selfPos != null) 
                return false;*/
            }
            return false;
        }

        public bool HitTestFigure(Pt pt)
        {
            if (SelectedFigureRect == null) return false;
            var selfPos = SelectedFigureRect as Point?;
            Debug.Assert(selfPos != null);
            return selfPos.Value.Equals(pt);
        }

        #endregion
    }
}
