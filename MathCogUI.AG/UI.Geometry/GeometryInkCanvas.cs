﻿/*******************************************************************************
 * Math Interactive Tutoring System
 * <p>
 * Copyright (C) 2015 Bo Kang, Joseph J. LaViola Jr.
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

using System;
using AlgebraGeometry;
using CSharpLogic;

namespace MathCogUIAG
{
    using DynamicGeometry;
    using MathCogUI;
    using starPadSDK.Geom;
    using starPadSDK.Inq;
    using starPadSDK.WPFHelp;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using GlobalHost = DynamicGeometry.GlobalHost;
    using HorizontalAlignment = System.Windows.HorizontalAlignment;
    using Tablet = System.Windows.Input.Tablet;
    using Timer = System.Threading.Timer;
    using ToolBar = System.Windows.Controls.ToolBar;

    public partial class AGGeometryInkCanvas : GestureInkCanvas
    {
        #region Properties, Constructors and Initialization

        public DrawingControl DC { set; get; }
        public ContainerVisualHost Underlay { get; set; }
        public GlobalHost _gh;

        public AGGeometryInkCanvas(GlobalHost _g)
            : base()
        {
            _gh = _g;
            StroqCollected += InkCanvas_StroqCollected;
            ActivateShapeRecognizer = true;
            myTimer = new Timer(AnalyzeSlidingWindow, null, TriggerGestureAnalyzerDueTime, TriggerGestureAnalyzerInterval);
            HorizontalAlignment = HorizontalAlignment.Stretch;
            VerticalAlignment = VerticalAlignment.Stretch;
            Background = Brushes.LightCyan;
        }

        public void InitInkCanvas(ToolBar creator, ContainerVisualHost underlay)
        {
            Underlay = underlay;
            AGEditor = new AGGeometryEditor(this, creator);
        }

        #region Shape Render Inheritance

        public static Point RoundZeroFive(Point point)
        {
            point.X = System.Math.Round(point.X * 2.0) / 2.0;
            point.Y = System.Math.Round(point.Y * 2.0) / 2.0;
            return point;
        }

        protected override void InputShapePoint(Point pt)
        {
            Point logicPt = DC.Drawing.CoordinateSystem.ToLogical(pt);

            var list = DC.Drawing.Figures.HitTestMany(logicPt);
            var figureList = list.Where(f => f is ILinearFigure).ToArray();

            IFigure created = null;

            if (figureList.IsEmpty())
            {
                var tempPt = RoundZeroFive(logicPt);
                var pt1 = new AlgebraGeometry.Point(tempPt.X, tempPt.Y);

                var ptSymbol = new PointSymbol(pt1);
                var pFigure = ptSymbol.ToFigure(DC.Drawing);
                Actions.Add(DC.Drawing, pFigure);

                var figureBase = pFigure as FigureBase;
                Debug.Assert(figureBase != null);

                figureBase.PropertyChanged += figureBase_PropertyChanged;

            }
            else
            {
                if (figureList.Length == 2
                  && IntersectionAlgorithms.CanIntersect(figureList[0], figureList[1]))
                {
                    created = Factory.CreateIntersectionPoint(
                        DC.Drawing,
                        figureList[0],
                        figureList[1],
                        logicPt);
                    Actions.Add(DC.Drawing, created);
                }
                else if (figureList.Length == 1 && PointOnFigure.CanBeOnFigure(figureList[0]))
                {
                    created = Factory.CreatePointOnFigure(
                        DC.Drawing,
                        figureList[0],
                        logicPt);
                    Actions.Add(DC.Drawing, created);
                }
            }



         /*   var algebraicEditor = _gh.AlgebraGrid.AlgebraicInkCanvas.AGEditor as AGAlgebraEditor;
            if (algebraicEditor != null)
            {
                algebraicEditor.GeneratePoint(DC, pt);
            }*/
        }

        void figureBase_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("Constraint"))
            {
                var algebraicEditor = _gh.AlgebraGrid.AlgebraicInkCanvas.AGEditor as AGAlgebraEditor;
                if (algebraicEditor != null)
                {
                    var figure = sender as FigureBase; 
                    algebraicEditor.UpdateFigureBase(figure);
                }
            }
        }

        protected override void InputShapeLine(List<Point> pts)
        {
/*            Point pt1 = pts[0];
            Point pt2 = pts[pts.Count - 1];

            Point logicPt1 = DC.Drawing.CoordinateSystem.
                    ToLogical(pt1);
            Point logicPt2 = DC.Drawing.CoordinateSystem.
                ToLogical(pt2);*/

            //object result = DC.Drawing.ApproximateLine(logicPt1, logicPt2);
            Point? pt1 = null;
            Point? pt2 = null;
            PointBase hitFigure1 = null;
            PointBase hitFigure2 = null;

            #region Hit Figure Pre-processing

            foreach (var pt in pts)
            {
                Point logicPt1 = DC.Drawing.CoordinateSystem.ToLogical(pt);
                var figure = DC.Drawing.Figures.HitTest(logicPt1, typeof(FreePoint));
                if (figure != null)
                {
                    if (hitFigure1 == null)
                    {
                        hitFigure1 = (FreePoint)figure;
                    }
                    else
                    {
                        if (hitFigure1.Equals(figure))
                        {
                            continue;
                        }
                    }
                    if (pt1 == null) pt1 = logicPt1;
                    else
                    {
                        pt2 = logicPt1;
                        hitFigure2 = (FreePoint)figure;
                    }
                }
            }

            #endregion

            Point firstPt = DC.Drawing.CoordinateSystem.ToLogical(pts[0]);
            Point lastPt = DC.Drawing.CoordinateSystem.ToLogical(pts[pts.Count - 1]);

            if (pt1 != null)
            {
                if (pt2 != null)
                {
                    #region Deterministic Infer zero point

                    double realDist   = Math.Distance(firstPt, lastPt);
                    double renderDist = Math.Distance(pt1.Value, pt2.Value);

                    double diff = (realDist - renderDist).Abs();

                    double firstDiff  = Math.Distance(firstPt, pt1.Value);
                    double secondDiff = Math.Distance(pt2.Value, lastPt);

                    var lst = new List<IFigure>() { hitFigure1, hitFigure2 };
                    //if (diff < 0.1)
                    if(firstDiff < 0.2 || secondDiff < 0.2)
                    {
                        Segment seg = Factory.CreateSegment(DC.Drawing, lst);
                        Actions.Add(DC.Drawing, seg);
                    }
                    else
                    {
                        LineTwoPoints lp = Factory.CreateLineTwoPoints(DC.Drawing, lst);

                        lp.PropertyChanged += figureBase_PropertyChanged;

                        Actions.Add(DC.Drawing, lp);
                    }
                    return;
                    #endregion
                }
                
                Debug.Assert(pt2 == null);

                #region Uncertainty Infer one Point

                if (Math.Distance(pt1.Value, firstPt) < Math.Distance(pt1.Value, lastPt))
                {
                    double firstDiff = Math.Distance(pt1.Value, firstPt);

                    double diff = Math.Distance(pt1.Value, firstPt);
                    double realDiff = Math.Distance(firstPt, lastPt);

                    //if (diff/realDiff < 0.1)
                    if (firstDiff < 0.2)
                    {
                        DC.Drawing.LineSegInferOnePoint(lastPt, hitFigure1);
                    }
                    else
                    {
                        LineTwoPoints ltp = DC.Drawing.LineInferOnePoint(lastPt, hitFigure1);
                        ltp.PropertyChanged += figureBase_PropertyChanged;
                    }
                }
                else
                {
                    double firstDiff = Math.Distance(pt1.Value, lastPt);

                    double diff = Math.Distance(pt1.Value, lastPt);
                    double realDiff = Math.Distance(firstPt, lastPt);

                    if (firstDiff < 0.2)
                    //if (diff/realDiff < 0.1)
                    {
                        DC.Drawing.LineSegInferOnePoint(firstPt, hitFigure1);  
                    }
                    else
                    {
                        LineTwoPoints ltp = DC.Drawing.LineInferOnePoint(firstPt, hitFigure1);
                        ltp.PropertyChanged += figureBase_PropertyChanged;
                    }
                }
                return;
                #endregion
            }

            Debug.Assert(pt1 == null);
            Debug.Assert(pt2 == null);

            #region Uncertainty infer two points

            LineTwoPoints ltp1 = DC.Drawing.ApproximateLine(firstPt, lastPt);
            if (ltp1 != null)
            {
                ltp1.PropertyChanged += figureBase_PropertyChanged;                
            }
            #endregion

            #region TODO

            /*            if (shape != null)
            {
                AddRange(shape);
            }*/


            /*            var algebraicEditor = _gh.AlgebraGrid.AlgebraicInkCanvas.AGEditor as AGAlgebraEditor;
                        if (algebraicEditor != null)
                        {
                            algebraicEditor.GenerateLine(DC, pt1, pt2);
                        }*/
            #endregion
        }

        protected override void InputShapeCircle(Point center, Point ptOnC)
        {
            Point logicPt1 = DC.Drawing.CoordinateSystem.ToLogical(center);
            Point logicPt2 = DC.Drawing.CoordinateSystem.ToLogical(ptOnC);
            double radius = Math.Distance(logicPt1, logicPt2);
            var centerPt = new AlgebraGeometry.Point(logicPt1.X, logicPt1.Y);
            var circle = new AlgebraGeometry.Circle(centerPt, radius);
            var cs = new CircleSymbol(circle);

            FreePoint fp1 = Factory.CreateFreePoint(DC.Drawing, logicPt1);
            FreePoint fp2 = Factory.CreateFreePoint(DC.Drawing, logicPt2);

            var lst = new List<IFigure>() { fp1, fp2};
            Circle circleFigure = Factory.CreateCircle(DC.Drawing, lst);
           
            Actions.Add(DC.Drawing, circleFigure);
            Actions.AddMany(DC.Drawing, circleFigure.Dependencies);

            circleFigure.PropertyChanged += figureBase_PropertyChanged; 

/*            var algebraicEditor = _gh.AlgebraGrid.AlgebraicInkCanvas.AGEditor as AGAlgebraEditor;
            if (algebraicEditor != null)
            {
                algebraicEditor.GenerateCircle(DC, center, ptOnC);
            }*/
        }

        protected override void InputShapeArc(Point pt1, Point pt2)
        {
            var algebraicEditor = _gh.AlgebraGrid.AlgebraicInkCanvas.AGEditor as AGAlgebraEditor;
            if (algebraicEditor != null)
            {
/*                algebraicEditor.AdjustTwoLinePerpendicular(DC, pt1, pt2);*/
            }
        }

        #endregion

        #endregion

        #region Stroq Collect and scribble event handler

        private bool DeleteUnderlaySymbols(Rct stkBB)
        {
            var deletedDrawingVisual = new List<DrawingVisual>();
            foreach (Visual visual in Underlay.Children)
            {
                if (visual is DrawingVisual)
                {
                    var dv = visual as DrawingVisual;
                    if (dv.Drawing == null) continue;
                    if (dv.Drawing.Bounds.IntersectsWith(new Rect(stkBB.TopLeft, stkBB.BottomRight)))
                    {
                        deletedDrawingVisual.Add(dv);
                    }
                }
            }

            if (deletedDrawingVisual.Count != 0)
            {
                foreach (DrawingVisual dv in deletedDrawingVisual)
                {
                    Underlay.Children.Remove(dv);
                    ActOnRemoveRecognizeRegion(dv);
                }
                return true;
            }
            return false;
        }

        private void ActOnRemoveRecognizeRegion(DrawingVisual dv)
        {
            var GeometryEditor = AGEditor as AGGeometryEditor;
            if (GeometryEditor == null) return;
            int index = GeometryEditor.SketchVisuals.ToList()
                .FindIndex(x => x.DC.Equals(dv));
            if (index == -1) return;
            //Expr expr = GeometryEditor.SketchVisuals[index].Expr;
            GeometryEditor.SketchVisuals.RemoveAt(index);
        }

        private bool ScribbleDeleteIFigure(Stroq stroq)
        {
            // IFigure HitTesting
            var deletedIFigures = new List<IFigure>();
            foreach (StylusPoint sp in stroq.StylusPoints)
            {
                var newSP = DC.Drawing.CoordinateSystem.ToLogical(sp.ToPoint());
                IFigure fig = DC.Drawing.Figures.HitTest(newSP);
                if (fig != null && !deletedIFigures.Contains(fig))
                {
                    deletedIFigures.Add(fig);
                }
            }
            if (deletedIFigures.Count == 0) return false;

            foreach (IFigure fig in deletedIFigures)
            {
                Actions.Remove(fig);
                //Actions.RemoveMany(DC.Drawing, fig.Dependencies);
            }

           /* foreach (IFigure fig in deletedIFigures)
            {
                AGAlgebraEditor algebraicEditor = _gh.AlgebraGrid.AlgebraicInkCanvas.AGEditor as AGAlgebraEditor;
                Debug.Assert(algebraicEditor != null);
                var figureBase = fig as FigureBase;
                Debug.Assert(figureBase != null);
                var region = algebraicEditor.FindInputRegion(figureBase);
                algebraicEditor.RemoveRange(region);
            }*/
            return true;
        }

        protected virtual bool ScribbleDelete(Stroq stroq, out StroqCollection erasedStroqs)
        {
            erasedStroqs = null;
            bool canBeScribble = DetectScribble(stroq);
            if (!canBeScribble) return false;
            bool result1 = DeleteStroqs(stroq, out erasedStroqs);
            //Hit Test Generated Region
            Rct stkBB = stroq.GetBounds();

            bool result2 = DeleteUnderlaySymbols(stkBB);
            if (result2) Stroqs.Remove(stroq);

            bool result3 = ScribbleDeleteIFigure(stroq);
            if (result3) Stroqs.Remove(stroq);

            return result1 || result2 || result3;
        }

        private void InkCanvas_StroqCollected(object sender, StroqCollectedEventArgs e)
        {
            TabletDevice device = Tablet.CurrentTabletDevice;
            if (device == null || 
                device.Type == TabletDeviceType.Touch)
            {
                Stroqs.Remove(e.Stroq);
                return;
            }

            #region Drawing Control Hit Testing

            var dragger = DC.Drawing.Behavior as Dragger;
            Debug.Assert(dragger != null);

            if (dragger.found != null)
            {
                Stroqs.Remove(e.Stroq);
                return;
            }

            #endregion

            if (IsTapGestureStroke)
            {
                IsTapGestureStroke = false;
                Stroqs.Remove(e.Stroq);
                return;
            }

            Deselect();

            StroqCollection stroqCollection;
            /* check for scribble delete */
            if (ScribbleDelete(e.Stroq, out stroqCollection))
            {
                #region Geometry Delete Count Recording

                var analytic = InteractiveLogger.Instance.CurrentProblemAnalytic;
                analytic.AdjustSketchGestureTime();

                #endregion

                while (_slidingWindow.Count != 0)
                {
                    AddStrokeToBackEnd(_slidingWindow.Dequeue());
                }

                if (stroqCollection != null)
                    RemoveStrokeFromBackEnd(stroqCollection);
                return;
            }
         

            //TODO Label Change
            _slidingWindow.Enqueue(e.Stroq);
            myTimer.Change(TriggerGestureAnalyzerDueTime, TriggerGestureAnalyzerInterval);
        }

        #endregion

        #region Label TODO

        private void Label()
        {
           /* var geoEditor = AGEditor as AGGeometryEditor;
            if (geoEditor == null) return;
            if (args.Enum == AGGeometryEnum.Label)
            {
                var algebraEditor = _gh.AlgebraGrid.AlgebraicInkCanvas.AGEditor as AGAlgebraEditor;
                if (algebraEditor == null) return;

                geoEditor.ConvertInkToTextFigure();

                geoEditor.hideSidebarAlts();

                if (DC.HittedFigure == null) return;

                if (geoEditor.SketchVisuals.Count == 0) return;
                GeometrySketchVisual vr = geoEditor.SketchVisuals[geoEditor.SketchVisuals.Count - 1];
                object agLabel;
                bool isLabel = vr.Expr.IsLabel(out agLabel);
                if (!isLabel)
                {
                    NotifyClientInfo("Not valid label.", true);
                    return;
                }

                if (DC.HittedFigure.AGLabel != null)
                {
                    NotifyClientInfo("The selected figure already has the label.", true);
                    DC.HittedFigure.Selected = false;
                    DC.HittedFigure = null;
                    return;
                }
                Underlay.Children.Remove(vr.DC); //delete drawingVisual
                geoEditor.SketchVisuals.Remove(vr); //remove from RenderVisual
                DC.HittedFigure.UpdateLabel(agLabel, vr.Position);

                DC.HittedFigure.Name = (string)agLabel;

                var figureBase = DC.HittedFigure as FigureBase;
                if (figureBase != null)
                {
                    figureBase.UpdateShapeSymbol();
                }

                DC.HittedFigure.Selected = false;
                DC.HittedFigure = null;
                GeoQueryUIEngine.Instance.Reset();
                return;
            }*/
        }



        #endregion

        #region Time Recording Geometry Sketch Input Condition

        protected override void OnPreviewStylusDown(StylusDownEventArgs e)
        {
            TabletDevice device = Tablet.CurrentTabletDevice;
            if (device.Type == TabletDeviceType.Stylus)
            {
                var analytic = InteractiveLogger.Instance.CurrentProblemAnalytic;
                analytic.StartGeometryInputTimer();
            }
        }

        protected override void OnPreviewStylusUp(StylusEventArgs e)
        {
            TabletDevice device = Tablet.CurrentTabletDevice;
            if (device.Type == TabletDeviceType.Stylus)
            {
                var analytic = InteractiveLogger.Instance.CurrentProblemAnalytic;
                analytic.EndGeometryInputTimer();
            }
        }

        #endregion
    }
}
