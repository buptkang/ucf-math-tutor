﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace DynamicGeometry
{
    using System.Windows;
    using System.Diagnostics;
    using AlgebraGeometry;
    using CSharpLogic;

    public static class DrawingControlExtension
    {
        public static LineTwoPoints ApproximateLine(this Drawing drawing,
            System.Windows.Point pt1,
            System.Windows.Point pt2,
            params object[] constraints)
        {
            var gPoint = new System.Windows.Point(0.2, 0.2);
            var pFigure1 = drawing.SpatialInfer(pt1, gPoint);
            var pFigure2 = drawing.SpatialInfer(pt2, gPoint);

            bool ls;
            if (pFigure1 == null)
            {
                if (pFigure2 == null)
                {
                    #region Unknown two points

                    var fp1 = Factory.CreateFreePoint(drawing, pt1);
                    var fp2 = Factory.CreateFreePoint(drawing, pt2);

                    ls = drawing.InferLineParallel(pt2, fp1);
                    if (ls) return null;

                    ls = drawing.InferLinePerpendicular(pt2, fp1);
                    if (ls) return null;

                    var lst = new List<IFigure>() {fp1, fp2};
                    var line = Factory.CreateLineTwoPoints(drawing, lst);
                   
                    Actions.AddMany(drawing, lst);
                    Actions.Add(drawing, line);
                    return line;

                    #endregion
                }

                #region Unknown one point

                Debug.Assert(pFigure2 != null);

                ls = drawing.InferLineParallel(pt1, pFigure2);
                if (ls) return null;

                ls = drawing.InferLinePerpendicular(pt1, pFigure2);
                if (ls) return null;

                var fp11 = Factory.CreateFreePoint(drawing, pt1);

                var lst1 = new List<IFigure>() { fp11, pFigure2 };

                var line1 = Factory.CreateLineTwoPoints(drawing, lst1);

                Actions.Add(drawing, fp11);
                Actions.Add(drawing, line1);

                return line1;

                #endregion
            }
            Debug.Assert(pFigure1 != null);
            if (pFigure2 == null)
            {
                #region Unknown one point

                ls = drawing.InferLineParallel(pt2, pFigure1);
                if (ls) return null;

                ls = drawing.InferLinePerpendicular(pt2, pFigure1);
                if (ls) return null;

                var fp22 = Factory.CreateFreePoint(drawing, pt2);

                var lst22 = new List<IFigure>() { fp22, pFigure1 };
                var line22 = Factory.CreateLineTwoPoints(drawing, lst22);
                Actions.Add(drawing, fp22);
                Actions.Add(drawing, line22);

                return line22;
                //drawing.LineInferOnePoint(pt2, pFigure1);

                #endregion
            }

            Debug.Assert(pFigure2 != null);

            #region Line Segment Inference : todo

          /*  //find out two figures, shorten the checking distance

            //Heuristic 1: drawing distance should be 
            //             less than the two render points.
            double renderDistance = pFigure1.Center.Distance(pFigure2.Center);
            double realDistance = pt1.Distance(pt2);

            var gPoint1 = new System.Windows.Point(0.3, 0.3);
            var pFigure3 = drawing.SpatialInfer(pt1, gPoint1);
            var pFigure4 = drawing.SpatialInfer(pt2, gPoint1);

            if (realDistance < renderDistance)
            {
                if (pFigure3 != null)
                {
                    if (pFigure4 != null)
                    {
                        //Line Segment
                        var gPt11 = new AlgebraGeometry.Point(pFigure3.X, pFigure3.Y);
                        var ps11 = new PointSymbol(gPt11);
                        var gPt21 = new AlgebraGeometry.Point(pFigure4.X, pFigure4.Y);
                        var ps21 = new PointSymbol(gPt21);
                        object gObj1 = LineSegBinaryRelation.Unify(ps11, ps21);
                        
                    }
                    else //pFigure4 == null
                    {
                        var gPt11 = new AlgebraGeometry.Point(pFigure3.X, pFigure3.Y);
                        var ps11 = new PointSymbol(gPt11);
                        var pt = new AlgebraGeometry.Point(System.Math.Round(pt2.X, 1), System.Math.Round(pt2.Y, 1));
                        var ps2 = new PointSymbol(pt);
                        object gObj1 = LineSegBinaryRelation.Unify(ps11, ps2);
                       
                    }
                }
                else
                {
                    if (pFigure4 != null)
                    {
                        var pt = new AlgebraGeometry.Point(System.Math.Round(pt1.X, 1), System.Math.Round(pt1.Y, 1));
                        var ps1 = new PointSymbol(pt);
                        var gPt222 = new AlgebraGeometry.Point(pFigure4.X, pFigure4.Y);
                        var ps222 = new PointSymbol(gPt222);
                        object gObj1 = LineSegBinaryRelation.Unify(ps1, ps222);
                        
                    }
                }
            }*/

            #endregion

            var lst33 = new List<IFigure>() { pFigure2, pFigure1 };
            var line33 = Factory.CreateLineTwoPoints(drawing, lst33);
            Actions.Add(drawing, line33);
            //LineInferNoPoint(pFigure1, pFigure2);
            return line33;
        }

        #region Line Inference

        private static object LineInferNoPoint(PointBase pFigure1, PointBase pFigure2)
        {
            //Line
            var gPt12 = new AlgebraGeometry.Point(pFigure1.X, pFigure1.Y);
            var ps12 = new PointSymbol(gPt12);
            var gPt22 = new AlgebraGeometry.Point(pFigure2.X, pFigure2.Y);
            var ps22 = new PointSymbol(gPt22);
            object gObj3 = LineBinaryRelation.Unify(ps12, ps22);
            return gObj3;
        }

        public static void LineSegInferOnePoint(this Drawing drawing,
            System.Windows.Point point, PointBase ptFigure)
        {
            FreePoint fp = Factory.CreateFreePoint(drawing, point);

            var lst = new List<IFigure>() { fp, ptFigure };
            var ltp = Factory.CreateSegment(drawing, lst);

            Actions.Add(drawing, fp);
            Actions.Add(drawing, ltp);
        }

        public static LineTwoPoints LineInferOnePoint(this Drawing drawing,
            System.Windows.Point point, PointBase ptFigure)
        {
            var ls = drawing.InferLineParallel(point, ptFigure);
            if (ls) return null;

            ls = drawing.InferLinePerpendicular(point, ptFigure);
            if (ls) return null;

            FreePoint fp = Factory.CreateFreePoint(drawing, point);

            var lst = new List<IFigure>() {fp, ptFigure};
            var ltp = Factory.CreateLineTwoPoints(drawing, lst);

            Actions.Add(drawing, fp);
            Actions.Add(drawing, ltp);

            return ltp;

            #region TODO

            /*var pt = new AlgebraGeometry.Point(System.Math.Round(point.X, 1), System.Math.Round(point.Y, 1));
            //var pt = new AlgebraGeometry.Point(pt1.X, pt1.Y);
            var ps1 = new PointSymbol(pt);

            var gPt2 = new AlgebraGeometry.Point(ptFigure.X, ptFigure.Y);
            var ps2 = new PointSymbol(gPt2);

            object gObj = LineBinaryRelation.Unify(ps1, ps2);*/

            #endregion
        }

        public static void LineInferTwoPoints(this Drawing drawing,
            System.Windows.Point pt1, System.Windows.Point pt2)
        {
/*            var ls = drawing.InferLineParallel(pt1, pt2);
            if (ls != null)
            {
                return null;
                //return ls;
            }

            ls = drawing.InferLinePerpendicular(pt1, pt2);
            if (ls != null) return ls;*/

            var gPoint = new System.Windows.Point(1.5, 1.5);

            var pFigure1 = drawing.SpatialInfer(pt1, gPoint);
            var pFigure2 = drawing.SpatialInfer(pt2, gPoint);
            if (pFigure1 != null && pFigure2 != null
                && pFigure1 != pFigure2)
            {
                //label = pFigure1.AGLabel.Text + pFigure2.AGLabel.Text;

                var gPt1 = new AlgebraGeometry.Point(pFigure1.X, pFigure1.Y);
                var gPt2 = new AlgebraGeometry.Point(pFigure2.X, pFigure2.Y);

                /*                int iNum;
                                bool cond1 = LogicSharp.IsInt(pFigure1.X, out iNum);
                                bool cond2 = LogicSharp.IsInt(pFigure1.Y, out iNum);
                                bool cond3 = LogicSharp.IsInt(pFigure2.X, out iNum);
                                bool cond4 = LogicSharp.IsInt(pFigure2.Y, out iNum);
                                if (cond1 && cond2 && cond3 && cond4)
                                {*/
                object gObj = LineBinaryRelation.Unify(new PointSymbol(gPt1), new PointSymbol(gPt2));
                var gLine = gObj as LineSymbol;
                Debug.Assert(gLine != null);
                Debug.Assert(gLine.Shape.Concrete);
   
                /*                }
                                else
                                {
                                    return null;
                                }*/
            }
            if (pFigure1 != null) pt1 = pFigure1.Center;
            if (pFigure2 != null) pt2 = pFigure2.Center;

            GeometryUtils.GenerateLineSymbol(pt1, pt2);
        }

        #endregion

        #region Relation Checking

        #region Parallel Checking

     
        private static bool InferLineParallel(this Drawing drawing,
            System.Windows.Point pt1, PointBase pointFig)
        {
            var pt2 = new System.Windows.Point(pointFig.X, pointFig.Y);

            foreach (var figure in drawing.Figures)
            {
                var lineFigure = figure as LineTwoPoints;
                if (lineFigure == null) continue;

                LineSymbol ls;
                if (IsLineParallelCandidate(lineFigure.Coordinates,
                    pt1, pt2, out ls))
                {
                    #region Actions

                    var lst = new List<IFigure>() {lineFigure, pointFig};
                    ParallelLine pl = Factory.CreateParallelLine(drawing, lst);
                    Actions.Add(drawing, pl);
                    Actions.AddMany(drawing, pl.Dependencies);
                    return true;

                    #endregion
                }
            }
            return false;
        }

        private static bool IsLineParallelCandidate(PointPair pp,
            System.Windows.Point pt1, System.Windows.Point pt2,
            out LineSymbol ls)
        {
            ls = null;

            #region vertical line case

            double yDiff = (pp.P1.Y - pp.P2.Y).Abs();
            if (yDiff < 0.01)
            {
                double yDiffCandidate = (pt2.Y - pt1.Y).Abs();
                if ((yDiff - yDiffCandidate).Abs() < 0.001)
                {
                    var newLine = new Line(0.0, 1.0, -1 * pt1.Y);
                    ls = new LineSymbol(newLine);
                    return true;
                }
                return false;
            }

            #endregion

            #region horizontal line case

            double xDiff = (pp.P1.X - pp.P2.X).Abs();
            if (xDiff < 0.01)
            {
                double xDiffCandidate = (pt2.X - pt1.X).Abs();
                if ((xDiff - xDiffCandidate).Abs() < 0.001)
                {
                    var newLine1 = new Line(1.0, null, -1 * pt1.X);
                    ls = new LineSymbol(newLine1);
                    return true;
                }
                return false;
            }

            #endregion

            #region general line case

            double slope = (pp.P2.Y - pp.P1.Y) / (pp.P2.X - pp.P1.X);
            double slopeCandidate = (pt2.Y - pt1.Y) / (pt2.X - pt1.X);

            if ((slope - slopeCandidate).Abs() < 0.1)
            {
                /*
                                double a = pt1.Y - pt2.Y;
                                double b = pt2.X - pt1.X;
                                double c = (pt1.X - pt2.X) * pt1.Y + (pt2.Y - pt1.Y) * pt1.X;
                */

                /*                a = System.Math.Round(a);
                                b = System.Math.Round(b);
                                c = System.Math.Round(c);*/

                double intercept = pt2.Y - slope * pt2.X;

                var newLine2 = new Line(slope, -1, intercept);
                ls = new LineSymbol(newLine2);
                return true;
            }
            #endregion

            return false;
        }

        #endregion

        #region Perpendicular Checking

        private static bool InferLinePerpendicular(this Drawing drawing,
            System.Windows.Point pt1, PointBase pointFig)
        {
            var pt2 = new System.Windows.Point(pointFig.X, pointFig.Y);

            foreach (var figure in drawing.Figures)
            {
                var lineFigure = figure as LineTwoPoints;
                if (lineFigure == null) continue;

                LineSymbol ls;
                if (IsLinePerpendicularCandidate(lineFigure.Coordinates,
                    pt1, pt2, out ls))
                {
                    #region Actions

                    var lst = new List<IFigure>() { lineFigure, pointFig };
                    PerpendicularLine pl = Factory.CreatePerpendicularLine(drawing, lst);
                    Actions.Add(drawing, pl);
                    Actions.AddMany(drawing, pl.Dependencies);
                    return true;

                    #endregion
                }
            }
            return false;
        }

        private static bool IsLinePerpendicularCandidate(PointPair pp,
            System.Windows.Point pt1, System.Windows.Point pt2,
            out LineSymbol ls)
        {
            ls = null;

            #region vertical line case

            double yDiff = (pp.P1.Y - pp.P2.Y).Abs();
            double xDiff;
            if (yDiff < 0.01)
            {
                xDiff = (pt2.X - pt1.X).Abs();
                if (xDiff < 0.01)
                {
                    var newLine1 = new Line(1.0, null, -1 * pt1.X);
                    ls = new LineSymbol(newLine1);
                    return true;
                }
                return false;
            }

            #endregion

            #region horizontal line case

            xDiff = (pp.P1.X - pp.P2.X).Abs();
            if (xDiff < 0.01)
            {
                yDiff = (pt2.Y - pt1.Y).Abs();
                if (yDiff < 0.001)
                {
                    var newLine = new Line(0.0, 1.0, -1 * pt1.Y);
                    ls = new LineSymbol(newLine);
                    return true;
                }
                return false;
            }

            #endregion

            #region general line case

            double slope1 = (pp.P2.Y - pp.P1.Y) / (pp.P2.X - pp.P1.X);
            double slopetemp = (pt2.Y - pt1.Y) / (pt2.X - pt1.X);
            double slope2Candidate = -1 / slopetemp;
            double slope2 = -1 / slope1;

            if ((slope1 - slope2Candidate).Abs() < 0.1)
            {
                double intercept = pt2.Y - slope2 * pt2.X;
                var newLine2 = new Line(slope2, -1, intercept);
                ls = new LineSymbol(newLine2);
                return true;
            }
            #endregion

            return false;
        }

        #endregion

        public static PointBase SpatialInfer(this Drawing drawing,
            System.Windows.Point logicPt, System.Windows.Point gapPoint)
        {
            PointBase pb = null;
            double minDist = double.MaxValue;

            foreach (var figure in drawing.Figures)
            {
                var pointFigure = figure as PointBase;
                if (pointFigure == null) continue;
                var xP = pointFigure.Center.X;
                var yP = pointFigure.Center.Y;

                double d = pointFigure.Center.Distance(gapPoint);
                var topLeftPt = new System.Windows.Point(xP - gapPoint.X, yP - gapPoint.Y);
                var bottomRightPt = new System.Windows.Point(xP + gapPoint.X, yP + gapPoint.Y);
                var rect = new Rect(topLeftPt, bottomRightPt);
                if (rect.Contains(logicPt))
                {
                    if (minDist > d)
                    {
                        minDist = d;
                        pb = pointFigure;
                    }
                }
            }
            //return null;
            return pb;
        }

        #endregion
    }
}
