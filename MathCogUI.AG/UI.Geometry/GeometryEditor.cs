﻿/*******************************************************************************
 * Math Interactive Tutoring System
 * <p>
 * Copyright (C) 2015 Bo Kang, Joseph J. LaViola Jr.
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

namespace MathCogUIAG
{
    using MathCogUI;
    using starPadSDK.CharRecognizer;
    using starPadSDK.Geom;
    using starPadSDK.Inq;
    using starPadSDK.MathExpr;
    using starPadSDK.MathRecognizer;
    using starPadSDK.MathUI;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Controls;
    using System.Windows.Media;
    using Point = System.Windows.Point;

    public class AGGeometryEditor : AGMathEditor
    {
        private AGGeometryInkCanvas _canvas;

        public List<GeometrySketchVisual> SketchVisuals;

        public AGGeometryEditor(AGGeometryInkCanvas canvas, ToolBar alternatesMenu)
        {
            SketchVisuals = new List<GeometrySketchVisual>();
            _canvas = canvas;
            _mrec = new MathRecognition(_canvas._cachedStroqs);
            _mrec.EnsureLoaded(); // this is optional, and should only be called once per program run
            _mrec.ParseUpdated += _mrec_ParseUpdated;
            _altsMenuCrea = new AlternatesMenuCreator(alternatesMenu, _mrec);
        }

        void _mrec_ParseUpdated(MathRecognition source, Recognition chchanged, bool updateMath)
        {
            /* Evaluate math if necessary */
            if (updateMath)
            {
                try
                {
                    Evaluator.UpdateMath(_mrec.Ranges.Select((starPadSDK.MathRecognizer.Parser.Range r) => r.Parse));
                }
                catch
                { }
            }

            /* reset geometry displayed: range displays, etc */
            _canvas.Underlay.Children.Clear();
            _canvas.Children.Clear();

            foreach (GeometrySketchVisual temp in SketchVisuals)
            {
                _canvas.Underlay.Children.Add(temp.DC);
            }

            foreach (Parser.Range rrr in _mrec.Ranges)
            {
                #region Yellow Background and Recognition Drawing

                Rct box = GetRangeBoundingBox(rrr);

                /* set up to draw background yellow thing for range displays */
                Brush fill3 = new SolidColorBrush(Color.FromArgb(50, 255, 255, 180));
                Brush fill2 = new SolidColorBrush(Color.FromArgb(75, 255, 255, 180));
                Brush fill1 = new SolidColorBrush(Color.FromArgb(100, 255, 255, 180));
                DrawingVisual dv = new DrawingVisual();
                DrawingContext dc = dv.RenderOpen();
                dc.DrawRoundedRectangle(fill3, null, box, 4, 4);
                dc.DrawRoundedRectangle(fill2, null, box.Inflate(-4, -4), 4, 4);
                dc.DrawRoundedRectangle(fill1, null, box.Inflate(-8, -8), 4, 4);
                dc.Close();
                _canvas.Underlay.Children.Add(dv);

                #endregion

                if (rrr.Parse != null && rrr.Parse.expr != null)
                {
                    Rct bb;
                    dv = MathUtils.RetrieveDrawingVisual(
                        new Pt(box.Left, box.Bottom + 24),
                        rrr.Parse.expr, Colors.Blue, out bb);
                    _canvas.Underlay.Children.Insert(0, dv);
                }
            }

            /* Update alternates menu if user wrote a char */
            if (chchanged != null)
            {
                showSidebarAlts(new[] { chchanged }, new StroqCollection(_mrec.Sim[chchanged.strokes]));
            }
        }

        public void ConvertInkToTextFigure()
        {
            var deletedStroqs = new StroqCollection();
            for (int i = 0; i < MathRecognizer.Ranges.Count; i++)
            {
                var rrr = MathRecognizer.Ranges[i];

                deletedStroqs.Add(MathRecognizer.Sim[rrr.Strokes]);

                Rct bb = deletedStroqs.GetBounds();
                var points = new List<Point>();
                points.AddRange(from stroq in deletedStroqs from sp in stroq.StylusPoints select new System.Windows.Point(sp.X, sp.Y));
               
                if (rrr.Parse != null && rrr.Parse.expr != null)
                {
                    Rct box = GetRangeBoundingBox(rrr);
                    var repr = new GeometrySketchVisual(rrr.Parse.expr, new Pt(box.Left, box.Bottom));
                    repr.DC = MathUtils.RetrieveDrawingVisual(
                        repr.Position, repr.Expr, Colors.Blue, out bb);
                    _canvas.Underlay.Children.Add(repr.DC);
                    SketchVisuals.Add(repr);
                }
            }
            _canvas.Stroqs.Remove(deletedStroqs);
            _canvas._cachedStroqs.Remove(deletedStroqs);
        }
    }

    public class GeometrySketchVisual
    {
        public Pt Position { get; set; }
        public Expr Expr { get; set; }
        public DrawingVisual DC { get; set; }

        public GeometrySketchVisual(Expr expr, Pt position)
        {
            Expr = expr;
            Position = position;
        }
    }
}
