﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlgebraGeometry;
using MathCog.UserModeling;
using NUnit.Framework;

namespace DynamicGeometry
{
    [TestFixture]
    public class Test
    {
        [Test]
        public void Test1()
        {
            var pt1 = new Point(2.0, 3.0);
            var pt2 = new Point(3.0, 4.0);
            var pt1Sym = new PointSymbol(pt1);
            var pt2Sym = new PointSymbol(pt2);

            var line = new Line(pt1, pt2);
            var lineSym = new LineSymbol(line);

            var reasoner = new HCIReasoner();

            object obj = reasoner.HCILoad(lineSym, null, false, false);
        }
    }
}
