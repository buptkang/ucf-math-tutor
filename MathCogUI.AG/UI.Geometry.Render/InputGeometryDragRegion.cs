﻿/*******************************************************************************
 * Math Interactive Tutoring System
 * <p>
 * Copyright (C) 2015 Bo Kang, Joseph J. LaViola Jr.
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

namespace MathCogUIAG
{
    using DynamicGeometry;
    using MathCog;
    using MathCogUI;
    using System.Collections.Generic;
    using System.Windows;

    public partial class GeometryDragKnowledgeRegion : DragKnowledgeRegion
    {
        public List<IFigure> RenderFigures { get; set; }

        public Label VerifyWrongLabel { get; set; }

        //True: Algebra side; False: Geometry side
        public bool? OnAlgebraSide { get; set; }

        public GeometryDragKnowledgeRegion(IKnowledge k, 
                                    Point pt, InputText it) 
                                : base(k, pt, it)
        {
            
        }
    }
}