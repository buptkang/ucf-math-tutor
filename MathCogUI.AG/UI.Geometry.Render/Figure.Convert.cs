﻿/*******************************************************************************
 * Copyright (c) 2015 Bo Kang
 *   
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

namespace DynamicGeometry
{
    using AlgebraGeometry;
    using CSharpLogic;

    public class PatternMatchVisitor
    {
        #region Singleton

        private static PatternMatchVisitor _instance;

        public static PatternMatchVisitor Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new PatternMatchVisitor();
                }
                return _instance;
            }
        }

        private PatternMatchVisitor()
        {
            
        }

        private Drawing _drawing;
        public void Load(Drawing drawing)
        {
            _drawing = drawing;
        }
        #endregion

        public IFigure Convert(ShapeSymbol symbol)
        {
            IFigure iFigure;
            var pointSym = symbol as PointSymbol;
            if (pointSym != null && pointSym.Shape.Concrete)
            {
                iFigure = pointSym.ToFigure(_drawing);
                return iFigure;
            }

            var lineSym = symbol as LineSymbol;
            if (lineSym != null && lineSym.Shape.Concrete)
            {
                iFigure = lineSym.ToFigure(_drawing);
                return iFigure;
            }

            var lineSegmentSymbol = symbol as LineSegmentSymbol;
            if (lineSegmentSymbol != null)
            {
                iFigure = lineSegmentSymbol.ToFigure(_drawing);
                return iFigure;
            }

            var circleSymbol = symbol as CircleSymbol;
            if (circleSymbol != null)
            {
                iFigure = circleSymbol.ToFigure(_drawing);
                return iFigure;
            }

            return null;
        }
       /*
        public ShapeSymbol Convert(IFigure figure)
        {
            var freePt = figure as FreePoint;
            if (freePt != null)
            {
                return freePt.ToAlgebra();
            }

            var line = figure as LineByEquation;
            if (line != null)
            {
                return line.ToAlgebra();
            }

           
            return null;
        }
         *  */
    }
}
