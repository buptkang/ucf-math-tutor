﻿/*******************************************************************************
 * Math Interactive Tutoring System
 * <p>
 * Copyright (C) 2015 Bo Kang, Joseph J. LaViola Jr.
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

namespace MathCogUIAG
{
    using System.Windows;
    using System.Linq;
    using MathCogUI;
    using System.Collections.Generic;
    using System.Diagnostics;
    using DynamicGeometry;
    using MathCog;

    public partial class GeometrySketchKnowledgeRegion
    {
        public override void GenerateVisualRegions()
        {
            base.GenerateVisualRegions();
            RenderFigures = Generate();
        }

        public List<IFigure> Generate()
        {
            Debug.Assert(Knowledge != null);
            var shape = Knowledge as AGShapeExpr;
            var query = Knowledge as AGQueryExpr;
            var lst = new List<IFigure>();
            if (shape != null)
            {
                shape.RetrieveRenderKnowledge();
                if (shape.RenderKnowledge == null) return null;
                foreach (IKnowledge k in shape.RenderKnowledge)
                {
                    var gShape = k as AGShapeExpr;
                    Debug.Assert(gShape != null);
                    if (!gShape.ShapeSymbol.Shape.Concrete) continue;
                    var obj = PatternMatchVisitor.Instance.Convert(gShape.ShapeSymbol);

                    var gFigure = obj as IFigure;
                    if (gFigure != null)
                    {
                        lst.Add(gFigure);                        
                    }
                    var gFigures = obj as List<IFigure>;
                    if (gFigures != null)
                    {
                        lst.AddRange(gFigures);
                    }
                }                
            }
            if (query != null)
            {
                query.RetrieveRenderKnowledge();
                if (query.RenderKnowledge == null) return null;
                foreach (IKnowledge k in query.RenderKnowledge)
                {
                    var gShape = k as AGShapeExpr;
                    if (gShape == null) continue;
                    var obj = PatternMatchVisitor.Instance.Convert(gShape.ShapeSymbol);
                    var gFigure = obj as IFigure;
                    if (gFigure != null)
                    {
                        lst.Add(gFigure);
                    }
                    var gFigures = obj as List<IFigure>;
                    if (gFigures != null)
                    {
                        lst.AddRange(gFigures);
                    }
                } 
            }
            return lst;
        }
    }

    public partial class GeometryDragKnowledgeRegion
    {
        public override void GenerateVisualRegions()
        {
            base.GenerateVisualRegions();
            RenderFigures = Generate();
        }

        public List<IFigure> Generate()
        {
            Debug.Assert(Knowledge != null);
            if (RenderedRegions.Count == 0) return null;
            var lst = new List<IFigure>();
            foreach (var gRegion in RenderedRegions)
            {
                var gShape = gRegion.Knowledge as AGShapeExpr;
                if (gShape != null)
                {
                    #region Shape
                    var gFigure = PatternMatchVisitor.Instance.Convert(gShape.ShapeSymbol);
                    if (gFigure == null) continue;

                    if (gRegion.VerifySuccess != null)
                    {
                        gFigure.VerifiedWrong = !gRegion.VerifySuccess;
                    }

                    if (gFigure.VerifiedWrong != null)
                    {
                        if (gFigure.VerifiedWrong.Value)
                        {
                            VerifyWrongLabel = Factory.CreateLabel(gFigure.Drawing, "Wrong");
                        }
                        else
                        {
                            VerifyWrongLabel = Factory.CreateLabel(gFigure.Drawing, "Correct");
                        }
                        var lstTextStyles = gFigure.Drawing.StyleManager.GetStyles<TextStyle>().ToList();
                        VerifyWrongLabel.Style = lstTextStyles[lstTextStyles.Count - 1];
                        VerifyWrongLabel.ApplyStyle();
                        VerifyWrongLabel.MoveTo(new Point(gFigure.Center.X - 0.3, gFigure.Center.Y - 0.5));
                    }

                    //selection checking
                    if (GeoQueryUIEngine.Instance.HitTestFigure(gFigure.Center))
                    {
                        gFigure.Selected = true;
                    }
                    lst.Add(gFigure);
                    #endregion
                }
            }
            return lst;
        }
    }

    public partial class GeometryKnowledgeRegion
    {
        public override void GenerateVisualRegions()
        {
            base.GenerateVisualRegions();
            IFigure = Generate();
        }

        private IFigure Generate()
        {
            Debug.Assert(Knowledge != null);
            if (RenderedRegions.Count == 0) return null;
            foreach (var gRegion in RenderedRegions)
            {
                var gShape = gRegion.Knowledge as AGShapeExpr;
                if (gShape == null) continue;
                var gFigure = PatternMatchVisitor.Instance.Convert(gShape.ShapeSymbol);
                if (gFigure == null) continue;

                if (gRegion.VerifySuccess != null)
                {
                    gFigure.VerifiedWrong = !gRegion.VerifySuccess;
                }

                if (gFigure.VerifiedWrong != null)
                {
                    if (gFigure.VerifiedWrong.Value)
                    {
                        VerifyWrongLabel = Factory.CreateLabel(gFigure.Drawing, "Wrong");                        
                    }
                    else
                    {
                        VerifyWrongLabel = Factory.CreateLabel(gFigure.Drawing, "Correct"); 
                    }
                    var lstTextStyles = gFigure.Drawing.StyleManager.GetStyles<TextStyle>().ToList();
                    VerifyWrongLabel.Style = lstTextStyles[lstTextStyles.Count - 1];
                    VerifyWrongLabel.ApplyStyle();
                    VerifyWrongLabel.MoveTo(new Point(gFigure.Center.X - 0.3, gFigure.Center.Y - 0.5));
                }

                //selection checking
                if (GeoQueryUIEngine.Instance.HitTestFigure(gFigure.Center))
                {
                    gFigure.Selected = true;
                }
                return gFigure;
            }

            return null;
        }
    }

}
