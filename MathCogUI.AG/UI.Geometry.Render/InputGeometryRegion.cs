﻿/*******************************************************************************
 * Math Interactive Tutoring System
 * <p>
 * Copyright (C) 2015 Bo Kang, Joseph J. LaViola Jr.
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

using MathCog;

namespace MathCogUIAG
{
    using MathCogUI;
    using DynamicGeometry;
    using starPadSDK.Geom;

    public partial class GeometryKnowledgeRegion : InputRegion
    {
        public IFigure IFigure { get; set; }
        public Label VerifyWrongLabel { get; set; }
        public string Constraint { get; set; }

        public IFigure OrigFigure { get; set; }

        public GeometryKnowledgeRegion(IKnowledge k,
                                       Pt position, 
                                       IFigure figure,
                                       bool userInput)
            : base(k, userInput)
        {
            IFigure = figure;
            GenerateCurrentVisual(k.Expr, position, DefaultColor);
            RenderPosition = position;
        }

        public GeometryKnowledgeRegion(
                               IFigure origin,
                               IKnowledge k,
                               Pt position,
                               string label,
                               bool userInput)
            : base(k, userInput)
        {
            OrigFigure = origin;
            Constraint = label;

            GenerateCurrentVisual(k.Expr, position, DefaultColor);
            RenderPosition = position;
        }
    }
}