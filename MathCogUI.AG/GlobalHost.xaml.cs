﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows;
using System.Windows.Documents;
using MathCog.Data;
using MathCog.UserModeling;
using System.Linq;
using System.Windows.Controls;
using MathCogUIAG;
using Microsoft.FSharp.Collections.Tagged;
using starPadSDK.MathExpr;

namespace DynamicGeometry
{
    /// <summary>
    /// Interaction logic for GlobalHost.xaml
    /// </summary>
    public partial class GlobalHost : UserControl
    {
        public GlobalHost(int problemIndex, bool geometrySketchInput, 
            bool dualInput, bool dualRepr)
        {
            InitializeComponent();

            if (dualInput)
            {
                InitSetting(geometrySketchInput, dualRepr);
                InitGeometryDrawingHost(geometrySketchInput);
                AddBehaviors();
            }
            else
            {
                if (dualRepr)
                {
                    InitSetting(geometrySketchInput, dualRepr);
                    InitGeometryDrawingHost(geometrySketchInput);
                }
                else
                {
                    InitSetting(geometrySketchInput, dualRepr);
                    InitGeometryDrawingHost(geometrySketchInput);
                    GeometryUI.Visibility = Visibility.Collapsed;
                    GeoUIDef.Width = GridLength.Auto;
                    AddBehaviors();
                }
            }

            #region Obsolete

/*            if (dualRepr)
            {
                if (dualInput)
                {
                    InitSetting(geometrySketchInput, dualRepr);
                    if (geometrySketchInput)
                    {
                        GeometryGrid = new DrawingHost(this);
                        GeometryGrid.AttachRecognitionHint(AlternatesMenu);                        
                    }
                    else
                    {
                        GeometryGrid = new DrawingHost();
                    }

                    AddBehaviors();
                    //InitializeCommands();
                }
                else
                {
                    InitSetting(false, dualRepr);
                    GeometryGrid = new DrawingHost();

                    //AddBehaviors();
                    //InitializeCommands();
                }
            }
            else
            {
                if (dualInput)
                {
                    InitSetting(geometrySketchInput, dualRepr);
                    if (geometrySketchInput)
                    {
                        GeometryGrid = new DrawingHost(this);
                        GeometryGrid.AttachRecognitionHint(AlternatesMenu);
                    }
                    else
                    {
                        GeometryGrid = new DrawingHost();
                    }
                }
                else
                {
                    InitSetting(geometrySketchInput, dualRepr);
                    GeometryGrid = new DrawingHost();
                    GeometryUI.Visibility = Visibility.Collapsed;
                    GeoUIDef.Width = GridLength.Auto;
                }

                AddBehaviors();
                //InitializeCommands();
            }*/

            #endregion

            //Settings.Instance.EnableSnapToGrid = true;
            Settings.Instance.AutoShowPointCoord = true;
            GeometryUI.Content = GeometryGrid;

            AlgebraGrid.Init(this, problemIndex);
            RecognitionToolBarTray.Visibility = Visibility.Collapsed;
        }

        private void InitGeometryDrawingHost(bool geometrySketchInput)
        {
            if (geometrySketchInput)
            {
                GeometryGrid = new DrawingHost(this);
                GeometryGrid.AttachRecognitionHint(AlternatesMenu);
            }
            else
            {
                GeometryGrid = new DrawingHost();
                GeometryGrid.DrawingControl.Host = this;
            }
        }

        public GlobalHost(int problemIndex, bool geometrySketchInput)
        {
            InitializeComponent();
            InitSetting(geometrySketchInput, false);

            if (geometrySketchInput)
            {
                GeometryGrid = new DrawingHost(this);
                GeometryGrid.AttachRecognitionHint(AlternatesMenu);
            }
            else
            {
                GeometryGrid = new DrawingHost();
            }

            //Settings.Instance.EnableSnapToGrid = true;
            Settings.Instance.AutoShowPointCoord = true;
            GeometryUI.Content = GeometryGrid;
            AddBehaviors();
            //InitializeCommands();

            AlgebraGrid.AttachRecognitionHint(AlternatesMenu);
            AlgebraGrid.AttachGlobalHost(this);
            AlgebraGrid.InitBackendReasoner(problemIndex);
            //TODO
            RecognitionToolBarTray.Visibility = Visibility.Collapsed;
        }

        private void InitSetting(bool geometrySketchInput, bool isDualRepr)
        {
            Settings.IsDualRepresentation = isDualRepr;
            Settings.GeometrySketchInput = geometrySketchInput;
            Settings.Instance.HideProperty = false;
            Settings.Instance.HideHints = true;
        }

        public void Reset()
        {
            AlgebraGrid.Reset();
            GeometryGrid.Clear();
        }

        public DrawingHost GeometryGrid { get; set; }

        public void SaveUserActions_AlgebraOnly(string path)
        {
            var editor = AlgebraGrid.AlgebraicInkCanvas.AGEditor as AGAlgebraEditor;
            if (editor == null) throw new Exception("Initilization failed for editor");
            var exprs = new List<TimeSeriesAlgebraData>();
            foreach (var region in editor.MathRecognizer.Ranges)
            {
                if (!region.Parse.parseError)
                {
                    var algebraData = new TimeSeriesAlgebraData(region.Parse.expr,region.Timer);
                    exprs.Add(algebraData);
                }
            }
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.None);
            formatter.Serialize(stream, exprs);
            stream.Close();
        }

        public void SaveUserActions_AlgebraGeometry(string algebra_path, string geometry_path)
        {
            SaveUserActions_AlgebraOnly(algebra_path);
            GeometryGrid.CurrentDrawing.Save(geometry_path);
        }

        public void ControlAlgebraSketchRecognition(bool showAlgebraSketchAlt)
        {
            AlgebraGrid.control_sketch_alt_visualize(showAlgebraSketchAlt);
        }

        #region Non Sketch Set up

        private void AddBehaviors()
        {
            var behaviors = Behavior.LoadBehaviors(typeof(Dragger).Assembly);

            Behavior.Default = behaviors.First(b => b is Dragger);     

            foreach (var behavior in behaviors)
            {
                GeometryGrid.AddToolButton(behavior);
            }

            if (Settings.GeometrySketchInput)
            {
                GeometryGrid.Ribbon.Visibility = Visibility.Collapsed;
            }
        }


        void InitializeCommands()
        {
            //GeometryGrid.AddToolbarButton(GeometryGrid.CommandToggleSnapToGrid);
            //DrawingHost.AddToolbarButton(DrawingHost.CommandToggleSnapToPoint);
            //DrawingHost.AddToolbarButton(DrawingHost.CommandToggleSnapToCenter);
            //DrawingHost.AddToolbarButton(DrawingHost.CommandToggleLabelNewPoints);
            //DrawingHost.AddToolbarButton(DrawingHost.CommandClearCanvas); 
            //GeometryGrid.AddToolbarButton(GeometryGrid.CommandShowFigureExplorer);
        }

        #endregion

        #region Concept Scaffold

        public void VisualizeConceptScaffoldGeometric(string concept)
        {
            if (concept.Equals("distance"))
            {
                DistanceGeometryProcedural();
            }
        }

        public void VisualizedConceptScaffoldAlgebraic(string concept)
        {
            
        }

        private void DistanceGeometryProcedural()
        {
            var drawing = GeometryGrid.CurrentDrawing;

            var pt1 = Factory.CreateFreePoint(drawing, new Point(0.0, 0.0));
            var pt2 = Factory.CreateFreePoint(drawing, new Point(3.0, 0.0));
            var pt3 = Factory.CreateFreePoint(drawing, new Point(0.0, 4.0)); 

            var line1 = Factory.CreateLineTwoPoints(GeometryGrid.CurrentDrawing, new IFigure[] {pt1, pt2});
            var line2 = Factory.CreateLineTwoPoints(GeometryGrid.CurrentDrawing, new IFigure[] {pt1, pt3});
            var line3 = Factory.CreateLineTwoPoints(GeometryGrid.CurrentDrawing, new IFigure[] {pt2, pt3});

            var distance = Factory.CreateDistanceMeasurement(drawing, new IFigure[] {pt2, pt3});

            var label = Factory.CreateLabel(drawing, "Distance measures the length between two objects spatially.");

            Actions.Add(GeometryGrid.CurrentDrawing, pt1);
            Actions.Add(GeometryGrid.CurrentDrawing, pt2);
            Actions.Add(GeometryGrid.CurrentDrawing, pt3);
            Actions.Add(GeometryGrid.CurrentDrawing, line1);
            Actions.Add(GeometryGrid.CurrentDrawing, line2);
            Actions.Add(GeometryGrid.CurrentDrawing, line3);
            Actions.Add(GeometryGrid.CurrentDrawing, label);
            Actions.Add(GeometryGrid.CurrentDrawing, distance);
        }

        #endregion
    }
}
