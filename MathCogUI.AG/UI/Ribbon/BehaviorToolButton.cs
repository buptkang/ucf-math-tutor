﻿using System;
using System.ComponentModel;
using System.Windows;
using MathCogUI;

namespace DynamicGeometry
{
    public class BehaviorToolButton : ToolButton
    {
        public static double IconTextGap = 0;
        public BehaviorToolButton(Behavior behavior)
        {
            ParentBehavior = behavior;
            behavior.PropertyChanged += behavior_PropertyChanged;

            buttonGrid = new ButtonGrid(behavior.Icon, behavior.Name);
            buttonGrid.IconTextGap = IconTextGap;
            Content = buttonGrid;
            buttonGrid.MouseLeftButtonDown += buttonGrid_MouseLeftButtonDown;
            buttonGrid.MouseLeftButtonUp += buttonGrid_MouseLeftButtonUp;
        }

        public override FrameworkElement CloneIcon()
        {
            return ParentBehavior.CreateIcon();
        }

        public bool IsChecked
        {
            get
            {
                return buttonGrid.IsChecked;
            }
            set
            {
                buttonGrid.IsChecked = value;
            }
        }

        void buttonGrid_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            #region Time Recording Mode Switch Time
            var analytic = InteractiveLogger.Instance.CurrentProblemAnalytic;
            analytic.StartGeometryModeSwitchTimer();
            #endregion

            Click();
        }

        void buttonGrid_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            #region Time Recording Mode Switch Time
            var analytic = InteractiveLogger.Instance.CurrentProblemAnalytic;
            analytic.EndGeometryModeSwitchTimer();
            #endregion
        }


        public override void Click()
        {
            if (DrawingHost.CurrentDrawing == null)
            {
                return;
            }
            DrawingHost.CurrentDrawing.Behavior = ParentBehavior;
            ParentPanel.SelectedToolButton = this;
        }

        void behavior_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Name")
            {
                buttonGrid.Text = ParentBehavior.Name;
            }
        }

        public Behavior ParentBehavior { get; set; }
    }
}