﻿using System;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;
using MathCogUI;

namespace DynamicGeometry
{
    public partial class DrawingControl : Canvas
    {
        #region Manipulation + Sketch Version

        public IFigure HittedFigure { get; set; }

        void DrawingControl_ManipulationCompleted(object sender, System.Windows.Input.ManipulationCompletedEventArgs e)
        {
/*            if (HittedFigure != null)
            {
                if (HittedFigure is FigureBase)
                {
                    var ff = HittedFigure as FigureBase;
                    if (!ff.IsOutsideCanvas)
                    {
                         ff.OnManipulationComplete(e);
                    }
                    else
                    {
                        ff.IsOutsideCanvas = false;
                    }
                }
                HittedFigure.Selected = false;
                HittedFigure = null;
            }*/

            #region Time Recording

            var analytic = InteractiveLogger.Instance.CurrentProblemAnalytic;
            analytic.EndGeometryManipulationTimer();

            #endregion
        }

        void DrawingControl_ManipulationStarted(object sender, System.Windows.Input.ManipulationStartedEventArgs e)
        {
            #region Time Recording

            var analytic = InteractiveLogger.Instance.CurrentProblemAnalytic;
            analytic.StartGeometryManipulationTimer();

            #endregion
/*
            HittedFigure = Drawing.Figures.HitTest(Drawing.CoordinateSystem.ToLogical(e.ManipulationOrigin));
            if (HittedFigure != null)
            {
                HittedFigure.Selected = true;
            }*/
        }

        void DrawingControl_ManipulationDelta(object sender, System.Windows.Input.ManipulationDeltaEventArgs e)
        {
            //IFigure f = DrawingControl.Drawing.Figures.HitTest(DrawingControl.Drawing.CoordinateSystem.ToLogical(e.ManipulationOrigin));
          /*  if (HittedFigure != null)
            {
                if (HittedFigure is FigureBase)
                {
                    var ff = HittedFigure as FigureBase;
                    ff.OnManipulationDelta(e);
                    ff.OnLabelMove(e);
                }
            }
            else
            {*/
                if (e.Manipulators.Count() > 1)
                {
                    Drawing.CoordinateSystem.TouchZoom(e.DeltaManipulation.Scale.X);
                }
                else
                {
                    var v = P2L(e.DeltaManipulation.Translation.X, e.DeltaManipulation.Translation.Y);
                    var newOrigin = Drawing.CoordinateSystem.ToLogical(Drawing.CoordinateSystem.Origin) + v;
                    Drawing.CoordinateSystem.MoveTo(newOrigin);
                }
            //}
        }

        System.Windows.Vector P2L(double deltaX, double deltaY)
        {
            var p1 = new System.Windows.Point(0d, 0d);
            var p2 = new System.Windows.Point(0d + deltaX, 0d + deltaY);
            var l1 = Drawing.CoordinateSystem.ToLogical(p1);
            var l2 = Drawing.CoordinateSystem.ToLogical(p2);
            return l2 - l1;
        }

        #endregion

        #region Manipulation  + Origin Version

        private void OnManipulationCompleted(object sender, ManipulationCompletedEventArgs manipulationCompletedEventArgs)
        {
            #region Time Recording

            var analytic = InteractiveLogger.Instance.CurrentProblemAnalytic;
            analytic.EndGeometryManipulationTimer();

            #endregion
        }

        private void OnManipulationDelta(object sender, ManipulationDeltaEventArgs e)
        {
            if (e.Manipulators.Count() > 1)
            {
                Drawing.CoordinateSystem.TouchZoom(e.DeltaManipulation.Scale.X);
            }
            else
            {
                var v = P2L(e.DeltaManipulation.Translation.X, e.DeltaManipulation.Translation.Y);
                var newOrigin = Drawing.CoordinateSystem.ToLogical(Drawing.CoordinateSystem.Origin) + v;
                Drawing.CoordinateSystem.MoveTo(newOrigin);
            }
        }

        private void OnManipulationStarted(object sender, ManipulationStartedEventArgs manipulationStartedEventArgs)
        {
            #region Time Recording

            var analytic = InteractiveLogger.Instance.CurrentProblemAnalytic;
            analytic.StartGeometryManipulationTimer();

            #endregion
        }

        #endregion
    }
}
