﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using MathCogUIAG;

namespace DynamicGeometry
{
    public partial class DrawingControl : Canvas
    {
        public event Action<Drawing> DrawingDetach = delegate { };
        public event Action<Drawing> DrawingAttach = delegate { };

        private Drawing mCurrentDrawing;
        public Drawing Drawing
        {
            get
            {
                return mCurrentDrawing;
            }
            set
            {
                if (mCurrentDrawing != null)
                {
                    DrawingDetach(mCurrentDrawing);
                    mCurrentDrawing.ActionManager.CollectionChanged -= ActionManager_CollectionChanged;
                    mCurrentDrawing.ConstructionStepStarted -= mCurrentDrawing_ConstructionStepStarted;
                    mCurrentDrawing.ConstructionStepComplete -= mCurrentDrawing_ConstructionStepComplete;
                    mCurrentDrawing.DocumentOpenRequested -= mCurrentDrawing_DocumentOpenRequested;
                    mCurrentDrawing.UserIsAddingFigures -= mCurrentDrawing_FiguresBeingAdded;
                    mCurrentDrawing.Canvas = null;
                }
                mCurrentDrawing = value;
                if (mCurrentDrawing != null)
                {
                    mCurrentDrawing.ActionManager.CollectionChanged += ActionManager_CollectionChanged;
                    mCurrentDrawing.ConstructionStepStarted += mCurrentDrawing_ConstructionStepStarted;
                    mCurrentDrawing.ConstructionStepComplete += mCurrentDrawing_ConstructionStepComplete;
                    mCurrentDrawing.DocumentOpenRequested += mCurrentDrawing_DocumentOpenRequested;
                    mCurrentDrawing.UserIsAddingFigures += mCurrentDrawing_FiguresBeingAdded;

                    DrawingAttach(mCurrentDrawing);
                    mCurrentDrawing.SetDefaultBehavior();
                }
                UpdateUndoRedo();
            }
        }

        private AGGeometryInkCanvas _canvas;
        private GlobalHost _host;
        public GlobalHost Host { get { return _host; } set { _host = value; }}

        public DrawingControl()
        {
            //_host = g;

            Background = new SolidColorBrush(Colors.White);
            SizeChanged += DrawingControl_SizeChanged;

            CommandUndo = new Command(Undo, null, "Undo", "Drawing");
            CommandRedo = new Command(Redo, null, "Redo", "Drawing");

            IsManipulationEnabled = true;            
            ManipulationStarted += OnManipulationStarted;
            ManipulationDelta +=OnManipulationDelta;
            ManipulationCompleted +=OnManipulationCompleted;

            //Settings.Instance.EnableSnapToGrid = true;
        }

        public DrawingControl(AGGeometryInkCanvas inkcanvas, GlobalHost g)
        {
            _canvas = inkcanvas;
            _host = g;

            Background = new SolidColorBrush(Colors.White);
            SizeChanged += DrawingControl_SizeChanged;

            CommandUndo = new Command(Undo, null, "Undo", "Drawing");
            CommandRedo = new Command(Redo, null, "Redo", "Drawing");

            HorizontalAlignment = HorizontalAlignment.Stretch;
            VerticalAlignment = VerticalAlignment.Stretch;

            AllowDrop = true;
            Drop += DrawingControl_Drop;

            IsManipulationEnabled = true;
            ManipulationStarted += DrawingControl_ManipulationStarted;
            ManipulationDelta += DrawingControl_ManipulationDelta;
            ManipulationCompleted += DrawingControl_ManipulationCompleted;
        }

        void DrawingControl_Drop(object sender, DragEventArgs e)
        {
            Console.WriteLine("test");
        }

        private void HandleException(Exception ex)
        {
            Drawing.RaiseError(this, ex);
        }

        public virtual void DrawingControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (Drawing == null)
            {
                SizeChanged -= DrawingControl_SizeChanged;
                Drawing = new Drawing(this);
                PatternMatchVisitor.Instance.Load(Drawing);
                if (_host == null)
                {
                    throw new Exception("drawing host cannot be null");
                }
               /* var canvas = _host.AlgebraGrid.AlgebraicInkCanvas;
                if (canvas != null)
                {
                    var editor = canvas.AGEditor as AGAlgebraEditor;
                    if (editor != null)
                    {
                        editor.RegisterFigures(Drawing.Figures);
                    }
                }*/
            }
        }

        public virtual void Clear()
        {
            Drawing = new Drawing(this);
            PatternMatchVisitor.Instance.Load(Drawing);

            if (_host != null)
            {
                /*var canvas = _host.AlgebraGrid.AlgebraicInkCanvas;
                if (canvas != null)
                {
                    var editor = canvas.AGEditor as AGAlgebraEditor;
                    if (editor != null)
                    {
                        editor.RegisterFigures(Drawing.Figures);
                    }
                }*/                
            }
        }
    }
}
