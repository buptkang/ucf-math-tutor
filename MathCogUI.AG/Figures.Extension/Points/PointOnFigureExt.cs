﻿using System.Windows;

namespace DynamicGeometry
{
    public partial class PointOnFigure : FreePoint, IPoint
    {
        #region Hao Hu - 8/25

        public override void OnManipulationDelta(System.Windows.Input.ManipulationDeltaEventArgs args)
        {
            var origin = Drawing.CoordinateSystem.ToLogical(args.ManipulationOrigin);
            var newPos = new Point(origin.X + P2L_X(args.DeltaManipulation.Translation.X), origin.Y + P2L_Y(args.DeltaManipulation.Translation.Y));
            this.MoveTo(newPos);

            //base.OnManipulationDelta(args);
        }

        public virtual Point GetActualDestinationPoint(Point newPosition)
        {
            ILinearFigure figure = LinearFigure;
            Parameter = figure.GetNearestParameterFromPoint(newPosition);
            newPosition = figure.GetPointFromParameter(Parameter);

            return newPosition;
        }

        #endregion
    }
}
