﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Point = System.Windows.Point;

namespace DynamicGeometry
{
    public class PointOnAxis : PointOnFigure
    {
        public LineByEquation Axis { get; set; }
        public LineByEquation AttacedLine { get; set; }
        public bool isXAxis { get; set; }
        public int Index { get; set; }

        public override void OnManipulationDelta(System.Windows.Input.ManipulationDeltaEventArgs args)
        {
            if (AttacedLine.ctrlPoints.Count < 2)
            {
                AttacedLine.OnManipulationDelta(args);
            }
            else
            {
                var origin = Drawing.CoordinateSystem.ToLogical(args.ManipulationOrigin);
                var newPos = new Point(origin.X + P2L_X(args.DeltaManipulation.Translation.X), 
                    origin.Y + P2L_Y(args.DeltaManipulation.Translation.Y));
                MoveTo(newPos);

                var actualPos = this.GetActualDestinationPoint(newPos);
                int another = Index == 0 ? 1 : 0;
                double intercept = 0;
                double k = 0;
                if (another == 0)
                {
                    intercept = actualPos.Y;
                    k = actualPos.Y / AttacedLine.ctrlPoints[another].X;
                }
                else
                {
                    intercept = AttacedLine.ctrlPoints[another].Y;
                    k = AttacedLine.ctrlPoints[another].Y / actualPos.X;
                }
                var l = AttacedLine.Equation as GeneralFormLineEquation;
            }            
            //base.OnManipulationDelta(args);
        }

        public override void OnManipulationComplete(ManipulationCompletedEventArgs args)
        {
            var origin = Drawing.CoordinateSystem.ToLogical(args.ManipulationOrigin);
            //var newPos = new Point(origin.X + P2L_X(args.TotalManipulation.Translation.X), origin.Y + P2L_Y(args.TotalManipulation.Translation.Y));
            var newPos = new Point(origin.X, origin.Y);
            
            this.MoveTo(newPos);

            var actualPos = this.GetActualDestinationPoint(newPos);
            if (isXAxis)
            {
                var newX = System.Math.Round(actualPos.X);
                actualPos.X = newX;
            }
            else
            {
                var newY = System.Math.Round(actualPos.Y);
                actualPos.Y = newY;
            }
            this.MoveTo(actualPos);

            int another = Index == 0 ? 1 : 0;
            double intercept = 0;
            double k = 0;
            if (another == 0)
            {
                intercept = actualPos.Y;
                k = actualPos.Y / AttacedLine.ctrlPoints[another].X;
            }
            else
            {
                intercept = AttacedLine.ctrlPoints[another].Y;
                k = AttacedLine.ctrlPoints[another].Y / actualPos.X;
            }
            var l = AttacedLine.Equation as GeneralFormLineEquation;

            //base.OnManipulationComplete(args);
        }
    }
}
