﻿using System;
using System.Diagnostics;
using System.Text;
using System.Windows.Documents;
using System.Xml.Linq;
using AlgebraGeometry;
using CSharpLogic;

namespace DynamicGeometry
{
    public partial class FreePoint
    {
        public override void OnManipulationDelta(System.Windows.Input.ManipulationDeltaEventArgs args)
        {
            var cX = this.Center.X;
            var cY = this.Center.Y;
            var newX = cX + P2L_X(args.DeltaManipulation.Translation.X);
            var newY = cY + P2L_Y(args.DeltaManipulation.Translation.Y);
            this.MoveTo(newX, newY);

            //Knowledge = this.ToAlgebra();

            #region dependent Analysis

            if (this.Dependents.Count > 0)
            {
                for (int i = 0; i < Dependents.Count; i++)
                {
                    var fig = Dependents[i];

                    if (fig is CircleByEquation)
                    {
                        var s = fig as CircleByEquation;
                        s.X = new DrawingExpression(this, "Center X =", newX.ToString());
                        s.Y = new DrawingExpression(this, "Center Y =", newY.ToString());
                        s.X.Recalculate();
                        s.Y.Recalculate();
                    }
                    else
                    {
                        fig.Recalculate();
                        fig.UpdateVisual();
                    }
                }
            }

            #endregion

            #region Dependency Analysis

            if (this.Dependencies.Count > 0)
            {
                var shapeWithCenter = this.Dependencies[0];
                if (shapeWithCenter is CircleByEquation)
                {
                    var s = shapeWithCenter as CircleByEquation;
                    s.X = new DrawingExpression(this, "Center X =", newX.ToString());
                    s.Y = new DrawingExpression(this, "Center Y =", newY.ToString());
                    s.X.Recalculate();
                    s.Y.Recalculate();
                }
                if (shapeWithCenter is EllipseByEquation)
                {
                    var s = shapeWithCenter as EllipseByEquation;
                    s.X = new DrawingExpression(this, "Center X =", newX.ToString());
                    s.Y = new DrawingExpression(this, "Center Y =", newY.ToString());
                    s.X.Recalculate();
                    s.Y.Recalculate();
                }
            }

            #endregion

            //base.OnManipulationDelta(args);
        }

        public override void OnManipulationComplete(System.Windows.Input.ManipulationCompletedEventArgs args)
        {
            var cX = Drawing.CoordinateSystem.ToLogical(args.ManipulationOrigin).X;
            var cY = Drawing.CoordinateSystem.ToLogical(args.ManipulationOrigin).Y;
            //var newX = cX + P2L_X(args.TotalManipulation.Translation.X);
            //var newY = cY + P2L_Y(args.TotalManipulation.Translation.Y);

            var dCx = cX * 2;
            var dCy = cY * 2;
            var newX = System.Math.Round(dCx, MidpointRounding.AwayFromZero) / 2;
            var newY = System.Math.Round(dCy, MidpointRounding.AwayFromZero) / 2;
            this.MoveTo(newX, newY);

            this.Recalculate();
            this.UpdateVisual();

            UpdateShapeSymbol();
        }

        public override void UpdateShapeSymbol()
        {
            //ShapeSymbol
            var newPtSymbol = this.ToAlgebra();

            if (Dependents.Count > 0)
            {
                for (int i = 0; i < Dependents.Count; i++)
                {
                    var figure = Dependents[i] as FigureBase;
                    if (figure == null) continue;
                    figure.UpdateShapeSymbol();
                    //figure.UpdateShapeDependents(ShapeSymbol, newPtSymbol);
                }
            }

            ShapeSymbol = newPtSymbol;
        }
    }

    public static class FreePointExtensions
    {
        public static ShapeSymbol ToAlgebra(this FreePoint point)
        {
            var pt = new Point(System.Math.Round(point.X, 1), System.Math.Round(point.Y, 1));

            if (point.AGLabel != null)
            {
                pt.Label = point.AGLabel.Text;
            }
            return new PointSymbol(pt);
        }

        public static IFigure ToFigure(this PointSymbol ps, Drawing drawing)
        {
            if (drawing == null) return null;
            var p = ps.Shape as Point;
            if (p == null || p.XCoordinate == null || p.YCoordinate == null) return null;
            try
            {
                double x = double.Parse(p.XCoordinate.ToString());
                double y = double.Parse(p.YCoordinate.ToString());
                var pt = new System.Windows.Point(x, y);
                FreePoint fp = Factory.CreateFreePoint(drawing, pt);
                if (p.Label != null)
                {
                    fp.AGLabel = Factory.CreateLabel(drawing, p.Label);
                    fp.Name = p.Label;
                }
                fp.ShapeSymbol = ps;
                return fp;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
