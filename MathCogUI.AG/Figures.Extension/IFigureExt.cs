﻿namespace DynamicGeometry
{
    using System;
    using CSharpLogic;

    public partial interface IFigure
    {
        Label AGLabel { get; set; }
        ShapeSymbol ShapeSymbol { get; set; }
        bool AlgebraicInput { get; set; }
        bool? VerifiedWrong { get; set; }
        DateTime InputTime { get; set; }
    }
}