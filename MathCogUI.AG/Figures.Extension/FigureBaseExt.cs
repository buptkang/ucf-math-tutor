﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using AlgebraGeometry;
using CSharpLogic;
using starPadSDK.Geom;
using Point = System.Windows.Point;

namespace DynamicGeometry
{
    public abstract partial class FigureBase :
        IFigure,
        IPropertyGridHost,
        IPropertyGridContentProvider,
        INotifyPropertyChanged,
        INotifyPropertyChanging
    {
        #region AG Touch Manipulation Events

        public virtual void OnManipulationDelta(System.Windows.Input.ManipulationDeltaEventArgs args)
        {
        }

        public virtual void OnManipulationComplete(ManipulationCompletedEventArgs args)
        {

        }

        protected double P2L_X(double deltaX)
        {
            var p1 = new Point(0d, 0d);
            var p2 = new Point(0d + deltaX, 0d);
            var l1 = Drawing.CoordinateSystem.ToLogical(p1);
            var l2 = Drawing.CoordinateSystem.ToLogical(p2);
            return (l2 - l1).X;
        }

        protected double P2L_Y(double deltaY)
        {
            var p1 = new Point(0d, 0d);
            var p2 = new Point(0d, 0d + deltaY);
            var l1 = Drawing.CoordinateSystem.ToLogical(p1);
            var l2 = Drawing.CoordinateSystem.ToLogical(p2);
            return (l2 - l1).Y;
        }

        private bool isOutsideCanvas = false;

        public bool IsOutsideCanvas
        {
            get { return isOutsideCanvas; }
            set { isOutsideCanvas = value; }
        }

        #endregion

        #region Algebraic Side

        private DateTime inputTime;
        public DateTime InputTime
        {
            get { return inputTime; }
            set { inputTime = value;}
        }

        private bool algebraicInput = false;

        public bool AlgebraicInput
        {
            get { return algebraicInput; }
            set { algebraicInput = value; }
        }

        private Label aglable = null;

        public Label AGLabel
        {
            get
            {
                return aglable;
            }
            set
            {
                aglable = value;

                RaisePropertyChanged("AGLabel");
            }
        }

        public virtual void OnLabelMove(System.Windows.Input.ManipulationDeltaEventArgs args)
        {
            if (AGLabel != null)
            {
                var origin = AGLabel.Center;
                var newPos = new Point(origin.X + P2L_X(args.DeltaManipulation.Translation.X), origin.Y + P2L_Y(args.DeltaManipulation.Translation.Y));
                AGLabel.MoveTo(newPos);
            }
        }

        private ShapeSymbol _ss;
        public ShapeSymbol ShapeSymbol
        {
            get { return _ss; }
            set
            {
                _ss = value;
                RaisePropertyChanged("Constraint");
            }
        }

        public virtual void UpdateShapeSymbol()
        {   
        }

        public virtual void UpdateShapeDependents(ShapeSymbol oldSym, ShapeSymbol newSym)
        {
        }

        #endregion

        private bool? _verifiedWrong;
        public bool? VerifiedWrong
        {
            get { return _verifiedWrong; }
            set { _verifiedWrong = value; }
        }
    }
}