﻿using System.Linq;
using System.Windows;

namespace DynamicGeometry
{
    public static class FigureLabelExt
    {
        public static void UpdateLabel(this IFigure ifigure, object agLabel, Point pt)
        {
            var label = Factory.CreateLabel(ifigure.Drawing);
            label.Text = (string)agLabel;
            label.MoveTo(pt);
            Actions.Add(ifigure.Drawing, label);

            if (ifigure is EllipseByEquation)
            {
                var fig = ifigure as EllipseByEquation;
                var c = fig.Dependencies.First() as FreePoint;
                label.MoveTo(c.Center);
                c.AGLabel = label;
            }
            else if (ifigure is CircleByEquation)
            {
                var fig = ifigure as CircleByEquation;
                var c = fig.Dependencies.First() as FreePoint;
                label.MoveTo(c.Center);
                c.AGLabel = label;
            }
            else if (ifigure is FreePoint)
            {
                var fig = ifigure as FreePoint;
                label.MoveTo(fig.Center);
                fig.AGLabel = label;
            }
            else if (ifigure is LineByEquation)
            {
                var fig = ifigure as LineByEquation;
                label.MoveTo(fig.Center);
                fig.AGLabel = label;
            }
            else
            {
                ifigure.AGLabel = label;
            }
        }
    }
}
