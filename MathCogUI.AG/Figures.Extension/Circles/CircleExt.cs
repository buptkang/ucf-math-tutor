﻿using System.Diagnostics;
using AlgebraGeometry;
using CSharpLogic;

namespace DynamicGeometry
{
    public partial class Circle
    {
        public override void UpdateShapeSymbol()
        {
            ShapeSymbol = this.ToAlgebra();
        }
    }

    public static class Circle2Extensions
    {
        public static ShapeSymbol ToAlgebra(this Circle circle)
        {
            var twoPoints = circle.Dependencies;
            var center = twoPoints[0] as FreePoint;
            var pt2 = twoPoints[1] as FreePoint;
            Debug.Assert(center != null && pt2 != null);
            var gPt1 = new System.Windows.Point(center.X, center.Y);
            var gPt2 = new System.Windows.Point(pt2.X, pt2.Y);
            double radius = Math.Distance(gPt1, gPt2);
            var centerPt = new Point(center.X, center.Y);
            var circle1 = new AlgebraGeometry.Circle(centerPt, radius);
            var cs = new CircleSymbol(circle1);
            return cs;
        }
    }
}
