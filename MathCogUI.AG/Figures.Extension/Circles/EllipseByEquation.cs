﻿using System;
using AlgebraGeometry;
using CSharpLogic;

namespace DynamicGeometry
{
    public class EllipseByEquation : EllipseBase, IShapeWithInterior
    {
        [PropertyGridVisible(false)]
        public override System.Windows.Point Center
        {
            get
            {
                if (X.Value == null || Y.Value == null)
                {
                    return new System.Windows.Point();
                }

                return new System.Windows.Point(X.Value(), Y.Value());
            }
        }

        [PropertyGridVisible(false)]
        public override double SemiMajor
        {
            get
            {
                if (A.Value == null)
                    return 0;

                var a = A.Value();
                return a > 0 ? a : DynamicGeometry.Math.Epsilon;
            }
        }

        [PropertyGridVisible(false)]
        public override double SemiMinor
        {
            get
            {
                if (B.Value == null)
                    return 0;

                var b = B.Value();
                return b > 0 ? b : DynamicGeometry.Math.Epsilon;
            }
        }

        public override double Inclination
        {
            get { return 0; }
        }

        [PropertyGridVisible]
        public DrawingExpression X { get; set; }

        [PropertyGridVisible]
        public DrawingExpression Y { get; set; }

        [PropertyGridVisible]
        public DrawingExpression A { get; set; }

        [PropertyGridVisible]
        public DrawingExpression B { get; set; }

        [PropertyGridVisible]
        public DrawingExpression F { get; set; }

        public override void UpdateVisual()
        {
            //base.UpdateVisual();
            var center = ToPhysical(Center);
            var logicalWidth = LogicalWidth();
            var major = ToPhysical(SemiMajor * 2 + logicalWidth);
            var minor = ToPhysical(SemiMinor * 2 + logicalWidth);
            //double angle = -Inclination.ToDegrees();
            //RotateTransform rotation = new RotateTransform();
            //rotation.CenterX = major / 2;
            //rotation.CenterY = minor / 2;
            //rotation.Angle = angle;
            //Shape.RenderTransform = rotation;
            Shape.Width = major;
            Shape.Height = minor;
            Shape.CenterAt(center);
        }

        #region Hao Hu - 8/24

        public override void OnManipulationDelta(System.Windows.Input.ManipulationDeltaEventArgs args)
        {
            var origin = Drawing.CoordinateSystem.ToLogical(args.ManipulationOrigin);
            var deltaX = P2L_X(args.DeltaManipulation.Translation.X);
            var deltaY = P2L_Y(args.DeltaManipulation.Translation.Y);

            double newA;
            double newB;
            if (origin.X > Center.X)
                newA = this.A.Value() + deltaX;
            else
                newA = this.A.Value() - deltaX;

            if (origin.Y > Center.Y)
                newB = this.B.Value() + deltaY;
            else
                newB = this.B.Value() - deltaY;

            this.A = new DrawingExpression(this, "Long Half Axis =", newA.ToString());
            this.B = new DrawingExpression(this, "Short Half Axis =", newB.ToString());
            // TO DO: Calculate F
            this.A.Recalculate();
            this.B.Recalculate();
        }

        #endregion
    }
}
