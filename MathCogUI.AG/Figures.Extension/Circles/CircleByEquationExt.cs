﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using AlgebraGeometry;
using CSharpLogic;
using Point = System.Windows.Point;

namespace DynamicGeometry
{
    public partial class CircleByEquation : CircleBase, IShapeWithInterior
    {
        #region AG Manipulation

        public override void OnManipulationDelta(System.Windows.Input.ManipulationDeltaEventArgs args)
        {
            #region Hao Hu - 8/21

            var logicalOrigin = Drawing.CoordinateSystem.ToLogical((args.ManipulationOrigin));
            var logicalDeltaX = P2L_X(args.DeltaManipulation.Translation.X);
            var logicalDeltaY = P2L_Y(args.DeltaManipulation.Translation.Y);

            var logicalEndPoint = new Point(logicalOrigin.X + logicalDeltaX, logicalOrigin.Y + logicalDeltaY);
            double r = (logicalEndPoint - Center).Length;

            R = new DrawingExpression(this, "Radius =", r.ToString());
            R.Recalculate();

            #endregion

            /*var x = X.Value();
            var y = Y.Value();
            var newX = x + P2L_X(args.DeltaManipulation.Translation.X);
            var newY = y + P2L_Y(args.DeltaManipulation.Translation.Y);
            X = new DrawingExpression(this, "Center X =", newX.ToString());
            Y = new DrawingExpression(this, "Center Y =", newY.ToString());
            X.Recalculate();
            Y.Recalculate();*/
        }

        public override void OnManipulationComplete(System.Windows.Input.ManipulationCompletedEventArgs args)
        {
            var finalPos = Drawing.CoordinateSystem.ToLogical(args.ManipulationOrigin);
            var r = (finalPos - this.Center).Length;
            r = System.Math.Round(r);

            R = new DrawingExpression(this, "Radius = ", r.ToString());
            R.Recalculate();
            //base.OnManipulationComplete(args);
        }

        #endregion
    }


    public static class CircleExtensions
    {
        public static IFigure ToFigure(this CircleSymbol cs, Drawing drawing)
        {
            if (drawing == null) return null;

            var circle = cs.Shape as AlgebraGeometry.Circle;
            if (circle == null) return null;

            var pt = new Point((double)circle.CenterPt.XCoordinate, (double)circle.CenterPt.YCoordinate);
            FreePoint fp = Factory.CreateFreePoint(drawing, pt);

            //Factory.CreateCircleByRadius()

            var lst = new List<IFigure>() {};
            
            CircleByEquation gFigure = Factory.CreateCircleByEquation(drawing,
                    circle.CenterPt.XCoordinate.ToString(),
                    circle.CenterPt.YCoordinate.ToString(),
                    circle.Radius.ToString());

            //gFigure.Dependencies.Add(fp);

            return gFigure;
        }

        public static Shape ToAlgebra(this CircleByEquation circle)
        {
/*            AGSemantic.KnowledgeBase.Circle circleShape;
            var circle = Figure as CircleByEquation;
            var pt = new AGSemantic.KnowledgeBase.Point(circle.Center.X, circle.Center.Y);
            if (circle.AGLabel == null)
            {
                circleShape = new AGSemantic.KnowledgeBase.Circle(pt, circle.Radius);
            }
            else
            {
                circleShape = new AGSemantic.KnowledgeBase.Circle(circle.AGLabel.Text, pt, circle.Radius);
            }

            return new CircleExpr(circleShape);*/

            throw new Exception("TODO");
        }

    }


}
