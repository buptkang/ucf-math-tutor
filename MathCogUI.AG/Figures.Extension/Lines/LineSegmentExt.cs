﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows.Input;
using AlgebraGeometry;
using CSharpLogic;

namespace DynamicGeometry
{
    public partial class Segment : LineBase, ILengthProvider, ILine
    {
        public override void OnManipulationDelta(System.Windows.Input.ManipulationDeltaEventArgs args)
        {

        }

        public override void UpdateShapeSymbol()
        {
            ShapeSymbol = this.ToAlgebra();
        }
    }

    public static class LineSegmentExtensions
    {
        public static ShapeSymbol ToAlgebra(this Segment segFigure)
        {
            var twoPoints = segFigure.Dependencies;
            var pt1 = twoPoints[0] as FreePoint;
            var pt2 = twoPoints[1] as FreePoint;
            Debug.Assert(pt1 != null && pt2 != null);

            //Line Segment
            var gPt11 = new AlgebraGeometry.Point(pt1.X, pt1.Y);
            var ps11 = new PointSymbol(gPt11);
            var gPt21 = new AlgebraGeometry.Point(pt2.X, pt2.Y);
            var ps21 = new PointSymbol(gPt21);
            return LineSegBinaryRelation.Unify(ps11, ps21);
        }

        public static IFigure ToFigure(this LineSegmentSymbol lss, Drawing drawing)
        {
            var lineSegment = lss.Shape as LineSegment;
            if (lineSegment == null) return null;

            if (lineSegment.Pt1 == null || lineSegment.Pt2 == null) throw new Exception("Cannot be null");

            var ptSym1 = new PointSymbol(lineSegment.Pt1);
            var ptFig1 = FindPoint(drawing, ptSym1);
            if (ptFig1 == null)
            {
                ptFig1 = ptSym1.ToFigure(drawing) as FreePoint;
            }
            var ptSym2 = new PointSymbol(lineSegment.Pt2);
            var ptFig2 = FindPoint(drawing, ptSym2);
            if (ptFig2 == null)
            {
                ptFig2 = ptSym2.ToFigure(drawing) as FreePoint;
            }

            var lineSegFigure = Factory.CreateSegment(drawing, ptFig1, ptFig2);

            Debug.Assert(ptFig1 != null && ptFig2 != null);
            ptFig1.Dependents.Add(lineSegFigure);
            ptFig2.Dependents.Add(lineSegFigure);

            if (lineSegment.Label != null)
            {
                lineSegFigure.Name = lineSegment.Label;
            }
            return lineSegFigure;
        }

        private static FreePoint FindPoint(Drawing drawing, PointSymbol ptSym)
        {
            var pt = ptSym.Shape as Point;
            Debug.Assert(pt != null);
            foreach (IFigure figure in drawing.Figures)
            {
                var fp = figure as FreePoint;
                if (fp == null) continue;

                double xDiff = (fp.X - (double)pt.XCoordinate).Abs();
                double yDiff = (fp.Y - (double)pt.YCoordinate).Abs();

                if (xDiff < 0.001 && yDiff < 0.001)
                {
                    return fp;
                }
                /*                if (figure.ShapeSymbol == null) continue;
                if (pt.ToString().Equals(figure.ShapeSymbol.ToString()))
                {
                    return figure as FreePoint;
                }*/
            }
            return null;
        }
    }
}
