﻿namespace DynamicGeometry
{
    using AlgebraGeometry;
    using CSharpLogic;
    using Line = AlgebraGeometry.Line;
    using System.Collections.Generic;

    public partial class LineByEquation
    {
        public override void OnManipulationDelta(System.Windows.Input.ManipulationDeltaEventArgs args)
        {
            var deltaX = P2L_X(args.DeltaManipulation.Translation.X);
            var deltaY = P2L_Y(args.DeltaManipulation.Translation.Y);

            if (Equation is SlopeInterseptLineEquation)
            {
                var l = Equation as SlopeInterseptLineEquation;
                var s = System.Math.Tan(System.Math.Atan(l.Slope.Value()) + args.DeltaManipulation.Rotation);
                var i = l.Intersept.Value() - s * deltaX + deltaY;
                this.Equation = new SlopeInterseptLineEquation(this, s.ToString(), i.ToString());
                this.Equation.Recalculate();
            }

            if (Equation is GeneralFormLineEquation)
            {
                var l = Equation as GeneralFormLineEquation;
                var newC = l.C.Value() - deltaX * l.A.Value() - deltaY * l.B.Value();

                this.Equation = new GeneralFormLineEquation(this, l.A.Value().ToString(), l.B.Value().ToString(), newC.ToString());
                this.Equation.Recalculate();
            }
            this.UpdateVisual();

            #region Control Points

            if (ctrlPoints == null) return;

            if (ctrlPoints.Count > 0)
            {
                foreach (var p in this.ctrlPoints)
                {
                    if (p.isXAxis)
                    {
                        p.MoveTo(new System.Windows.Point(-((GeneralFormLineEquation)Equation).C.Value() / ((GeneralFormLineEquation)Equation).A.Value(), 0d));
                    }
                    else
                    {
                        p.MoveTo(new System.Windows.Point(0d, -((GeneralFormLineEquation)Equation).C.Value() / ((GeneralFormLineEquation)Equation).B.Value()));
                    }

                }
            }

            #endregion
        }

        public override void OnManipulationComplete(System.Windows.Input.ManipulationCompletedEventArgs args)
        {
            var deltaX = P2L_X(args.TotalManipulation.Translation.X);
            var deltaY = P2L_Y(args.TotalManipulation.Translation.Y);

            if (Equation is SlopeInterseptLineEquation)
            {
                var l = Equation as SlopeInterseptLineEquation;
                var s = System.Math.Tan(System.Math.Atan(l.Slope.Value()) + args.TotalManipulation.Rotation);
                var i = l.Intersept.Value() - s * deltaX + deltaY;
                this.Equation = new SlopeInterseptLineEquation(this, s.ToString(), i.ToString());
                this.Equation.Recalculate();
            }

            if (Equation is GeneralFormLineEquation)
            {
                var l = Equation as GeneralFormLineEquation;
                var newC = l.C.Value() - deltaX * l.A.Value() - deltaY * l.B.Value();

                this.Equation = new GeneralFormLineEquation(this, l.A.Value().ToString(), l.B.Value().ToString(), newC.ToString());
                this.Equation.Recalculate();
                /*
                                //KB: ad-hoc
                                Drawing.AGOldShapeExpr = Drawing.AGNewShapeExpr;

                                //KB: ad-hoc
                                var e = this.Equation as GeneralFormLineEquation;
                                var line = new Line(e.A.Value(), e.B.Value(), e.C.Value());
                                var lineExp = new LineExpr(line); //this.TransformToShape();//
                                lineExp.GeometryInput = true;
                                lineExp.Tracers = AGLogicSharp.Instance.RetrieveLineTracers(lineExp);
                                Drawing.AGNewShapeExpr = lineExp;

                                Drawing.AGNewShapeExpr = lineExp; //this.TransformToShape();//
                                Drawing.CallUpdateAGIFigure();
                 */
            }
            this.UpdateVisual();

            #region Control Points

            if (ctrlPoints == null) return;

            if (ctrlPoints.Count > 0)
            {
                foreach (var p in this.ctrlPoints)
                {
                    if (p.isXAxis)
                    {
                        p.MoveTo(new System.Windows.Point(-((GeneralFormLineEquation)Equation).C.Value() / ((GeneralFormLineEquation)Equation).A.Value(), 0d));
                    }
                    else
                    {
                        p.MoveTo(new System.Windows.Point(0d, -((GeneralFormLineEquation)Equation).C.Value() / ((GeneralFormLineEquation)Equation).B.Value()));
                    }

                }
            }

            #endregion
        }

        public List<PointOnAxis> ctrlPoints { get; set; }
    }

    public static class LineExtensions
    {
        public static ShapeSymbol ToAlgebra(this LineByEquation line)
        {
            var lineEquation = line.Equation as GeneralFormLineEquation;
            if (lineEquation == null) return null;

            var a = lineEquation.A.Value == null ? (object)null : lineEquation.A.Value();
            var b = lineEquation.B.Value == null ? (object)null : lineEquation.B.Value();
            var c = lineEquation.C.Value == null ? (object)null : lineEquation.C.Value();
            var lineShape = new Line(a, b, c);
            if (line.AGLabel != null) lineShape.Label = line.AGLabel.Text;

            /*
                        var line.Knowledge as Line;

                        if (CSharpLogic.LogicSharp.IsNumeric(originLine.A))
                        {
                            lineShape.A = originLine.A;
                        }
                        if (CSharpLogic.LogicSharp.IsNumeric(originLine.B))
                        {
                            lineShape.B = originLine.B;
                        }
                        if (CSharpLogic.LogicSharp.IsNumeric(originLine.C))
                        {
                            lineShape.C = originLine.C;
                        }            
            */
            return new LineSymbol(lineShape);
        }
    }
}
