﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using AlgebraGeometry;
using CSharpLogic;

namespace DynamicGeometry
{
    public partial class LineTwoPoints
    {
        public override void UpdateShapeSymbol()
        {
            ShapeSymbol = this.ToAlgebra();
        }

        public override void UpdateShapeDependents(ShapeSymbol oldSymbol, ShapeSymbol newSymbol)
        {
            var oldPtSym = oldSymbol as PointSymbol;
            var newPtSym = newSymbol as PointSymbol;
            if (oldPtSym == null || newPtSym == null) return;

            var oldPt = oldPtSym.Shape as Point;
            var newPt = newPtSym.Shape as Point;
            Debug.Assert(oldPt != null);
            var xPt = (double) oldPt.XCoordinate;
            var yPt = (double) oldPt.YCoordinate;

            var line = ShapeSymbol.Shape as Line;
            Debug.Assert(line != null);
            var pt1 = (System.Windows.Point)line.Rel1;
            var pt2 = (System.Windows.Point)line.Rel2;

            Debug.Assert(pt1 != null && pt2 != null);
            if ((pt1.X - xPt).Abs() < 0.001 && (pt1.Y - yPt).Abs() < 0.001)
            {
                line.Rel1 = new System.Windows.Point((double) newPt.XCoordinate, (double) newPt.YCoordinate);
            }

            if ((pt2.X - xPt).Abs() < 0.001 && (pt2.Y - yPt).Abs() < 0.001)
            {
                line.Rel2 = new System.Windows.Point((double) newPt.XCoordinate, (double) newPt.YCoordinate);
            }
        }
    }

    public static class LineTwoPointsExtensions
    {
        public static IFigure ToFigure(this LineSymbol ps, Drawing drawing)
        {
            if (drawing == null) return null;

            var line = ps.Shape as Line;
            if (line == null) return null;

            if (line.Rel1 == null && line.Rel2 == null)
            {
                var lineFigure =
                    Factory.CreateLineByEquation(drawing,
                        line.A == null ? "0.0" : line.A.ToString(),
                        line.B == null ? "0.0" : line.B.ToString(),
                        line.C == null || line.C.Equals(0.0) ? "0.00000001" : line.C.ToString());

                if (line.Label != null)
                {
                    lineFigure.Name = line.Label;
                }
                return lineFigure;
            }

            Debug.Assert(line.Rel1 != null && line.Rel2 != null);
            var pt1 = (System.Windows.Point)line.Rel1;
            var pt2 = (System.Windows.Point)line.Rel2;
            Debug.Assert(pt1 != null && pt2 != null);

            var ptSym1 = new PointSymbol(new Point(pt1.X, pt1.Y));
            var ptSym2 = new PointSymbol(new Point(pt2.X, pt2.Y));

            var ptFig1 = FindPoint(drawing, ptSym1);
            if (ptFig1 == null)
            {
                ptFig1 = ptSym1.ToFigure(drawing) as FreePoint;
            }

            var ptFig2 = FindPoint(drawing, ptSym2);
            if (ptFig2 == null)
            {
                ptFig2 = ptSym2.ToFigure(drawing) as FreePoint;
            }

            var lst = new List<IFigure> { ptFig1, ptFig2 };
            var lineFigure2 = Factory.CreateLineTwoPoints(drawing, lst);
            lineFigure2.ShapeSymbol = ps;
            return lineFigure2;
        }

        public static ShapeSymbol ToAlgebra(this LineTwoPoints line)
        {
            var twoPoints = line.Dependencies;
            var pt1 = twoPoints[0] as FreePoint;
            var pt2 = twoPoints[1] as FreePoint;

            Debug.Assert(pt1 != null && pt2 != null);

            var gPt1 = new System.Windows.Point(pt1.X, pt1.Y);
            var gPt2 = new System.Windows.Point(pt2.X, pt2.Y);

            Console.WriteLine(gPt1.ToString() + "," + gPt2.ToString());
             
            return GeometryUtils.GenerateLineSymbol(gPt1, gPt2);
        }

        private static FreePoint FindPoint(Drawing drawing, PointSymbol ptSym)
        {
            var pt = ptSym.Shape as Point;
            Debug.Assert(pt != null);
            foreach (IFigure figure in drawing.Figures)
            {
                var fp = figure as FreePoint;
                if (fp == null) continue;

                double xDiff = (fp.X - (double)pt.XCoordinate).Abs();
                double yDiff = (fp.Y - (double)pt.YCoordinate).Abs();

                if (xDiff < 0.001 && yDiff < 0.001)
                {
                    return fp;
                }
                /*                if (figure.ShapeSymbol == null) continue;
                if (pt.ToString().Equals(figure.ShapeSymbol.ToString()))
                {
                    return figure as FreePoint;
                }*/
            }
            return null;
        }
    }
}
