﻿/*******************************************************************************
 * Math Interactive Tutoring System
 * <p>
 * Copyright (C) 2015 Bo Kang, Joseph J. LaViola Jr.
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/
namespace DynamicGeometry
{
    using MathCog.UserModeling;
    using MathCogUI;
    using MathCogUIAG;
    using starPadSDK.CharRecognizer;
    using starPadSDK.Inq;
    using starPadSDK.MathExpr;
    using starPadSDK.MathUI;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Runtime.Serialization;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;

    /// <summary>
    /// Interaction logic for GlobalAlgebra.xaml
    /// </summary>
    public partial class GlobalAlgebra : UserControl
    {
        public GlobalHost GH { get; set; }

        public GlobalAlgebra()
        {
            InitializeComponent();
            Background = InitAGAlgebraDrawingBrush();

            VisualHost.Width = double.NaN; //Auto
            VisualHost.Height = double.NaN;
        }

        public void Init(GlobalHost gh, int problemIndex)
        {
            AttachRecognitionHint(alternatesMenu);
            AttachGlobalHost(gh);
            InitBackendReasoner(problemIndex);
        }

        public void Reset()
        {
            AlgebraicInkCanvas.ClearKnowledge();
            VisualHost.Children.Clear();
            AlgebraicInkCanvas.Reset();
        }

        public void AttachRecognitionHint(ToolBar toolbar)
        {
            AlgebraicInkCanvas.InitInkCanvas(toolbar, VisualHost);
        }

        public void AttachGlobalHost(GlobalHost gh)
        {
            GH = gh;
            var editor = AlgebraicInkCanvas.AGEditor as AGAlgebraEditor;
            if (editor == null) throw new Exception("Initilization failed for editor");
            editor.GH = GH;
        }

        public void InitBackendReasoner(int problemIndex)
        {
            var reasoner = new HCIReasoner();
            reasoner.InitProblem(problemIndex);
            var editor = AlgebraicInkCanvas.AGEditor as AGAlgebraEditor;
            if (editor == null) throw new Exception("Initilization failed for editor");
            editor.BackReasoner = reasoner;
        }

        public void SaveUserActions(string path)
        {
            
        }

        private DrawingBrush InitAGAlgebraDrawingBrush()
        {
            var drawingBrush = new DrawingBrush();
            drawingBrush.Viewport = new Rect(0, 0, 20, 20);
            drawingBrush.ViewboxUnits = BrushMappingMode.Absolute;
            drawingBrush.ViewportUnits = BrushMappingMode.Absolute;
            drawingBrush.AlignmentX = AlignmentX.Left;
            drawingBrush.AlignmentY = AlignmentY.Top;
            drawingBrush.TileMode = TileMode.Tile;
            drawingBrush.Stretch = Stretch.None;
            drawingBrush.Opacity = 0.75;

            var drawing = new GeometryDrawing();
            drawing.Geometry = new LineGeometry(new Point(0, 0), new Point(20, 0));
            drawing.Pen = new Pen(new SolidColorBrush(Colors.RoyalBlue), 0.5);
            drawingBrush.Drawing = drawing;
            return drawingBrush;
        }
       
        #region Alternative Menu

        AlternatesMenuCreator _altsMenuCrea;

        public void control_sketch_alt_visualize(bool showAlt)
        {
            if (showAlt)
            {
                algebraToolBarTray.Visibility = Visibility.Visible;
            }
            else
            {
                algebraToolBarTray.Visibility = Visibility.Collapsed;
            }
        }
        #endregion
    }
}
