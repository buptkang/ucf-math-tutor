﻿/*******************************************************************************
 * Math Interactive Tutoring System
 * <p>
 * Copyright (C) 2015 Bo Kang, Joseph J. LaViola Jr.
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

namespace MathCogUIAG
{
    using System;
    using MathCog.UserModeling;
    using DynamicGeometry;
    using MathCog;
    using MathCogUI;
    using starPadSDK.Geom;
    using starPadSDK.WPFHelp;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using Drawing = DynamicGeometry.Drawing;
    using System.Collections.Specialized;

    public partial class AGAlgebraEditor : AlgebraicEditor
    {
        #region Properties, Events and Constructor

        private GlobalHost _gh;

        //global shared memory
        public GlobalHost GH
        {
            get { return _gh; }
            set
            {
                _gh = value;
                //Drawing drawing = GH.GeometryGrid.CurrentDrawing;
                //drawing.Figures.CollectionChanged += Figures_CollectionChanged;
            }
        }

        public AGAlgebraEditor(AGAlgebraInkCanvas canvas, ToolBar alternatesMenu, ContainerVisualHost underlay)
            : base(canvas, alternatesMenu, underlay)
        {
            //BackReasoner.PropertyChanged += Instance_PropertyChanged;
        }

        #endregion

        #region Override Functions

        public void RegisterFigures(RootFigureList figures)
        {
            figures.CollectionChanged += Figures_CollectionChanged;
        }

        void Figures_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            try
            {
                if (e.Action == NotifyCollectionChangedAction.Add)
                {
                    foreach (IFigure fig in e.NewItems)
                    {
                        var figureBase = fig as FigureBase;
                        if (figureBase == null) continue;
                        Debug.Assert(figureBase != null);
                        if (figureBase.AlgebraicInput) continue;
                        AddRange(figureBase);
                    }
                }
                else if (e.Action == NotifyCollectionChangedAction.Remove)
                {
                    foreach (IFigure fig in e.OldItems)
                    {
                        var figureBase = fig as FigureBase;
                        if (figureBase == null) continue;
                        Debug.Assert(figureBase != null);
                        if (figureBase.AlgebraicInput) continue;

                        var region = FindInputRegion(figureBase);
                        RemoveRange(region);
                    }
                }
            }
            catch (Exception ee)
            {
                Console.WriteLine("Exception Message: " + ee.Message);
            }
            finally
            {
                //Interactive Analytics
                //InteractiveAnalyzeKnowledgeRegion();
            }
        }

        #region Add Operation

        protected override void AddVisualInputRegion(InputRegion kr)
        {
            Debug.Assert(kr != null);

            var geometryInput  = kr as GeometryKnowledgeRegion;
            var geoSketchInput = kr as GeometrySketchKnowledgeRegion;
            var geoDragInput   = kr as GeometryDragKnowledgeRegion;

            if (!HCIReasoner.TutorMode)
            {
                base.AddVisualInputRegion(kr);
                AddVisualFigure(kr);
                return;
            }
            Debug.Assert(HCIReasoner.TutorMode);

            if (geometryInput != null)
            {
                if (Settings.IsDualRepresentation)
                {
                    base.AddVisualInputRegion(kr);
                }
                //AddVisualFigure(kr);                    
            }

            if (geoSketchInput != null)
            {
                base.AddVisualInputRegion(kr);
                if (Settings.IsDualRepresentation)
                {
                    AddVisualFigure(kr);
                }
            }

            if (geoDragInput != null)
            {
              /*  if (APConfig.AllowMappingInTutor)
                {
                    base.AddVisualInputRegion(kr);
                    AddVisualFigure(kr);
                }
                else
                {*/
                    bool? algebraSide = geoDragInput.InputText.OnAlgebraSide;
                    if (algebraSide == null || algebraSide.Value) 
                    {
                        base.AddVisualInputRegion(kr);
                    }
                    else
                    {
                        AddVisualFigure(kr);
                    }
                //}
            }
        }

        private void AddVisualFigure(InputRegion kr)
        {
            Drawing drawing = GH.GeometryGrid.CurrentDrawing;
            var sketchRegion = kr as GeometrySketchKnowledgeRegion;
            var dragRegion = kr as GeometryDragKnowledgeRegion;
            var geometryRegion = kr as GeometryKnowledgeRegion;

            if (sketchRegion != null)
            {
                if (sketchRegion.RenderFigures == null) return;

                if (true)
                {
                    foreach (IFigure fig in sketchRegion.RenderFigures)
                    {
                        var figureBase = fig as FigureBase; 
                        Debug.Assert(figureBase != null);
                        figureBase.IsHitTestVisible = false;
                        figureBase.Locked = true;
                        figureBase.UpdateShapeSymbol();
                        figureBase.AlgebraicInput = true;
                        figureBase.PropertyChanged += figure_PropertyChanged;
                    }
                    Actions.AddMany(drawing, sketchRegion.RenderFigures);
                }
            }
            else if (dragRegion != null)
            {
                #region Drag Region
                if (dragRegion.RenderFigures == null) return;
                if (true)
                {
                    foreach (IFigure fig in dragRegion.RenderFigures)
                    {
                        var figureBase = fig as FigureBase;
                        Debug.Assert(figureBase != null);
                        //figureBase.UpdateConstraint();
                        figureBase.PropertyChanged += figure_PropertyChanged;

                        if (dragRegion.VerifyWrongLabel != null)
                        {
                            Actions.Add(drawing, dragRegion.VerifyWrongLabel);
                        }
                    }
                    Actions.AddMany(drawing, dragRegion.RenderFigures);
                }
                #endregion
            }
            else if (geometryRegion != null)
            {
                if (geometryRegion.IFigure == null) return;
                if (geometryRegion.IFigure.Dependencies != null)
                {
                    Actions.AddMany(drawing, geometryRegion.IFigure.Dependencies);
                }
                Actions.Add(drawing, geometryRegion.IFigure);

                var figureBase = geometryRegion.IFigure as FigureBase;
                Debug.Assert(figureBase != null);
                //figureBase.UpdateConstraint();
                figureBase.PropertyChanged += figure_PropertyChanged;

                if (geometryRegion.IFigure.Selected)
                {
                    GH.GeometryGrid.DrawingControl.HittedFigure = geometryRegion.IFigure;
                }

                if (geometryRegion.VerifyWrongLabel != null)
                {
                    Actions.Add(drawing, geometryRegion.VerifyWrongLabel);
                }
            }
        }

        protected override void AddVisualGenerateRegion(GeneratedRegion gr)
        {
            Debug.Assert(gr != null);
            base.AddVisualGenerateRegion(gr);

            var shapeExpr = gr.Knowledge as AGShapeExpr;
            if (shapeExpr == null) return;

            var gFigure = PatternMatchVisitor.Instance.Convert(shapeExpr.ShapeSymbol);
            gr.GeometryTag = gFigure;
            var figureBase = gFigure as FigureBase;
            if (figureBase != null)
            {
                figureBase.UpdateShapeSymbol();
            }

            AddVisualFigure(gr);
        }

        private void AddVisualFigure(GeneratedRegion gr)
        {
            Drawing drawing = GH.GeometryGrid.CurrentDrawing;
            if (gr == null || gr.GeometryTag == null) return;

            var figure = gr.GeometryTag as IFigure;
            if (figure != null)
            {
                Actions.Add(drawing, figure);
                //figure.IsHitTestVisible = false;
                figure.Locked = true;
            }
        }

        #endregion

        #region Remove Operation

        protected override void RemoveVisualInputRegion(InputRegion kr)
        {
            Debug.Assert(kr != null);
            base.RemoveVisualInputRegion(kr);
            RemoveVisualFigure(kr);
        }

        private void RemoveVisualFigure(InputRegion kr)
        {
            Drawing drawing = GH.GeometryGrid.CurrentDrawing;

            var sketchRegion = kr as GeometrySketchKnowledgeRegion;
            var dragRegion = kr as GeometryDragKnowledgeRegion;
            var geometryRegion = kr as GeometryKnowledgeRegion;

            if (sketchRegion != null)
            {
                if (sketchRegion.RenderFigures != null)
                {
                    foreach (IFigure fig in sketchRegion.RenderFigures)
                    {
                        var figureBase = fig as FigureBase;
                        if (figureBase == null) continue;
                        Debug.Assert(figureBase != null);
                        figureBase.PropertyChanged -= figure_PropertyChanged;
                    }
                    Actions.RemoveMany(drawing, sketchRegion.RenderFigures);
                }
            }
            else if (dragRegion != null)
            {
                if (dragRegion.RenderFigures != null)
                {
                    foreach (IFigure fig in dragRegion.RenderFigures)
                    {
                        var figureBase = fig as FigureBase;
                        Debug.Assert(figureBase != null);
                        figureBase.PropertyChanged -= figure_PropertyChanged;
                    }
                    Actions.RemoveMany(drawing, dragRegion.RenderFigures);
                }
            }
            else if (geometryRegion != null)
            {
                if (geometryRegion.IFigure == null) return;
                var figureBase = geometryRegion.IFigure as FigureBase;
                Debug.Assert(figureBase != null);
                figureBase.PropertyChanged -= figure_PropertyChanged;
                Actions.Remove(geometryRegion.IFigure);

                //Actions.RemoveMany(drawing, geometryRegion.IFigure.Dependencies);

                if (geometryRegion.VerifyWrongLabel != null)
                {
                    Actions.Remove(geometryRegion.VerifyWrongLabel);
                }
            }
        }

        private void RemoveVisualFigure(GeneratedRegion gr)
        {
            if (gr == null || gr.GeometryTag == null) return;
            var figure = gr.GeometryTag as IFigure;
            if (figure != null)
            {
                Actions.Remove(figure);
            }
        }

        protected override void RemoveVisualGenerateRegion(GeneratedRegion gr)
        {
            base.RemoveVisualGenerateRegion(gr);
            RemoveVisualFigure(gr);
        }
      
        #endregion

        public override void InteractiveAnalyzeKnowledgeRegion()
        {
            var analytic = InteractiveLogger.Instance.CurrentProblemAnalytic;
            List<SketchKnowledgeRegion> inputRegions = RetrieveSketchRegion();
            int total = inputRegions.Count;
            analytic.AlgebraicInputSteps = total;
        }

        void figure_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("AGLabel"))
            {
                var figure = sender as FigureBase;
                if (figure != null)
                {
                    /*  RemoveFigureRegion(figure);
                      AddRange(figure.);*/
                }
            }
            else if (e.PropertyName.Equals("Constraint"))
            {
                var figure = sender as FigureBase;
                if (figure != null)
                {
                    UpdateFigureBase(figure);
                }
            }
        }

        public void UpdateFigureBase(FigureBase figure)
        {
            var region = FindInputRegion(figure);
            Pt? position = RemoveRange(region);
            var shapeExpr = region.Knowledge as AGShapeExpr;
            if (shapeExpr != null)
            {
                AddRange(figure, position);
                Parse();
            }            
        }

        #endregion

        #region Utils

        public List<InputRegion> RetrieveGeometryInputs()
        {
            var lst = new List<InputRegion>();
            foreach (Region region in KnowledgeRegions)
            {
                var inputRegion = region as GeometryKnowledgeRegion;
                if (inputRegion != null)
                {
                    lst.Add(inputRegion);
                }
                var dragGeoRegion = region as GeometryDragKnowledgeRegion;
                if (dragGeoRegion != null)
                {
                    bool? result = dragGeoRegion.InputText.OnAlgebraSide;
                    if (result != null && !result.Value)
                    {
                        lst.Add(dragGeoRegion);
                    }
                }                
            }
            return lst;
        }

        public override bool Delete(Rct bb)
        {
            bool result1 = base.Delete(bb);
            bool result2 = false;
            var rct = new Rect(bb.TopLeft, bb.BottomRight);
            var regions = IntersectGeometryRegion(rct);
            if (regions != null && regions.Count != 0)
            {
                foreach (var region in regions)
                {
                    var geometryRegion = region as GeometryKnowledgeRegion;
                    if (geometryRegion != null) KnowledgeRegions.Remove(region);
                }
                result2 = true;
            }
            return result1 || result2;
        }

        private List<Region> IntersectGeometryRegion(Rect bb)
        {
            var lst = new List<Region>();
            foreach (var r in KnowledgeRegions)
            {
                var gR = r as GeometryKnowledgeRegion;
                if (gR == null || gR.CurrentDrawingVisual == null
                    || gR.CurrentDrawingVisual.Drawing == null) continue;

                if (gR.CurrentDrawingVisual.Drawing.Bounds.IntersectsWith(bb))
                {
                    lst.Add(gR);
                }
            }
            return lst;
        }

        void Instance_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("QueryEnded"))
            {
                throw new Exception("TODO");

                /*   //notify front-end to show figures
                   var knowledge = QueryUIEngine.Instance.QueriedKnowledge;
                   if (knowledge == null) return;

                   var findRegion = FindInputRegion(knowledge);
                   if (findRegion == null) return;
                   if (QueryUIEngine.Instance.QueryEnded)
                   {
                       AddVisualInputRegion(findRegion);
                   }
                   else
                   {
                       //RemoveVisualFigure(findRegion);
                   }*/
            }
        }

        public void Clear()
        {
            foreach (var region in KnowledgeRegions)
            {
                var inputRegion = region as InputRegion;
                if (inputRegion != null)
                {
                    RemoveVisualFigure(inputRegion);
                }
            }
        }

        private InputRegion FindInputRegion(IKnowledge knowledge)
        {
            foreach (var region in KnowledgeRegions)
            {
                if (region.Knowledge.Expr == null) continue;
                if (region.Knowledge.Expr.Equals(knowledge.Expr))
                {
                    var ir = region as InputRegion;
                    if (ir != null) return ir;
                }
            }
            return null;
        }

        public InputRegion FindInputRegion(FigureBase figure)
        {
            foreach (var region in KnowledgeRegions)
            {
                var geometryRegion = region as GeometryKnowledgeRegion;
                if (geometryRegion != null)
                {
                    if (geometryRegion.IFigure == null) continue;
                    bool result = geometryRegion.OrigFigure.Equals(figure);
                    if (result) return geometryRegion;
                }

                var sketchRegion = region as GeometrySketchKnowledgeRegion;
                if (sketchRegion != null  && sketchRegion.RenderFigures!= null)
                {
                    if (sketchRegion.RenderFigures.Select(tempFigure => tempFigure.Equals(figure)).Any(result => result))
                    {
                        return sketchRegion;
                    }
                }

                var dropRegion = region as GeometryDragKnowledgeRegion;
                if (dropRegion != null)
                {
                    if (dropRegion.RenderFigures.Select(tempFigure => tempFigure.Equals(figure)).Any(result => result))
                    {
                        return dropRegion;
                    }
                }
            }
            return null;
        }

        public new Pt FindPlaceToRender()
        {
            if (KnowledgeRegions.Count == 0)
            {
                return new Pt(200, 200);

/*                if (!HCIReasoner.TutorMode)
                {
                    return new Pt(200, 200);
                }
                else
                {
                    return new Pt(3000, 3000);                    
                }*/
            }
            else
            {
                Pt largest = new Pt();
                foreach (Region region in KnowledgeRegions)
                {
                    if (region.ExprVisual.DrawPos == null) continue;
                    Pt curr = region.ExprVisual.DrawPos.Value;
                    if (curr.X > largest.X)
                    {
                        largest.X = curr.X;
                    }
                    if (curr.Y > largest.Y)
                    {
                        largest.Y = curr.Y;
                    }
                }
                return new Pt(largest.X, largest.Y + 100);
            }
        }

        private void RemoveDependency(IFigure fig)
        {
            if (fig is FigureBase)
            {
                var ff = fig as FigureBase;
                if (fig is LineByEquation)
                {
                    var l = fig as LineByEquation;
                    foreach (var c in l.ctrlPoints)
                    {
                        if (c.AGLabel != null)
                            Actions.Remove(c.Label);
                        Actions.Remove(c);
                    }
                }
                foreach (var d in ff.Dependencies)
                {
                    if (d.AGLabel != null)
                        Actions.Remove(d.AGLabel);
                    Actions.Remove(d);
                }

                if (ff.AGLabel != null)
                    Actions.Remove(ff.AGLabel);
            }
        }

        #endregion
    }
}