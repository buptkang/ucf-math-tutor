﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlgebraGeometry;

namespace DynamicGeometry
{
    public static class GeometryUtils
    {
        public static LineSymbol GenerateLineSymbol(System.Windows.Point pt1, System.Windows.Point pt2)
        {
            LineSymbol ls;

            #region Rounding
/*            pt1.X = System.Math.Round(pt1.X);
            pt1.Y = System.Math.Round(pt1.Y);
            pt2.X = System.Math.Round(pt2.X);
            pt2.Y = System.Math.Round(pt2.Y);*/
            #endregion

            #region Special Case Vertical Line "y = a"

            double yDiff = (pt1.Y - pt2.Y).Abs();
            if (yDiff < 0.01)
            {
                var newLine = new Line(0.0, 1.0, -1 * pt1.Y);
                newLine.Rel1 = pt1;
                newLine.Rel2 = pt2;
                ls = new LineSymbol(newLine);
                return ls;
            }

            #endregion

            #region Special Case Horizontal Line "x = b"

            double xDiff = (pt1.X - pt2.X).Abs();
            if (xDiff < 0.01)
            {
                var newLine1 = new Line(1.0, null, -1 * pt1.X);
                newLine1.Rel1 = pt1;
                newLine1.Rel2 = pt2;
                ls = new LineSymbol(newLine1);
                return ls;
            }

            #endregion

            #region General Case

            double a = pt1.Y - pt2.Y;
            double b = pt2.X - pt1.X;
            double c = (pt1.X - pt2.X) * pt1.Y + (pt2.Y - pt1.Y) * pt1.X;

            a = System.Math.Round(a);
            b = System.Math.Round(b);
            c = System.Math.Round(c);

            var newLine2 = new Line(a, b, c);
            newLine2.Rel1 = pt1;
            newLine2.Rel2 = pt2;
            ls = new LineSymbol(newLine2);

            #endregion

            return ls;
        }
    }
}
