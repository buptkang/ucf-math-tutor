﻿/*******************************************************************************
 * Math Interactive Tutoring System
 * <p>
 * Copyright (C) 2015 Bo Kang, Joseph J. LaViola Jr.
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

namespace MathCogUIAG
{
    using System;
    using System.Windows.Input;
    using MathCogUI;
    using starPadSDK.WPFHelp;
    using System.Windows.Controls;

    public class AGAlgebraInkCanvas : AlgebraicInkCanvas
    {
        public override void InitInkCanvas(ToolBar creator, ContainerVisualHost underlay)
        {
            StroqCollected += CustomInkCanvas_StroqCollected;
            AGEditor = new AGAlgebraEditor(this, creator, underlay);
        }

        public void ClearKnowledge()
        {
            var editor = AGEditor as AGAlgebraEditor;
            if(editor == null) throw new Exception("cannot be null");
            editor.Clear();
        }

        protected override void OnPreviewStylusDown(StylusDownEventArgs e)
        {
            TabletDevice device = Tablet.CurrentTabletDevice;
            if (device.Type == TabletDeviceType.Stylus)
            {
                var analytic = InteractiveLogger.Instance.CurrentProblemAnalytic;
                analytic.StartAlgebraTimer();
            }
        }

        protected override void OnPreviewStylusUp(StylusEventArgs e)
        {
            TabletDevice device = Tablet.CurrentTabletDevice;
            if (device.Type == TabletDeviceType.Stylus)
            {
                var analytic = InteractiveLogger.Instance.CurrentProblemAnalytic;
                analytic.EndAlgebraTimer();
            }
        }
    }
}
