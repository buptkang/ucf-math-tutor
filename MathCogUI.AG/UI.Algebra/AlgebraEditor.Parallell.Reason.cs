﻿/*******************************************************************************
 * Math Interactive Tutoring System
 * <p>
 * Copyright (C) 2015 Bo Kang, Joseph J. LaViola Jr.
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

using System.Collections.Generic;
using System.Linq;
using System.Windows.Documents;
using DynamicGeometry;

namespace MathCogUIAG
{
    using MathCogUI;
    using CSharpLogic;
    using MathCog;
    using starPadSDK.Geom;
    using System;
    using Parser = starPadSDK.MathRecognizer.Parser;

    public partial class AGAlgebraEditor
    {
        #region Common API

        public override void Parse()
        {
            base.Parse();

            IEnumerable<GeometryKnowledgeRegion> gRegions =
                    KnowledgeRegions.OfType<GeometryKnowledgeRegion>().ToList();

            for (int i = 0; i < gRegions.Count(); i++)
            {
                var gRegion = gRegions.ToList()[i];
                ReAdd(gRegion);
            }
        }

        public Pt? RemoveRange(InputRegion ir, bool isUpdate = false)
        {
            var geometryRegion = ir as GeometryKnowledgeRegion;
            if (geometryRegion != null)
            {
                RemoveRange(geometryRegion, isUpdate);
                return geometryRegion.RenderPosition;
            }

            var sketchRegion = ir as SketchKnowledgeRegion;
            if (sketchRegion != null)
            {
                base.RemoveRange(sketchRegion);
                return sketchRegion.RenderPosition;
            }

            var dragRegion = ir as DragKnowledgeRegion;
            if (dragRegion != null)
            {
                base.RemoveRange(dragRegion, isUpdate);
                return dragRegion.RenderPosition;
            }

            return null;
        }

        #endregion

        #region Geometry-based Reasoning

        public void AddRange(FigureBase figureBase, Pt? renderPt = null, bool userInput = false)
        {
            ShapeSymbol ss = figureBase.ShapeSymbol;
            //InteractiveAnalytics.Instance.StartTimer();
            object obj = BackReasoner.HCILoad(ss, null, userInput, false);
            if (obj == null) throw new Exception("TODO");
            var k = obj as IKnowledge;
            if (k == null) return;
            Pt position;
            if (renderPt == null)
            {
                position = FindPlaceToRender();
            }
            else
            {
                position = renderPt.Value;
            }
            var geometryRegion = new GeometryKnowledgeRegion(figureBase, k, position, k.ToString(), userInput);
            KnowledgeRegions.Add(geometryRegion);
        }

/*        public void AddRange(ShapeSymbol ss, Pt? renderPt = null, bool userInput = false)
        {
            //InteractiveAnalytics.Instance.StartTimer();
            object obj = BackReasoner.HCILoad(ss, null, userInput, false);
            if (obj == null) throw new Exception("TODO");
            var k = obj as IKnowledge;
            if (k == null) return;
            Pt position;
            if (renderPt == null)
            {
                position = FindPlaceToRender();
            }
            else
            {
                position = renderPt.Value;
            }
            var geometryRegion = new GeometryKnowledgeRegion(k, position, k.ToString(), userInput);
            KnowledgeRegions.Add(geometryRegion);
        }*/

        private bool RemoveRange(GeometryKnowledgeRegion geoRegion, bool isUpdate = false)
        {
            bool userInput = geoRegion.UserInput;
            var agShapeExpr = geoRegion.Knowledge as AGShapeExpr;
            if (agShapeExpr != null)
            {
                BackReasoner.HCIUnLoad(agShapeExpr.ShapeSymbol.ToString());
            }         
            KnowledgeRegions.Remove(geoRegion);
            if (!isUpdate)
            {
                BackQuerier.UpdateVerifyRegion(geoRegion.ExprVisual.BoundingBox);
            } 
            return userInput;
        }

        public void ReAdd(GeometryKnowledgeRegion region)
        {
            var currentFigure = region.IFigure;
            if (currentFigure == null) return;
            var agShapeExpr = region.Knowledge as AGShapeExpr;
            if (agShapeExpr != null)
            {
                int index = KnowledgeRegions.IndexOf(region);
                KnowledgeRegions.Remove(region);
                //AddRange(range);
                KnowledgeRegions.Insert(index, region);

                //RemoveRange(region, true);
                //AddRange(agShapeExpr.ShapeSymbol, region.ExprVisual.DrawPos);
            }
        }

        #endregion

        #region Algebra-based Reasoning

        protected override void AddRange(Parser.Range range, ShapeType st, 
            bool userInput = false, bool isUpdate = false)
        {
            try
            {
                //InteractiveAnalytics.Instance.StartTimer();
                IKnowledge k;
                Rct bb;
                AddRangeInternal(range, st, out k, out bb, userInput, isUpdate);
                KnowledgeRegions.Add(new GeometrySketchKnowledgeRegion(range, k, bb, userInput));
            }
            catch (Exception ee)
            {
                Console.WriteLine(@"Exception information: {0}", ee);
            }
        }

        #endregion

        #region Drag-and-Drop-based Reasoning

        public override InputRegion AddRange(InputText it, System.Windows.Point? pt, bool isUpdate = false)
        {
            //InteractiveAnalytics.Instance.StartTimer();
            it.IsDraggable = false;
            object obj;
            if (it.OnAlgebraSide != null)
            {
                obj = BackReasoner.HCILoad(it.DragContent, null, false, it.OnAlgebraSide.Value); 
            }
            else
            {
                obj = BackReasoner.HCILoad(it.DragContent);
            }
            
            if (obj == null) throw new Exception("TODO");
            var k = obj as IKnowledge;
            if (k == null) return null;
            if (!isUpdate)
            {
                SendFeedback(k);
            }

            if (pt == null)
            {
                pt = FindPlaceToRender();
            }

            var region = new GeometryDragKnowledgeRegion(k, pt.Value, it);

            //Buf Fix for real-time drag reify shape update 
            RegisterShapeRegion(k);
            KnowledgeRegions.Add(region);
            return region;
        }
        #endregion
    }
}