﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Markup;
using System.Windows.Shapes;
using MathCogUI;

namespace DynamicGeometry
{
    [Category(BehaviorCategories.Shapes)]
    [Order(9)]
    public class FigureDeleter : Behavior
    {
        public override void MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            #region Time Record for the structural input deletion

            var analytic = InteractiveLogger.Instance.CurrentProblemAnalytic;
            analytic.EndGeometryStructuralDeleteTimer();
            analytic.GeometryDeleteCount++;
            #endregion

            var coordinates = Coordinates(e);
            var underMouse = Drawing.Figures.HitTest(coordinates);

            if (underMouse != null)
            {
                Actions.Remove(underMouse);
            }
        }

        public override string Name
        {
            get { return "Delete"; }
        }

        public override FrameworkElement CreateIcon()
        {
            var builder = IconBuilder.BuildIcon();
            var size = IconBuilder.IconSize;
            var centerX = (size - 4) / (2 * size);
            var centerY = 1 - 4 / size;
            builder.Line(centerX, centerY, 0.8, 0.2)
                .Line(centerX, centerY, 1, centerY);

            var xaml = string.Format(@"
  <Path xmlns='http://schemas.microsoft.com/winfx/2006/xaml/presentation'
    Stroke='Black'
    StrokeThickness='1'
    Fill='#33FF33'
    Data='M 15 4 C 14.477778 4 13.939531 4.1854695 13.5625 4.5625 C 13.185469 4.9395305 13 5.4777778 13 6 L 13 7 L 7 7 L 7 9 L 8 9 L 8 25 C 8 26.645455 9.3545455 28 11 28 L 23 28 C 24.645455 28 26 26.645455 26 25 L 26 9 L 27 9 L 27 7 L 21 7 L 21 6 C 21 5.4777778 20.814531 4.9395305 20.4375 4.5625 C 20.060469 4.1854695 19.522222 4 19 4 L 15 4 z M 15 6 L 19 6 L 19 7 L 15 7 L 15 6 z M 10 9 L 24 9 L 24 25 C 24 25.554545 23.554545 26 23 26 L 11 26 C 10.445455 26 10 25.554545 10 25 L 10 9 z M 12 12 L 12 23 L 14 23 L 14 12 L 12 12 z M 16 12 L 16 23 L 18 23 L 18 12 L 16 12 z M 20 12 L 20 23 L 22 23 L 22 12 L 20 12 z'
  />", size, (size - 4) / 2, size - 4, size / 2, (size - 4) / 4 - 1);
#if SILVERLIGHT
            var path = XamlReader.Load(xaml) as Path;
#else
            var path = XamlReader.Parse(xaml) as Path;
#endif
            builder.Canvas.Children.Add(path);

            /*var radius = ((size - 4) / 2) * 0.95;
            var radiusSmall = radius * 0.8;
            Point center = new Point(radius + 1, size - 4);
            for (double i = 0; i < 16; i++)
            {
                var angle = i * Math.PI / 15;
                builder.Line((center.X + radius * System.Math.Cos(angle)) / size,
                    (center.Y - radius * System.Math.Sin(angle)) / size,
                    (center.X + radiusSmall * System.Math.Cos(angle)) / size,
                    (center.Y - radiusSmall * System.Math.Sin(angle)) / size);
            }*/

            return builder.Canvas;
        }
    }
}
