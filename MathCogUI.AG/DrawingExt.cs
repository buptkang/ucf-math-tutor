﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DynamicGeometry
{
    public static class DrawingExt
    {
        public static List<IFigure> GetSpecifiedFigures(this Drawing drawing)
        {
            if (drawing == null) return null;
            var lst = new List<IFigure>();
            foreach (IFigure figure in drawing.Figures.Where(f => 
                (f is FreePoint) || (f is LineTwoPoints) || (f is Circle)))
            {
                lst.Add(figure);
            }
            return lst;
        }
    }
}
