﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using MathCogUI;

namespace DynamicGeometry
{
    [Category(BehaviorCategories.Shapes)]
    [Order(4)]
    public class CircleCreator : FigureCreator
    {
        protected override IEnumerable<IFigure> CreateFigures()
        {
            yield return Factory.CreateCircle(Drawing, FoundDependencies);
        }

        protected override DependencyList InitExpectedDependencies()
        {
            return DependencyList.PointPoint;
        }

        public override void MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            TabletDevice device = Tablet.CurrentTabletDevice;
            if (device.Type == TabletDeviceType.Touch)
            {
                return;
            }

            count++;
            if (count == 1)
            {
                var analytic = InteractiveLogger.Instance.CurrentProblemAnalytic;
                analytic.StartGeometryInputTimer(); 
            }

            base.MouseDown(sender, e);
        }

        public override void MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            TabletDevice device = Tablet.CurrentTabletDevice;
            if (device.Type == TabletDeviceType.Touch)
            {
                return;
            }
            if (count == 2)
            {
                count = 0;
                var analytic = InteractiveLogger.Instance.CurrentProblemAnalytic;
                analytic.EndGeometryInputTimer();                
            }
            base.MouseUp(sender, e);
        }
 
        private int count = 0;

        public override string Name
        {
            get
            {
                return "Circle";
            }
        }

        public override string HintText
        {
            get
            {
                return "Click the circle center and then click a point on a circle.";
            }
        }

        public override FrameworkElement CreateIcon()
        {
            return IconBuilder.BuildIcon()
                .Circle(0.5, 0.5, 0.5)
                .Point(0.5, 0.5)
                .Point(0.85, 0.15)
                .Canvas;
        }
    }
}