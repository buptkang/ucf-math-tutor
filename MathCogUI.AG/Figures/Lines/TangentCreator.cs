﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;

namespace DynamicGeometry
{
    [Category(BehaviorCategories.Shapes)]
    [Order(7)]
    [Ignore]
    public class TangentCreator : FigureCreator
    {
        public override string Name
        {
            get
            {
                return "Tangent";
            }
        }

        public override FrameworkElement CreateIcon()
        {
            return IconBuilder.BuildIcon()
               .Circle(0.5, 0.5, 0.5)
               .Point(0.5, 0.5)
               .Point(0.85, 0.15)
               .Line(0.85,0.15, 1.5,1.0)
               .Canvas;
        }

        protected override IEnumerable<IFigure> CreateFigures()
        {
            return null;
            //yield return Factory.CreatePerpendicularLine(Drawing, FoundDependencies);
        }

        protected override DependencyList InitExpectedDependencies()
        {
            return DependencyList.Point;
        }
    }
}
