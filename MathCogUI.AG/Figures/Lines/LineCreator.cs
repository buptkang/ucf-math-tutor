﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using MathCogUI;

namespace DynamicGeometry
{
    [Category(BehaviorCategories.Shapes)]
    [Order(2)]
    public class LineTwoPointsCreator : FigureCreator
    {
        protected override IEnumerable<IFigure> CreateFigures()
        {
            yield return Factory.CreateLineTwoPoints(Drawing, FoundDependencies);
        }

        protected override DependencyList InitExpectedDependencies()
        {
            return DependencyList.PointPoint;
        }

        public override void MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            TabletDevice device = Tablet.CurrentTabletDevice;
            if (device.Type == TabletDeviceType.Touch)
            {
                return;
            }

            count++;
            if (count == 1)
            {
                var analytic = InteractiveLogger.Instance.CurrentProblemAnalytic;
                analytic.StartGeometryInputTimer();
            }

            base.MouseDown(sender, e);
        }

        public override void MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            TabletDevice device = Tablet.CurrentTabletDevice;
            if (device.Type == TabletDeviceType.Touch)
            {
                return;
            }

            if (count == 2)
            {
                count = 0;
                var analytic = InteractiveLogger.Instance.CurrentProblemAnalytic;
                analytic.EndGeometryInputTimer();
            }
            base.MouseUp(sender, e);
        }

        private int count = 0;


        public override string Name
        {
            get
            {
                return "Line";
            }
        }

        public override string HintText
        {
            get
            {
                return "Click (and release) twice to draw a line between two points.";
            }
        }

        public override FrameworkElement CreateIcon()
        {
            return IconBuilder.BuildIcon()
                .Line(0, 1, 1, 0)
                .Point(0.25, 0.75)
                .Point(0.75, 0.25)
                .Canvas;
        }
    }
}