﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using MathCogUI;

namespace DynamicGeometry
{
    [Category(BehaviorCategories.Shapes)]
    [Order(4)]
    public class ParallelLineCreator : FigureCreator
    {
        #region Time Recording

        public override void MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            TabletDevice device = Tablet.CurrentTabletDevice;
            if (device.Type == TabletDeviceType.Touch)
            {
                return;
            }

            count++;
            if (count == 1)
            {
                var analytic = InteractiveLogger.Instance.CurrentProblemAnalytic;
                analytic.StartGeometryInputTimer();
            }
            base.MouseDown(sender, e);
        }

        public override void MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            TabletDevice device = Tablet.CurrentTabletDevice;
            if (device.Type == TabletDeviceType.Touch)
            {
                return;
            }

            if (count == 2)
            {
                count = 0;
                var analytic = InteractiveLogger.Instance.CurrentProblemAnalytic;
                analytic.EndGeometryInputTimer();
            }
            base.MouseUp(sender, e);
        }

        private int count = 0;

        #endregion

        protected override IEnumerable<IFigure> CreateFigures()
        {
            yield return Factory.CreateParallelLine(Drawing, FoundDependencies);
        }

        protected override DependencyList InitExpectedDependencies()
        {
            return DependencyList.LinePoint;
        }

        public override string Name
        {
            get
            {
                return "Parallel";
            }
        }

        public override string HintText
        {
            get
            {
                return "Click a line and then click a point.";
            }
        }

        public override FrameworkElement CreateIcon()
        {
            return IconBuilder.BuildIcon()
                .Line(0, 0.7, 0.7, 0)
                .Line(0.3, 1, 1, 0.3)
                .Point(0.35, 0.35)
                .Canvas;
        }
    }
}